package com.app.storemanager.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.app.a3store.R;
import com.app.storemanager.utils.CurvedBottomNavigationView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CurvedBottomNavigationView curvedBottomNavigationView = findViewById(R.id.customBottomBar);
        curvedBottomNavigationView.inflateMenu(R.menu.bottom_menu);
    }
}