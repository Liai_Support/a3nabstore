package com.app.a3store.model.profile;

public class UpdateProfileResponse {

	String error;
	String message;

	public void setError(String error) {
		this.error = error;
	}

	public String getError() {
		return error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
