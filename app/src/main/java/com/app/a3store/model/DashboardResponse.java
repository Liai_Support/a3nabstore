package com.app.a3store.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class DashboardResponse implements Serializable {

	@SerializedName("error")
	Boolean error;
	@SerializedName("message")
	String message;
	@SerializedName("data")
	DashboardData data;

	public static class CartDetails implements Serializable {

		@SerializedName("error")
		boolean error;
		@SerializedName("cartCount")
		int cartCount;
		@SerializedName("cartAmount")
		String cartAmount;
		@SerializedName("discountAmount")
		String discountAmount;

		public boolean isError() {
			return error;
		}

		public void setError(boolean error) {
			this.error = error;
		}

		public int getCartCount() {
			return cartCount;
		}

		public void setCartCount(int cartCount) {
			this.cartCount = cartCount;
		}

		public String getCartAmount() {
			return cartAmount;
		}

		public void setCartAmount(String cartAmount) {
			this.cartAmount = cartAmount;
		}

		public String getDiscountAmount() {
			return discountAmount;
		}

		public void setDiscountAmount(String discountAmount) {
			this.discountAmount = discountAmount;
		}
	}

	public static class DashboardData implements Serializable {

		@SerializedName("cartDetails")
		CartDetails cartDetails = new CartDetails();
		@SerializedName("bannerImages")
		ArrayList<BannerData> bannerData = new ArrayList<>();
		@SerializedName("category")
		ArrayList<Categories> stores = new ArrayList<>();
		@SerializedName("bestProducts")
		ArrayList<BestProduct> bestProducts = new ArrayList<>();

		public ArrayList<BannerData> getBannerData() {
			return bannerData;
		}

		public void setBannerData(ArrayList<BannerData> bannerData) {
			this.bannerData = bannerData;
		}

		public ArrayList<Categories> getStores() {
			return stores;
		}

		public void setStores(ArrayList<Categories> stores) {
			this.stores = stores;
		}

		public ArrayList<BestProduct> getBestProducts() {
			return bestProducts;
		}

		public void setBestProducts(ArrayList<BestProduct> bestProducts) {
			this.bestProducts = bestProducts;
		}

		public CartDetails getCartDetails() {
			return cartDetails;
		}

		public void setCartDetails(CartDetails cartDetails) {
			this.cartDetails = cartDetails;
		}
	}

	public static class BannerData implements Serializable {

		@SerializedName("id")
		int id;
		@SerializedName("image")
		String image;
		@SerializedName("status")
		String status;
		@SerializedName("title")
		String title;
		@SerializedName("description")
		String description;
		@SerializedName("couponCode")
		String couponCode;
		@SerializedName("discount")
		String discount;
		@SerializedName("minimumValue")
		String minimumValue;
		@SerializedName("startDate")
		String startDate;
		@SerializedName("endDate")
		String endDate;
		@SerializedName("count")
		int count;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getImage() {
			return image;
		}

		public void setImage(String image) {
			this.image = image;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getCouponCode() {
			return couponCode;
		}

		public void setCouponCode(String couponCode) {
			this.couponCode = couponCode;
		}

		public String getDiscount() {
			return discount;
		}

		public void setDiscount(String discount) {
			this.discount = discount;
		}

		public String getMinimumValue() {
			return minimumValue;
		}

		public void setMinimumValue(String minimumValue) {
			this.minimumValue = minimumValue;
		}

		public int getCount() {
			return count;
		}

		public void setCount(int count) {
			this.count = count;
		}

		public String getStartDate() {
			return startDate;
		}

		public void setStartDate(String startDate) {
			this.startDate = startDate;
		}

		public String getEndDate() {
			return endDate;
		}

		public void setEndDate(String endDate) {
			this.endDate = endDate;
		}
	}

	public static class BestProduct implements Serializable, Parcelable {

		@SerializedName("id")
		int id;
		@SerializedName("productCategoryId")
		int productCategoryId;
		@SerializedName("productSubCategoryId")
		int productSubCategoryId;
		@SerializedName("productName")
		String productName;

		@SerializedName("arabicName")
		String arabicName;

		@SerializedName("productStatus")
		String productStatus = "";
		@SerializedName("productWeight")
		String productWeight;
		@SerializedName("productPrice")
		String productPrice;
		@SerializedName("productDiscount")
		String productDiscount;
		@SerializedName("productDiscountStatus")
		String productDiscountStatus;
		@SerializedName("specialInstructions")
		String instruction;
		@SerializedName("isBestProduct")
		int isBestProduct;
		@SerializedName("isComingSoon")
		String isComingSoon;
		@SerializedName("status")
		String status;
		@SerializedName("createdAt")
		String createdAt;
		@SerializedName("quantity")
		int count;
		@SerializedName("storeId")
		int storeId;

		@SerializedName("isFavourite")
		String isFavourite;
		@SerializedName("productImages")
		ArrayList<ProductImages> productImages = new ArrayList<>();


		@SerializedName("storeStock")
		int storeStock;


		public int getStoreStock() {
			return storeStock;
		}

		public void setStoreStock(int storeStock) {
			this.storeStock = storeStock;
		}

		public int getStoreId() {
			return storeId;
		}

		public void setStoreId(int storeId) {
			this.storeId = storeId;
		}

		protected BestProduct(Parcel in) {
			id = in.readInt();
			productCategoryId = in.readInt();
			productSubCategoryId = in.readInt();
			productName = in.readString();
			productStatus = in.readString();
			productWeight = in.readString();
			productPrice = in.readString();
			productDiscount = in.readString();
			productDiscountStatus = in.readString();
			instruction = in.readString();
			isBestProduct = in.readInt();
			isComingSoon = in.readString();
			status = in.readString();
			createdAt = in.readString();
			count = in.readInt();
			isFavourite = in.readString();
			productImages = in.createTypedArrayList(ProductImages.CREATOR);
			storeStock = in.readInt();
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeInt(id);
			dest.writeInt(productCategoryId);
			dest.writeInt(productSubCategoryId);
			dest.writeString(productName);
			dest.writeString(productStatus);
			dest.writeString(productWeight);
			dest.writeString(productPrice);
			dest.writeString(productDiscount);
			dest.writeString(productDiscountStatus);
			dest.writeString(instruction);
			dest.writeInt(isBestProduct);
			dest.writeString(isComingSoon);
			dest.writeString(status);
			dest.writeString(createdAt);
			dest.writeInt(count);
			dest.writeString(isFavourite);
			dest.writeTypedList(productImages);
			dest.writeInt(storeStock);
		}

		@Override
		public int describeContents() {
			return 0;
		}

		public static final Creator<BestProduct> CREATOR = new Creator<BestProduct>() {
			@Override
			public BestProduct createFromParcel(Parcel in) {
				return new BestProduct(in);
			}

			@Override
			public BestProduct[] newArray(int size) {
				return new BestProduct[size];
			}
		};

		public String getIsComingSoon() {
			return isComingSoon;
		}

		public void setIsComingSoon(String isComingSoon) {
			this.isComingSoon = isComingSoon;
		}

		public String getIsFavourite() {
			return isFavourite;
		}

		public void setIsFavourite(String isFavourite) {
			this.isFavourite = isFavourite;
		}

		public int getCount() {
			return count;
		}

		public void setCount(int count) {
			this.count = count;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public int getProductCategoryId() {
			return productCategoryId;
		}

		public void setProductCategoryId(int productCategoryId) {
			this.productCategoryId = productCategoryId;
		}

		public int getProductSubCategoryId() {
			return productSubCategoryId;
		}

		public void setProductSubCategoryId(int productSubCategoryId) {
			this.productSubCategoryId = productSubCategoryId;
		}

		public String getProductName() {
			return productName;
		}

		public String getArabicName() {
			return arabicName;
		}

		public void setProductName(String productName) {
			this.productName = productName;
		}

		public String getProductStatus() {
			return productStatus;
		}

		public void setProductStatus(String productStatus) {
			this.productStatus = productStatus;
		}

		public String getProductWeight() {
			return productWeight;
		}

		public void setProductWeight(String productWeight) {
			this.productWeight = productWeight;
		}

		public String getProductPrice() {
			return productPrice;
		}

		public void setProductPrice(String productPrice) {
			this.productPrice = productPrice;
		}

		public String getProductDiscount() {
			return productDiscount;
		}

		public void setProductDiscount(String productDiscount) {
			this.productDiscount = productDiscount;
		}

		public String getProductDiscountStatus() {
			return productDiscountStatus;
		}

		public void setProductDiscountStatus(String productDiscountStatus) {
			this.productDiscountStatus = productDiscountStatus;
		}

		public String getInstruction() {
			return instruction;
		}

		public void setInstruction(String instruction) {
			this.instruction = instruction;
		}

		public int getIsBestProduct() {
			return isBestProduct;
		}

		public void setIsBestProduct(int isBestProduct) {
			this.isBestProduct = isBestProduct;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}

		public ArrayList<ProductImages> getProductImages() {
			return productImages;
		}

		public void setProductImages(ArrayList<ProductImages> productImages) {
			this.productImages = productImages;
		}
	}

	public static class Categories implements Serializable {

		@SerializedName("id")
		int id;
		@SerializedName("storeId")
		int storeId;
		@SerializedName("categoryName")
		String categoryName;
		@SerializedName("categoryImage")
		String categoryImage;
		@SerializedName("categoryStatus")
		String categoryStatus;
		@SerializedName("minimum")
		String minimum;
		@SerializedName("orderProcessing")
		String orderProcessing;
		@SerializedName("createdAt")
		String createdAt;
		@SerializedName("isComingSoon")
		String isComingSoon;
		boolean isSelected = false;

		public boolean isSelected() {
			return isSelected;
		}

		public void setSelected(boolean selected) {
			isSelected = selected;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public int getStoreId() {
			return storeId;
		}

		public void setStoreId(int storeId) {
			this.storeId = storeId;
		}

		public String getCategoryName() {
			return categoryName;
		}

		public void setCategoryName(String categoryName) {
			this.categoryName = categoryName;
		}

		public String getCategoryImage() {
			return categoryImage;
		}

		public void setCategoryImage(String categoryImage) {
			this.categoryImage = categoryImage;
		}

		public String getCategoryStatus() {
			return categoryStatus;
		}

		public void setCategoryStatus(String categoryStatus) {
			this.categoryStatus = categoryStatus;
		}

		public String getMinimum() {
			return minimum;
		}

		public void setMinimum(String minimum) {
			this.minimum = minimum;
		}

		public String getOrderProcessing() {
			return orderProcessing;
		}

		public void setOrderProcessing(String orderProcessing) {
			this.orderProcessing = orderProcessing;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}

		public int getIsComingSoon() {
			if(isComingSoon.equals("true")) {
				return 1;
			} else {
				return 0;
			}
		}

		public void setIsComingSoon(String isComingSoon) {
			this.isComingSoon = isComingSoon;
		}
	}

	public Boolean getError() {
		return error;
	}

	public void setError(Boolean error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public DashboardData getData() {
		return data;
	}

	public void setData(DashboardData data) {
		this.data = data;
	}

	public static class ProductImages implements Serializable, Parcelable {

		@SerializedName("id")
		int id;
		@SerializedName("productId")
		int productId;
		@SerializedName("productImage")
		String productImage;
		@SerializedName("productImageStatus")
		String productImageStatus;
		@SerializedName("createdAt")
		String createdAt;

		protected ProductImages(Parcel in) {
			id = in.readInt();
			productId = in.readInt();
			productImage = in.readString();
			productImageStatus = in.readString();
			createdAt = in.readString();
		}

		public static final Creator<ProductImages> CREATOR = new Creator<ProductImages>() {
			@Override
			public ProductImages createFromParcel(Parcel in) {
				return new ProductImages(in);
			}

			@Override
			public ProductImages[] newArray(int size) {
				return new ProductImages[size];
			}
		};

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public int getProductId() {
			return productId;
		}

		public void setProductId(int productId) {
			this.productId = productId;
		}

		public String getProductImage() {
			return productImage;
		}

		public void setProductImage(String productImage) {
			this.productImage = productImage;
		}

		public String getProductImageStatus() {
			return productImageStatus;
		}

		public void setProductImageStatus(String productImageStatus) {
			this.productImageStatus = productImageStatus;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(Parcel parcel, int i) {
			parcel.writeInt(id);
			parcel.writeInt(productId);
			parcel.writeString(productImage);
			parcel.writeString(productImageStatus);
			parcel.writeString(createdAt);
		}
	}
}


