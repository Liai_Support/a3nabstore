package com.app.a3store.model.orders;

public class OrderResponse {

	String error;
	String message;
	OrderData data;

	public void setError(String error) {
		this.error = error;
	}

	public String getError() {
		return error;
	}

	public OrderData getData() {
		return data;
	}

	public void setData(OrderData data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
