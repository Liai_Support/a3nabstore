package com.app.a3store.model;

import java.util.List;

public class NotificationResponse {

	private String error;
	private String message;
	private Data data;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public class Data {

		private Integer pages;
		private List<Notification> notification = null;

		public Integer getPages() {
			return pages;
		}

		public void setPages(Integer pages) {
			this.pages = pages;
		}

		public List<Notification> getNotification() {
			return notification;
		}

		public void setNotification(List<Notification> notification) {
			this.notification = notification;
		}

		public class Notification {

			private Integer id;
			private Integer stoteManagerId;
			private Object driverId;
			private Integer storeId;
			private Integer userId;
			private Object routeId;
			private Object assigId;
			private Integer orderId;
			private String ordIds;
			private Integer amount;
			private String notifyType;
			private String notifyMessage;
			private String created_at;
			private String updated_at;

			public Integer getId() {
				return id;
			}

			public void setId(Integer id) {
				this.id = id;
			}

			public Integer getStoteManagerId() {
				return stoteManagerId;
			}

			public void setStoteManagerId(Integer stoteManagerId) {
				this.stoteManagerId = stoteManagerId;
			}

			public Object getDriverId() {
				return driverId;
			}

			public void setDriverId(Object driverId) {
				this.driverId = driverId;
			}

			public Integer getStoreId() {
				return storeId;
			}

			public void setStoreId(Integer storeId) {
				this.storeId = storeId;
			}

			public Integer getUserId() {
				return userId;
			}

			public void setUserId(Integer userId) {
				this.userId = userId;
			}

			public Object getRouteId() {
				return routeId;
			}

			public void setRouteId(Object routeId) {
				this.routeId = routeId;
			}

			public Object getAssigId() {
				return assigId;
			}

			public void setAssigId(Object assigId) {
				this.assigId = assigId;
			}

			public Integer getOrderId() {
				return orderId;
			}

			public void setOrderId(Integer orderId) {
				this.orderId = orderId;
			}

			public String getOrdIds() {
				return ordIds;
			}

			public void setOrdIds(String ordIds) {
				this.ordIds = ordIds;
			}

			public Integer getAmount() {
				return amount;
			}

			public void setAmount(Integer amount) {
				this.amount = amount;
			}

			public String getNotifyType() {
				return notifyType;
			}

			public void setNotifyType(String notifyType) {
				this.notifyType = notifyType;
			}

			public String getNotifyMessage() {
				return notifyMessage;
			}

			public void setNotifyMessage(String notifyMessage) {
				this.notifyMessage = notifyMessage;
			}

			public String getCreatedAt() {
				return created_at;
			}

			public void setCreatedAt(String created_at) {
				this.created_at = created_at;
			}

			public String getUpdatedAt() {
				return updated_at;
			}

			public void setUpdatedAt(String updated_at) {
				this.updated_at = updated_at;
			}
		}
	}
}
