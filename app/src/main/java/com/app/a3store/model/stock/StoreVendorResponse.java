package com.app.a3store.model.stock;

import java.util.List;

public class StoreVendorResponse {

	private String error;
	private String message;
	private Data data;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public class Data {

		private List<Vendor> vendor = null;

		public List<Vendor> getVendor() {
			return vendor;
		}

		public void setVendor(List<Vendor> vendor) {
			this.vendor = vendor;
		}
	}
}
