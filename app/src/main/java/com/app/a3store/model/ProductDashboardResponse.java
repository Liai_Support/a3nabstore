package com.app.a3store.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductDashboardResponse {

    private String error;
    private String message;
    private Data data;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        private List<BannerImage> bannerImages = null;
        @SerializedName(value = "category", alternate = {"productCategory"})
        private List<Category> category = null;
        private List<BestProduct> bestProducts = null;

        public List<BannerImage> getBannerImages() {
            return bannerImages;
        }

        public void setBannerImages(List<BannerImage> bannerImages) {
            this.bannerImages = bannerImages;
        }

        public List<Category> getCategory() {
            return category;
        }

        public void setCategory(List<Category> category) {
            this.category = category;
        }

        public List<BestProduct> getBestProducts() {
            return bestProducts;
        }

        public void setBestProducts(List<BestProduct> bestProducts) {
            this.bestProducts = bestProducts;
        }

        public class BannerImage {

            private Integer id;
            private String image;
            private String status;
            private String title;
            private String description;
            private String couponCode;
            private Integer discount;
            private Integer minimumValue;
            private Integer count;
            private String startDate;
            private String endDate;
            private String createdAt;
            private String updatedAt;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getCouponCode() {
                return couponCode;
            }

            public void setCouponCode(String couponCode) {
                this.couponCode = couponCode;
            }

            public Integer getDiscount() {
                return discount;
            }

            public void setDiscount(Integer discount) {
                this.discount = discount;
            }

            public Integer getMinimumValue() {
                return minimumValue;
            }

            public void setMinimumValue(Integer minimumValue) {
                this.minimumValue = minimumValue;
            }

            public Integer getCount() {
                return count;
            }

            public void setCount(Integer count) {
                this.count = count;
            }

            public String getStartDate() {
                return startDate;
            }

            public void setStartDate(String startDate) {
                this.startDate = startDate;
            }

            public String getEndDate() {
                return endDate;
            }

            public void setEndDate(String endDate) {
                this.endDate = endDate;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }
        }

        public class Category {

            private Integer id;
            private Integer storeId;
            @SerializedName(value = "categoryName", alternate = {"productCategoryName"})
            private String categoryName;
            @SerializedName(value = "categoryImage", alternate = {"productCategoryImage"})
            private String categoryImage;
            @SerializedName(value = "categoryStatus", alternate = {"productCategoryStatus"})
            private String categoryStatus;
            private String minimum;
            private String arabicName;
            private String isComingSoon;
            private String managerPrice;
            private String isDelete;
            private String orderProcessing;
            private String createdAt;
            private String updatedAt;

            @SerializedName(value = "storeProductStatus")
            private String storeProductStatus;

            public String getStoreProductStatus() {
                return storeProductStatus;
            }

            public void setStoreProductStatus(String storeProductStatus) {
                this.storeProductStatus = storeProductStatus;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getStoreId() {
                return storeId;
            }

            public void setStoreId(Integer storeId) {
                this.storeId = storeId;
            }

            public String getCategoryName() {
                return categoryName;
            }

            public void setCategoryName(String categoryName) {
                this.categoryName = categoryName;
            }

            public String getCategoryImage() {
                return categoryImage;
            }

            public void setCategoryImage(String categoryImage) {
                this.categoryImage = categoryImage;
            }

            public String getCategoryStatus() {
                return categoryStatus;
            }

            public void setCategoryStatus(String categoryStatus) {
                this.categoryStatus = categoryStatus;
            }

            public String getMinimum() {
                return minimum;
            }

            public void setMinimum(String minimum) {
                this.minimum = minimum;
            }

            public String getArabicName() {
                return arabicName;
            }

            public void setArabicName(String arabicName) {
                this.arabicName = arabicName;
            }

            public String getIsComingSoon() {
                return isComingSoon;
            }

            public void setIsComingSoon(String isComingSoon) {
                this.isComingSoon = isComingSoon;
            }

            public String getManagerPrice() {
                return managerPrice;
            }

            public void setManagerPrice(String managerPrice) {
                this.managerPrice = managerPrice;
            }

            public String getIsDelete() {
                return isDelete;
            }

            public void setIsDelete(String isDelete) {
                this.isDelete = isDelete;
            }

            public String getOrderProcessing() {
                return orderProcessing;
            }

            public void setOrderProcessing(String orderProcessing) {
                this.orderProcessing = orderProcessing;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }
        }

        public class BestProduct {

            private Integer id;
            private Integer categoryId;
            private Integer productCategoryId;
            private Object productSubCategoryId;
            private Integer storeId;
            private Integer storeStock;
            private String productName;
            private String productStatus;
            private String productWeight;
            private Double productPrice;
            private Integer productDiscount;
            private String productDiscountStatus;
            private String productDescription;
            private Integer isBestProduct;
            private String orderVariants;
            private String specialInstructions;
            private String instructionsStatus;
            private String differentPriceVariant;
            private String status;
            private String createdAt;
            private String updatedAt;
            private List<ProductListResponse.ProductListData.ProductImage> productImages = null;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getCategoryId() {
                return categoryId;
            }

            public void setCategoryId(Integer categoryId) {
                this.categoryId = categoryId;
            }

            public Integer getProductCategoryId() {
                return productCategoryId;
            }

            public void setProductCategoryId(Integer productCategoryId) {
                this.productCategoryId = productCategoryId;
            }

            public Object getProductSubCategoryId() {
                return productSubCategoryId;
            }

            public void setProductSubCategoryId(Object productSubCategoryId) {
                this.productSubCategoryId = productSubCategoryId;
            }

            public Integer getStoreId() {
                return storeId;
            }

            public void setStoreId(Integer storeId) {
                this.storeId = storeId;
            }

            public Integer getStoreStock() {
                return storeStock;
            }

            public void setStoreStock(Integer storeStock) {
                this.storeStock = storeStock;
            }

            public String getProductName() {
                return productName;
            }

            public void setProductName(String productName) {
                this.productName = productName;
            }

            public String getProductStatus() {
                return productStatus;
            }

            public void setProductStatus(String productStatus) {
                this.productStatus = productStatus;
            }

            public String getProductWeight() {
                return productWeight;
            }

            public void setProductWeight(String productWeight) {
                this.productWeight = productWeight;
            }

            public Double getProductPrice() {
                return productPrice;
            }

            public void setProductPrice(Double productPrice) {
                this.productPrice = productPrice;
            }

            public Integer getProductDiscount() {
                return productDiscount;
            }

            public void setProductDiscount(Integer productDiscount) {
                this.productDiscount = productDiscount;
            }

            public String getProductDiscountStatus() {
                return productDiscountStatus;
            }

            public void setProductDiscountStatus(String productDiscountStatus) {
                this.productDiscountStatus = productDiscountStatus;
            }

            public String getProductDescription() {
                return productDescription;
            }

            public void setProductDescription(String productDescription) {
                this.productDescription = productDescription;
            }

            public Integer getIsBestProduct() {
                return isBestProduct;
            }

            public void setIsBestProduct(Integer isBestProduct) {
                this.isBestProduct = isBestProduct;
            }

            public String getOrderVariants() {
                return orderVariants;
            }

            public void setOrderVariants(String orderVariants) {
                this.orderVariants = orderVariants;
            }

            public String getSpecialInstructions() {
                return specialInstructions;
            }

            public void setSpecialInstructions(String specialInstructions) {
                this.specialInstructions = specialInstructions;
            }

            public String getInstructionsStatus() {
                return instructionsStatus;
            }

            public void setInstructionsStatus(String instructionsStatus) {
                this.instructionsStatus = instructionsStatus;
            }

            public String getDifferentPriceVariant() {
                return differentPriceVariant;
            }

            public void setDifferentPriceVariant(String differentPriceVariant) {
                this.differentPriceVariant = differentPriceVariant;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public List<ProductListResponse.ProductListData.ProductImage> getProductImages() {
                return productImages;
            }

            public void setProductImages(List<ProductListResponse.ProductListData.ProductImage> productImages) {
                this.productImages = productImages;
            }
        }
    }
}
