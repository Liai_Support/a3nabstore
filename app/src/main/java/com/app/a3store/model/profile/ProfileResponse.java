package com.app.a3store.model.profile;

public class ProfileResponse {

	String error;
	String message;
	ProfileData data;

	public ProfileData getData() {
		return data;
	}

	public void setData(ProfileData data) {
		this.data = data;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getError() {
		return error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
