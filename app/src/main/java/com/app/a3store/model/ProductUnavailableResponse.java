package com.app.a3store.model;

import java.util.List;

public class ProductUnavailableResponse {

	private String error;
	private String message;
	private Data data;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public class Data {

		private Integer pages;
		private List<Product> products = null;

		public Integer getPages() {
			return pages;
		}

		public void setPages(Integer pages) {
			this.pages = pages;
		}

		public List<Product> getProducts() {
			return products;
		}

		public void setProducts(List<Product> products) {
			this.products = products;
		}

		public class Product {

			private Integer id;
			private Integer categoryId;
			private Integer productCategoryId;
			private Integer productSubCategoryId;
			private String productName;
			private String arabicName;
			private double productPrice;
			private String productStatus;
			private String productWeight;
			private Integer productDiscount;
			private String productDiscountStatus;
			private String productDescription;
			private Integer isBestProduct;
			private String orderVariants;
			private String specialInstructions;
			private String instructionsStatus;
			private String differentPriceVariant;
			private String status;
			private String createdAt;
			private String updatedAt;
			private List<ProductImage> productImages = null;

			public String getArabicName(){
				return arabicName;
			}

			public double getProductPrice() {
				return productPrice;
			}

			public void setProductPrice(double productPrice) {
				this.productPrice = productPrice;
			}

			public Integer getId() {
				return id;
			}

			public void setId(Integer id) {
				this.id = id;
			}

			public Integer getCategoryId() {
				return categoryId;
			}

			public void setCategoryId(Integer categoryId) {
				this.categoryId = categoryId;
			}

			public Integer getProductCategoryId() {
				return productCategoryId;
			}

			public void setProductCategoryId(Integer productCategoryId) {
				this.productCategoryId = productCategoryId;
			}

			public Integer getProductSubCategoryId() {
				return productSubCategoryId;
			}

			public void setProductSubCategoryId(Integer productSubCategoryId) {
				this.productSubCategoryId = productSubCategoryId;
			}

			public String getProductName() {
				return productName;
			}

			public void setProductName(String productName) {
				this.productName = productName;
			}

			public String getProductStatus() {
				return productStatus;
			}

			public void setProductStatus(String productStatus) {
				this.productStatus = productStatus;
			}

			public String getProductWeight() {
				return productWeight;
			}

			public void setProductWeight(String productWeight) {
				this.productWeight = productWeight;
			}

			public Integer getProductDiscount() {
				return productDiscount;
			}

			public void setProductDiscount(Integer productDiscount) {
				this.productDiscount = productDiscount;
			}

			public String getProductDiscountStatus() {
				return productDiscountStatus;
			}

			public void setProductDiscountStatus(String productDiscountStatus) {
				this.productDiscountStatus = productDiscountStatus;
			}

			public String getProductDescription() {
				return productDescription;
			}

			public void setProductDescription(String productDescription) {
				this.productDescription = productDescription;
			}

			public Integer getIsBestProduct() {
				return isBestProduct;
			}

			public void setIsBestProduct(Integer isBestProduct) {
				this.isBestProduct = isBestProduct;
			}

			public String getOrderVariants() {
				return orderVariants;
			}

			public void setOrderVariants(String orderVariants) {
				this.orderVariants = orderVariants;
			}

			public String getSpecialInstructions() {
				return specialInstructions;
			}

			public void setSpecialInstructions(String specialInstructions) {
				this.specialInstructions = specialInstructions;
			}

			public String getInstructionsStatus() {
				return instructionsStatus;
			}

			public void setInstructionsStatus(String instructionsStatus) {
				this.instructionsStatus = instructionsStatus;
			}

			public String getDifferentPriceVariant() {
				return differentPriceVariant;
			}

			public void setDifferentPriceVariant(String differentPriceVariant) {
				this.differentPriceVariant = differentPriceVariant;
			}

			public String getStatus() {
				return status;
			}

			public void setStatus(String status) {
				this.status = status;
			}

			public String getCreatedAt() {
				return createdAt;
			}

			public void setCreatedAt(String createdAt) {
				this.createdAt = createdAt;
			}

			public String getUpdatedAt() {
				return updatedAt;
			}

			public void setUpdatedAt(String updatedAt) {
				this.updatedAt = updatedAt;
			}

			public List<ProductImage> getProductImages() {
				return productImages;
			}

			public void setProductImages(List<ProductImage> productImages) {
				this.productImages = productImages;
			}

			public class ProductImage {

				private Integer id;
				private String productImage;
				private Integer productId;
				private String productImageStatus;

				public Integer getId() {
					return id;
				}

				public void setId(Integer id) {
					this.id = id;
				}

				public String getProductImage() {
					return productImage;
				}

				public void setProductImage(String productImage) {
					this.productImage = productImage;
				}

				public Integer getProductId() {
					return productId;
				}

				public void setProductId(Integer productId) {
					this.productId = productId;
				}

				public String getProductImageStatus() {
					return productImageStatus;
				}

				public void setProductImageStatus(String productImageStatus) {
					this.productImageStatus = productImageStatus;
				}
			}
		}
	}
}
