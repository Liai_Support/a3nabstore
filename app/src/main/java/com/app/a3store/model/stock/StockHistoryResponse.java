package com.app.a3store.model.stock;

import java.util.List;

public class StockHistoryResponse {

	private String error;
	private String message;
	private Data data;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public class Data {

		private List<Product> product = null;

		public List<Product> getProduct() {
			return product;
		}

		public void setProduct(List<Product> product) {
			this.product = product;
		}

		public class Product {

			private Integer id;
			private String vendorName;
			private String stockType;
			private Integer units;
			private String expiryDate;
			private String stockReason;
			private String date;
			private String managerName;
			private String createdAt;

			public String getManagerName() {
				if(managerName != null && !managerName.isEmpty()) return managerName;
				return "Admin";
			}

			public void setManagerName(String managerName) {
				this.managerName = managerName;
			}

			public Integer getId() {
				return id;
			}

			public void setId(Integer id) {
				this.id = id;
			}

			public String getVendorName() {
				return vendorName;
			}

			public void setVendorName(String vendorName) {
				this.vendorName = vendorName;
			}

			public String getStockType() {
				return stockType;
			}

			public void setStockType(String stockType) {
				this.stockType = stockType;
			}

			public Integer getUnits() {
				return units;
			}

			public void setUnits(Integer units) {
				this.units = units;
			}

			public String getExpiryDate() {
				return expiryDate;
			}

			public void setExpiryDate(String expiryDate) {
				this.expiryDate = expiryDate;
			}

			public String getStockReason() {
				return stockReason;
			}

			public void setStockReason(String stockReason) {
				this.stockReason = stockReason;
			}

			public String getDate() {
				return date;
			}

			public void setDate(String date) {
				this.date = date;
			}

			public String getCreatedAt() {
				return createdAt;
			}

			public void setCreatedAt(String createdAt) {
				this.createdAt = createdAt;
			}
		}
	}
}
