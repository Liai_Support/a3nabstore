package com.app.a3store.model;

import java.util.ArrayList;

public class SubCategoryResponse {

	String error;
	String message;
	SubCategoryData data;

	public String isError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public SubCategoryData getData() {
		return data;
	}

	public void setData(SubCategoryData data) {
		this.data = data;
	}

	public class SubCategoryData {

		ArrayList<ProductSubCategoryData> productCategory;

		public ArrayList<ProductSubCategoryData> getProductCategory() {
			return productCategory;
		}

		public void setProductCategory(ArrayList<ProductSubCategoryData> productCategory) {
			this.productCategory = productCategory;
		}

		public class ProductSubCategoryData {

			int id;
			int productCategoryId;
			int storeId;
			String productSubCategoryName;
			String minimumOrderValue = "";
			String orderProcessing;
			String arabicName;
			String categoryDescription;
			String minOrderTime;
			String managerPrice;
			String isComingSoon;
			String productSubCategoryImage;
			String productSubCategoryStatus;
			boolean isSelected;

			public String getArabicName() {
				return arabicName;
			}

			public void setArabicName(String arabicName) {
				this.arabicName = arabicName;
			}

			public int getId() {
				return id;
			}

			public void setId(int id) {
				this.id = id;
			}

			public int getProductCategoryId() {
				return productCategoryId;
			}

			public void setProductCategoryId(int productCategoryId) {
				this.productCategoryId = productCategoryId;
			}

			public int getStoreId() {
				return storeId;
			}

			public void setStoreId(int storeId) {
				this.storeId = storeId;
			}

			public String getProductSubCategoryName() {
				return productSubCategoryName;
			}

			public void setProductSubCategoryName(String productSubCategoryName) {
				this.productSubCategoryName = productSubCategoryName;
			}

			public String getMinimumOrderValue() {
				return minimumOrderValue;
			}

			public void setMinimumOrderValue(String minimumOrderValue) {
				this.minimumOrderValue = minimumOrderValue;
			}

			public String getOrderProcessing() {
				return orderProcessing;
			}

			public void setOrderProcessing(String orderProcessing) {
				this.orderProcessing = orderProcessing;
			}

			public String getCategoryDescription() {
				return categoryDescription;
			}

			public void setCategoryDescription(String categoryDescription) {
				this.categoryDescription = categoryDescription;
			}

			public String getMinOrderTime() {
				return minOrderTime;
			}

			public void setMinOrderTime(String minOrderTime) {
				this.minOrderTime = minOrderTime;
			}

			public String getManagerPrice() {
				return managerPrice;
			}

			public void setManagerPrice(String managerPrice) {
				this.managerPrice = managerPrice;
			}

			public String getIsComingSoon() {
				return isComingSoon;
			}

			public void setIsComingSoon(String isComingSoon) {
				this.isComingSoon = isComingSoon;
			}

			public String getProductSubCategoryImage() {
				return productSubCategoryImage;
			}

			public void setProductSubCategoryImage(String productSubCategoryImage) {
				this.productSubCategoryImage = productSubCategoryImage;
			}

			public String getProductSubCategoryStatus() {
				return productSubCategoryStatus;
			}

			public void setProductSubCategoryStatus(String productSubCategoryStatus) {
				this.productSubCategoryStatus = productSubCategoryStatus;
			}

			public boolean isSelected() {
				return isSelected;
			}

			public void setSelected(boolean selected) {
				isSelected = selected;
			}
		}
	}
}
