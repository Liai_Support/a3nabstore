package com.app.a3store.model.profile;

import java.util.List;

import static com.app.a3store.utils.TimeCalculator.getDateVal;
import static com.app.a3store.utils.Utils.round;

public class ProfileDashboardResponse {

	private String error;
	private String message;
	private Data data;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public class Data {

		private OrderInfo orderInfo;
		private ProfileDetail profile;
		private List<CurrentDue> CurrentDue;
		private String storemanagernumber = "";

		public String getStoremanagernumber() {
			return storemanagernumber;
		}

		public void setStoremanagernumber(String storemanagernumber) {
			this.storemanagernumber = storemanagernumber;
		}

		public OrderInfo getOrderInfo() {
			return orderInfo;
		}

		public void setOrderInfo(OrderInfo orderInfo) {
			this.orderInfo = orderInfo;
		}

		public ProfileDetail getProfile() {
			return profile;
		}

		public void setProfile(ProfileDetail profile) {
			this.profile = profile;
		}

		public List<CurrentDue> getCurrentDue() {
			return CurrentDue;
		}

		public void setCurrentDue(List<CurrentDue> currentDue) {
			this.CurrentDue = currentDue;
		}

		public class OrderInfo {

			private Integer orderCount;
			private double totalAmount;

			public Integer getOrderCount() {
				return orderCount;
			}

			public void setOrderCount(Integer orderCount) {
				this.orderCount = orderCount;
			}

			public String getTotalAmount() {
				return round(totalAmount);
			}

			public void setTotalAmount(double totalAmount) {
				this.totalAmount = totalAmount;
			}
		}

		public class CurrentDue {

			private Integer isPaid;
			private Integer storeId;
			private Integer year;
			private Integer month;
			private double amount;
			private String from;
			private String to;
			private Integer orders;

			public Integer getIsPaid() {
				return isPaid;
			}

			public void setIsPaid(Integer isPaid) {
				this.isPaid = isPaid;
			}

			public Integer getStoreId() {
				return storeId;
			}

			public void setStoreId(Integer storeId) {
				this.storeId = storeId;
			}

			public Integer getYear() {
				return year;
			}

			public void setYear(Integer year) {
				this.year = year;
			}

			public Integer getMonth() {
				return month;
			}

			public void setMonth(Integer month) {
				this.month = month;
			}

			public String getAmount() {
				return round(amount);
			}

			public void setAmount(double amount) {
				this.amount = amount;
			}

			public String getFrom() {
				return getDateVal(from);
			}

			public void setFrom(String from) {
				this.from = from;
			}

			public String getTo() {
				return getDateVal(to);
			}

			public void setTo(String to) {
				this.to = to;
			}

			public Integer getOrders() {
				return orders;
			}

			public void setOrders(Integer orders) {
				this.orders = orders;
			}
		}
	}
}
