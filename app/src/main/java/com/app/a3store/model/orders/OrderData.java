package com.app.a3store.model.orders;

import java.util.ArrayList;

public class OrderData {

	int pages;
	ArrayList<OrderInfo> orderList;

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public ArrayList<OrderInfo> getOrderList() {
		return orderList;
	}

	public void setOrderList(ArrayList<OrderInfo> orderList) {
		this.orderList = orderList;
	}
}
