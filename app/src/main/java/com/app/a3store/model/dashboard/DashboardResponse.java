package com.app.a3store.model.dashboard;

import java.util.ArrayList;
import java.util.List;

public class DashboardResponse {

	private String error;
	private String message;
	private Data data;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public class Data {

		private ArrayList<Order> orders = null;
		private List<Graph> graph = null;
		private Profile profile;
		private Store store;
		private int notificationCount;
		private StockProduct stockProduct;


		public int getNotificationCount() {
			return notificationCount;
		}

		public void setNotificationCount(int notificationCount) {
			this.notificationCount = notificationCount;
		}

		public ArrayList<Order> getOrders() {
			return orders;
		}

		public void setOrders(ArrayList<Order> orders) {
			this.orders = orders;
		}

		public List<Graph> getGraph() {
			return graph;
		}

		public void setGraph(List<Graph> graph) {
			this.graph = graph;
		}

		public Profile getProfile() {
			return profile;
		}

		public void setProfile(Profile profile) {
			this.profile = profile;
		}

		public Store getStore() {
			return store;
		}

		public void setStore(Store store) {
			this.store = store;
		}

		public StockProduct getStockProduct() {
			return stockProduct;
		}

		public void setStockProduct(StockProduct stockProduct) {
			this.stockProduct = stockProduct;
		}

		public class Order {

			private String orderIDs;
			private Integer orderId;
			private Integer storeId;
			private String orderOn;
			private String deliveryOn;
			private String orderStatus;

			String deliveryDate;
			String fromTime;
			String toTime;
			String timeText;

			public String getDeliveryDate() {
				return deliveryDate;
			}

			public void setDeliveryDate(String deliveryDate) {
				this.deliveryDate = deliveryDate;
			}

			public String getFromTime() {
				return fromTime;
			}

			public void setFromTime(String fromTime) {
				this.fromTime = fromTime;
			}

			public String getToTime() {
				return toTime;
			}

			public void setToTime(String toTime) {
				this.toTime = toTime;
			}

			public String getTimeText() {
				return timeText;
			}

			public void setTimeText(String timeText) {
				this.timeText = timeText;
			}

			public String getOrderStatus() {
				return orderStatus;
			}

			public void setOrderStatus(String orderStatus) {
				this.orderStatus = orderStatus;
			}

			public String getOrderIDs() {
				return orderIDs;
			}

			public void setOrderIDs(String orderIDs) {
				this.orderIDs = orderIDs;
			}

			public Integer getOrderId() {
				return orderId;
			}

			public void setOrderId(Integer orderId) {
				this.orderId = orderId;
			}

			public Integer getStoreId() {
				return storeId;
			}

			public void setStoreId(Integer storeId) {
				this.storeId = storeId;
			}

			public String getOrderOn() {
				return orderOn;
			}

			public void setOrderOn(String orderOn) {
				this.orderOn = orderOn;
			}

			public String getDeliveryOn() {
				return deliveryOn;
			}

			public void setDeliveryOn(String deliveryOn) {
				this.deliveryOn = deliveryOn;
			}
		}

		public class Graph {

			private Integer year;
			private Integer month;
			private Double amount;

			public Integer getYear() {
				return year;
			}

			public void setYear(Integer year) {
				this.year = year;
			}

			public Integer getMonth() {
				return month;
			}

			public void setMonth(Integer month) {
				this.month = month;
			}

			public Double getAmount() {
				return amount;
			}

			public void setAmount(Double amount) {
				this.amount = amount;
			}
		}

		public class Profile {

			private Integer id;
			private String firstName;
			private String lastName;
			private String email;
			private Integer countryCode;
			private String mobileNumber;
			private String status;

			public Integer getId() {
				return id;
			}

			public void setId(Integer id) {
				this.id = id;
			}

			public String getFirstName() {
				return firstName;
			}

			public void setFirstName(String firstName) {
				this.firstName = firstName;
			}

			public String getLastName() {
				return lastName;
			}

			public void setLastName(String lastName) {
				this.lastName = lastName;
			}

			public String getEmail() {
				return email;
			}

			public void setEmail(String email) {
				this.email = email;
			}

			public Integer getCountryCode() {
				return countryCode;
			}

			public void setCountryCode(Integer countryCode) {
				this.countryCode = countryCode;
			}

			public String getMobileNumber() {
				return mobileNumber;
			}

			public void setMobileNumber(String mobileNumber) {
				this.mobileNumber = mobileNumber;
			}

			public String getStatus() {
				return status;
			}

			public void setStatus(String status) {
				this.status = status;
			}
		}
		public class Store {

			private Integer id;
			private String storeName;
			private String storeID;
			private String status;

			public Integer getId() {
				return id;
			}

			public void setId(Integer id) {
				this.id = id;
			}

			public String getStoreName() {
				return storeName;
			}

			public void setStoreName(String storeName) {
				this.storeName = storeName;
			}

			public String getStoreID() {
				return storeID;
			}

			public void setStoreID(String storeID) {
				this.storeID = storeID;
			}

			public String getStatus() {
				return status;
			}

			public void setStatus(String status) {
				this.status = status;
			}
		}

		public class StockProduct {

			private Integer outOfStock;
			private Integer lowStock;

			public Integer getOutOfStock() {
				return outOfStock;
			}

			public void setOutOfStock(Integer outOfStock) {
				this.outOfStock = outOfStock;
			}

			public Integer getLowStock() {
				return lowStock;
			}

			public void setLowStock(Integer lowStock) {
				this.lowStock = lowStock;
			}
		}
	}
}
