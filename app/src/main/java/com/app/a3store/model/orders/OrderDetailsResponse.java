package com.app.a3store.model.orders;

import java.util.List;

public class OrderDetailsResponse {

	private String error;
	private String message;
	private Data data;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public class Data {

		private double totalAmount;
		private double tax;
		private double fastDelivery;
		private double grandTotal;
		private OrdersInfo ordersInfo;
		private List<OrderItem> orderItems = null;

		public double getFasttax() {
			return fastDelivery;
		}

		public void setFasttax(double fasttax) {
			this.fastDelivery = fastDelivery;
		}

		public double getGrandTotal() {
			return grandTotal;
		}

		public void setGrandTotal(double grandTotal) {
			this.grandTotal = grandTotal;
		}

		public double getTotalAmount() {
			return totalAmount;
		}

		public void setTotalAmount(double totalAmount) {
			this.totalAmount = totalAmount;
		}

		public double getTax() {
			return tax;
		}

		public void setTax(double tax) {
			this.tax = tax;
		}

		public OrdersInfo getOrdersInfo() {
			return ordersInfo;
		}

		public void setOrdersInfo(OrdersInfo ordersInfo) {
			this.ordersInfo = ordersInfo;
		}

		public List<OrderItem> getOrderItems() {
			return orderItems;
		}

		public void setOrderItems(List<OrderItem> orderItems) {
			this.orderItems = orderItems;
		}

		public class OrdersInfo {

			private Integer id;
			private String orderIDs;
			private String orderOn;
			private String deliveryOn;
			private String driverName;
			private String driverNumber;
			private String type;
			private String orderStatus;
			private Integer deleteItems;
			String deliveryDate;
			String fromTime;
			String toTime;
			String timeText;
			String countryCode;

			public String getCountryCode() {
				return countryCode;
			}

			public void setCountryCode(String countryCode) {
				this.countryCode = countryCode;
			}

			public String getDeliveryDate() {
				return deliveryDate;
			}

			public void setDeliveryDate(String deliveryDate) {
				this.deliveryDate = deliveryDate;
			}

			public String getFromTime() {
				return fromTime;
			}

			public void setFromTime(String fromTime) {
				this.fromTime = fromTime;
			}

			public String getToTime() {
				return toTime;
			}

			public void setToTime(String toTime) {
				this.toTime = toTime;
			}

			public String getTimeText() {
				return timeText;
			}

			public void setTimeText(String timeText) {
				this.timeText = timeText;
			}

			public String getOrderStatus() {
				return orderStatus;
			}

			public void setOrderStatus(String orderStatus) {
				this.orderStatus = orderStatus;
			}

			public Integer getId() {
				return id;
			}

			public void setId(Integer id) {
				this.id = id;
			}

			public String getOrderIDs() {
				return orderIDs;
			}

			public void setOrderIDs(String orderIDs) {
				this.orderIDs = orderIDs;
			}

			public String getOrderOn() {
				return orderOn;
			}

			public void setOrderOn(String orderOn) {
				this.orderOn = orderOn;
			}

			public String getDeliveryOn() {
				return deliveryOn;
			}

			public void setDeliveryOn(String deliveryOn) {
				this.deliveryOn = deliveryOn;
			}

			public String getType() {
				return type;
			}

			public void setType(String type) {
				this.type = type;
			}

			public Integer getDeleteItems() {
				return deleteItems;
			}

			public void setDeleteItems(Integer deleteItems) {
				this.deleteItems = deleteItems;
			}

			public String getDriverName() {
				return driverName;
			}

			public void setDriverName(String driverName) {
				this.driverName = driverName;
			}

			public String getDriverNumber() {
				return driverNumber;
			}

			public void setDriverNumber(String driverNumber) {
				this.driverNumber = driverNumber;
			}
		}

		public class OrderItem {

			private Integer id;
			private Integer productId;
			private String productName;
			private String arabicName;
			private Integer storeId;
			private Integer quantity;
			private String productImage;
			private String productStatus;
			private String storeStatus;
			private Integer categoryId;
			private Integer productCategoryId;
			private Object productSubCategoryId;
			private Integer discount;
			private double supplyPrice;
			private double price;
			private Integer productStatusPacked;
			private String orderInstructions;
			private Object cuttingName;
			private String cuttingPrice;
			private Object boxName;
			private String boxPrice;


			public String getArabicName(){
				return arabicName;
			}

			public String getStoreStatus() {
				return storeStatus;
			}

			public void setStoreStatus(String storeStatus) {
				this.storeStatus = storeStatus;
			}

			public Integer getProductId() {
				return productId;
			}

			public void setProductId(Integer productId) {
				this.productId = productId;
			}

			public Integer getId() {
				return id;
			}

			public void setId(Integer id) {
				this.id = id;
			}

			public String getProductName() {
				return productName;
			}

			public void setProductName(String productName) {
				this.productName = productName;
			}

			public Integer getStoreId() {
				return storeId;
			}

			public void setStoreId(Integer storeId) {
				this.storeId = storeId;
			}

			public Integer getQuantity() {
				return quantity;
			}

			public void setQuantity(Integer quantity) {
				this.quantity = quantity;
			}

			public String getProductImage() {
				return productImage;
			}

			public void setProductImage(String productImage) {
				this.productImage = productImage;
			}

			public String getProductStatus() {
				return productStatus;
			}

			public void setProductStatus(String productStatus) {
				this.productStatus = productStatus;
			}

			public Integer getCategoryId() {
				return categoryId;
			}

			public void setCategoryId(Integer categoryId) {
				this.categoryId = categoryId;
			}

			public Integer getProductCategoryId() {
				return productCategoryId;
			}

			public void setProductCategoryId(Integer productCategoryId) {
				this.productCategoryId = productCategoryId;
			}

			public Object getProductSubCategoryId() {
				return productSubCategoryId;
			}

			public void setProductSubCategoryId(Object productSubCategoryId) {
				this.productSubCategoryId = productSubCategoryId;
			}

			public Integer getDiscount() {
				return discount;
			}

			public void setDiscount(Integer discount) {
				this.discount = discount;
			}

			public double getSupplyPrice() {
				return supplyPrice;
			}

			public void setSupplyPrice(float supplyPrice) {
				this.supplyPrice = supplyPrice;
			}

			public double getPrice() {
				return price;
			}

			public void setPrice(double price) {
				this.price = price;
			}

			public String getOrderInstructions() {
				return orderInstructions;
			}

			public void setOrderInstructions(String orderInstructions) {
				this.orderInstructions = orderInstructions;
			}

			public Object getCuttingName() {
				return cuttingName;
			}

			public void setCuttingName(Object cuttingName) {
				this.cuttingName = cuttingName;
			}

			public String getCuttingPrice() {
				return cuttingPrice;
			}

			public void setCuttingPrice(String cuttingPrice) {
				this.cuttingPrice =  cuttingPrice;
			}

			public Object getBoxName() {
				return boxName;
			}

			public void setBoxName(Object boxName) {
				this.boxName = boxName;
			}

			public String getBoxPrice() {
				return boxPrice;
			}

			public void setBoxPrice(String boxPrice) {
				this.boxPrice = (String) boxPrice;
			}

			public void setSupplyPrice(double supplyPrice) {
				this.supplyPrice = supplyPrice;
			}

			public Integer getProductStatusPacked() {
				return productStatusPacked;
			}

			public void setProductStatusPacked(Integer productStatusPacked) {
				this.productStatusPacked = productStatusPacked;
			}
		}
	}
}
