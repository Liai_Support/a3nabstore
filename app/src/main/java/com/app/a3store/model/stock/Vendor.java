package com.app.a3store.model.stock;

import androidx.annotation.NonNull;

public class Vendor {

	private Integer id;
	private Integer storeId;
	private String vendorName;
	private Integer activeStatus;
	private String createdAt;
	private String updateAt;

	public Vendor(String vendorName, int id) {
		this.vendorName = vendorName;
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public Integer getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(Integer activeStatus) {
		this.activeStatus = activeStatus;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(String updateAt) {
		this.updateAt = updateAt;
	}

	@NonNull
	@Override
	public String toString() {
		return vendorName;
	}
}
