package com.app.a3store.model.profile;

import static com.app.a3store.utils.Utils.round;

public class ProfileData {

	private ProfileDetail profile = null;
	double totalEarn;
	int totalOrders;

	public ProfileDetail getProfileDetails() {
		return profile;
	}

	public void setProfileDetails(ProfileDetail profileDetails) {
		this.profile = profileDetails;
	}

	public String getTotalEarn() {
		return round(totalEarn);
	}

	public void setTotalEarn(int totalEarn) {
		this.totalEarn = totalEarn;
	}

	public int getTotalOrders() {
		return totalOrders;
	}

	public void setTotalOrders(int totalOrders) {
		this.totalOrders = totalOrders;
	}
}
