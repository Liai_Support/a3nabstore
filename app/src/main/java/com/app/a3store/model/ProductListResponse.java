package com.app.a3store.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProductListResponse {

	private String message;
	private ProductListDataData data;
	private String error;

	public String isError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ProductListDataData getData() {
		return data;
	}

	public void setData(ProductListDataData data) {
		this.data = data;
	}

	public class ProductListDataData {

		int pages;
		@SerializedName(value = "products", alternate = {"productList"})
		ArrayList<DashboardResponse.BestProduct> products;

		public int getPages() {
			return pages;
		}

		public void setPages(int pages) {
			this.pages = pages;
		}

		public ArrayList<DashboardResponse.BestProduct> getProducts() {
			return products;
		}

		public void setProducts(ArrayList<DashboardResponse.BestProduct> products) {
			this.products = products;
		}
	}

	public class ProductListData implements Parcelable {

		int id;
		int productCategoryId;
		int productSubCategoryId;
		int isBestProduct;
		String productName;
		String arabicName;
		String productStatus;
		String productWeight;
		String productPrice;
		String productDiscount;
		String productDiscountStatus;
		String instruction;
		String status;
		String createdAt;
		ArrayList<ProductImage> productImages;
		boolean addedInCart;

		public String getArabicName() {
			return arabicName;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public int getProductCategoryId() {
			return productCategoryId;
		}

		public void setProductCategoryId(int productCategoryId) {
			this.productCategoryId = productCategoryId;
		}

		public int getProductSubCategoryId() {
			return productSubCategoryId;
		}

		public void setProductSubCategoryId(int productSubCategoryId) {
			this.productSubCategoryId = productSubCategoryId;
		}

		public int getIsBestProduct() {
			return isBestProduct;
		}

		public void setIsBestProduct(int isBestProduct) {
			this.isBestProduct = isBestProduct;
		}

		public String getProductName() {
			return productName;
		}

		public void setProductName(String productName) {
			this.productName = productName;
		}

		public String getProductStatus() {
			return productStatus;
		}

		public void setProductStatus(String productStatus) {
			this.productStatus = productStatus;
		}

		public String getProductWeight() {
			return productWeight;
		}

		public void setProductWeight(String productWeight) {
			this.productWeight = productWeight;
		}

		public String getProductPrice() {
			return productPrice;
		}

		public void setProductPrice(String productPrice) {
			this.productPrice = productPrice;
		}

		public String getProductDiscount() {
			return productDiscount;
		}

		public void setProductDiscount(String productDiscount) {
			this.productDiscount = productDiscount;
		}

		public String getProductDiscountStatus() {
			return productDiscountStatus;
		}

		public void setProductDiscountStatus(String productDiscountStatus) {
			this.productDiscountStatus = productDiscountStatus;
		}

		public String getInstruction() {
			return instruction;
		}

		public void setInstruction(String instruction) {
			this.instruction = instruction;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}

		public boolean isAddedInCart() {
			return addedInCart;
		}

		public void setAddedInCart(boolean addedInCart) {
			this.addedInCart = addedInCart;
		}

		public Creator<ProductListData> getCREATOR() {
			return CREATOR;
		}

		public ArrayList<ProductImage> getProductImages() {
			return productImages;
		}

		public void setProductImages(ArrayList<ProductImage> productImages) {
			this.productImages = productImages;
		}

		ProductListData(Parcel parcel) {
			id = parcel.readInt();
			productCategoryId = parcel.readInt();
			productSubCategoryId = parcel.readInt();
			isBestProduct = parcel.readInt();
			productName = parcel.readString();
			productStatus = parcel.readString();
			productWeight = parcel.readString();
			productPrice = parcel.readString();
			productDiscount = parcel.readString();
			productDiscountStatus = parcel.readString();
			instruction = parcel.readString();
			status = parcel.readString();
			createdAt = parcel.readString();
			addedInCart = parcel.readByte() != 0;
		}

		@Override
		public void writeToParcel(Parcel parcel, int flags) {
			parcel.writeInt(id);
			parcel.writeInt(productCategoryId);
			parcel.writeInt(productSubCategoryId);
			parcel.writeInt(isBestProduct);
			parcel.writeString(productName);
			parcel.writeString(productStatus);
			parcel.writeString(productWeight);
			parcel.writeString(productPrice);
			parcel.writeString(productDiscount);
			parcel.writeString(productDiscountStatus);
			parcel.writeString(instruction);
			parcel.writeString(status);
			parcel.writeString(createdAt);
			if(addedInCart) parcel.writeByte((byte) 1);
			else parcel.writeByte((byte) 0);
		}

		@Override
		public int describeContents() {
			return 0;
		}

		public final Creator<ProductListData> CREATOR = new Creator<ProductListData>() {
			@Override
			public ProductListData createFromParcel(Parcel in) {
				return new ProductListData(in);
			}

			@Override
			public ProductListData[] newArray(int size) {
				return new ProductListData[size];
			}
		};

		class ProductImage implements Parcelable {

			int id;
			int productId;
			String productImage;
			String productImageStatus;

			ProductImage(Parcel parcel) {
				id = parcel.readInt();
				productId = parcel.readInt();
				productImage = parcel.readString();
				productImageStatus = parcel.readString();
			}

			@Override
			public void writeToParcel(Parcel parcel, int flags) {
				parcel.writeInt(id);
				parcel.writeInt(productId);
				parcel.writeString(productImage);
				parcel.writeString(productImageStatus);
			}

			@Override
			public int describeContents() {
				return 0;
			}

			public final Creator<ProductImage> CREATOR = new Creator<ProductImage>() {
				@Override
				public ProductImage createFromParcel(Parcel in) {
					return new ProductImage(in);
				}

				@Override
				public ProductImage[] newArray(int size) {
					return new ProductImage[size];
				}
			};
		}
	}
}

