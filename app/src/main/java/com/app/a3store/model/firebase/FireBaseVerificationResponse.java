package com.app.a3store.model.firebase;

public class FireBaseVerificationResponse {

	String error;
	String message;
	String setVerificationCode;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getVerificationCode() {
		return setVerificationCode;
	}

	public void setVerificationCode(String setVerificationCode) {
		this.setVerificationCode = setVerificationCode;
	}
}
