package com.app.a3store.model.orders;

public class OrderInfo {

	int id;
	int orderId;
	String orderIDs;
	String orderOn;
	String deliveryOn;

	String storeId;
	String quantity;
	int currency_id;
	int price;
	int status_id;
	String createdAt;
	String updatedAt;
	String status_name;
	String orderStatus;
	String storeStatus = "";
	String deliveryDate;
	String fromTime;
	String toTime;
	String timeText;
	int deleteItems;
	int productcount;
	int packedcount;

	public int getProductcount() {
		return productcount;
	}

	public void setProductcount(int productcount) {
		this.productcount = productcount;
	}

	public int getPackedcount() {
		return packedcount;
	}

	public void setPackedcount(int packedcount) {
		this.packedcount = packedcount;
	}

	public String getStoreStatus() {
		return storeStatus;
	}

	public void setStoreStatus(String storeStatus) {
		this.storeStatus = storeStatus;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getFromTime() {
		return fromTime;
	}

	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}

	public String getToTime() {
		return toTime;
	}

	public void setToTime(String toTime) {
		this.toTime = toTime;
	}

	public String getTimeText() {
		return timeText;
	}

	public void setTimeText(String timeText) {
		this.timeText = timeText;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public String getOrderOn() {
		return orderOn;
	}

	public void setOrderOn(String orderOn) {
		this.orderOn = orderOn;
	}

	public String getDeliveryOn() {
		return deliveryOn;
	}

	public void setDeliveryOn(String deliveryOn) {
		this.deliveryOn = deliveryOn;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public int getCurrency_id() {
		return currency_id;
	}

	public void setCurrency_id(int currency_id) {
		this.currency_id = currency_id;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getStatus_id() {
		return status_id;
	}

	public void setStatus_id(int status_id) {
		this.status_id = status_id;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getStatus_name() {
		return status_name;
	}

	public void setStatus_name(String status_name) {
		this.status_name = status_name;
	}

	public String getOrderIDs() {
		return orderIDs;
	}

	public void setOrderIDs(String orderIDs) {
		this.orderIDs = orderIDs;
	}

	public int getDeleteItems() {
		return deleteItems;
	}

	public void setDeleteItems(int deleteItems) {
		this.deleteItems = deleteItems;
	}

}
