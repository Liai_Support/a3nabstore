package com.app.a3store.model;

import com.app.a3store.utils.Utils;

import java.util.List;

public class OrderTransactionResponse {

	private String error;
	private String message;
	private Data data;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public class Data {

		private List<Order> orders = null;
		private int pages;

		public int getPages() {
			return pages;
		}

		public void setPages(int pages) {
			this.pages = pages;
		}

		public List<Order> getOrders() {
			return orders;
		}

		public void setOrders(List<Order> orders) {
			this.orders = orders;
		}

		public class Order {

			private Integer id;
			private Integer orderId;
			private String orderIDs;
			private Object deliveryOn;
			private String orderOn;
			private String deliveryDate;
			private Double amount;
			private Double taxPrice;

			public Double getTaxPrice() {
				return taxPrice;
			}

			public void setTaxPrice(Double taxPrice) {
				this.taxPrice = taxPrice;
			}

			public Integer getId() {
				return id;
			}

			public void setId(Integer id) {
				this.id = id;
			}

			public Integer getOrderId() {
				return orderId;
			}

			public void setOrderId(Integer orderId) {
				this.orderId = orderId;
			}

			public String getOrderIDs() {
				return orderIDs;
			}

			public void setOrderIDs(String orderIDs) {
				this.orderIDs = orderIDs;
			}

			public Object getDeliveryOn() {
				return deliveryOn;
			}

			public void setDeliveryOn(Object deliveryOn) {
				this.deliveryOn = deliveryOn;
			}

			public String getOrderOn() {
				return orderOn;
			}

			public void setOrderOn(String orderOn) {
				this.orderOn = orderOn;
			}

			public String getDeliveryDate() {
				return deliveryDate;
			}

			public void setDeliveryDate(String deliveryDate) {
				this.deliveryDate = deliveryDate;
			}

			public Double getAmount() {
				return amount;
			}

		}
	}
}
