package com.app.a3store.model.product;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ProductListResponse implements Serializable {

	private String error;
	private String message;
	private Data data;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public class Data implements Serializable {

		private Integer pages;
		@SerializedName(value = "products", alternate = {"productList"})
		private List<Product> products = null;
		private List<BestProduct> bestProducts = null;

		public Integer getPages() {
			return pages;
		}

		public void setPages(Integer pages) {
			this.pages = pages;
		}

		public List<Product> getProducts() {
			return products;
		}

		public void setProducts(List<Product> products) {
			this.products = products;
		}

		public List<BestProduct> getBestProducts() {
			return bestProducts;
		}

		public void setBestProducts(List<BestProduct> bestProducts) {
			this.bestProducts = bestProducts;
		}


		public class BestProduct implements Serializable {

			private Integer id;
			private Integer categoryId;
			private Integer productCategoryId;
			private Object productSubCategoryId;
			private Integer storeId;
			private Integer storeStock;
			private String productName;
			private String productStatus;
			private String productWeight;
			private Double productPrice;
			private Integer productDiscount;
			private String productDiscountStatus;
			private String productDescription;
			private Integer isBestProduct;
			private String orderVariants;
			private String specialInstructions;
			private String instructionsStatus;
			private String differentPriceVariant;
			private String status;
			private String createdAt;
			private String updatedAt;
			private List<ProductImage_> productImages = null;

			public Integer getId() {
				return id;
			}

			public void setId(Integer id) {
				this.id = id;
			}

			public Integer getCategoryId() {
				return categoryId;
			}

			public void setCategoryId(Integer categoryId) {
				this.categoryId = categoryId;
			}

			public Integer getProductCategoryId() {
				return productCategoryId;
			}

			public void setProductCategoryId(Integer productCategoryId) {
				this.productCategoryId = productCategoryId;
			}

			public Object getProductSubCategoryId() {
				return productSubCategoryId;
			}

			public void setProductSubCategoryId(Object productSubCategoryId) {
				this.productSubCategoryId = productSubCategoryId;
			}

			public Integer getStoreId() {
				return storeId;
			}

			public void setStoreId(Integer storeId) {
				this.storeId = storeId;
			}

			public Integer getStoreStock() {
				return storeStock;
			}

			public void setStoreStock(Integer storeStock) {
				this.storeStock = storeStock;
			}

			public String getProductName() {
				return productName;
			}

			public void setProductName(String productName) {
				this.productName = productName;
			}

			public String getProductStatus() {
				return productStatus;
			}

			public void setProductStatus(String productStatus) {
				this.productStatus = productStatus;
			}

			public String getProductWeight() {
				return productWeight;
			}

			public void setProductWeight(String productWeight) {
				this.productWeight = productWeight;
			}

			public Double getProductPrice() {
				return productPrice;
			}

			public void setProductPrice(Double productPrice) {
				this.productPrice = productPrice;
			}

			public Integer getProductDiscount() {
				return productDiscount;
			}

			public void setProductDiscount(Integer productDiscount) {
				this.productDiscount = productDiscount;
			}

			public String getProductDiscountStatus() {
				return productDiscountStatus;
			}

			public void setProductDiscountStatus(String productDiscountStatus) {
				this.productDiscountStatus = productDiscountStatus;
			}

			public String getProductDescription() {
				return productDescription;
			}

			public void setProductDescription(String productDescription) {
				this.productDescription = productDescription;
			}

			public Integer getIsBestProduct() {
				return isBestProduct;
			}

			public void setIsBestProduct(Integer isBestProduct) {
				this.isBestProduct = isBestProduct;
			}

			public String getOrderVariants() {
				return orderVariants;
			}

			public void setOrderVariants(String orderVariants) {
				this.orderVariants = orderVariants;
			}

			public String getSpecialInstructions() {
				return specialInstructions;
			}

			public void setSpecialInstructions(String specialInstructions) {
				this.specialInstructions = specialInstructions;
			}

			public String getInstructionsStatus() {
				return instructionsStatus;
			}

			public void setInstructionsStatus(String instructionsStatus) {
				this.instructionsStatus = instructionsStatus;
			}

			public String getDifferentPriceVariant() {
				return differentPriceVariant;
			}

			public void setDifferentPriceVariant(String differentPriceVariant) {
				this.differentPriceVariant = differentPriceVariant;
			}

			public String getStatus() {
				return status;
			}

			public void setStatus(String status) {
				this.status = status;
			}

			public String getCreatedAt() {
				return createdAt;
			}

			public void setCreatedAt(String createdAt) {
				this.createdAt = createdAt;
			}

			public String getUpdatedAt() {
				return updatedAt;
			}

			public void setUpdatedAt(String updatedAt) {
				this.updatedAt = updatedAt;
			}

			public List<ProductImage_> getProductImages() {
				return productImages;
			}

			public void setProductImages(List<ProductImage_> productImages) {
				this.productImages = productImages;
			}

			public class ProductImage_ implements Serializable {

				private Integer id;
				private String productImage;
				private Integer productId;
				private String productImageStatus;

				public Integer getId() {
					return id;
				}

				public void setId(Integer id) {
					this.id = id;
				}

				public String getProductImage() {
					return productImage;
				}

				public void setProductImage(String productImage) {
					this.productImage = productImage;
				}

				public Integer getProductId() {
					return productId;
				}

				public void setProductId(Integer productId) {
					this.productId = productId;
				}

				public String getProductImageStatus() {
					return productImageStatus;
				}

				public void setProductImageStatus(String productImageStatus) {
					this.productImageStatus = productImageStatus;
				}
			}
		}
	}
}
