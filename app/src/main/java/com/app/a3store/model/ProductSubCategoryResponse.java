package com.app.a3store.model;

import java.util.List;

public class ProductSubCategoryResponse {

	private String error;
	private String message;
	private Data data;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public class Data {

		private List<ProductCategory> productCategory = null;

		public List<ProductCategory> getProductCategory() {
			return productCategory;
		}

		public void setProductCategory(List<ProductCategory> productCategory) {
			this.productCategory = productCategory;
		}

		public class ProductCategory {

			private Integer id;
			private Integer productCategoryId;
			private String productCategoryName;
			private String arabicName;
			private String productSubCategoryStatus;
			private Integer isDelete;
			private String createdAt;
			private String updatedAt;

			public Integer getId() {
				return id;
			}

			public void setId(Integer id) {
				this.id = id;
			}

			public Integer getProductCategoryId() {
				return productCategoryId;
			}

			public void setProductCategoryId(Integer productCategoryId) {
				this.productCategoryId = productCategoryId;
			}

			public String getProductSubCategoryName() {
				return productCategoryName;
			}

			public void setProductSubCategoryName(String productSubCategoryName) {
				this.productCategoryName = productSubCategoryName;
			}

			public String getArabicName() {
				return arabicName;
			}

			public void setArabicName(String arabicName) {
				this.arabicName = arabicName;
			}

			public String getProductSubCategoryStatus() {
				return productSubCategoryStatus;
			}

			public void setProductSubCategoryStatus(String productSubCategoryStatus) {
				this.productSubCategoryStatus = productSubCategoryStatus;
			}

			public Integer getIsDelete() {
				return isDelete;
			}

			public void setIsDelete(Integer isDelete) {
				this.isDelete = isDelete;
			}

			public String getCreatedAt() {
				return createdAt;
			}

			public void setCreatedAt(String createdAt) {
				this.createdAt = createdAt;
			}

			public String getUpdatedAt() {
				return updatedAt;
			}

			public void setUpdatedAt(String updatedAt) {
				this.updatedAt = updatedAt;
			}
		}
	}
}
