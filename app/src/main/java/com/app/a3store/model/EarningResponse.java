package com.app.a3store.model;

import java.io.Serializable;
import java.util.List;

import static com.app.a3store.utils.TimeCalculator.getDateVal;
import static com.app.a3store.utils.Utils.round;

public class EarningResponse implements Serializable{

	private String error;
	private String message;
	private Data data;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public class Data implements Serializable{

		private List<Pending> pending = null;
		private List<Past> past = null;

		public List<Pending> getPending() {
			return pending;
		}

		public void setPending(List<Pending> pending) {
			this.pending = pending;
		}

		public List<Past> getPast() {
			return past;
		}

		public void setPast(List<Past> past) {
			this.past = past;
		}

		public class Pending implements Serializable {

			private Integer isPaid;
			private Integer storeId;
			private Integer year;
			private Integer month;
			private double amount;
			private String from;
			private String to;
			private Integer orders;

			public Integer getIsPaid() {
				return isPaid;
			}

			public void setIsPaid(Integer isPaid) {
				this.isPaid = isPaid;
			}

			public Integer getStoreId() {
				return storeId;
			}

			public void setStoreId(Integer storeId) {
				this.storeId = storeId;
			}

			public Integer getYear() {
				return year;
			}

			public void setYear(Integer year) {
				this.year = year;
			}

			public Integer getMonth() {
				return month;
			}

			public void setMonth(Integer month) {
				this.month = month;
			}

			public String getAmount() {
				return round(amount);
			}

			public void setAmount(double amount) {
				this.amount = amount;
			}

			public String getFrom() {
				return getDateVal(from);
			}

			public void setFrom(String from) {
				this.from = from;
			}

			public String getTo() {
				return getDateVal(to);
			}

			public void setTo(String to) {
				this.to = to;
			}

			public Integer getOrders() {
				return orders;
			}

			public void setOrders(Integer orders) {
				this.orders = orders;
			}
		}

		public class Past implements Serializable{

			private Integer isPaid;
			private Integer storeId;
			private Integer year;
			private Integer month;
			private double amount;
			private String from;
			private String to;
			private Integer orders;
			private String imageUrl = "";

			public Integer getIsPaid() {
				return isPaid;
			}

			public void setIsPaid(Integer isPaid) {
				this.isPaid = isPaid;
			}

			public Integer getStoreId() {
				return storeId;
			}

			public void setStoreId(Integer storeId) {
				this.storeId = storeId;
			}

			public Integer getYear() {
				return year;
			}

			public void setYear(Integer year) {
				this.year = year;
			}

			public Integer getMonth() {
				return month;
			}

			public void setMonth(Integer month) {
				this.month = month;
			}

			public String getAmount() {
				return round(amount);
			}

			public void setAmount(double amount) {
				this.amount = amount;
			}

			public String getFrom() {
				return getDateVal(from);
			}

			public void setFrom(String from) {
				this.from = from;
			}

			public String getTo() {
				return getDateVal(to);
			}

			public void setTo(String to) {
				this.to = to;
			}

			public Integer getOrders() {
				return orders;
			}

			public void setOrders(Integer orders) {
				this.orders = orders;
			}

			public String getImageUrl() {
				return imageUrl;
			}

			public void setImageUrl(String imageUrl) {
				this.imageUrl = imageUrl;
			}
		}
	}
}
