package com.app.a3store.utils;

import android.content.Context;

import com.app.a3store.R;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class TimeCalculator {

	public static String convertDateFormat(String date) {
		if(date != null && !date.trim().isEmpty()) {
			SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
			parser.setTimeZone(TimeZone.getTimeZone("UTC"));
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			String output = null;
			try {
				output = formatter.format(parser.parse(date));
			} catch(ParseException e) {
				e.printStackTrace();
			}
			return output;
		}
		return "";
	}

	public static String convertDate(String date, Context context) {
		if(date != null && !date.trim().isEmpty()) {
			SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
			parser.setTimeZone(TimeZone.getTimeZone("UTC"));
			SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
			SimpleDateFormat timeFormatter = new SimpleDateFormat("hh:mma", Locale.ENGLISH);
			String output = null;
			try {
				output = dateFormatter.format(parser.parse(date)) + " " + context.getString(R.string.at) + " " + timeFormatter.format(parser.parse(date));
				output = output.toLowerCase();
			} catch(ParseException e) {
				e.printStackTrace();
			}
			return output;
		}
		return "";
	}

	public static String getTime(String date) {
		if(date != null && !date.trim().isEmpty()) {
			SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
			parser.setTimeZone(TimeZone.getTimeZone("UTC"));
			SimpleDateFormat formatter = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
			String output = null;
			try {
				output = formatter.format(parser.parse(date));
			} catch(ParseException e) {
				e.printStackTrace();
			}
			return output;
		}
		return "";
	}

	public static String getTimeFormat(String date) {
		if(date != null && !date.trim().isEmpty()) {
			SimpleDateFormat parser = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
			parser.setTimeZone(TimeZone.getTimeZone("UTC"));
			SimpleDateFormat formatter = new SimpleDateFormat("hh:mm a",  Locale.ENGLISH);
			String output = null;
			try {
				output = formatter.format(parser.parse(date));
			} catch(ParseException e) {
				e.printStackTrace();
			}
			return output;
		}
		return "";
	}

	public static String getTimeSlot(String date) {
		if(date != null && !date.trim().isEmpty()) {
			SimpleDateFormat parser = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
			SimpleDateFormat formatter = new SimpleDateFormat("hh:mm a",  Locale.ENGLISH);
			String output = null;
			try {
				output = formatter.format(parser.parse(date));
			} catch(ParseException e) {
				e.printStackTrace();
			}
			return output;
		}
		return "";
	}

	public static String getDate(String date) {
		if(!date.trim().isEmpty()) {
			SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
			parser.setTimeZone(TimeZone.getTimeZone("UTC"));
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM, yyyy", Locale.ENGLISH);
			String output = null;
			try {
				output = formatter.format(parser.parse(date));
			} catch(ParseException e) {
				e.printStackTrace();
			}
			return output;
		}
		return "";
	}

	public static String getDateValue(String date) {
		if(date.trim().isEmpty()) {
			return date;
		}
		String dateVal = "";
		try {
			DateFormatSymbols dfs = new DateFormatSymbols();
			String[] months = dfs.getMonths();
			Calendar myDate = new GregorianCalendar();
			Date date1 = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(date);
			myDate.setTime(date1);
			if(myDate.get(Calendar.DATE) == 1) dateVal = myDate.get(Calendar.DATE) + "ST ";
			else if(myDate.get(Calendar.DATE) == 2) dateVal = myDate.get(Calendar.DATE) + "ND ";
			else if(myDate.get(Calendar.DATE) == 3) dateVal = myDate.get(Calendar.DATE) + "RD ";
			else dateVal = myDate.get(Calendar.DATE) + "TH ";
			myDate.get(Calendar.MONTH);
			if(myDate.get(Calendar.MONTH) <= 11) {
				dateVal = dateVal + months[myDate.get(Calendar.MONTH)] + " ";
			}
			dateVal = dateVal + myDate.get(Calendar.YEAR) % 100;
		} catch(ParseException e) {
			e.printStackTrace();
		}
		return dateVal;
	}

	public static String getNotificationDate(String date) {
		if(date.trim().isEmpty()) {
			return date;
		}
		String dateVal = "";
		try {
			DateFormatSymbols dfs = new DateFormatSymbols();
			String[] months = dfs.getMonths();
			Calendar myDate = new GregorianCalendar();
			Date date1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH).parse(date);
			myDate.setTime(date1);
			String Date = "";
			if(myDate.get(Calendar.DATE) < 10) Date = "0" + myDate.get(Calendar.DATE);
			else Date = myDate.get(Calendar.DATE) + "";
			if(myDate.get(Calendar.DATE) == 1) dateVal = Date + "st ";
			else if(myDate.get(Calendar.DATE) == 2) dateVal = Date + "nd ";
			else if(myDate.get(Calendar.DATE) == 3) dateVal = Date + "rd ";
			else dateVal = myDate.get(Calendar.DATE) + "th ";
			myDate.get(Calendar.MONTH);
			if(myDate.get(Calendar.MONTH) <= 11) {
				dateVal = dateVal + months[myDate.get(Calendar.MONTH)].substring(0,3) + ", ";
			}
			dateVal = dateVal + myDate.get(Calendar.YEAR);
		} catch(ParseException e) {
			e.printStackTrace();
		}
		return dateVal;
	}

	public static String getDateVal(String date) {
		if(!date.trim().isEmpty()) {
			String output = null;
			try {
				SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
				parser.setTimeZone(TimeZone.getTimeZone("UTC"));
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
				output = formatter.format(parser.parse(date));
			} catch(ParseException e) {
				e.printStackTrace();
			}
			return output;
		}
		return "";
	}
}
