package com.app.a3store.utils;

import android.content.Context;

import com.app.a3store.volley.ApiUtils;
import com.app.a3store.volley.InputForAPI;

import org.json.JSONObject;

public class UrlUtils {

	public static InputForAPI postInputs(Context context, JSONObject jsonObject, String method) {

		InputForAPI inputForAPI = new InputForAPI(context);
		inputForAPI.setJsonObject(jsonObject);
		inputForAPI.setUrl(method);
		inputForAPI.setHeaders(ApiUtils.getAuthorizationHeaderTwo(context));
		return inputForAPI;
	}

	public static InputForAPI postLang(Context context, JSONObject jsonObject, String method) {

		InputForAPI inputForAPI = new InputForAPI(context);
		inputForAPI.setJsonObject(jsonObject);
		inputForAPI.setUrl(method);
		inputForAPI.setHeaders(ApiUtils.getLangHeader(context));

		return inputForAPI;
	}

	public static InputForAPI getInputs(Context context, String method) {

		InputForAPI inputForAPI = new InputForAPI(context);
		inputForAPI.setUrl(method);
		inputForAPI.setHeaders(ApiUtils.getAuthorizationHeaderTwo(context));

		return inputForAPI;
	}
}
