package com.app.a3store.utils.helper;

import android.content.Context;

public class SharedHelper {

	private final SharedPreference sharedPreference;

	public SharedHelper(Context context) {
		sharedPreference = new SharedPreference(context);
	}

	private String authToken;
	private String selectedLanguage;
	private String firebaseToken;
	private String userId;
	private String firstName;
	private String lastName;
	private String email;
	private String latitude;
	private String longitude;
	private String countryCode;
	private String mobileNumber;
	private String profilePic;
	private Boolean isLoggedIn;
	private String lastKnownLat;
	private String lastKnownLng;
	private String ncount;
	private boolean status;
	private boolean showSoundDialog;

	public Boolean getLoggedIn() {
		isLoggedIn = sharedPreference.getBoolean("loggedIn");
		return isLoggedIn;
	}

	public void setLoggedIn(Boolean loggedIn) {
		sharedPreference.putBoolean("loggedIn", loggedIn);
		isLoggedIn = loggedIn;
	}

	public String getAuthToken() {
//				authToken = "ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SnBaQ0k2TXl3aWJXOWlhV3hsVG5WdFltVnlJam9pT1RZeU9UWXhNemMyT0NJc0ltbGhkQ0k2TVRZeE1qWXhOREkwTkN3aVpYaHdJam94TmpFMU1qQTJNalEwZlEua3FLMnUxVkFJOU5kaHB2Ukx6SHZOOVRuVjc4eVpET1lGRmh5V1RuNDZxdw==";
		authToken = sharedPreference.getKey("authToken");
		return authToken;
	}

	public void setAuthToken(String authToken) {
		//		authToken = "ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SnBaQ0k2TVN3aWJXOWlhV3hsVG5WdFltVnlJam9pT1Rjek9EY3lNemd4TXlJc0ltbGhkQ0k2TVRZd056TXlNemN6TkN3aVpYaHdJam94TmpBNU9URTFOek0wZlEuNHlvR1JnbHFDZ09mWWJqRHZwSjB4Z29qY1JKdWpyWlpJY1hJSUktc3RPTQ==";
		this.sharedPreference.putKey("authToken", authToken);
		this.authToken = authToken;
	}

	public String getSelectedLanguage() {
		selectedLanguage = sharedPreference.getKey("selectedLanguage");
		if(selectedLanguage.equals("")) {
			return "ar";
		}
		return selectedLanguage;
	}

	public void setSelectedLanguage(String selectedLanguage) {
		this.sharedPreference.putKey("selectedLanguage", selectedLanguage);
		this.selectedLanguage = selectedLanguage;
	}

	public String getFirebaseToken() {
		firebaseToken = sharedPreference.getKey("firebaseToken");
		return firebaseToken;
	}

	public void setFirebaseToken(String firebaseToken) {
		this.sharedPreference.putKey("firebaseToken", firebaseToken);
		this.firebaseToken = firebaseToken;
	}

	public String getUserId() {
		userId = sharedPreference.getKey("userId");
		return userId;
	}

	public void setUserId(String userId) {
		this.sharedPreference.putKey("userId", userId);
		this.userId = userId;
	}

	public String getFirstName() {
		firstName = sharedPreference.getKey("firstName");
		return firstName;
	}

	public void setFirstName(String firstName) {
		if(firstName != null) this.sharedPreference.putKey("firstName", firstName);
		this.firstName = firstName;
	}

	public String getLastName() {
		lastName = sharedPreference.getKey("lastName");
		return lastName;
	}

	public void setLastName(String lastName) {
		if(lastName != null) this.sharedPreference.putKey("lastName", lastName);
		this.lastName = lastName;
	}

	public String getEmail() {
		email = sharedPreference.getKey("email");
		return email;
	}

	public void setEmail(String email) {
		if(email != null) this.sharedPreference.putKey("email", email);
		this.email = email;
	}

	public String getLatitude() {
		latitude = sharedPreference.getKey("latitude");
		return latitude;
	}

	public void setLatitude(String latitude) {
		if(latitude != null) this.sharedPreference.putKey("latitude", latitude);
		this.latitude = latitude;
	}

	public String getLongitude() {
		longitude = sharedPreference.getKey("longitude");
		return longitude;
	}

	public void setLongitude(String longitude) {
		if(longitude != null) this.sharedPreference.putKey("longitude", longitude);
		this.longitude = longitude;
	}

	public String getCountryCode() {
		countryCode = sharedPreference.getKey("countryCode");
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		if(countryCode != null) this.sharedPreference.putKey("countryCode", countryCode);
		this.countryCode = countryCode;
	}

	public String getMobileNumber() {
		mobileNumber = sharedPreference.getKey("mobileNumber");
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		if(mobileNumber != null) this.sharedPreference.putKey("mobileNumber", mobileNumber);
		this.mobileNumber = mobileNumber;
	}

	public String getProfilePic() {
		profilePic = sharedPreference.getKey("profilePic");
		return profilePic;
	}

	public void setProfilePic(String profilePic) {
		if(profilePic != null) this.sharedPreference.putKey("profilePic", profilePic);
		this.profilePic = profilePic;
	}

	public String getLastKnownLat() {
		lastKnownLat = sharedPreference.getKey("lastKnownLat");
		return lastKnownLat;
	}

	public void setLastKnownLat(String lastKnownLat) {
		this.sharedPreference.putKey("lastKnownLat", lastKnownLat);
		this.lastKnownLat = lastKnownLat;
	}

	public String getLastKnownLng() {
		lastKnownLng = sharedPreference.getKey("lastKnownLng");
		return lastKnownLng;
	}

	public void setLastKnownLng(String lastKnownLng) {
		this.sharedPreference.putKey("lastKnownLng", lastKnownLng);
		this.lastKnownLng = lastKnownLng;
	}

	public boolean getProfileStatus() {
		status = sharedPreference.getBoolean("status");
		return status;
	}

	public void setProfileStatus(boolean mStatus) {
		this.sharedPreference.putBoolean("status", mStatus);
		this.status = mStatus;
	}

	public String getNcount() {
		ncount = sharedPreference.getKey("ncount");
		return ncount;
	}

	public void setNcount(String nCount) {
		this.sharedPreference.putKey("ncount", nCount);
		this.ncount = nCount;
	}

	public boolean isShowSoundDialog() {
		showSoundDialog = sharedPreference.getBoolean("showSoundDialog");
		return showSoundDialog;
	}

	public void setShowSoundDialog(boolean showSoundDialog) {
		this.sharedPreference.putBoolean("showSoundDialog", showSoundDialog);
		this.showSoundDialog = showSoundDialog;
	}
}

