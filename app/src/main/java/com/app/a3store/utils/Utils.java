package com.app.a3store.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.app.a3store.BuildConfig;
import com.app.a3store.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.snackbar.Snackbar;

import java.math.BigDecimal;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

	public static void showSnack(View view, String text) {
		Snackbar.make(view, text, Snackbar.LENGTH_LONG).show();
	}

	public static String round(double d) {
		if(d % 1 == 0) {
			return String.valueOf((int) d);
		}
		BigDecimal bd = new BigDecimal(Double.toString(d));
		bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		return bd.toString();
	}

	public static void navigateToFragment(FragmentManager manager, int id, Fragment fragment) {
		FragmentTransaction fragmentTransaction = manager.beginTransaction();
		fragmentTransaction.replace(id, fragment);
		fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
		fragmentTransaction.commit();
	}

	public static String formattedValues(int value) {
		return String.format(Locale.ENGLISH, "%02d", value);
	}

	public static void logD(String TAG, String content) {
		if(BuildConfig.DEBUG) {
			Log.d(TAG, content);
		}
	}

	public static boolean isNetworkConnected(Context context) {
		boolean isConnected;
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		NetworkInfo activeWIFIInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		isConnected = activeWIFIInfo.isConnected() || activeNetInfo.isConnected();
		return isConnected;
	}

	public static void loadImage(ImageView imageview, String imageUrl) {
		if(imageUrl == null) {
			return;
		}

		Glide.with(imageview.getContext()).load(imageUrl).apply(new RequestOptions().placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher)).into(imageview);
	}

	public static boolean validName(String text) {
		String regex = "^[A-Za-z\\s]+[.]?[A-Za-z\\s]*$";
		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(text);
		return matcher.find();
	}

	public static boolean validateEmail(String text) {
		return (!TextUtils.isEmpty(text) && Patterns.EMAIL_ADDRESS.matcher(text).matches());
	}


//	public static void  openDialPad(Activity content, String mobileNum) {
//		Intent callIntent =  new Intent(Intent.ACTION_DIAL);
//		callIntent.setData( Uri.parse("tel:" + mobileNum)) ;
//		content.startActivity(callIntent);
//
//	}
}
