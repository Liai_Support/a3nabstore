package com.app.a3store.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;

import androidx.core.content.ContextCompat;

import com.app.a3store.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class CurvedBottomNavigationView extends BottomNavigationView {

	private Path mPath, mCirclePath;
	private Paint mPaint, mPaintCircle, mPaintYellow;
	private Paint mPaintStroke;
	/**
	 * the CURVE_CIRCLE_RADIUS represent the radius of the fab button
	 */
	private final int CURVE_CIRCLE_RADIUS = 128 / 2;
	// the coordinates of the first curve
	private Point mFirstCurveStartPoint = new Point();
	private Point mFirstCurveEndPoint = new Point();
	private Point mFirstCurveControlPoint1 = new Point();
	private Point mFirstCurveControlPoint2 = new Point();
	//the coordinates of the second curve
	@SuppressWarnings("FieldCanBeLocal")
	private Point mSecondCurveStartPoint = new Point();
	private Point mSecondCurveEndPoint = new Point();
	private Point mSecondCurveControlPoint1 = new Point();
	private Point mSecondCurveControlPoint2 = new Point();
	private int mNavigationBarWidth;
	private int mNavigationBarHeight;
	final RectF oval = new RectF();
	Rect barRect = new Rect();
	Rect imageRec = new Rect();

	public CurvedBottomNavigationView(Context context) {
		super(context);
		init();
	}

	public CurvedBottomNavigationView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public CurvedBottomNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}


	private void init() {
		mPath = new Path();
		mCirclePath = new Path();
		mPaint = new Paint();
		mPaintStroke = new Paint();
		mPaintCircle = new Paint();
		mPaintYellow = new Paint();
		mPaintStroke.setStyle(Paint.Style.STROKE);
		mPaintStroke.setStrokeWidth(3f);

		mPaintYellow.setStyle(Paint.Style.FILL_AND_STROKE);
		mPaintYellow.setStrokeWidth(3f);
		mPaint.setStyle(Paint.Style.FILL);
		mPaint.setColor(ContextCompat.getColor(getContext(), R.color.white));
		mPaintStroke.setColor(ContextCompat.getColor(getContext(), R.color.black_alpha_12));
		setBackgroundColor(Color.TRANSPARENT);
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		super.onLayout(changed, left, top, right, bottom);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		// get width and height of navigation bar
		// Navigation bar bounds (width & height)
		mNavigationBarWidth = getWidth();
		mNavigationBarHeight = getHeight();
		// the coordinates (x,y) of the start point before curve
		mFirstCurveStartPoint.set((mNavigationBarWidth / 2) - (CURVE_CIRCLE_RADIUS * 3 / 2) - (CURVE_CIRCLE_RADIUS / 3), 0);  //(change 3/2 to 1)
		// the coordinates (x,y) of the end point after curve
		mFirstCurveEndPoint.set(mNavigationBarWidth / 2, CURVE_CIRCLE_RADIUS + (CURVE_CIRCLE_RADIUS / 5));
		// same thing for the second curve
		mSecondCurveStartPoint = mFirstCurveEndPoint;
		//  mSecondCurveEndPoint.set((mNavigationBarWidth / 2) + (CURVE_CIRCLE_RADIUS * 2) + (CURVE_CIRCLE_RADIUS / 3), 0);
		mSecondCurveEndPoint.set((mNavigationBarWidth / 2) + (CURVE_CIRCLE_RADIUS * 2), 0);

		// the coordinates (x,y)  of the 1st control point on a cubic curve
		mFirstCurveControlPoint1.set(mFirstCurveStartPoint.x + CURVE_CIRCLE_RADIUS + (CURVE_CIRCLE_RADIUS / 4), mFirstCurveStartPoint.y);
		// the coordinates (x,y)  of the 2nd control point on a cubic curve
		mFirstCurveControlPoint2.set(mFirstCurveEndPoint.x - (CURVE_CIRCLE_RADIUS * 2) + CURVE_CIRCLE_RADIUS, mFirstCurveEndPoint.y);

		mSecondCurveControlPoint1.set(mSecondCurveStartPoint.x + (CURVE_CIRCLE_RADIUS * 2) - CURVE_CIRCLE_RADIUS, mSecondCurveStartPoint.y);
		mSecondCurveControlPoint2.set(mSecondCurveEndPoint.x - (CURVE_CIRCLE_RADIUS + (CURVE_CIRCLE_RADIUS / 4)), mSecondCurveEndPoint.y);

		mPath.reset();
		mPath.moveTo(0, 0);
		mPath.lineTo(mFirstCurveStartPoint.x, mFirstCurveStartPoint.y);


		float center_x, center_y;

		mPaintCircle.setStyle(Paint.Style.STROKE);

		center_x = mFirstCurveStartPoint.x;
		center_y = mFirstCurveStartPoint.y;
		float radius;

		if(center_x > center_y) {
			radius = center_y / 4;
		} else {
			radius = center_x / 4;
		}
		mCirclePath.moveTo(mNavigationBarWidth / 2 - CURVE_CIRCLE_RADIUS, mFirstCurveStartPoint.y);

		oval.set(mNavigationBarWidth / 2 - CURVE_CIRCLE_RADIUS * 2, mFirstCurveStartPoint.y, mNavigationBarWidth / 2 + CURVE_CIRCLE_RADIUS * 2, mFirstCurveStartPoint.y);

		mCirclePath.arcTo(oval, 180F, 90F, true);

		mPath.lineTo(mNavigationBarWidth, 0);
		mPath.lineTo(mNavigationBarWidth, mNavigationBarHeight);
		mPath.lineTo(0, mNavigationBarHeight);
		mPath.close();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.drawPath(mPath, mPaint);
		canvas.drawPath(mPath, mPaintStroke);
		canvas.drawPath(mCirclePath, mPaintYellow);
	}

	public Path canvasDrawRoundShaped(Rect bounds, Rect avatarBounds) {
		PointF handlePoint = new PointF(bounds.left + (bounds.width() * 0.25f), bounds.top);
		Path curvePath = new Path();
		curvePath.moveTo(bounds.left, bounds.top);
		//5
		curvePath.lineTo(avatarBounds.centerX(), avatarBounds.centerY());
		//6
		RectF rectF = new RectF(avatarBounds.left, avatarBounds.top, avatarBounds.right, avatarBounds.bottom);
		curvePath.arcTo(rectF, -180f, 180f, false);
		//7
		curvePath.lineTo(bounds.right, bounds.bottom);
		//8.curvePath/lineTo(bounds.topRight.x, bounds.topRight.y)
		//9
		curvePath.quadTo(handlePoint.x, handlePoint.y, bounds.left, bounds.top);
		//10
		curvePath.close();
		return curvePath;
	}
}
