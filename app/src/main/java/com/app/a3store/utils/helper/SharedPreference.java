package com.app.a3store.utils.helper;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreference {

	private SharedPreferences sharedPreferences;
	private SharedPreferences.Editor editor;
	public Context context;

	SharedPreference(Context context) {
		this.context = context;
	}

	void putKey(String Key, String Value) {
		sharedPreferences = context.getSharedPreferences("Cache", Context.MODE_PRIVATE);
		editor = sharedPreferences.edit();
		editor.putString(Key, Value);
		editor.apply();
	}

	void putBoolean(String Key, boolean Value) {
		sharedPreferences = context.getSharedPreferences("Cache", Context.MODE_PRIVATE);
		editor = sharedPreferences.edit();
		editor.putBoolean(Key, Value);
		editor.apply();
	}

	String getKey(String Key) {
		sharedPreferences = context.getSharedPreferences("Cache", Context.MODE_PRIVATE);
		return sharedPreferences.getString(Key, "");
	}

	boolean getBoolean(String Key) {
		sharedPreferences = context.getSharedPreferences("Cache", Context.MODE_PRIVATE);
		return sharedPreferences.getBoolean(Key, false);
	}
}
