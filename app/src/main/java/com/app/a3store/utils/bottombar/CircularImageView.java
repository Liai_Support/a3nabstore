package com.app.a3store.utils.bottombar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;

import androidx.appcompat.widget.AppCompatImageView;

public class CircularImageView extends AppCompatImageView {

	private int viewWidth = 0;
	private int viewHeight = 0;
	private Paint paint;

	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		this.viewWidth = this.measureWidth(widthMeasureSpec);
		this.viewHeight = this.measureHeight(heightMeasureSpec);
		this.setMeasuredDimension(this.viewWidth, this.viewHeight);
	}

	private int measureWidth(int measureSpec) {
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);
		return specMode == MeasureSpec.EXACTLY ? specSize : this.viewWidth;
	}

	private int measureHeight(int measureSpecHeight) {
		int specMode = MeasureSpec.getMode(measureSpecHeight);
		int specSize = MeasureSpec.getSize(measureSpecHeight);
		return specMode == MeasureSpec.EXACTLY ? specSize : this.viewHeight;
	}

	public CircularImageView(Context context, int defStyleAttr) {
		super(context, null, 0);
		this.paint = new Paint();
	}

	public CircularImageView(Context context) {
		this(context, 0);
		this.paint = new Paint();
	}

	@SuppressLint("DrawAllocation")
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		BitmapDrawable bitmapDrawable = (BitmapDrawable) getDrawable();
		if(bitmapDrawable != null && bitmapDrawable.getBitmap() != null) {
			Shader shader = new BitmapShader(Bitmap.createScaledBitmap(bitmapDrawable.getBitmap(), bitmapDrawable.getBitmap().getWidth(), bitmapDrawable.getBitmap().getHeight(), false), Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
			paint.setShader(shader);
			int circleCenter = viewWidth / 2;
			canvas.drawCircle((float) circleCenter, (float) circleCenter, (float) circleCenter, paint);
		}
	}
}
