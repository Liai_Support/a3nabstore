package com.app.a3store.constants;

public class OrderConstants {

	public enum OrderType {onGoing, readyToPickUp, completed}

	public enum StatusType {active, inactive}

	public enum StoreStatus {ON, OFF}
}
