package com.app.a3store.constants;

public class AppConstants {

	public static String kStoreManager = "storemanager";
	public static String kLang = "lang";
	public static String kEnglish = "en";
	public static String kAuthorization = "authorization";
	public static String kRole = "role";
	public static String kContentType = "Content-Type";
	public static String kContentTypeJSON = "application/json";
	public static String kFalse = "false";
	public static String kActive = "active";
	public static String kInactive = "inactive";
	public static String kPercentage = "percentage";
	public static String productOngoingStatusString = "ONGOING";
	public static String productCompletedStatusString = "COMPLETED";
	public static String kTotalOrders = "totalOrders";
	public static String kTotalAmount = "totalAmount";
	public static String kIntentType = "intentType";
	public static String kTransactionList = "transactionList";
	public static String kDateLabel = "mm/dd/yyyy";
	public static String kAction = "acton";
	public static String kAdminNumber = "+966598765570";
	public static String kRejected = "REJECTED";

}
