package com.app.a3store.constants;

public class IntentConstants {

	public static int code_ProfileEdit = 100;
	public static String intentProfileEdit = "editProfile";
	public static String pastEarning = "PastEarning";
	public static String pendingEarning = "PendingEarning";
	public static String lowStock = "LOWSTOCK";
	public static String outOfStock = "OUTOFSTOCK";
}
