package com.app.a3store.constants;

public class URLHelper {

	//	private static final String BASE_URL = "http://15.184.21.76/dev/";
	//	private static final String BASE_URL = "http://15.185.182.194/dev/";
//	private static final String BASE_URL = "http://65.1.122.8/api/";
	private static final String BASE_URL = "http://3.109.15.127/api/";
	private static final String BASE_URL_STORE_MANAGER = BASE_URL + "storemanager/";
	private static final String BASE_URL_USER = BASE_URL + "user/";
	public static String login = BASE_URL_STORE_MANAGER + "login";
	public static String getMyProfile = BASE_URL_STORE_MANAGER + "getMyProfile";
	public static String profileDashBoard = BASE_URL_STORE_MANAGER + "profileDashBoard";
	public static String updateProfile = BASE_URL_STORE_MANAGER + "updateProfile";
	public static String updateDeviceToken = BASE_URL_STORE_MANAGER + "updateDeviceToken";
	public static String dashboard = BASE_URL_STORE_MANAGER + "dashboard";
	public static String updateManagerCount = BASE_URL_STORE_MANAGER + "updateManagerCount";
	public static String ongoingOrderList = BASE_URL_STORE_MANAGER + "ongoingOrderList";
	public static String orderList = BASE_URL_STORE_MANAGER + "orderList";
	public static String updateOrderStatus = BASE_URL_STORE_MANAGER + "updateOrderStatus";
	public static String viewOrder = BASE_URL_STORE_MANAGER + "viewOrder";
	public static String replaceOrderItems = BASE_URL_STORE_MANAGER + "replaceOrderItems";
	public static String deleteOrderItems = BASE_URL_STORE_MANAGER + "deleteOrderItems";
	public static String homeDashboard = BASE_URL_STORE_MANAGER + "homeDashboard";
	public static String viewStockHistory = BASE_URL_STORE_MANAGER + "viewStockHistory";
	public static String getStoreVendorList = BASE_URL_STORE_MANAGER + "getStoreVendorList";
	public static String addStoreVendor = BASE_URL_STORE_MANAGER + "addStoreVendor";
	public static String updateStoreStock = BASE_URL_STORE_MANAGER + "updateStoreStock";
	public static String updateStock = BASE_URL_STORE_MANAGER + "updateStock";
	public static String getProductList = BASE_URL_STORE_MANAGER + "getProductList";
	public static String getProductCategory = BASE_URL_STORE_MANAGER + "getProductCategory";
	public static String getProductSubCategory = BASE_URL_STORE_MANAGER + "getProductSubCategory";
	public static String productSearch = BASE_URL_STORE_MANAGER + "productSearch";
	public static String newProductList = BASE_URL_STORE_MANAGER + "newProductList";
	public static String addNewProduct = BASE_URL_STORE_MANAGER + "addnewProduct";
	public static String changeStoreProduct = BASE_URL_STORE_MANAGER + "changeStoreProduct";
	public static String changeStoreCategoryProduct = BASE_URL_STORE_MANAGER + "changeStoreCategoryProduct";
	public static String searchNewProduct = BASE_URL_STORE_MANAGER + "searchNewProduct";
	public static String updateOnlineStatus = BASE_URL_STORE_MANAGER + "updateOnlineStatus";
	public static String updateStoreStatus = BASE_URL_STORE_MANAGER + "updateStoreStatus";
	public static String getReport = BASE_URL_STORE_MANAGER + "getReport";
	public static String OrderTransactionLog = BASE_URL_STORE_MANAGER + "OrderTransactionLog";
	public static String getProductOutOfStock = BASE_URL_STORE_MANAGER + "getproductOutOfStock";
	public static String managerNotification = BASE_URL_STORE_MANAGER + "managerNotification";
	public static String updateProductPackedStatus = BASE_URL_STORE_MANAGER + "updateProductPackedStatus";
	public static String verifyOTP = BASE_URL_USER + "verifyOTP";

}
