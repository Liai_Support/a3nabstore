package com.app.a3store.constants;

public class UrlConstants {

	public static String kOS = "os";
	public static String kFCMToken = "fcmToken";
	public static String kID = "id";
	//url constant value
	public static String getOS = "android";
	public static String kPageNumber = "pageNumber";
	public static String kType = "type";
	public static String Type_ReadyToPickup = "PICKUP";
	public static String Type_Completed = "COMPLETED";
	public static String kOrderId = "orderId";
	public static String kStatus = "status";
	public static String statusAccepted = "ACCEPTED";
	public static String statusRejected = "REJECTED";
	public static String statusPickup = "ONGOING";
	public static String kFromDate = "fromDate";
	public static String kToDate = "toDate";
}
