package com.app.a3store;

import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.LocaleList;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.a3store.utils.helper.SharedHelper;

import java.util.Locale;

public abstract class BaseActivity extends AppCompatActivity {

	ProgressDialog mProgressDialog;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Loading");
		mProgressDialog.setCancelable(false);
		setPhoneLanguage();
	}

	private void setPhoneLanguage() {

		Resources res = getResources();
		Configuration conf = res.getConfiguration();
		Locale locale;
		if(new SharedHelper(this).getSelectedLanguage().length() > 0) {
			locale = new Locale(new SharedHelper(this).getSelectedLanguage().toLowerCase());
		} else {
			locale = new Locale("en");
		}

		Locale.setDefault(locale);
		conf.setLocale(locale);
		getApplicationContext().createConfigurationContext(conf);

		DisplayMetrics dm = res.getDisplayMetrics();
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			conf.setLocales(new LocaleList(locale));
		} else {
			conf.locale = locale;
		}
		res.updateConfiguration(conf, dm);
	}
}
