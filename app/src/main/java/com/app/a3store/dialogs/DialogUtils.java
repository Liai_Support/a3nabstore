package com.app.a3store.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.app.a3store.R;

public class DialogUtils {

    private static Dialog dialog;

    public static void showLoader(Context context) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_progressbar_lottie);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        }
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public static void dismissLoader() {
        if (dialog != null) {
            dialog.cancel();
            dialog.hide();
        }
    }

    public static void showAlert(Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle("Alert")
                .setMessage("One Product only ordered,Can't delete the item,You can cancel the order")
                .setPositiveButton("OK", (dialog, which) -> {
                    dialog.dismiss();
                });

        builder.show();

    }
}
