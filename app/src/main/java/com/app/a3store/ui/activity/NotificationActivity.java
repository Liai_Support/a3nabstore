package com.app.a3store.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3store.BaseActivity;
import com.app.a3store.R;
import com.app.a3store.databinding.ActivityNotificationBinding;
import com.app.a3store.dialogs.DialogUtils;
import com.app.a3store.ui.paging.notificationList.NotificationPagedAdapter;
import com.app.a3store.utils.helper.SharedHelper;
import com.app.a3store.viewmodel.NotificationViewModel;

import static com.app.a3store.constants.AppConstants.kEnglish;

public class NotificationActivity extends BaseActivity {

	ActivityNotificationBinding binding;
	NotificationViewModel viewModel;
	NotificationPagedAdapter pagedAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = DataBindingUtil.setContentView(this, R.layout.activity_notification);
		viewModel = new ViewModelProvider(this).get(NotificationViewModel.class);
		initViews();
		setListeners();
		setAdapter();
		viewModel.updateNotificationView(this);
	}

	private void initViews() {
		if(new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase(kEnglish)) {
			binding.backArrow.setScaleX(1);
		} else {
			binding.backArrow.setScaleX(-1);
		}
		/*TextView TV = findViewById(R.id.text);
		Spannable wordToSpan = new SpannableString("Received New Assignment 12th May, 2020 in queue");
		wordToSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.flush_orange)), 24, 38, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
		wordToSpan.setSpan(bss, 39, 47, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
		TV.setText(wordToSpan);

		TextView TV1 = findViewById(R.id.text1);
		Spannable wordToSpan1 = new SpannableString("Submit Floating Cash 09th May, 2020 SAR 1200 Today");
		wordToSpan1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.flush_orange)), 21, 35, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		wordToSpan1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 36, 50, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		wordToSpan1.setSpan(bss, 36, 50, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
		TV1.setText(wordToSpan1);*/
	}

	private void setAdapter() {
		pagedAdapter = new NotificationPagedAdapter(this, position->{});
		binding.notificationItem.setAdapter(pagedAdapter);
		getNotificationList();
	}

	private void getNotificationList() {
		DialogUtils.showLoader(this);
		viewModel.notificationList(this, position->{
			if(position == 0) {
				binding.noNotification.setVisibility(View.VISIBLE);
				binding.notificationContent.setVisibility(View.GONE);
			} else {
				binding.noNotification.setVisibility(View.GONE);
				binding.notificationContent.setVisibility(View.VISIBLE);
			}
		}, (position, value)->{ });
		viewModel.pagedListLiveData.observe(this, notifications->{
			DialogUtils.dismissLoader();
			pagedAdapter.submitList(notifications);
		});
	}

	private void setListeners() {
		binding.backArrow.setOnClickListener(view->backPressed());
		binding.bottomDashboard.setOnClickListener(view->gotoDashboard());
		binding.bottomAssignmnet.setOnClickListener(view->gotoStore());
		binding.orders.setOnClickListener(view->gotoOrders());
	}

	private void gotoDashboard() {
		Intent intent = new Intent(NotificationActivity.this, DashboardActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
	}

	private void gotoStore() {
		Intent intent = new Intent(NotificationActivity.this, ProductAddItemsToStoreActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
	}

	private void gotoOrders() {
		Intent intent = new Intent(NotificationActivity.this, OrderListingActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
	}

	private void backPressed() {
		super.onBackPressed();
	}
}