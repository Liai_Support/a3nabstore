package com.app.a3store.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import com.app.a3store.BaseActivity;
import com.app.a3store.R;
import com.app.a3store.databinding.ActivityProductDetailWithoutPriceChangeBinding;
import com.app.a3store.dialogs.DialogUtils;
import com.app.a3store.model.product.Product;
import com.app.a3store.ui.adapter.ProductDetailAdapter;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;
import com.app.a3store.viewmodel.ProductDetailViewModel;

import static com.app.a3store.constants.AppConstants.kFalse;

public class ProductDetailWithOutPriceChange extends BaseActivity {

    ProductDetailAdapter adapter;
    ProductDetailViewModel productDetailViewModel;
    Product product;
    boolean inStock = true, specialInstruction = false;
    int currentPos;
    double productPrice;
    long storeStock;
    ActivityProductDetailWithoutPriceChangeBinding productDetailPageBinding;
    SharedHelper sharedHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        productDetailPageBinding = DataBindingUtil.setContentView(this, R.layout.activity_product_detail_without_price_change);
        productDetailViewModel = new ViewModelProvider(this).get(ProductDetailViewModel.class);
        product = (Product) getIntent().getSerializableExtra("productDetails");
        storeStock = product.getStoreStock();
        productPrice = product.getProductPrice();

        initViews();
        setAdapters();
        setListeners();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2 && data != null) {
            storeStock = data.getLongExtra("storeStock", 0);
            productDetailPageBinding.stockUnits.setText(String.format("%s %s", storeStock, this.getString(R.string.units_small)));
        }
    }

    @Override
    public void onBackPressed() {
        backPressed();
    }

    private void initViews() {
        sharedHelper = new SharedHelper(this);
        productDetailPageBinding.leftArrow.setVisibility(View.GONE);
        productDetailPageBinding.productName.setText(product.getProductName());
        productDetailPageBinding.productPrice.setText(String.format("%s %s", this.getString(R.string.sar), Utils.round(product.getProductPrice())));
        productDetailPageBinding.stockUnits.setText(String.format("%s %s", product.getStoreStock(), this.getString(R.string.units_small)));
        if (product.getProductImages().size() < 2) {
            productDetailPageBinding.leftArrow.setVisibility(View.GONE);
            productDetailPageBinding.rightArrow.setVisibility(View.GONE);
            productDetailPageBinding.viewPagerDotIndicator.setVisibility(View.GONE);
        } else {
            productDetailPageBinding.leftArrow.setVisibility(View.VISIBLE);
            productDetailPageBinding.rightArrow.setVisibility(View.VISIBLE);
            productDetailPageBinding.viewPagerDotIndicator.setVisibility(View.VISIBLE);
        }
        if (sharedHelper.getSelectedLanguage().equals("en")) {
            productDetailPageBinding.backArrow.setScaleX(1);
            productDetailPageBinding.layoutBackground.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.rounded_rect_top_right_corner_white, getTheme()));
        } else {

            productDetailPageBinding.backArrow.setScaleX(-1);
            productDetailPageBinding.layoutBackground.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.rounded_rect_top_left_corner_white, getTheme()));
        }
    }

    private void setAdapters() {
        adapter = new ProductDetailAdapter(this, product.getProductImages());
        productDetailPageBinding.productViewPager.setAdapter(adapter);
        productDetailPageBinding.viewPagerDotIndicator.setViewPager(productDetailPageBinding.productViewPager);
        productDetailPageBinding.productViewPager.addOnPageChangeListener(changeListener);
    }

    private void setListeners() {
        productDetailPageBinding.backArrow.setOnClickListener(view -> backPressed());
        productDetailPageBinding.closeIcon.setOnClickListener(view -> backPressed());
        productDetailPageBinding.bottomDashboard.setOnClickListener(view -> gotoDashboard());
        productDetailPageBinding.bottomAssignmnet.setOnClickListener(view -> gotoStore());

        productDetailPageBinding.updateProduct.setOnClickListener(v -> {

            DialogUtils.showLoader(this);
            productDetailViewModel.updateStock(this, product.getId(), productPrice + "", storeStock, inStock, specialInstruction).observe(this, commonResponse -> {
                DialogUtils.dismissLoader();
                if (commonResponse.getError().equalsIgnoreCase(kFalse)) {
                    Utils.showSnack(productDetailPageBinding.getRoot(), this.getString(R.string.stock_price_message));
                    productDetailPageBinding.productPrice.setText(String.format("%s %s", this.getString(R.string.sar), Utils.round(productPrice)));
                    finish();
                } else {
                    Utils.showSnack(productDetailPageBinding.getRoot(), commonResponse.getMessage());
                }
            });
        });

        productDetailPageBinding.update.setOnClickListener(v -> {
            Intent intent = new Intent(this, CurrentStockActivity.class);
            intent.putExtra("productId", product.getId());
            intent.putExtra("storeStock", product.getStoreStock());
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, 2);
        });


        productDetailPageBinding.stockEnableIcon.setOnClickListener(v -> {
            inStock = !inStock;
            if (inStock)
                productDetailPageBinding.stockEnableIcon.setImageResource(R.drawable.green_enable);
            else
                productDetailPageBinding.stockEnableIcon.setImageResource(R.drawable.orange_disable);
        });

        productDetailPageBinding.splInfoIcon.setOnClickListener(v -> {
            specialInstruction = !specialInstruction;
            if (specialInstruction)
                productDetailPageBinding.splInfoIcon.setImageResource(R.drawable.green_enable);
            else productDetailPageBinding.splInfoIcon.setImageResource(R.drawable.orange_disable);
        });

        productDetailPageBinding.rightArrow.setOnClickListener(view -> {
            productDetailPageBinding.productViewPager.setCurrentItem(++currentPos);
            if (productDetailPageBinding.productViewPager.getCurrentItem() == adapter.getCount() - 1) {
                productDetailPageBinding.rightArrow.setVisibility(View.GONE);
            } else {
                productDetailPageBinding.rightArrow.setVisibility(View.VISIBLE);
            }
            productDetailPageBinding.leftArrow.setVisibility(View.VISIBLE);
        });
        productDetailPageBinding.leftArrow.setOnClickListener(view -> {
            productDetailPageBinding.productViewPager.setCurrentItem(--currentPos);
            if (productDetailPageBinding.productViewPager.getCurrentItem() == 0) {
                productDetailPageBinding.leftArrow.setVisibility(View.GONE);
            } else {
                productDetailPageBinding.leftArrow.setVisibility(View.VISIBLE);
            }
            productDetailPageBinding.rightArrow.setVisibility(View.VISIBLE);
        });

        productDetailPageBinding.orders.setOnClickListener(view -> {
            Intent intent = new Intent(ProductDetailWithOutPriceChange.this, OrderListingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        });
    }

    ViewPager.OnPageChangeListener changeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            currentPos = position;
            if (currentPos != 0) {
                productDetailPageBinding.leftArrow.setVisibility(View.VISIBLE);
            } else {
                productDetailPageBinding.leftArrow.setVisibility(View.GONE);
            }

            if (currentPos != adapter.getCount() - 1) {
                productDetailPageBinding.rightArrow.setVisibility(View.VISIBLE);
            } else {
                productDetailPageBinding.rightArrow.setVisibility(View.GONE);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private void gotoDashboard() {
        Intent intent = new Intent(ProductDetailWithOutPriceChange.this, DashboardActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void gotoStore() {
        Intent intent = new Intent(ProductDetailWithOutPriceChange.this, ProductAddItemsToStoreActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void backPressed() {
        Intent intent = new Intent();
        intent.putExtra("storeStock", storeStock);
        intent.putExtra("productPrice", productPrice);
        setResult(2, intent);
        finish();
    }
}