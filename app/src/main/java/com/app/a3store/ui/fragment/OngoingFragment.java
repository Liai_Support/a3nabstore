package com.app.a3store.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3store.R;
import com.app.a3store.constants.OrderConstants;
import com.app.a3store.constants.UrlConstants;
import com.app.a3store.databinding.LayoutOrderStatusBinding;
import com.app.a3store.dialogs.DialogUtils;
import com.app.a3store.model.orders.OrderInfo;
import com.app.a3store.ui.activity.OrderDetailOngoingOrder;
import com.app.a3store.ui.activity.OrderListingActivity;
import com.app.a3store.ui.paging.ongoingorders.OrdersOngoingPagedAdapter;
import com.app.a3store.utils.Utils;
import com.app.a3store.viewmodel.OrdersViewModel;

public class OngoingFragment extends Fragment {

	OrdersViewModel viewModel;
	LayoutOrderStatusBinding binding;

	@Override
	public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);
		//loadOrders();
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = LayoutInflater.from(requireContext()).inflate(R.layout.layout_order_status, container, false);
		binding = LayoutOrderStatusBinding.bind(view);
		viewModel = new ViewModelProvider(this).get(OrdersViewModel.class);
		//initViews();
		if(((OrderListingActivity)getActivity()).currentPosition == 0){
			initViews();
		}
		return binding.getRoot();
	}

	private void initViews() {
		viewModel.ongoingAdapter = new OrdersOngoingPagedAdapter(getContext(), position->{
			OrderInfo orderInfo = viewModel.ongoingAdapter.getCurrentList().get(position);
			if(orderInfo != null) {
				DialogUtils.showLoader(getContext());
				updateOrderStatus(orderInfo, UrlConstants.statusAccepted);
			}
		}, position->{
			OrderInfo orderInfo = viewModel.ongoingAdapter.getCurrentList().get(position);
			if(orderInfo != null) {
				DialogUtils.showLoader(getContext());
				updateOrderStatus(orderInfo, UrlConstants.statusRejected);
			}
		}, position->gotoTemplateOne(viewModel.ongoingAdapter.getCurrentList().get(position).getOrderId()));

		binding.recyclerOrders.setAdapter(viewModel.ongoingAdapter);
		loadOrders();
	}

	private void updateOrderStatus(OrderInfo orderInfo, String status) {
		if(orderInfo != null) {
			viewModel.updateOrderStatus(getContext(), orderInfo.getOrderId(), status).observe(getViewLifecycleOwner(), response->{
				DialogUtils.dismissLoader();
				if(response.getError().equalsIgnoreCase("true")) {
					Utils.showSnack(binding.getRoot(), response.getMessage());
				} else {
					loadOrders();
				}
			});
		}
	}

	private void loadOrders() {
		viewModel.pagingOngoingOrders(getContext(), position1->{
			if(position1 == 0) {
				binding.txtNoOrdersFound.setVisibility(View.VISIBLE);
				binding.recyclerOrders.setVisibility(View.GONE);
			} else {
				binding.txtNoOrdersFound.setVisibility(View.GONE);
				binding.recyclerOrders.setVisibility(View.VISIBLE);
			}
			DialogUtils.dismissLoader();
		}, (position12, value)->{
		});
		viewModel.orderOngoingPagedList.observe(getViewLifecycleOwner(), items->viewModel.ongoingAdapter.submitList(items));
	}

	@Override
	public void onSaveInstanceState(@NonNull Bundle outState) {

	}

	private void gotoTemplateOne(int id) {
		Intent intent = new Intent(getContext(), OrderDetailOngoingOrder.class);
		intent.putExtra("id", id);
		intent.putExtra("orderType", OrderConstants.OrderType.onGoing);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivityForResult(intent, 108);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == 108) loadOrders();
	}
}
