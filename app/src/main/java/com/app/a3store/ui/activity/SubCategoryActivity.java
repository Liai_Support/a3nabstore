package com.app.a3store.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3store.BaseActivity;
import com.app.a3store.R;
import com.app.a3store.databinding.ActivitySubcategoryProductsBinding;
import com.app.a3store.dialogs.DialogUtils;
import com.app.a3store.model.DashboardResponse;
import com.app.a3store.ui.adapter.ListProductAdapter;
import com.app.a3store.ui.adapter.ProductCategoriesAdapter;
import com.app.a3store.ui.adapter.SubCategoryTopAdapter;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;
import com.app.a3store.viewmodel.CategoryViewModel;
import com.app.a3store.viewmodel.ProductViewModel;

import java.util.ArrayList;

import static com.app.a3store.constants.AppConstants.kEnglish;
import static com.app.a3store.constants.AppConstants.kFalse;

public class SubCategoryActivity extends BaseActivity {

    ActivitySubcategoryProductsBinding binding;
    CategoryViewModel viewModel;
    ProductViewModel productViewModel;
    int selectedCategoryId;
    int havingSubcategory = 0;
    ProductCategoriesAdapter productCategoriesAdapter;
    ListProductAdapter adapter;
    boolean fromSearch = false, setFlag = false;
    SubCategoryTopAdapter subCategoryAdapter;
    ArrayList<DashboardResponse.BestProduct> productList = new ArrayList<>();
    int categoryId, orderItemId, quantity, selectedProductId = -1, storeId = -1,selectedQuantity = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_subcategory_products);
        viewModel = new ViewModelProvider(this).get(CategoryViewModel.class);
        productViewModel = new ViewModelProvider(this).get(ProductViewModel.class);

        categoryId = getIntent().getIntExtra("categoryId", -1);
        orderItemId = getIntent().getIntExtra("orderItemId", -1);
        quantity = getIntent().getIntExtra("quantity", -1);

        viewModel.selectedCategoryId = categoryId;
        productViewModel.selectedQuantity = quantity;

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase(kEnglish)) {
            binding.back.setScaleX(1);
        } else {
            binding.back.setScaleX(-1);
        }
        getIntentTextValues();
        setAdapters();
        initListener();
    }

    private void initListener() {

        binding.bottomAssignmnet.setOnClickListener(view -> {
            Intent intent = new Intent(SubCategoryActivity.this, ProductAddItemsToStoreActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        });

        binding.orders.setOnClickListener(view -> {
            Intent intent = new Intent(SubCategoryActivity.this, OrderListingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        });

        binding.bottomDashboard.setOnClickListener(view -> {
            Intent intent = new Intent(SubCategoryActivity.this, DashboardActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        });

        binding.searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 2) {
                    setFlag = true;
                    viewModel.getSearchProductList(SubCategoryActivity.this, s.toString()).observe(SubCategoryActivity.this, productListResponse -> {
                        if (!productListResponse.isError().equalsIgnoreCase(kFalse)) {
                            Utils.showSnack(binding.getRoot(), productListResponse.getMessage());
                        } else {
                            if (productListResponse.getData().getProducts().isEmpty()) {
                                binding.noProduct.setVisibility(View.VISIBLE);
                                binding.productList.setVisibility(View.GONE);
                            } else {
                                binding.noProduct.setVisibility(View.GONE);
                                binding.productList.setVisibility(View.VISIBLE);
                                adapter = new ListProductAdapter(SubCategoryActivity.this, productListResponse.getData().getProducts(), position -> selectedProductId = productListResponse.getData().getProducts().get(position).getId(), positionDel -> {
                                },quantity);
                                binding.productList.setAdapter(adapter);
                            }
                        }
                    });
                }
                if (setFlag && s.length() < 3) {
                    setFlag = false;
                    binding.noProduct.setVisibility(View.GONE);
                    binding.productList.setVisibility(View.VISIBLE);
                    adapter = new ListProductAdapter(SubCategoryActivity.this, productList, position -> selectedProductId = productList.get(position).getId(), positionDel -> {
                    },quantity);
                    binding.productList.setAdapter(adapter);
                }
            }
        });

        binding.back.setOnClickListener(view -> onBackPressed());

        binding.replaceItem.setOnClickListener(v -> {
            if (selectedProductId != -1 && storeId != -1) {
                DialogUtils.showLoader(SubCategoryActivity.this);
                productViewModel.replaceOrderItem(SubCategoryActivity.this, orderItemId, selectedProductId, selectedQuantity, storeId).observe(SubCategoryActivity.this, commonResponse -> {
                    DialogUtils.dismissLoader();
                    if (!commonResponse.getError().equalsIgnoreCase(kFalse)) {
                        Utils.showSnack(binding.getRoot(), commonResponse.getMessage());
                    } else {
                        Intent intent = getIntent();
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                });
            } else {
                Utils.showSnack(binding.getRoot(), "Select Product");
            }
        });
    }

    private void setAdapters() {
        DialogUtils.showLoader(this);
        viewModel.getCategoryDetails(this, categoryId).observe(this, productDashboardResponse -> {
            if (productDashboardResponse.getError().equalsIgnoreCase(kFalse)) {
                productCategoriesAdapter = new ProductCategoriesAdapter(this, productDashboardResponse.getData().getCategory(), position -> {
                    viewModel.selectedCategoryId = productDashboardResponse.getData().getCategory().get(position).getId();
                    viewModel.getSubCategory(this).observe(this, productSubCategoryResponse -> {
                        if (productSubCategoryResponse.isError().equalsIgnoreCase(kFalse)) {
                            if (productSubCategoryResponse.getData().getProductCategory().isEmpty()) {
                                binding.subcategoryList.setVisibility(View.GONE);
                                viewModel.selectedSubCategoryId = 0;
                                viewModel.getProductList(this).observe(this, productListResponse -> {
                                    DialogUtils.dismissLoader();
                                    if (productListResponse.isError().equalsIgnoreCase(kFalse)) {
                                        if (productListResponse.getData().getProducts().isEmpty()) {
                                            binding.productList.setVisibility(View.GONE);
                                            binding.noProduct.setVisibility(View.VISIBLE);
                                        } else {
                                            binding.productList.setVisibility(View.VISIBLE);
                                            binding.noProduct.setVisibility(View.GONE);
                                            adapter = new ListProductAdapter(this, productListResponse.getData().getProducts(), onAddItemPosition -> {
                                                selectedProductId = productListResponse.getData().getProducts().get(onAddItemPosition).getId();
                                                storeId = productListResponse.getData().getProducts().get(onAddItemPosition).getStoreId();
                                                selectedQuantity = productListResponse.getData().getProducts().get(onAddItemPosition).getCount();
                                                Utils.logD("Products", "SelectedID " + selectedProductId + "--" + onAddItemPosition);
                                            }, onDeleteItemPosition -> {
                                            },quantity);
                                            binding.productList.setAdapter(adapter);
                                        }
                                    } else {
                                        Utils.showSnack(binding.getRoot(), productListResponse.getMessage());
                                    }
                                });
                            } else {
                                binding.subcategoryList.setVisibility(View.VISIBLE);
                                Utils.logD("SubCategory", "Size " + productSubCategoryResponse.getData().getProductCategory().size());
                                subCategoryAdapter = new SubCategoryTopAdapter(this, productSubCategoryResponse.getData().getProductCategory(), position1 -> {
                                    viewModel.selectedSubCategoryId = productSubCategoryResponse.getData().getProductCategory().get(position1).getId();
                                    viewModel.getProductList(this).observe(this, productListResponse -> {
                                        DialogUtils.dismissLoader();
                                        if (productListResponse.isError().equalsIgnoreCase(kFalse)) {
                                            if (productListResponse.getData().getProducts().isEmpty()) {
                                                binding.productList.setVisibility(View.GONE);
                                                binding.noProduct.setVisibility(View.VISIBLE);
                                            } else {
                                                binding.productList.setVisibility(View.VISIBLE);
                                                binding.noProduct.setVisibility(View.GONE);
                                                adapter = new ListProductAdapter(this, productListResponse.getData().getProducts(), onAddItemPosition -> {
                                                    selectedProductId = productListResponse.getData().getProducts().get(onAddItemPosition).getId();
                                                    storeId = productListResponse.getData().getProducts().get(onAddItemPosition).getStoreId();
                                                    selectedQuantity = productListResponse.getData().getProducts().get(onAddItemPosition).getCount();

                                                }, onDeleteItemPosition -> {
                                                },quantity);
                                                binding.productList.setAdapter(adapter);
                                            }
                                        } else {
                                            Utils.showSnack(binding.getRoot(), productListResponse.getMessage());
                                        }
                                    });
                                    //									subCategoryAdapter.notifyDataSetChanged();
                                });
                                binding.subcategoryList.setAdapter(subCategoryAdapter);
                            }
                        } else {
                            Utils.showSnack(binding.getRoot(), productSubCategoryResponse.getMessage());
                        }
                    });
                });
                binding.categoryList.setAdapter(productCategoriesAdapter);
                DialogUtils.dismissLoader();
            } else {
                Utils.showSnack(binding.getRoot(), productDashboardResponse.getMessage());
            }
        });
    }

    private void getIntentTextValues() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            selectedCategoryId = bundle.getInt("categoryId", 0);
            viewModel.selectedCategoryId = selectedCategoryId;

            fromSearch = bundle.getBoolean("fromSearch", false);

            havingSubcategory = bundle.getInt("havingSubCategory", 0);
            viewModel.havingSubCategory = havingSubcategory;
        }
    }
}
