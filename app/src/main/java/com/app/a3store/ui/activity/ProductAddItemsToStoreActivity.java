package com.app.a3store.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3store.BaseActivity;
import com.app.a3store.R;
import com.app.a3store.databinding.ActivityProductAddItemsToStoreBinding;
import com.app.a3store.dialogs.DialogUtils;
import com.app.a3store.model.ProductDashboardResponse;
import com.app.a3store.model.product.Product;
import com.app.a3store.ui.adapter.ProductCategoriesAdapter;
import com.app.a3store.ui.adapter.ProductCategoriesNameAdapter;
import com.app.a3store.ui.adapter.SearchProductsAddItemToStoreListAdapter;
import com.app.a3store.ui.paging.storeProduct.StoreProductPagedAdapter;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;
import com.app.a3store.viewmodel.ProductDashboardViewModel;

import java.util.List;

import static com.app.a3store.constants.AppConstants.kActive;
import static com.app.a3store.constants.AppConstants.kEnglish;
import static com.app.a3store.constants.AppConstants.kFalse;
import static com.app.a3store.constants.AppConstants.kInactive;

public class ProductAddItemsToStoreActivity extends BaseActivity {

    ActivityProductAddItemsToStoreBinding binding;
    ProductCategoriesAdapter productCategoriesAdapter;
    ProductCategoriesNameAdapter productCategoriesNameAdapter;
    StoreProductPagedAdapter pagedAdapter;
    SearchProductsAddItemToStoreListAdapter searchProductAdapter;
    ProductDashboardViewModel productDashboardViewModel;
    List<Product> productList;
    List<ProductDashboardResponse.Data.Category> categoryList;
    SharedHelper sharedHelper;
    int productPosition, categoryPosition;
    boolean setFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_product_add_items_to_store);
        productDashboardViewModel = new ViewModelProvider(this).get(ProductDashboardViewModel.class);
        sharedHelper = new SharedHelper(this);
        if (sharedHelper.getSelectedLanguage().equalsIgnoreCase(kEnglish)) {
            binding.backArrow.setScaleX(1);
        } else {
            binding.backArrow.setScaleX(-1);
        }
        setAdapters();
        setListeners();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //		Utils.logD("s2s", "in onActivityResult 1");
        if (requestCode == 2 && data != null && data.getExtras() != null) {
            //			Utils.logD("s2s", "in onActivityResult 2");
            pagedAdapter.getCurrentList().get(productPosition).setStoreStock(data.getLongExtra("storeStock", 0));
            pagedAdapter.notifyItemChanged(productPosition);
        } else if (requestCode == 3 && data != null && data.getExtras() != null) {
            //			Utils.logD("s2s", "in onActivityResult 3");
            productList.get(productPosition).setStoreStock(data.getLongExtra("storeStock", 0));
            productList.get(productPosition).setProductPrice(data.getDoubleExtra("productPrice", 0));
            pagedAdapter.notifyItemChanged(productPosition);
        } else if (requestCode == 17 && resultCode == RESULT_OK) {
            DialogUtils.showLoader(this);
            //			Utils.logD("s2s", "in onActivityResult 4");
            loadProducts();
            //			Utils.logD("s2s", "in onActivityResult 5");
        }
    }

    private void setAdapters() {
        setProductListAdapter();
        searchProductAdapter = new SearchProductsAddItemToStoreListAdapter(this, availablePosition -> {
            productPosition = availablePosition;
            Log.d("Testing", "Result" + searchProductAdapter.getProductList() + " " + searchProductAdapter.getProductList().get(productPosition).getStoreProductStatus());
            if (searchProductAdapter.getProductList().get(productPosition).getStoreProductStatus().equalsIgnoreCase(kActive)) {
                Utils.showSnack(binding.getRoot(), getString(R.string.available_error));
            } else {
                DialogUtils.showLoader(this);
                productDashboardViewModel.changeStoreProduct(this, searchProductAdapter.getProductList().get(productPosition).getId(), kActive).observe(this, commonResponse -> {
                    DialogUtils.dismissLoader();
                    if (commonResponse.getError().equalsIgnoreCase(kFalse)) {
                        searchProductAdapter.getProductList().get(productPosition).setStoreProductStatus(kActive);
                        searchProductAdapter.notifyItemChanged(productPosition);
                        Utils.showSnack(binding.getRoot(), getString(R.string.product_status_message));
                    } else {
                        Utils.showSnack(binding.getRoot(), commonResponse.getMessage());
                    }
                });
            }
        }, unavailablePosition -> {
            productPosition = unavailablePosition;
            if (searchProductAdapter.getProductList().get(productPosition).getStoreProductStatus().equalsIgnoreCase(kInactive)) {
                Utils.showSnack(binding.getRoot(), getString(R.string.unavailable_error));
            } else {
                DialogUtils.showLoader(this);
                productDashboardViewModel.changeStoreProduct(this, searchProductAdapter.getProductList().get(productPosition).getId(), kInactive).observe(this, commonResponse -> {
                    DialogUtils.dismissLoader();
                    if (commonResponse.getError().equalsIgnoreCase(kFalse)) {
                        searchProductAdapter.getProductList().get(productPosition).setStoreProductStatus(kInactive);
                        searchProductAdapter.notifyItemChanged(productPosition);
                        Utils.showSnack(binding.getRoot(), getString(R.string.product_status_message));
                    } else {
                        Utils.showSnack(binding.getRoot(), commonResponse.getMessage());
                    }
                });
            }
        }, editPosition -> {
            productPosition = editPosition;
            Intent intent;
            if (searchProductAdapter.getProductList().get(productPosition).getManagerPrice() == null || searchProductAdapter.getProductList().get(productPosition).getManagerPrice().equalsIgnoreCase(kFalse)) {
                intent = new Intent(this, ProductDetailWithOutPriceChange.class);
            } else {
                intent = new Intent(this, ProductDetailPage.class);
            }
            intent.putExtra("productDetails", searchProductAdapter.getProductList().get(productPosition));
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, 3);
        }, updatePosition -> {
            productPosition = updatePosition;
            Intent intent = new Intent(this, CurrentStockActivity.class);
            intent.putExtra("productId", searchProductAdapter.getProductList().get(productPosition).getId());
            intent.putExtra("storeStock", searchProductAdapter.getProductList().get(productPosition).getStoreStock());
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, 2);
        });

        //DialogUtils.showLoader(this);
        productDashboardViewModel.getCategoryDetails(this).observe(this, productDashboardResponse -> {
            if (productDashboardResponse.getError().equalsIgnoreCase(kFalse)) {
                categoryList = productDashboardResponse.getData().getCategory();
                binding.categoryGroup.setVisibility(View.VISIBLE);
                productCategoriesAdapter = new ProductCategoriesAdapter(this, categoryList, position -> {
                    categoryPosition = position;
                    if (sharedHelper.getSelectedLanguage().equalsIgnoreCase(kEnglish)) {
                        binding.categoryLabel.setText(categoryList.get(categoryPosition).getCategoryName());
                    } else {
                        binding.categoryLabel.setText(categoryList.get(categoryPosition).getArabicName());
                    }
                    setCategoryIconStatus();
                    productDashboardViewModel.selectedCategoryId = categoryList.get(categoryPosition).getId();
                    productDashboardViewModel.getSubCategoryDetails(this).observe(this, productSubCategoryResponse -> {
                        if (productSubCategoryResponse.getError().equalsIgnoreCase(kFalse)) {
                            if (productSubCategoryResponse.getData().getProductCategory().isEmpty()) {
                                binding.recyclerCategoriesName.setVisibility(View.GONE);
                                productDashboardViewModel.selectedSubCategoryId = 0;
                                loadProducts();
                            } else {
                                binding.recyclerCategoriesName.setVisibility(View.VISIBLE);
                                productCategoriesNameAdapter = new ProductCategoriesNameAdapter(this, productSubCategoryResponse.getData().getProductCategory(), position1 -> {
                                    productDashboardViewModel.selectedSubCategoryId = productSubCategoryResponse.getData().getProductCategory().get(position1).getId();
                                    loadProducts();
                                });
                                binding.recyclerCategoriesName.setAdapter(productCategoriesNameAdapter);
                            }
                        } else {
                            Utils.showSnack(binding.getRoot(), productSubCategoryResponse.getMessage());
                        }
                    });
                });
                binding.recyclerCategories.setAdapter(productCategoriesAdapter);
            } else {
                binding.categoryGroup.setVisibility(View.GONE);
                Utils.showSnack(binding.getRoot(), productDashboardResponse.getMessage());
            }
        });
    }

    private void setListeners() {
        binding.searchProducts.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(binding.searchProducts, InputMethodManager.SHOW_IMPLICIT);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 2) {
                    setFlag = true;
                    //					ObjectAnimator animation = ObjectAnimator.ofInt(binding.progressBar, "progress", 0, 500); // see this max value coming back here, we animate towards that value
                    //					animation.setDuration(5000); // in milliseconds
                    //					animation.setInterpolator(new DecelerateInterpolator());
                    //					animation.start();

                    binding.progressBar.setVisibility(View.VISIBLE);
                    //					binding.productLayout.setVisibility(View.GONE);
                    searchProducts(s.toString());
                } else if (setFlag && s.length() == 0) {
                    setFlag = false;
                    //					binding.noProduct.setVisibility(View.GONE);
                    //					binding.recyclerProducts.setVisibility(View.VISIBLE);
                    //					DialogUtils.showLoader(ProductAddItemsToStoreActivity.this);
                    loadProducts();
                }
            }
        });
        binding.categoryEnableIcon.setOnClickListener(v -> {
            String storeProductStatus;
            if (categoryList.get(categoryPosition).getCategoryStatus().equalsIgnoreCase(kActive)) {
                storeProductStatus = kInactive;
            } else {
                storeProductStatus = kActive;
            }
            DialogUtils.showLoader(this);
            productDashboardViewModel.changeStoreCategoryProduct(this, productDashboardViewModel.selectedCategoryId, storeProductStatus).observe(this, commonResponse -> {
                DialogUtils.dismissLoader();
                if (commonResponse.getError().equalsIgnoreCase(kFalse)) {
                    categoryList.get(categoryPosition).setCategoryStatus(storeProductStatus);
                    setCategoryIconStatus();
                } else {
                    Utils.showSnack(binding.getRoot(), commonResponse.getMessage());
                }
            });
        });
        binding.viewReplaceItem.setOnClickListener(view -> {
            Intent intent = new Intent(ProductAddItemsToStoreActivity.this, ProductUnavailableItemActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, 17);
        });

        binding.bottomAssignmnet.setOnClickListener(view -> {

        });
        binding.backArrow.setOnClickListener(view -> backPressed());
        binding.bottomDashboard.setOnClickListener(view -> gotoDashboard());
        binding.orders.setOnClickListener(view -> {
            Intent intent = new Intent(ProductAddItemsToStoreActivity.this, OrderListingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        });
    }

    private void searchProducts(String text) {
        productDashboardViewModel.getSearchProductList(ProductAddItemsToStoreActivity.this, text).observe(ProductAddItemsToStoreActivity.this, productListResponse -> {
            binding.progressBar.clearAnimation();
            binding.progressBar.setVisibility(View.GONE);
            binding.productLayout.setVisibility(View.VISIBLE);

            if (!productListResponse.getError().equalsIgnoreCase(kFalse)) {
                Utils.showSnack(binding.getRoot(), productListResponse.getMessage());
            } else {
                if (productListResponse.getData().getProducts().isEmpty()) {
                    binding.noProduct.setVisibility(View.VISIBLE);
                    binding.recyclerProducts.setVisibility(View.GONE);
                } else {
                    productList = productListResponse.getData().getProducts();
                    binding.noProduct.setVisibility(View.GONE);
                    binding.recyclerProducts.setVisibility(View.VISIBLE);
                    binding.recyclerProducts.setAdapter(searchProductAdapter);
                    searchProductAdapter.setProductList(productList);

                    //DialogUtils.showLoader(ProductAddItemsToStoreActivity.this);
                    //					setProductListAdapter();
                }
            }
        });
    }

    private void loadProducts() {
        //		Utils.logD("s2s", "in loadProducts 1");
        binding.recyclerProducts.setAdapter(pagedAdapter);
        //		Utils.logD("s2s", "in loadProducts 2");
        productDashboardViewModel.getProductOutOfStock(this, position -> {
            if (position == 0) {
                binding.noProduct.setVisibility(View.VISIBLE);
                binding.recyclerProducts.setVisibility(View.GONE);
            } else {
                binding.noProduct.setVisibility(View.GONE);
                binding.recyclerProducts.setVisibility(View.VISIBLE);
            }
        }, (i, value) -> {

        });

        productDashboardViewModel.storeProductPagedList.observe(this, products -> {
            //			Utils.logD("s2s", "in loadProducts 4");
            DialogUtils.dismissLoader();
            pagedAdapter.submitList(products);
            productList = pagedAdapter.getCurrentList();
            //			Utils.logD("s2s", "in loadProducts 5");
        });
    }

    private void setProductListAdapter() {
        pagedAdapter = new StoreProductPagedAdapter(this, availablePosition -> {
            productPosition = availablePosition;
            productList = pagedAdapter.getCurrentList();
            if (productList.get(productPosition).getStoreProductStatus().equalsIgnoreCase(kActive)) {
                Utils.showSnack(binding.getRoot(), getString(R.string.available_error));
            } else {
                DialogUtils.showLoader(this);
                productDashboardViewModel.changeStoreProduct(this, productList.get(productPosition).getId(), kActive).observe(this, commonResponse -> {
                    DialogUtils.dismissLoader();
                    if (commonResponse.getError().equalsIgnoreCase(kFalse)) {
                        productList.get(productPosition).setStoreProductStatus(kActive);
                        pagedAdapter.notifyItemChanged(productPosition);
                        Utils.showSnack(binding.getRoot(), getString(R.string.product_status_message));
                    } else {
                        Utils.showSnack(binding.getRoot(), commonResponse.getMessage());
                    }
                });
            }
        }, unavailablePosition -> {
            productPosition = unavailablePosition;
            productList = pagedAdapter.getCurrentList();
            if (productList.get(productPosition).getStoreProductStatus().equalsIgnoreCase(kInactive)) {
                Utils.showSnack(binding.getRoot(), getString(R.string.unavailable_error));
            } else {
                DialogUtils.showLoader(this);
                productDashboardViewModel.changeStoreProduct(this, productList.get(productPosition).getId(), kInactive).observe(this, commonResponse -> {
                    DialogUtils.dismissLoader();
                    if (commonResponse.getError().equalsIgnoreCase(kFalse)) {
                        productList.get(productPosition).setStoreProductStatus(kInactive);
                        pagedAdapter.notifyItemChanged(productPosition);
                        Utils.showSnack(binding.getRoot(), getString(R.string.product_status_message));
                    } else {
                        Utils.showSnack(binding.getRoot(), commonResponse.getMessage());
                    }
                });
            }
        }, editPosition -> {
            productPosition = editPosition;
            productList = pagedAdapter.getCurrentList();
            Intent intent;
            if (pagedAdapter.getCurrentList().get(productPosition).getManagerPrice().equalsIgnoreCase(kFalse)) {
                intent = new Intent(this, ProductDetailWithOutPriceChange.class);
            } else {
                intent = new Intent(this, ProductDetailPage.class);
            }
            intent.putExtra("productDetails", pagedAdapter.getCurrentList().get(productPosition));
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, 3);
        }, updatePosition -> {
            productPosition = updatePosition;
            Intent intent = new Intent(this, CurrentStockActivity.class);
            intent.putExtra("productId", pagedAdapter.getCurrentList().get(productPosition).getId());
            intent.putExtra("storeStock", pagedAdapter.getCurrentList().get(productPosition).getStoreStock());
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, 2);
        });
        binding.recyclerProducts.setAdapter(pagedAdapter);
        DialogUtils.showLoader(this);

        productDashboardViewModel.getProductsList(this).observe(this, productListResponse -> {
            DialogUtils.dismissLoader();
            if (productListResponse.getError().equalsIgnoreCase(kFalse)) {
                if (productListResponse.getData().getProducts().isEmpty()) {
                    binding.recyclerProducts.setVisibility(View.GONE);
                    binding.noProduct.setVisibility(View.VISIBLE);
                } else {
                    binding.recyclerProducts.setVisibility(View.VISIBLE);
                    binding.noProduct.setVisibility(View.GONE);
                    //productList = productListResponse.getData().getProducts();
                    setProductListAdapter();
                }
            } else {
                Utils.showSnack(binding.getRoot(), productListResponse.getMessage());
            }
        });
    }

    private void setCategoryIconStatus() {
        if (categoryList.get(categoryPosition).getCategoryStatus().equalsIgnoreCase(kActive)) {
            binding.categoryEnableIcon.setImageResource(R.drawable.green_enable);
        } else {
            binding.categoryEnableIcon.setImageResource(R.drawable.orange_disable);
        }
    }

    private void gotoDashboard() {
        Intent intent = new Intent(ProductAddItemsToStoreActivity.this, DashboardActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void backPressed() {
        super.onBackPressed();
    }
}