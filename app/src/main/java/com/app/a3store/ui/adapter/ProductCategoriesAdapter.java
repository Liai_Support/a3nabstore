package com.app.a3store.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.constants.AppConstants;
import com.app.a3store.databinding.RowCategoryInfoBinding;
import com.app.a3store.model.ProductDashboardResponse;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;

import java.util.List;

public class ProductCategoriesAdapter extends RecyclerView.Adapter<ProductCategoriesAdapter.MyViewHolder> {

	private final Context mContext;
	private final List<ProductDashboardResponse.Data.Category> categoryList;
	ItemClickListener onItemClick;
	int previousCategoryPosition = 0, selectedCategoryPosition = 0;

	public ProductCategoriesAdapter(Context context, List<ProductDashboardResponse.Data.Category> categoryList, ItemClickListener onItemClick) {
		this.mContext = context;
		this.categoryList = categoryList;
		this.onItemClick = onItemClick;
	}

	static class MyViewHolder extends RecyclerView.ViewHolder {

		RowCategoryInfoBinding binding;

		public MyViewHolder(@NonNull View itemView) {
			super(itemView);
			binding = RowCategoryInfoBinding.bind(itemView);
		}
	}

	@NonNull
	@Override
	public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
		View listItem = layoutInflater.inflate(R.layout.row_category_info, parent, false);
		return new MyViewHolder(listItem);
	}

	@Override
	public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
		if(holder.getAdapterPosition() == selectedCategoryPosition) {
			holder.binding.isSelected.setVisibility(View.VISIBLE);
			holder.binding.categoryImage.animate().alpha(1f).setDuration(2000);
			holder.binding.categoryName.animate().alpha(1f).setDuration(2000);
			onItemClick.onItemClick(holder.getAdapterPosition());
		} else {
			holder.binding.isSelected.setVisibility(View.INVISIBLE);
			holder.binding.categoryImage.animate().alpha(0.4f).setDuration(300);
			holder.binding.categoryName.animate().alpha(0.4f).setDuration(300);
		}
		holder.binding.root.setOnClickListener(v->{
			selectedCategoryPosition = holder.getAdapterPosition();
			notifyItemChanged(selectedCategoryPosition);
			notifyItemChanged(previousCategoryPosition);
			previousCategoryPosition = selectedCategoryPosition;
		});
		ProductDashboardResponse.Data.Category category = categoryList.get(holder.getAdapterPosition());
		Utils.loadImage(holder.binding.categoryImage, category.getCategoryImage());
		if(new SharedHelper(mContext).getSelectedLanguage().equalsIgnoreCase(AppConstants.kEnglish)) {
			holder.binding.categoryName.setText(category.getCategoryName());
		} else {
			holder.binding.categoryName.setText(category.getArabicName());
		}
	}

	@Override
	public int getItemCount() {
		return categoryList.size();
	}
}
