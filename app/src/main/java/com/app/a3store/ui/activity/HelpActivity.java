package com.app.a3store.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.app.a3store.BaseActivity;
import com.app.a3store.R;
import com.app.a3store.constants.AppConstants;
import com.app.a3store.databinding.ActivityHelpBinding;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;

import static com.app.a3store.constants.AppConstants.kEnglish;

public class HelpActivity extends BaseActivity {

	ActivityHelpBinding helpBinding;
	String mobileNumber = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		helpBinding = DataBindingUtil.setContentView(this, R.layout.activity_help);
		setListeners();
		mobileNumber = getIntent().getStringExtra("mobileNumber");
	}

	private void setListeners() {
		if(new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase(kEnglish)) {
			helpBinding.backArrow.setScaleX(1);
		} else {
			helpBinding.backArrow.setScaleX(-1);
		}
		helpBinding.backArrow.setOnClickListener(view->backPressed());

		helpBinding.call.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent callIntent =  new Intent(Intent.ACTION_DIAL);
				callIntent.setData( Uri.parse("tel:" + mobileNumber)) ;
				startActivity(callIntent);

			}
		});
	}

	private void backPressed() {
		super.onBackPressed();
	}
}