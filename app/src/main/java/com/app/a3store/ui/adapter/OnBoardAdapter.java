package com.app.a3store.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.app.a3store.R;
import com.app.a3store.model.onboard.OnBoardItem;

import java.util.ArrayList;

public class OnBoardAdapter extends PagerAdapter {

	private final Context mContext;
	ArrayList<OnBoardItem> onBoardItems;

	public OnBoardAdapter(Context mContext, ArrayList<OnBoardItem> items) {
		this.mContext = mContext;
		this.onBoardItems = items;
	}

	@Override
	public int getCount() {
		return onBoardItems.size();
	}

	@Override
	public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
		return view == object;
	}

	@NonNull
	@Override
	public Object instantiateItem(@NonNull ViewGroup container, int position) {
		View itemView = LayoutInflater.from(mContext).inflate(R.layout.slides_layout, container, false);

		OnBoardItem item = onBoardItems.get(position);

		ImageView imageView = itemView.findViewById(R.id.sliderImage);
		imageView.setImageResource(item.getImageID());

		TextView tv_title = itemView.findViewById(R.id.title);
		tv_title.setText(item.getTitle());

		TextView tv_content = itemView.findViewById(R.id.titleDesc);
		tv_content.setText(item.getDescription());

		container.addView(itemView);

		return itemView;
	}

	@Override
	public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
	}
}
