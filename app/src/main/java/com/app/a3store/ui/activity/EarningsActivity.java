package com.app.a3store.ui.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3store.BaseActivity;
import com.app.a3store.R;
import com.app.a3store.constants.IntentConstants;
import com.app.a3store.databinding.ActivityEarningsBinding;
import com.app.a3store.databinding.DialogImagePreviewBinding;
import com.app.a3store.dialogs.DialogUtils;
import com.app.a3store.model.EarningResponse;
import com.app.a3store.ui.adapter.EarningPastAdapter;
import com.app.a3store.ui.adapter.EarningPendingAdapter;
import com.app.a3store.utils.CustomBarChartRenderer;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;
import com.app.a3store.viewmodel.EarningViewModel;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.app.a3store.constants.AppConstants.kFalse;
import static com.app.a3store.constants.AppConstants.kIntentType;
import static com.app.a3store.constants.AppConstants.kTotalAmount;
import static com.app.a3store.constants.AppConstants.kTotalOrders;
import static com.app.a3store.constants.AppConstants.kTransactionList;

public class EarningsActivity extends BaseActivity {

	ActivityEarningsBinding binding;
	EarningPendingAdapter pendingEarningsAdapter;
	EarningPastAdapter pastEarningAdapter;
	EarningViewModel viewModel;
	SharedHelper sharedHelper;
	private Dialog dialog;
	List<EarningResponse.Data.Past> pastList;
	List<EarningResponse.Data.Pending> pendingList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = DataBindingUtil.setContentView(this, R.layout.activity_earnings);
		viewModel = new ViewModelProvider(this).get(EarningViewModel.class);
		binding.setLifecycleOwner(this);
		sharedHelper = new SharedHelper(this);

		setAdapters();
		initChart();
		setListeners();
		getEarnings();
	}

	private void setListeners() {
		binding.viewTransactionLog.setOnClickListener(view->openTransactionLog());
		binding.bottomDashboard.setOnClickListener(view->gotoDashboard());
		binding.bottomAssignmnet.setOnClickListener(view->gotoStore());
		binding.orders.setOnClickListener(view->{
			Intent intent = new Intent(EarningsActivity.this, OrderListingActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(intent);
		});
		binding.imgNotification.setOnClickListener(view->{
			Intent intent = new Intent(this, NotificationActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(intent);
		});
		binding.pendingEarnings.findViewById(R.id.seeAll).setOnClickListener(v->openPendingTransaction());
		binding.pastEarnings.findViewById(R.id.seeAll).setOnClickListener(v->openPastTransaction());
	}

	private void setAdapters() {
		((TextView) binding.pendingEarnings.findViewById(R.id.title)).setText(getString(R.string.pending_earnings));
		((TextView) binding.pastEarnings.findViewById(R.id.title)).setText(getString(R.string.past_earnings));
	}

	private void initChart() {

		//Customizing Bar Graph
		CustomBarChartRenderer chartRenderer = new CustomBarChartRenderer(binding.barChart, binding.barChart.getAnimator(), binding.barChart.getViewPortHandler());
		chartRenderer.mRadius = 15;
		binding.barChart.setRenderer(chartRenderer);
		binding.barChart.setPinchZoom(false);

		ArrayList<BarEntry> barOrderList = new ArrayList<>();

		for(int i = 1; i <= 5; i++) {
			BarEntry barEntry = new BarEntry((float) i, (float) i);
			barOrderList.add(barEntry);
		}

		Legend l = binding.barChart.getLegend();
		l.setEnabled(false);

		YAxis leftAxis = binding.barChart.getAxisLeft();
		leftAxis.setDrawGridLines(false);
		leftAxis.setTextColor(getResources().getColor(R.color.nobel));
		leftAxis.setDrawZeroLine(true);

		binding.barChart.getAxisRight().setDrawZeroLine(true);
		binding.barChart.getXAxis().setDrawGridLines(false);
		binding.barChart.getAxisRight().setDrawLabels(false);
		binding.barChart.getAxisLeft().setDrawGridLines(false);

		binding.barChart.setDrawGridBackground(false);
		binding.barChart.setDrawValueAboveBar(false);
		binding.barChart.setPinchZoom(false);
		binding.barChart.setDrawBorders(false);
		binding.barChart.animateY(2000);

		BarDataSet set1;

		float barWidth = 0.2f;

		if(binding.barChart.getData() != null && binding.barChart.getData().getDataSetCount() > 0) {
			set1 = (BarDataSet) binding.barChart.getData().getDataSetByIndex(0);

			set1.setValues(barOrderList);
			binding.barChart.getData().notifyDataChanged();
		} else {
			// create 2 DataSets
			set1 = new BarDataSet(barOrderList, "");
			set1.setColor(getResources().getColor(R.color.colorPrimary));
			set1.setDrawValues(false);

			BarData data = new BarData(set1);
			binding.barChart.setData(data);
		}

		// specify the width each bar should have
		binding.barChart.getBarData().setBarWidth(barWidth);

		binding.barChart.setDrawValueAboveBar(false);
		binding.barChart.getDescription().setEnabled(false);

		binding.barChart.getXAxis().setDrawGridLines(false);
		binding.barChart.getXAxis().setDrawAxisLine(false);

		binding.barChart.getAxisLeft().setDrawAxisLine(false);
		binding.barChart.getAxisLeft().setDrawGridLines(false);

		binding.barChart.getAxisRight().setDrawAxisLine(false);
		binding.barChart.getAxisRight().setDrawGridLines(false);

		binding.barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
		binding.barChart.getXAxis().setTextColor(getResources().getColor(R.color.nobel));

		binding.barChart.getXAxis().setCenterAxisLabels(false);

		binding.barChart.getXAxis().setAvoidFirstLastClipping(false);

		binding.barChart.setDoubleTapToZoomEnabled(false);

		binding.barChart.setTouchEnabled(false);
		binding.barChart.setHighlightFullBarEnabled(false);

		binding.barChart.getDescription().setEnabled(false);
		binding.barChart.setPadding(2, 2, 0, 0);
		binding.barChart.setScaleEnabled(false);
		binding.barChart.getAxisRight().setDrawZeroLine(true);
		binding.barChart.getXAxis().setDrawLimitLinesBehindData(true);

		binding.barChart.getXAxis().setDrawGridLines(false);
		LimitLine limitLine = new LimitLine(0f);
		limitLine.setLineColor(getResources().getColor(R.color.nobel));

		binding.barChart.getXAxis().addLimitLine(limitLine);
		binding.barChart.getXAxis().setGranularity(1f);
		binding.barChart.getXAxis().setAxisMinimum(0f);
		binding.barChart.getXAxis().setAxisMaximum(6f);

		binding.barChart.getAxisRight().setLabelCount(6, true);
		binding.barChart.getAxisLeft().setLabelCount(6, true);

		binding.barChart.getAxisLeft().setGranularity(1f);
		binding.barChart.getAxisLeft().setAxisMinimum(0f);
		binding.barChart.getAxisRight().setGranularity(1f);
		binding.barChart.getAxisRight().setAxisMinimum(0f);

		binding.barChart.setDrawBorders(false);
		binding.barChart.setBorderColor(getResources().getColor(R.color.nobel));
		binding.barChart.setFitBars(true);
		binding.barChart.invalidate();
	}

	private void getEarnings() {
		DialogUtils.showLoader(this);
		viewModel.getReport(this).observe(this, earningResponse->{
			viewModel.getMyProfile(this).observe(this, profileResponse->{
				DialogUtils.dismissLoader();
				if(profileResponse.getError().equalsIgnoreCase(kFalse)) {
					binding.earnedValue.setText(profileResponse.getData().getTotalEarn());
					binding.orderValue.setText(String.valueOf(profileResponse.getData().getTotalOrders()));
					binding.earnedValue.setText(new SpannableStringBuilder(getString(R.string.priceUnit) + " " + profileResponse.getData().getTotalEarn()));
					binding.orderValue.setText(new SpannableStringBuilder(getResources().getQuantityString(R.plurals.numberOfOrders, profileResponse.getData().getTotalOrders(), profileResponse.getData().getTotalOrders())));
				} else {
					Utils.showSnack(binding.getRoot(), profileResponse.getMessage());
				}
			});

			if(earningResponse.getError().equalsIgnoreCase(kFalse)) {
				if(earningResponse.getData().getPast().isEmpty()) {
					binding.pastEarnings.setVisibility(View.GONE);
				} else {
					pastList = earningResponse.getData().getPast();
					pastEarningAdapter = new EarningPastAdapter(this, pastList, Math.min(5, pastList.size()), viewReceiptPosition->{
						if(pastList.get(viewReceiptPosition).getImageUrl().isEmpty()) {
							Utils.showSnack(binding.getRoot(), getString(R.string.view_receipt_empty));
						} else {
							showDialog(this, pastList.get(viewReceiptPosition).getImageUrl());
						}
					});
					binding.pastEarningsList.setAdapter(pastEarningAdapter);
				}
				if(earningResponse.getData().getPending().isEmpty()) {
					binding.pendingEarnings.setVisibility(View.GONE);
				} else {
					pendingList = earningResponse.getData().getPending();
					pendingEarningsAdapter = new EarningPendingAdapter(this, pendingList, Math.min(5, pendingList.size()));
					binding.pendingEarningsList.setAdapter(pendingEarningsAdapter);
				}
			} else {
				Utils.showSnack(binding.getRoot(), earningResponse.getMessage());
			}
		});
	}

	private void showDialog(Context context, String imageUrl) {
		dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		DialogImagePreviewBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_image_preview, null, false);
		dialog.setContentView(binding.getRoot());
		if(dialog.getWindow() != null) {
			dialog.getWindow().setGravity(Gravity.CENTER);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
			ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
			InsetDrawable inset = new InsetDrawable(back, 20);
			dialog.getWindow().setBackgroundDrawable(inset);
		}
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
		binding.closeIcon.setOnClickListener(v->dialogClose());
		Utils.loadImage(binding.previewImage, imageUrl);
	}

	private void dialogClose() {
		if(dialog != null) {
			dialog.cancel();
			dialog.hide();
		}
	}

	private void openTransactionLog() {
		Intent intent = new Intent(EarningsActivity.this, OrderTransactionLogActivity.class);
		intent.putExtra(kTotalOrders, binding.orderValue.getText().toString());
		intent.putExtra(kTotalAmount, binding.earnedValue.getText().toString());
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
	}

	private void openPendingTransaction() {
		Intent intent = new Intent(EarningsActivity.this, PendingPastEarningsActivity.class);
		intent.putExtra(kIntentType, IntentConstants.pendingEarning);
		intent.putExtra(kTransactionList, (Serializable) pendingList);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
	}

	private void openPastTransaction() {
		Intent intent = new Intent(EarningsActivity.this, PendingPastEarningsActivity.class);
		intent.putExtra(kIntentType, IntentConstants.pastEarning);
		intent.putExtra(kTransactionList, (Serializable) pastList);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
	}

	private void gotoDashboard() {
		Intent intent = new Intent(EarningsActivity.this, DashboardActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}

	private void gotoStore() {
		Intent intent = new Intent(EarningsActivity.this, ProductAddItemsToStoreActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(!sharedHelper.getFirstName().isEmpty() || !sharedHelper.getLastName().isEmpty()) {
			binding.name.setText(String.format("Hi, %s %s", sharedHelper.getFirstName(), sharedHelper.getLastName()));
		}
	}
}