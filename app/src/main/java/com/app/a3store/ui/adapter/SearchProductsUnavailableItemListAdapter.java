package com.app.a3store.ui.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.databinding.RowAddProductToStoreBinding;
import com.app.a3store.databinding.RowProductInfoBinding;
import com.app.a3store.model.product.Product;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.ui.paging.newProductList.NewProductListPagedAdapter;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.app.a3store.constants.AppConstants.kActive;
import static com.app.a3store.constants.AppConstants.kPercentage;

public class SearchProductsUnavailableItemListAdapter extends RecyclerView.Adapter<SearchProductsUnavailableItemListAdapter.ItemViewHolder> {

	private final Context mContext;
	ItemClickListener itemClickListener;
	int previousPosition = -1, currentPosition = -1;
	private List<Product> productList = new ArrayList<Product>();
	public SearchProductsUnavailableItemListAdapter(Context context, ItemClickListener itemClickListener) {
		this.mContext = context;
		this.itemClickListener = itemClickListener;
	}

	public void setProductList(List<Product> mProductList){
		this.productList.clear();
		this.productList = mProductList;
		notifyDataSetChanged();
	}

	public List<Product> getProductList(){
		return  this.productList;
	}
	public static class ItemViewHolder extends RecyclerView.ViewHolder {
		RowProductInfoBinding binding;

		public ItemViewHolder(View itemView) {
			super(itemView);
			binding = RowProductInfoBinding.bind(itemView);
		}
	}

	@NonNull
	@Override
	public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
		View listItem = layoutInflater.inflate(R.layout.row_product_info, parent, false);
		return new SearchProductsUnavailableItemListAdapter.ItemViewHolder(listItem);
	}

	@Override
	public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
		Product product = productList.get(holder.getAdapterPosition());
		Utils.loadImage(holder.binding.productImage, product.getProductImages().get(0).getProductImage());

		if(new SharedHelper(mContext).getSelectedLanguage().equalsIgnoreCase("ar")){
			holder.binding.productName.setText(product.getArabicName());
		}else {
			holder.binding.productName.setText(product.getProductName());
		}

		holder.binding.productWeight.setText(String.format(Locale.ENGLISH, "%s", product.getProductWeight()));

		if(product.getProductDiscountStatus().equalsIgnoreCase(kPercentage)) {
			holder.binding.discount.setText(String.format(Locale.ENGLISH, "%% %d", product.getProductDiscount()));
			holder.binding.oldPrice.setText(String.format(Locale.ENGLISH, "%s %s", Utils.round(product.getProductPrice()),mContext.getString(R.string.sar)));
			holder.binding.oldPrice.setPaintFlags(holder.binding.oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
			double price = product.getProductPrice() - ((product.getProductPrice() * product.getProductDiscount()) / 100);
			holder.binding.productPrice.setText(String.format(Locale.ENGLISH, "%s %s", Utils.round(price),mContext.getString(R.string.sar)));
		}
		if(product.getStoreProductStatus().equalsIgnoreCase(kActive)) {
			holder.binding.productStatus.setImageResource(R.drawable.green_enable_long);
		} else {
			holder.binding.productStatus.setImageResource(R.drawable.orange_disable_long);
		}
		if(currentPosition == position) {
			holder.binding.viewDisable.setVisibility(View.VISIBLE);
			holder.binding.check.setVisibility(View.VISIBLE);
		} else {
			holder.binding.viewDisable.setVisibility(View.GONE);
			holder.binding.check.setVisibility(View.INVISIBLE);
		}

		holder.itemView.setOnClickListener(v->{
			if(currentPosition == holder.getAdapterPosition()) {
				itemClickListener.onItemClick(-1);
				currentPosition = -1;
				previousPosition = -1;
				notifyItemChanged(holder.getAdapterPosition());
			} else {
				itemClickListener.onItemClick(holder.getAdapterPosition());
				previousPosition = currentPosition;
				currentPosition = holder.getAdapterPosition();
				if(previousPosition != -1) notifyItemChanged(previousPosition);
				notifyItemChanged(currentPosition);
			}
		});
	}

	@Override
	public int getItemCount() {
		if(productList== null || productList.isEmpty())
			return 0;
		return productList.size();
	}
}
