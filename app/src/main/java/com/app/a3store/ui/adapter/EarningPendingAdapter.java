package com.app.a3store.ui.adapter;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.databinding.RowEarningPendingInfoBinding;
import com.app.a3store.model.EarningResponse;

import java.text.DateFormatSymbols;
import java.util.List;

public class EarningPendingAdapter extends RecyclerView.Adapter<EarningPendingAdapter.ViewHolder> {

	private final Context mContext;
	private final List<EarningResponse.Data.Pending> pendingList;
	private final int size;

	public EarningPendingAdapter(Context context, List<EarningResponse.Data.Pending> pendingList,int size) {
		this.mContext = context;
		this.pendingList = pendingList;
		this.size = size;
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {

		RowEarningPendingInfoBinding binding;

		public ViewHolder(View itemView) {
			super(itemView);
			binding = RowEarningPendingInfoBinding.bind(itemView);
		}
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
		View listItem = layoutInflater.inflate(R.layout.row_earning_pending_info, parent, false);
		return new ViewHolder(listItem);
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
		EarningResponse.Data.Pending pending = pendingList.get(position);
		String Date = new DateFormatSymbols().getShortMonths()[pending.getMonth() - 1].toUpperCase();
		holder.binding.date.setText(new SpannableStringBuilder(Date + " : " + pending.getFrom() + " - " + pending.getTo()));
		holder.binding.orders.setText(new SpannableStringBuilder(pending.getOrders() + " " + mContext.getString(R.string.orders)));
		holder.binding.amount.setText(new SpannableStringBuilder(mContext.getString(R.string.sar) + " " + pending.getAmount()));
	}

	@Override
	public int getItemCount() {
		return size;
	}
}
