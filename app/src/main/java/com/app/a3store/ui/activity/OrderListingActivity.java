package com.app.a3store.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3store.BaseActivity;
import com.app.a3store.R;
import com.app.a3store.databinding.ActivityOrderStatusBinding;
import com.app.a3store.ui.adapter.OrderStatusFragmentPagerAdapter;
import com.app.a3store.ui.fragment.CompletedFragment;
import com.app.a3store.ui.fragment.OngoingFragment;
import com.app.a3store.utils.helper.SharedHelper;
import com.app.a3store.viewmodel.OrdersViewModel;
import com.google.android.material.tabs.TabLayout;

public class OrderListingActivity extends BaseActivity {

	ActivityOrderStatusBinding orderStatusBinding;
	OrderStatusFragmentPagerAdapter orderStatusPagerAdapter;
	OrdersViewModel ordersViewModel;
	SharedHelper sharedHelper;
	public int currentPosition = 0;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		orderStatusBinding = DataBindingUtil.setContentView(this, R.layout.activity_order_status);

		initViews();
		setListeners();
	}

	private void initTabs() {

		orderStatusBinding.tabLayout.addTab(orderStatusBinding.tabLayout.newTab().setText(getString(R.string.ongoing_orders_small)));
		orderStatusBinding.tabLayout.addTab(orderStatusBinding.tabLayout.newTab().setText(getString(R.string.ready_to_pickup_small)));
		orderStatusBinding.tabLayout.addTab(orderStatusBinding.tabLayout.newTab().setText(getString(R.string.completed_orders)));

		for(int i = 0; i < orderStatusBinding.tabLayout.getTabCount(); i++) {

			TabLayout.Tab tab = orderStatusBinding.tabLayout.getTabAt(i);
			if(tab != null) {

				AppCompatTextView tabTextView = new AppCompatTextView(this);

				// First tab is the selected tab, so if i==0 then set BOLD typeface
				if(i == 0) {
					tabTextView.setTextAppearance(OrderListingActivity.this, R.style.tab_selected);
				} else {
					tabTextView.setTextAppearance(OrderListingActivity.this, R.style.tab_unselected);
				}
				tabTextView.setText(tab.getText());
				tab.setCustomView(tabTextView);
			}
		}

		orderStatusPagerAdapter = new OrderStatusFragmentPagerAdapter(getSupportFragmentManager(), 3);
		orderStatusBinding.viewPager.setAdapter(orderStatusPagerAdapter);
		orderStatusBinding.viewPager.setOffscreenPageLimit(0);

		orderStatusBinding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(orderStatusBinding.tabLayout));

		orderStatusBinding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
			@Override
			public void onTabSelected(TabLayout.Tab tab) {
				currentPosition = tab.getPosition();
				orderStatusBinding.viewPager.setAdapter(orderStatusPagerAdapter);
				orderStatusBinding.viewPager.setCurrentItem(tab.getPosition());

				AppCompatTextView view = (AppCompatTextView) tab.getCustomView();
				if(view != null) {
					view.setTextAppearance(OrderListingActivity.this, R.style.tab_selected);
				}
			}

			@Override
			public void onTabUnselected(TabLayout.Tab tab) {
				AppCompatTextView view = (AppCompatTextView) tab.getCustomView();
				if(view != null) {
					view.setTextAppearance(OrderListingActivity.this, R.style.tab_unselected);
				}
			}

			@Override
			public void onTabReselected(TabLayout.Tab tab) {
				orderStatusBinding.viewPager.setAdapter(orderStatusPagerAdapter);
				orderStatusBinding.viewPager.setCurrentItem(tab.getPosition());
			}
		});
	}

	private void initViews() {
		sharedHelper = new SharedHelper(this);
		ordersViewModel = new ViewModelProvider(this).get(OrdersViewModel.class);
		initTabs();
	}

	private void setListeners() {

		orderStatusBinding.bottomDashboard.setOnClickListener(view->gotoDashboard());
		orderStatusBinding.bottomAssignmnet.setOnClickListener(view->gotoStore());
		orderStatusBinding.imgNotification.setOnClickListener(view->{
			Intent intent = new Intent(this, NotificationActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(intent);
		});

		orderStatusBinding.name.setOnClickListener(view->{
			Intent intent = new Intent(this, ProfileActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(intent);
		});

		orderStatusBinding.orders.setOnClickListener(view->{
			Intent intent = new Intent(OrderListingActivity.this, OrderListingActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(intent);
		});
	}

	private void gotoDashboard() {
		Intent intent = new Intent(OrderListingActivity.this, DashboardActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}

	private void gotoStore() {
		Intent intent = new Intent(OrderListingActivity.this, ProductAddItemsToStoreActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
	}

	@Override
	protected void onResume() {
		super.onResume();
		orderStatusBinding.name.setText(String.format("%s %s %s", this.getString(R.string.hi), sharedHelper.getFirstName(), sharedHelper.getLastName()));
	}
}
