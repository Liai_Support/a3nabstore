package com.app.a3store.ui.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3store.R;
import com.app.a3store.constants.UrlConstants;
import com.app.a3store.databinding.LayoutOrderStatusBinding;
import com.app.a3store.dialogs.DialogUtils;
import com.app.a3store.ui.activity.OrderDetailReadyForPickup;
import com.app.a3store.ui.activity.OrderListingActivity;
import com.app.a3store.ui.paging.pickuporders.OrdersPickupPagedAdapter;
import com.app.a3store.utils.Utils;
import com.app.a3store.viewmodel.OrdersViewModel;

import static com.app.a3store.constants.AppConstants.kFalse;

public class ReadyToPickupFragment extends Fragment {

	OrdersViewModel viewModel;
	LayoutOrderStatusBinding binding;
	public TextView readytopick;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = LayoutInflater.from(requireContext()).inflate(R.layout.layout_order_status, container, false);
		binding = LayoutOrderStatusBinding.bind(view);
		viewModel = new ViewModelProvider(this).get(OrdersViewModel.class);
		if(((OrderListingActivity)getActivity()).currentPosition == 1){
			initViews();
		}
		//initViews();
		return binding.getRoot();
	}

	private void initViews() {
		viewModel.readyToPickupAdapter = new OrdersPickupPagedAdapter(ReadyToPickupFragment.this,getContext(), position->{
			DialogUtils.showLoader(getContext());
			updateOrderStatus(viewModel.readyToPickupAdapter.getCurrentList().get(position).getOrderId(), UrlConstants.statusPickup, position);
		}, position->gotoTemplateTwo(viewModel.readyToPickupAdapter.getCurrentList().get(position).getOrderId()),
                position -> callDriver(viewModel.readyToPickupAdapter.getCurrentList().get(position).getOrderStatus()));

		binding.recyclerOrders.setAdapter(viewModel.readyToPickupAdapter);
		viewModel.pagingPickupOrders(getContext(), position1->{

			if(position1 == 0) {
				binding.txtNoOrdersFound.setVisibility(View.VISIBLE);
				binding.recyclerOrders.setVisibility(View.GONE);
			} else {
				binding.txtNoOrdersFound.setVisibility(View.GONE);
				binding.recyclerOrders.setVisibility(View.VISIBLE);
			}
			DialogUtils.dismissLoader();
		}, (position12, value)->{
		});
		viewModel.orderPickupPagedList.observe(getViewLifecycleOwner(), items->viewModel.readyToPickupAdapter.submitList(items));
	}

    private void callDriver(String mobileNumber) {

        Intent callIntent =  new Intent(Intent.ACTION_DIAL);
        callIntent.setData( Uri.parse("tel:" + mobileNumber)) ;
        startActivity(callIntent);

    }

    private void updateOrderStatus(int orderId, String status, int position) {
		viewModel.updateOrderStatus(getContext(), orderId, status).observe(getViewLifecycleOwner(), response->{
			DialogUtils.dismissLoader();
			Utils.showSnack(binding.getRoot(), response.getMessage());
			if(response.getError().equalsIgnoreCase(kFalse)) {
//				viewModel.readyToPickupAdapter.getCurrentList().get(position).setOrderStatus("ONGOING");
				viewModel.readyToPickupAdapter.getCurrentList().get(position).setStoreStatus("ONGOING");
				viewModel.readyToPickupAdapter.notifyItemChanged(position);
			}
			else {
				readytopick.setEnabled(true);
			}

		});
	}

	private void gotoTemplateTwo(int id) {
		Intent intent = new Intent(getContext(), OrderDetailReadyForPickup.class);
		intent.putExtra("id", id);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivityForResult(intent, 110);
	}

	@Override
	public void onSaveInstanceState(@NonNull Bundle outState) {

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == 110) initViews();
	}
}
