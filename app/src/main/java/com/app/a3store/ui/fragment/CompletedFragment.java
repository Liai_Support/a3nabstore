package com.app.a3store.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3store.R;
import com.app.a3store.constants.OrderConstants;
import com.app.a3store.databinding.LayoutOrderStatusBinding;
import com.app.a3store.dialogs.DialogUtils;
import com.app.a3store.ui.activity.OrderDetailOngoingOrder;
import com.app.a3store.ui.activity.OrderListingActivity;
import com.app.a3store.ui.paging.completedorders.OrdersCompletedPagedAdapter;
import com.app.a3store.viewmodel.OrdersViewModel;

public class CompletedFragment extends Fragment {

	OrdersViewModel viewModel;
	LayoutOrderStatusBinding binding;

	@Override
	public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = LayoutInflater.from(requireContext()).inflate(R.layout.layout_order_status, container, false);
		binding = LayoutOrderStatusBinding.bind(view);
		viewModel = new ViewModelProvider(this).get(OrdersViewModel.class);
		//initViews();
		if(((OrderListingActivity)getActivity()).currentPosition == 2){
			initViews();
		}
		return binding.getRoot();
	}

	public void initViews() {
		viewModel.completedPagedAdapter = new OrdersCompletedPagedAdapter(getContext(), position->gotoTemplateThree(viewModel.completedPagedAdapter.getCurrentList().get(position).getOrderId()));

		binding.recyclerOrders.setAdapter(viewModel.completedPagedAdapter);
		viewModel.pagingCompletedOrders(getContext(), position1->{

			if(position1 == 0) {
				binding.txtNoOrdersFound.setVisibility(View.VISIBLE);
				binding.recyclerOrders.setVisibility(View.GONE);
			} else {
				binding.txtNoOrdersFound.setVisibility(View.GONE);
				binding.recyclerOrders.setVisibility(View.VISIBLE);
			}
			DialogUtils.dismissLoader();
		}, (position12, value)->{
		});
		viewModel.orderCompletedPagedList.observe(getViewLifecycleOwner(), items->{
			//in case of any changes
			//submitting the items to adapter
			viewModel.completedPagedAdapter.submitList(items);
		});
	}

	private void gotoTemplateThree(int id) {
		Intent intent = new Intent(getContext(), OrderDetailOngoingOrder.class);
		intent.putExtra("id", id);
		intent.putExtra("orderType", OrderConstants.OrderType.completed);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
	}

	@Override
	public void onSaveInstanceState(@NonNull Bundle outState) {

	}
}
