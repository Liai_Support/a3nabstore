package com.app.a3store.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.constants.AppConstants;
import com.app.a3store.databinding.ChildSubcategoryTopBinding;
import com.app.a3store.model.SubCategoryResponse;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.utils.helper.SharedHelper;

import java.util.ArrayList;

public class SubCategoryTopAdapter extends RecyclerView.Adapter<SubCategoryTopAdapter.MyViewHolder> {

	Context context;
	ArrayList<SubCategoryResponse.SubCategoryData.ProductSubCategoryData> list;
	ItemClickListener onItemClick;
	int previousSubCategoryPosition = 0, selectedSubCategoryPosition = 0;

	public SubCategoryTopAdapter(Context context, ArrayList<SubCategoryResponse.SubCategoryData.ProductSubCategoryData> list, ItemClickListener onItemClick) {
		this.context = context;
		this.list = list;
		this.onItemClick = onItemClick;
	}

	@NonNull
	@Override
	public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.child_subcategory_top, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

		if(position == previousSubCategoryPosition) {
			onItemClick.onItemClick(position);

			holder.binding.name.setBackground(ResourcesCompat.getDrawable(context.getResources(), R.drawable.sub_category_selected, null));
			holder.binding.name.setTextColor(context.getResources().getColor(R.color.white));
		} else {
			holder.binding.name.setBackground(ResourcesCompat.getDrawable(context.getResources(), R.drawable.sub_category_unselected, null));
			holder.binding.name.setTextColor(context.getResources().getColor(R.color.dorado));
		}

		if(new SharedHelper(context).getSelectedLanguage().equalsIgnoreCase(AppConstants.kEnglish))
			holder.binding.name.setText(list.get(position).getProductSubCategoryName());
		else holder.binding.name.setText(list.get(position).getArabicName());

		holder.itemView.setOnClickListener(view->{
			selectedSubCategoryPosition = position;
			notifyItemChanged(selectedSubCategoryPosition);
			notifyItemChanged(previousSubCategoryPosition);
			previousSubCategoryPosition = selectedSubCategoryPosition;
		});
	}

	@Override
	public int getItemCount() {
		return list == null ? 0 : list.size();
	}

	static class MyViewHolder extends RecyclerView.ViewHolder {

		ChildSubcategoryTopBinding binding;

		public MyViewHolder(@NonNull View itemView) {
			super(itemView);
			binding = ChildSubcategoryTopBinding.bind(itemView);
		}
	}
}
