package com.app.a3store.ui.paging.storeProduct;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.app.a3store.model.product.Product;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.ui.interfaces.ItemClickListenerWithTwoIds;

public class ProductListDataSourceFactory extends DataSource.Factory<Integer, Product> {

	//creating the mutable live data
	private final MutableLiveData<PageKeyedDataSource<Integer, Product>> itemLiveDataSource = new MutableLiveData<>();
	Context mContext;
	int id;
	String type;
	ItemClickListener mItemClickListener;
	ItemClickListenerWithTwoIds mItemClickListenerWithTwoIds;

	public ProductListDataSourceFactory(Context context, int id, String type, ItemClickListener itemClickListener, ItemClickListenerWithTwoIds itemClickListenerWithTwoIds) {
		this.mContext = context;
		this.type = type;
		this.id = id;
		this.mItemClickListener = itemClickListener;
		this.mItemClickListenerWithTwoIds = itemClickListenerWithTwoIds;
	}

	@Override
	public DataSource<Integer, Product> create() {
		String TAG = "OrderPickupFactory";
		Log.d(TAG, "pagingOngoingOrders:");
		//getting our data source object
		ProductListDataSource itemDataSource = new ProductListDataSource(mContext, id, type, mItemClickListener, mItemClickListenerWithTwoIds);

		//posting the dataSource to get the values
		itemLiveDataSource.postValue(itemDataSource);

		//returning the dataSource
		return itemDataSource;
	}

	public MutableLiveData<PageKeyedDataSource<Integer, Product>> getItemLiveDataSource() {
		return itemLiveDataSource;
	}
}
