package com.app.a3store.ui.adapter;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.databinding.RowProfilCurrentDueBinding;
import com.app.a3store.model.profile.ProfileDashboardResponse;

import java.util.List;

public class ProfileCurrentDueAdapter extends RecyclerView.Adapter<ProfileCurrentDueAdapter.ViewHolder> {

	private final Context mContext;
	private final List<ProfileDashboardResponse.Data.CurrentDue> currentDues;

	public ProfileCurrentDueAdapter(Context context, List<ProfileDashboardResponse.Data.CurrentDue> currentDues) {
		this.mContext = context;
		this.currentDues = currentDues;
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {

		RowProfilCurrentDueBinding binding;

		public ViewHolder(View itemView) {
			super(itemView);
			binding = RowProfilCurrentDueBinding.bind(itemView);
		}
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
		View listItem = layoutInflater.inflate(R.layout.row_profil_current_due, parent, false);
		return new ViewHolder(listItem);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		ProfileDashboardResponse.Data.CurrentDue currentDue = currentDues.get(position);
		holder.binding.dueDate.setText(new SpannableStringBuilder(currentDue.getFrom() + " - " + currentDue.getTo()));
		holder.binding.duePrice.setText(new SpannableStringBuilder(mContext.getString(R.string.sr) + currentDue.getAmount()));
		if(currentDue.getIsPaid() == 0) {
			holder.binding.dueStatus.setText(R.string.pending);
			holder.binding.dueStatus.setTextColor(mContext.getResources().getColor(R.color.flush_orange));
		} else {
			holder.binding.dueStatus.setText(R.string.paid);
			holder.binding.dueStatus.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
		}
	}

	@Override
	public int getItemCount() {
		return currentDues.size();
	}
}
