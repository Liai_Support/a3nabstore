package com.app.a3store.ui.paging.newProductList;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.app.a3store.model.ProductUnavailableResponse;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.ui.interfaces.ItemClickListenerWithTwoIds;

public class NewProductListDataSourceFactory extends DataSource.Factory<Integer, ProductUnavailableResponse.Data.Product> {

	//creating the mutable live data
	private final MutableLiveData<PageKeyedDataSource<Integer, ProductUnavailableResponse.Data.Product>> itemLiveDataSource = new MutableLiveData<>();
	Context mContext;
	ItemClickListener mItemClickListener;
	ItemClickListenerWithTwoIds mItemClickListenerWithTwoIds;
	int id;
	String type;

	public NewProductListDataSourceFactory(Context context, ItemClickListener itemClickListener, ItemClickListenerWithTwoIds itemClickListenerWithTwoIds, int id, String type) {
		this.mContext = context;
		this.mItemClickListener = itemClickListener;
		this.mItemClickListenerWithTwoIds = itemClickListenerWithTwoIds;
		this.id = id;
		this.type = type;
	}

	@Override
	public DataSource<Integer, ProductUnavailableResponse.Data.Product> create() {
		String TAG = "OrderPickupFactory";
		Log.d(TAG, "pagingOngoingOrders:");
		//getting our data source object
		NewProductListDataSource itemDataSource = new NewProductListDataSource(mContext, mItemClickListener, mItemClickListenerWithTwoIds, this.id, this.type);

		//posting the dataSource to get the values
		itemLiveDataSource.postValue(itemDataSource);

		//returning the dataSource
		return itemDataSource;
	}

	public MutableLiveData<PageKeyedDataSource<Integer, ProductUnavailableResponse.Data.Product>> getItemLiveDataSource() {
		return itemLiveDataSource;
	}
}
