package com.app.a3store.ui.activity;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3store.BaseActivity;
import com.app.a3store.R;
import com.app.a3store.constants.IntentConstants;
import com.app.a3store.constants.OrderConstants;
import com.app.a3store.databinding.ActivityProfileBinding;
import com.app.a3store.dialogs.DialogUtils;
import com.app.a3store.ui.adapter.ProfileCurrentDueAdapter;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;
import com.app.a3store.viewmodel.ProfileViewModel;

import java.util.Locale;

import static com.app.a3store.constants.AppConstants.kActive;
import static com.app.a3store.constants.AppConstants.kFalse;
import static com.app.a3store.constants.AppConstants.kInactive;
import static com.app.a3store.constants.AppConstants.kTotalAmount;
import static com.app.a3store.constants.AppConstants.kTotalOrders;
import static com.app.a3store.constants.IntentConstants.intentProfileEdit;

public class ProfileActivity extends BaseActivity {

    ProfileCurrentDueAdapter profileCurrentDueAdapter;
    ColorStateList colorStateSelectedList, colorStateUnSelectedList;
    ActivityProfileBinding binding;
    SharedHelper sharedHelper;
    ProfileViewModel viewModel;
    OrderConstants.StatusType statusType;
    OrderConstants.StoreStatus storeStatus;

    String mobileNumber = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);
        statusType = (OrderConstants.StatusType) getIntent().getSerializableExtra("status");
        initViews();
        setListeners();
        getMyProfile();
    }

    private void initViews() {
        colorStateSelectedList = new ColorStateList(new int[][]{new int[]{android.R.attr.state_enabled}}, new int[]{ContextCompat.getColor(this, R.color.colorPrimary)});//enabled
        colorStateUnSelectedList = new ColorStateList(new int[][]{new int[]{android.R.attr.state_enabled} //enabled
        }, new int[]{ContextCompat.getColor(this, R.color.dorado)}); //disabled
        viewModel = new ViewModelProvider(this).get(ProfileViewModel.class);
        viewModel.setContext(this);
        sharedHelper = new SharedHelper(this);
        binding.backArrow.setOnClickListener(view -> closeActivity());
        binding.rgStatus.setOnCheckedChangeListener((radioGroup, i) -> {
            if (binding.radioOn.isChecked()) {
                binding.radioOn.setButtonTintList(colorStateSelectedList);
                binding.radioOff.setButtonTintList(colorStateUnSelectedList);
            } else {
                binding.radioOn.setButtonTintList(colorStateUnSelectedList);
                binding.radioOff.setButtonTintList(colorStateSelectedList);
            }
        });

        if (sharedHelper.getProfileStatus()) {
            statusType = OrderConstants.StatusType.active;
            binding.radioOn.setChecked(true);
            storeStatus = OrderConstants.StoreStatus.ON;
            binding.status.setText(getText(R.string.active));
            sharedHelper.setProfileStatus(true);
        } else {
            statusType = OrderConstants.StatusType.inactive;
            binding.radioOff.setChecked(true);
            storeStatus = OrderConstants.StoreStatus.OFF;
            binding.status.setText(getText(R.string.inactive));
            sharedHelper.setProfileStatus(false);
        }

        binding.name.setText(String.format("%s %s", sharedHelper.getFirstName(), sharedHelper.getLastName()));
    }

    private void closeActivity() {
        finish();
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void setListeners() {
        binding.bottomAssignmnet.setOnClickListener(view -> {
            Intent intent = new Intent(this, ProductAddItemsToStoreActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        });

        binding.orders.setOnClickListener(view -> {
            Intent intent = new Intent(this, OrderListingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        });
        binding.bottomDashboard.setOnClickListener(view -> {
            Intent intent = new Intent(this, DashboardActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        });
        binding.edit.setOnClickListener(view -> gotoEditProfile());
        binding.backArrow.setOnClickListener(view -> backPressed());
        binding.transactionLog.setOnClickListener(view -> gotoOrderTransactionLog());

        binding.help.setOnClickListener(view -> gotoHelp());
        binding.logout.setOnClickListener(view -> logout());
        binding.rgStatus.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.radioOn) {
                updateOnlineStatus(kActive);
            } else if (checkedId == R.id.radioOff) {
                updateOnlineStatus(kInactive);
            }
        });

        binding.lang.setOnClickListener(view -> {

            if (sharedHelper.getSelectedLanguage().equals("en")) {
                sharedHelper.setSelectedLanguage("ar");
            } else {
                sharedHelper.setSelectedLanguage("en");
            }
            Intent intent = new Intent(ProfileActivity.this, ProfileActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

        });
    }

    private void updateOnlineStatus(String status) {
        DialogUtils.showLoader(this);
        viewModel.updateOnlineStatus(this, status).observe(this, commonResponse -> {
            DialogUtils.dismissLoader();
            if (commonResponse.getError().equalsIgnoreCase(kFalse)) {
                Utils.showSnack(binding.getRoot(), getString(R.string.store_status_success));
                if (storeStatus == OrderConstants.StoreStatus.ON) {
                    binding.radioOff.setChecked(true);
                    storeStatus = OrderConstants.StoreStatus.OFF;
                    binding.status.setText(getText(R.string.inactive));
                    sharedHelper.setProfileStatus(false);
                } else {
                    binding.radioOn.setChecked(true);
                    storeStatus = OrderConstants.StoreStatus.ON;
                    binding.status.setText(getText(R.string.active));
                    sharedHelper.setProfileStatus(true);
                }
            } else {
                if (storeStatus == OrderConstants.StoreStatus.ON) {
                    binding.radioOn.setChecked(true);
                } else {
                    binding.radioOff.setChecked(true);
                }
                Utils.showSnack(binding.getRoot(), commonResponse.getMessage());
            }
        });
    }

    private void gotoEditProfile() {
        Intent intent = new Intent(ProfileActivity.this, ProfileRegistrationActivity.class);
        intent.putExtra(intentProfileEdit, viewModel.profileDetail);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, IntentConstants.code_ProfileEdit);
    }

    private void gotoOrderTransactionLog() {
        Intent intent = new Intent(ProfileActivity.this, OrderTransactionLogActivity.class);
        intent.putExtra(kTotalOrders, binding.orderValue.getText().toString());
        intent.putExtra(kTotalAmount, binding.earnedValue.getText().toString());
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private void gotoHelp() {
        if (mobileNumber.equalsIgnoreCase("")) {
            return;
        }
        Intent intent = new Intent(ProfileActivity.this, HelpActivity.class);
        intent.putExtra("mobileNumber", mobileNumber);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private void logout() {
        sharedHelper.setAuthToken("");
        sharedHelper.setLoggedIn(false);
        Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finishAffinity();
    }

    private void backPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (binding != null) {
            binding.name.setText(String.format("%s %s", sharedHelper.getFirstName(), sharedHelper.getLastName()));
        }
    }

    private void getMyProfile() {
        DialogUtils.showLoader(this);
        viewModel.profileDashBoard(this).observe(this, profileResponse -> {
            DialogUtils.dismissLoader();
            if (profileResponse.getError().equalsIgnoreCase("true")) {
                Utils.showSnack(binding.getRoot(), profileResponse.getMessage());
            } else {
                viewModel.profileDetail = profileResponse.getData().getProfile();
                if (viewModel.profileDetail != null) {
                    sharedHelper.setFirstName(viewModel.profileDetail.getFirstName());
                    sharedHelper.setLastName(viewModel.profileDetail.getLastName());
                    sharedHelper.setMobileNumber(viewModel.profileDetail.getMobileNumber());
                    sharedHelper.setEmail(viewModel.profileDetail.getEmail());

                    mobileNumber =  profileResponse.getData().getStoremanagernumber();
                }
                if (profileResponse.getData().getCurrentDue().isEmpty()) {
                    binding.dueView.setVisibility(View.GONE);
                } else {
                    profileCurrentDueAdapter = new ProfileCurrentDueAdapter(this, profileResponse.getData().getCurrentDue());
                    binding.recyclerCurrentDue.setAdapter(profileCurrentDueAdapter);
                }

                binding.earnedValue.setText(new SpannableStringBuilder(getString(R.string.priceUnit) + " " + profileResponse.getData().getOrderInfo().getTotalAmount()));
               // binding.orderValue.setText(new SpannableStringBuilder(getResources().getQuantityString(R.plurals.numberOfOrders, profileResponse.getData().getOrderInfo().getOrderCount(), profileResponse.getData().getOrderInfo().getOrderCount())));
                binding.orderValue.setText(new SpannableStringBuilder(""+profileResponse.getData().getOrderInfo().getOrderCount()+" "+getString(R.string.orders)));

                binding.fullName.setText(new SpannableStringBuilder(profileResponse.getData().getProfile().getFirstName() + " " + profileResponse.getData().getProfile().getLastName()));
                String countryCode = "";
                if (profileResponse.getData().getProfile().getCountryCode() != null) {
                    countryCode = profileResponse.getData().getProfile().getCountryCode().toString();
                }
                binding.phone.setText(String.format(Locale.ENGLISH, " %s", profileResponse.getData().getProfile().getMobileNumber()));
                binding.mobile.setText(String.format(Locale.ENGLISH, "%s", profileResponse.getData().getProfile().getMobileNumber()));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IntentConstants.code_ProfileEdit && resultCode == RESULT_OK) {
            getMyProfile();
        }
    }
}