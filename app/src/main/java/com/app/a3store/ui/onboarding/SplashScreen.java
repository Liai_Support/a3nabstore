package com.app.a3store.ui.onboarding;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;

import androidx.appcompat.app.AppCompatActivity;

import com.app.a3store.R;
import com.app.a3store.ui.activity.DashboardActivity;
import com.app.a3store.ui.activity.LoginActivity;
import com.app.a3store.utils.helper.SharedHelper;

public class SplashScreen extends AppCompatActivity {

	SharedHelper sharedHelper;
	CountDownTimer countDownTimer = new CountDownTimer(3 * 1000, 1000) {
		@Override
		public void onTick(long l) {

		}

		@Override
		public void onFinish() {
			if(sharedHelper.getAuthToken() != null && !sharedHelper.getAuthToken().isEmpty()) {
				openDashboardScreens();
				closeActivity();
				return;
			}
			//openOnBoardingScreens();
			openLoginScreens();
			closeActivity();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);
		sharedHelper = new SharedHelper(this);
		countDownTimer.start();
	}

	private void openOnBoardingScreens() {
		Intent intent = new Intent(this, OnBoardingActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	private void openLoginScreens() {
		Intent intent = new Intent(this, LoginActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	private void openDashboardScreens() {
		Intent intent = new Intent(this, DashboardActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	private void closeActivity() {
		finishAffinity();
	}

	@Override
	public void onBackPressed() {
		closeActivity();
	}
}