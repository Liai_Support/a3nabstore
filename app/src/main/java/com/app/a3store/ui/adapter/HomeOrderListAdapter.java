package com.app.a3store.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.model.dashboard.DashboardResponse;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.ui.viewholder.AcceptRejectViewHolder;
import com.app.a3store.ui.viewholder.OrderCompleteViewHolder;
import com.app.a3store.ui.viewholder.ReadyToPickupViewHolder;
import com.app.a3store.viewmodel.DashboardViewModel;

public class HomeOrderListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

	private final Context mContext;
	private final int ACCEPT_REJECT = 0;
	private final int READY_TO_PICKUP = 1;
	private final DashboardViewModel viewModel;
	private final ItemClickListener acceptClickListener;
	private final ItemClickListener rejectClickListener;
	private final ItemClickListener viewClickListener;
	private final ItemClickListener readyForPickupListener;
	private final ItemClickListener readyForPickupViewListener;

	public HomeOrderListAdapter(Context context, DashboardViewModel viewModel,
	                            ItemClickListener mAcceptClickListener,
	                            ItemClickListener mRejectClickListener,
	                            ItemClickListener mViewClickListener,
	                            ItemClickListener mReadyForPickupListener,
	                            ItemClickListener mReadyForPickupViewListener) {
		this.mContext = context;
		this.viewModel = viewModel;
		this.acceptClickListener = mAcceptClickListener;
		this.rejectClickListener = mRejectClickListener;
		this.viewClickListener = mViewClickListener;
		this.readyForPickupListener = mReadyForPickupListener;
		this.readyForPickupViewListener = mReadyForPickupViewListener;
	}

	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
		if(viewType == ACCEPT_REJECT) {
			View listItem = layoutInflater.inflate(R.layout.row_orders_accept_reject, parent, false);
			return new AcceptRejectViewHolder(listItem);
		} else if(viewType == READY_TO_PICKUP) {
			View listItem = layoutInflater.inflate(R.layout.row_orders_ready_to_pickup, parent, false);
			return new ReadyToPickupViewHolder(listItem);
		} else {
			View listItem = layoutInflater.inflate(R.layout.row_orderslist_complete, parent, false);
			return new OrderCompleteViewHolder(listItem);
		}
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		DashboardResponse.Data.Order data = viewModel.ordersList.get(holder.getAdapterPosition());
		if(holder.getItemViewType() == ACCEPT_REJECT) {
			((AcceptRejectViewHolder) holder).bindData(mContext, data, holder.getAdapterPosition(), acceptClickListener, rejectClickListener, viewClickListener);
		} else if(holder.getItemViewType() == READY_TO_PICKUP) {
			((ReadyToPickupViewHolder) holder).bindData(holder.getAdapterPosition(), data, this.readyForPickupListener, readyForPickupViewListener);
		} else {
			((OrderCompleteViewHolder) holder).bindData(data);
		}
	}

	@Override
	public int getItemViewType(int position) {
		DashboardResponse.Data.Order data = viewModel.ordersList.get(position);
		if(data.getOrderStatus().equalsIgnoreCase("PENDING")) {
			return ACCEPT_REJECT;
		} else {
			return READY_TO_PICKUP;
		}
	}

	@Override
	public int getItemCount() {
		return Math.min(viewModel.ordersList.size(), 5);
	}
}
