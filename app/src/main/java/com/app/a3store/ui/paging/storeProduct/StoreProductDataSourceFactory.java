package com.app.a3store.ui.paging.storeProduct;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.app.a3store.model.product.Product;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.ui.interfaces.ItemClickListenerWithTwoIds;

public class StoreProductDataSourceFactory extends DataSource.Factory<Integer, Product> {

	//creating the mutable live data
	private final MutableLiveData<PageKeyedDataSource<Integer, Product>> itemLiveDataSource = new MutableLiveData<>();
	Context mContext;
	String type;
	ItemClickListener mItemClickListener;
	ItemClickListenerWithTwoIds mItemClickListenerWithTwoIds;

	public StoreProductDataSourceFactory(Context context, String type, ItemClickListener itemClickListener, ItemClickListenerWithTwoIds itemClickListenerWithTwoIds) {
		this.mContext = context;
		this.type = type;
		this.mItemClickListener = itemClickListener;
		this.mItemClickListenerWithTwoIds = itemClickListenerWithTwoIds;
	}

	@Override
	public DataSource<Integer, Product> create() {
		String TAG = "OrderPickupFactory";
		Log.d(TAG, "pagingOngoingOrders:");
		//getting our data source object
		StoreProductDataSource itemDataSource = new StoreProductDataSource(mContext, type, mItemClickListener, mItemClickListenerWithTwoIds);

		//posting the dataSource to get the values
		itemLiveDataSource.postValue(itemDataSource);

		//returning the dataSource
		return itemDataSource;
	}

	public MutableLiveData<PageKeyedDataSource<Integer, Product>> getItemLiveDataSource() {
		return itemLiveDataSource;
	}
}
