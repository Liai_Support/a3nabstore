package com.app.a3store.ui.interfaces;

public interface ItemClickListener {

	void onItemClick(int position);
}
