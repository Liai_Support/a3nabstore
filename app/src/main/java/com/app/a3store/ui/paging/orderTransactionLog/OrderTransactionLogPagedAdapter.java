package com.app.a3store.ui.paging.orderTransactionLog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.databinding.RowOrdersTransactionLogBinding;
import com.app.a3store.model.OrderTransactionResponse;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.utils.TimeCalculator;
import com.app.a3store.utils.Utils;

import java.util.Objects;

public class OrderTransactionLogPagedAdapter extends PagedListAdapter<OrderTransactionResponse.Data.Order, OrderTransactionLogPagedAdapter.ViewHolder> {

    private final Context mContext;
    private final ItemClickListener clickListener;

    public OrderTransactionLogPagedAdapter(Context context, ItemClickListener clickListener) {
        super(DIFF_CALLBACK);
        this.mContext = context;
        this.clickListener = clickListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        RowOrdersTransactionLogBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = RowOrdersTransactionLogBinding.bind(itemView);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_orders_transaction_log, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OrderTransactionResponse.Data.Order order = getItem(holder.getAdapterPosition());

        holder.binding.orderId.setText(order.getOrderIDs());
        holder.binding.date.setText(TimeCalculator.convertDate(order.getOrderOn(), mContext));
        String amount = String.valueOf(order.getAmount() + order.getTaxPrice());
        holder.binding.amount.setText(new StringBuilder(mContext.getString(R.string.sar) + " " + amount));
        holder.binding.view.setOnClickListener(view -> clickListener.onItemClick(holder.getAdapterPosition()));
    }

    private static final DiffUtil.ItemCallback<OrderTransactionResponse.Data.Order> DIFF_CALLBACK = new DiffUtil.ItemCallback<OrderTransactionResponse.Data.Order>() {
        @Override
        public boolean areItemsTheSame(OrderTransactionResponse.Data.Order oldItem, OrderTransactionResponse.Data.Order newItem) {
            return oldItem.getId().equals(newItem.getId());
        }

        @Override
        public boolean areContentsTheSame(@NonNull OrderTransactionResponse.Data.Order oldItem, @NonNull OrderTransactionResponse.Data.Order newItem) {
            return Objects.equals(oldItem, newItem);
        }
    };
}
