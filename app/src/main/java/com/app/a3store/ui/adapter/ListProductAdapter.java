package com.app.a3store.ui.adapter;

import static com.app.a3store.volley.AppController.TAG;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.databinding.ChildProductBinding;
import com.app.a3store.model.DashboardResponse;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;

import java.util.ArrayList;
import java.util.Locale;

public class ListProductAdapter extends RecyclerView.Adapter<ListProductAdapter.MyViewHolder> {

	Context context;
	ArrayList<DashboardResponse.BestProduct> list;
	SharedHelper sharedHelper;
	ItemClickListener onAddItem, onDeleteItem;
	int previousPosition = -1, currentPosition = -1,quantity = 1;

	public ListProductAdapter(Context context, ArrayList<DashboardResponse.BestProduct> list, ItemClickListener onAddItem, ItemClickListener onDeleteItem,int quantity) {
		this.context = context;
		this.list = list;
		this.onAddItem = onAddItem;
		this.onDeleteItem = onDeleteItem;
		sharedHelper = new SharedHelper(context);
		this.quantity = quantity;
	}

	@NonNull
	@Override
	public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.child_product, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

		DashboardResponse.BestProduct data = list.get(position);

		if(currentPosition == position) {
			holder.binding.select.setBackgroundColor(context.getResources().getColor(R.color.silver_chalice));
			holder.binding.check.setVisibility(View.VISIBLE);
		} else {
			holder.binding.select.setBackgroundColor(Color.WHITE);
			holder.binding.check.setVisibility(View.INVISIBLE);
		}

		if(data != null) {
			if(data.getProductDiscount().equals("") || data.getProductDiscount().equals("0")) {
				holder.binding.discount.setVisibility(View.GONE);
			} else {
				holder.binding.discount.setVisibility(View.VISIBLE);
				holder.binding.discount.setText(String.format("%s%%", data.getProductDiscount()));
			}


			if(new SharedHelper(context).getSelectedLanguage().equalsIgnoreCase("ar")){
				holder.binding.productName.setText(data.getArabicName());
			}else {
				holder.binding.productName.setText(data.getProductName());
			}
			holder.binding.count.setText(""+quantity);

			if(data.getProductDiscount() != null && !data.getProductDiscount().equals("0") && !data.getProductDiscount().equals("")) {
				holder.binding.oldAmount.setVisibility(View.VISIBLE);
				holder.binding.oldCurrency.setVisibility(View.VISIBLE);
				holder.binding.amount.setText(String.valueOf(Double.parseDouble(data.getProductPrice()) - ((Double.parseDouble(data.getProductPrice()) * Double.parseDouble(data.getProductDiscount())) / 100)));
				holder.binding.oldAmount.setText(data.getProductPrice());
			} else {
				holder.binding.oldAmount.setVisibility(View.GONE);
				holder.binding.oldCurrency.setVisibility(View.GONE);
				holder.binding.amount.setText(data.getProductPrice());
			}
			holder.binding.weight.setText(String.format(Locale.ENGLISH,"%s", data.getProductWeight()));

			if(data.getProductImages() != null && data.getProductImages().size() != 0) {
				Utils.loadImage(holder.binding.imageView20, data.getProductImages().get(0).getProductImage());
			}
		}

		try {
			if(list.get(position).getProductStatus().isEmpty() || list.get(position).getProductStatus().equalsIgnoreCase("active")) {

				holder.binding.outOfStock.setVisibility(View.GONE);
				holder.binding.disableView.setVisibility(View.GONE);
			} else {
				holder.binding.outOfStock.setVisibility(View.VISIBLE);
				holder.binding.disableView.setVisibility(View.VISIBLE);
			}
		} catch(Exception e) {
			holder.binding.outOfStock.setVisibility(View.GONE);
			holder.binding.disableView.setVisibility(View.GONE);
		}

		if(list.get(position).getProductDiscount().equals("") || list.get(position).getProductDiscount().equals("0")) {
			holder.binding.discount.setVisibility(View.INVISIBLE);
		} else {
			holder.binding.discount.setVisibility(View.VISIBLE);
		}


		holder.binding.addBtn.setOnClickListener(v->{
			if(list.get(position).getStoreStock() != 0) {
				onAddItem.onItemClick(holder.getAdapterPosition());
				previousPosition = currentPosition;
				currentPosition = holder.getAdapterPosition();
				if (previousPosition != -1) notifyItemChanged(previousPosition);
				notifyItemChanged(currentPosition);
			}else {
				Toast.makeText(context,context.getString(R.string.out_of_stock),Toast.LENGTH_LONG).show();
			}
		});
		holder.binding.add.setOnClickListener(v -> {
			quantity = Integer.parseInt(holder.binding.count.getText().toString() )+ 1;
			holder.binding.count.setText(""+quantity);
			list.get(position).setCount(quantity);
			Log.d(TAG, "onBindViewHolder: "+list.get(position).getCount());
		});
		holder.binding.minus.setOnClickListener(v -> {
			Log.d(TAG, "onBindViewHolder: "+holder.binding.count.getText().toString());
			if (!holder.binding.count.getText().toString().equals("1")){
				quantity = Integer.parseInt(holder.binding.count.getText().toString() ) - 1;
				holder.binding.count.setText(""+quantity);
			}
			list.get(position).setCount(quantity);
		});

		if(new SharedHelper(context).getSelectedLanguage().equals("en")) {
			holder.binding.discount.setBackgroundResource(R.drawable.product_discount_bg);
		} else {
			holder.binding.discount.setBackgroundResource(R.drawable.product_discount_bg_arabic);
		}

//		setView(holder, position);
	}

	@Override
	public int getItemCount() {
		return list == null ? 0 : list.size();
	}

	static class MyViewHolder extends RecyclerView.ViewHolder {

		ChildProductBinding binding;

		public MyViewHolder(@NonNull View itemView) {
			super(itemView);
			binding = ChildProductBinding.bind(itemView);
		}
	}

}
