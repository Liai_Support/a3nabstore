package com.app.a3store.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.app.a3store.R;
import com.app.a3store.model.stock.Vendor;

import java.util.List;

public class VendorListAdapter extends ArrayAdapter<Vendor> {

	public VendorListAdapter(Context context, List<Vendor> vendorResponses) {
		super(context, 0, vendorResponses);
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, @NonNull ViewGroup parent) {
		Vendor vendorResponse = getItem(position);
		if(convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.spinner_item, parent, false);
		}
		TextView tvName = convertView.findViewById(R.id.value);
		tvName.setText(vendorResponse.getVendorName());

		return convertView;
	}
}
