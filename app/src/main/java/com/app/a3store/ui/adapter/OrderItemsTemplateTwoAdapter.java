package com.app.a3store.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.dialogs.DialogUtils;
import com.app.a3store.model.orders.OrderDetailsResponse;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.ui.interfaces.ItemClickListenerWithTwoIds;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;
import com.app.a3store.utils.helper.SharedPreference;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;
import java.util.Locale;

public class OrderItemsTemplateTwoAdapter extends RecyclerView.Adapter<OrderItemsTemplateTwoAdapter.ViewHolder> {

    private final Context context;
    List<OrderDetailsResponse.Data.OrderItem> orderItems;
    ItemClickListener onItemClick;
    ItemClickListener onDeleteClick;
    int isDeleteOrReplace;
    ItemClickListenerWithTwoIds mItemClickListener;
    ItemClickListenerWithTwoIds checkForPickUp;

    public OrderItemsTemplateTwoAdapter(Context context, List<OrderDetailsResponse.Data.OrderItem> orderItems, int mIsDeleteOrReplace, ItemClickListener onItemClick, ItemClickListener onDeleteClick, ItemClickListenerWithTwoIds itemClickListener, ItemClickListenerWithTwoIds checkForPickUp) {
        this.context = context;
        this.orderItems = orderItems;
        this.onItemClick = onItemClick;
        this.onDeleteClick = onDeleteClick;
        this.isDeleteOrReplace = mIsDeleteOrReplace;
        this.mItemClickListener = itemClickListener;
        this.checkForPickUp = checkForPickUp;
    }

    public OrderDetailsResponse.Data.OrderItem getItem(int position) {
        if (position >= 0 && position < this.orderItems.size()) {
            return this.orderItems.get(position);
        }
        return null;
    }

    public void updateItemInfo(int position, int value) {
        if (orderItems != null && position < orderItems.size()) {
            OrderDetailsResponse.Data.OrderItem item = orderItems.get(position);
            item.setProductStatusPacked(value);
            orderItems.remove(position);
            orderItems.add(position, item);
            notifyItemChanged(position);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView itemName, itemPrice, priceQty, splInstruction, totalPrice, deleteItem, replaceItem, splInstructionTitle;
        View bottomView, bottomGreenView;
        public AppCompatImageView orderImage, trash, recycle, orderStatusImage;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemName = itemView.findViewById(R.id.itemName);
            this.splInstructionTitle = itemView.findViewById(R.id.splInstructionTitle);
            this.itemPrice = itemView.findViewById(R.id.itemPrice);
            this.priceQty = itemView.findViewById(R.id.priceQty);
            this.orderImage = itemView.findViewById(R.id.orderImage);
            this.splInstruction = itemView.findViewById(R.id.splInstruction);
            this.totalPrice = itemView.findViewById(R.id.totalPrice);
            this.bottomView = itemView.findViewById(R.id.bottomView);
            this.bottomGreenView = itemView.findViewById(R.id.bottomGreenView);
            this.deleteItem = itemView.findViewById(R.id.deleteItem);
            this.replaceItem = itemView.findViewById(R.id.replaceItem);
            this.trash = itemView.findViewById(R.id.trash);
            this.recycle = itemView.findViewById(R.id.recycle);
            this.orderStatusImage = itemView.findViewById(R.id.orderStatusImage);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_order_items_type_three, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        OrderDetailsResponse.Data.OrderItem orderItem = orderItems.get(position);

        double discountPrice = orderItem.getPrice();
        if (orderItem.getDiscount() != 0) {
            discountPrice = orderItem.getPrice() * (orderItem.getDiscount() / 100f);
            discountPrice = orderItem.getPrice() - discountPrice;
        }

        if (orderItem.getBoxPrice() != null) {
            discountPrice += Double.parseDouble(orderItem.getBoxPrice());
        }

        if (orderItem.getCuttingPrice() != null) {
            discountPrice += Double.parseDouble(orderItem.getCuttingPrice());
        }


        if (!orderItem.getOrderInstructions().isEmpty())
            holder.splInstructionTitle.setText(orderItem.getOrderInstructions());
        else
            holder.splInstructionTitle.setText(context.getString(R.string.no_spl_instruction));


        if(new SharedHelper(context).getSelectedLanguage().equalsIgnoreCase("ar")){
            holder.itemName.setText(orderItem.getArabicName());
        }
        else {
            holder.itemName.setText(orderItem.getProductName());
        }

        holder.itemPrice.setText(Utils.round(discountPrice));
        if (new SharedHelper(context).getSelectedLanguage().toLowerCase().equalsIgnoreCase("ar")) {
            holder.priceQty.setText(String.format(Locale.ENGLISH, "%dX", orderItem.getQuantity()));
        } else {
            holder.priceQty.setText(String.format(Locale.ENGLISH, "X%d", orderItem.getQuantity()));
        }

        holder.totalPrice.setText(Utils.round(orderItem.getQuantity() * discountPrice));

        if (!orderItem.getOrderInstructions().isEmpty())
            holder.splInstruction.setText(orderItem.getOrderInstructions());
        RequestOptions options = new RequestOptions().centerCrop().placeholder(R.drawable.applogo).error(R.drawable.applogo);
        Glide.with(context).load(orderItem.getProductImage()).apply(options).into(holder.orderImage);
        holder.replaceItem.setOnClickListener(v -> onItemClick.onItemClick(position));
        holder.deleteItem.setOnClickListener(v -> {

                    if (getItemCount() != 1)
                        onDeleteClick.onItemClick(position);
                    else
                        DialogUtils.showAlert(context);
                }
        );
        if (isDeleteOrReplace == 0) {
            holder.bottomView.setVisibility(View.VISIBLE);
            holder.bottomGreenView.setVisibility(View.GONE);
            holder.deleteItem.setVisibility(View.VISIBLE);
            holder.replaceItem.setVisibility(View.GONE);
            holder.trash.setVisibility(View.VISIBLE);
            holder.recycle.setVisibility(View.GONE);
        } else {
            holder.bottomView.setVisibility(View.VISIBLE);
            holder.bottomGreenView.setVisibility(View.VISIBLE);
            holder.deleteItem.setVisibility(View.VISIBLE);
            holder.replaceItem.setVisibility(View.VISIBLE);
            holder.trash.setVisibility(View.VISIBLE);
            holder.recycle.setVisibility(View.VISIBLE);
        }

        if (orderItem.getProductStatusPacked() == 1) {
            holder.orderStatusImage.setImageResource(R.drawable.green_enable);
        } else {
            holder.orderStatusImage.setImageResource(R.drawable.orange_disable);
        }

        holder.orderStatusImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (orderItem.getProductStatusPacked() == 1) {
                    mItemClickListener.onItemClick(holder.getAdapterPosition(), "0");
                } else {
                    mItemClickListener.onItemClick(holder.getAdapterPosition(), "1");
                }
            }
        });

        checkForPickUp.onItemClick(0, "0");

    }

    @Override
    public int getItemCount() {
        return orderItems.size();
    }


    public boolean changeMakeReady() {

        boolean isAllEnabled = false;
        for (int i = 0; i < orderItems.size(); i++) {
            if (orderItems.get(i).getProductStatusPacked() != 1) {
                isAllEnabled = false;
                break;
            } else {
                isAllEnabled = true;
            }
        }

        return isAllEnabled;
    }

}
