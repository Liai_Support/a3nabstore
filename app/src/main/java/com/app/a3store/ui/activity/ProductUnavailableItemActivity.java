package com.app.a3store.ui.activity;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3store.BaseActivity;
import com.app.a3store.R;
import com.app.a3store.databinding.ActivityProductUnavailableItemBinding;
import com.app.a3store.dialogs.DialogUtils;
import com.app.a3store.model.ProductUnavailableResponse;
import com.app.a3store.ui.adapter.ProductCategoriesAdapter;
import com.app.a3store.ui.adapter.ProductCategoriesNameAdapter;
import com.app.a3store.ui.adapter.ProductUnAvailableItemAdapter;
import com.app.a3store.ui.paging.newProductList.NewProductListPagedAdapter;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;
import com.app.a3store.viewmodel.ProductUnavailableViewModel;

import java.util.List;

import static com.app.a3store.constants.AppConstants.kEnglish;
import static com.app.a3store.constants.AppConstants.kFalse;

public class ProductUnavailableItemActivity extends BaseActivity {

	ActivityProductUnavailableItemBinding binding;
	List<ProductUnavailableResponse.Data.Product> productList;
	NewProductListPagedAdapter pagedAdapter;
	ProductUnAvailableItemAdapter productAdapter;
	ProductCategoriesAdapter productCategoriesAdapter;
	ProductCategoriesNameAdapter productCategoriesNameAdapter;
	ProductUnavailableViewModel viewModel;
	int productPosition = -1;
	boolean setFlag = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = DataBindingUtil.setContentView(this, R.layout.activity_product_unavailable_item);
		viewModel = new ViewModelProvider(this).get(ProductUnavailableViewModel.class);
		if(new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase(kEnglish)) {
			binding.backArrow.setScaleX(1);
		} else {
			binding.backArrow.setScaleX(-1);
		}
		setAdapters();
		setListeners();
	}

	private void setAdapters() {
		pagedAdapter = new NewProductListPagedAdapter(this, selectedProductPosition->productPosition = selectedProductPosition);
		binding.recyclerProducts.setAdapter(pagedAdapter);

		viewModel.getCategoryDetails(this).observe(this, productDashboardResponse->{
			if(productDashboardResponse.getError().equalsIgnoreCase(kFalse)) {
				productCategoriesAdapter = new ProductCategoriesAdapter(this, productDashboardResponse.getData().getCategory(), position->{
					viewModel.selectedCategoryId = productDashboardResponse.getData().getCategory().get(position).getId();
					viewModel.getSubCategoryDetails(this).observe(this, productSubCategoryResponse->{
						if(productSubCategoryResponse.getError().equalsIgnoreCase(kFalse)) {
							if(productSubCategoryResponse.getData().getProductCategory().isEmpty()) {
								binding.recyclerCategoriesName.setVisibility(View.GONE);
								viewModel.selectedSubCategoryId = 0;
								loadProductList();
							} else {
								binding.recyclerCategoriesName.setVisibility(View.VISIBLE);
								productCategoriesNameAdapter = new ProductCategoriesNameAdapter(this, productSubCategoryResponse.getData().getProductCategory(), position1->{

									viewModel.selectedSubCategoryId = productSubCategoryResponse.getData().getProductCategory().get(position1).getId();
									loadProductList();
								});
								binding.recyclerCategoriesName.setAdapter(productCategoriesNameAdapter);
							}
						} else {
							Utils.showSnack(binding.getRoot(), productSubCategoryResponse.getMessage());
						}
					});
				});
				binding.recyclerCategories.setAdapter(productCategoriesAdapter);
			} else {
				Utils.showSnack(binding.getRoot(), productDashboardResponse.getMessage());
			}
		});
	}

	private void setListeners() {
		binding.requestUnavailableItem.setOnClickListener(view->{
			ProductUnavailableResponse.Data.Product product;
			if(productPosition >= 0) {
				if(setFlag) {
					product = productList.get(productPosition);
				} else {
					product = pagedAdapter.getCurrentList().get(productPosition);
				}
				viewModel.addNewProduct(this, product.getId(), product.getCategoryId()).observe(this, commonResponse->{
					if(commonResponse.getError().equalsIgnoreCase(kFalse)) {
						setResult(RESULT_OK);
						finish();
					} else {
						Utils.showSnack(binding.getRoot(), commonResponse.getMessage());
					}
				});
			} else {
				Utils.showSnack(binding.getRoot(), getString(R.string.product_select_error));
			}
		});
		binding.backArrow.setOnClickListener(view->backPressed());
		binding.bottomDashboard.setOnClickListener(view->gotoDashboard());
		binding.bottomAssignmnet.setOnClickListener(view->gotoStore());

		binding.orders.setOnClickListener(view->{
			Intent intent = new Intent(ProductUnavailableItemActivity.this, OrderListingActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(intent);
		});

		binding.searchProducts.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				if(s.toString().trim().length() > 2) {
					setFlag = true;
					ObjectAnimator animation = ObjectAnimator.ofInt(binding.progressBar, "progress", 0, 500); // see this max value coming back here, we animate towards that value
					animation.setDuration(5000); // in milliseconds
					animation.setInterpolator(new DecelerateInterpolator());
					animation.start();
					binding.progressBar.setVisibility(View.VISIBLE);
					binding.productLayout.setVisibility(View.GONE);
					searchProducts(s.toString());
				}
				if(setFlag && s.length() < 3) {
					setFlag = false;
					//					DialogUtils.showLoader(ProductUnavailableItemActivity.this);
					loadProductList();
				}
			}
		});
	}

	private void searchProducts(String text) {
		viewModel.getSearchProductList(ProductUnavailableItemActivity.this, text).observe(ProductUnavailableItemActivity.this, productUnavailableResponse->{
			binding.progressBar.clearAnimation();
			binding.progressBar.setVisibility(View.GONE);
			binding.productLayout.setVisibility(View.VISIBLE);
			if(!productUnavailableResponse.getError().equalsIgnoreCase(kFalse)) {
				Utils.showSnack(binding.getRoot(), productUnavailableResponse.getMessage());
			} else {
				if(productUnavailableResponse.getData().getProducts().isEmpty()) {
					setVisibility(false);
				} else {
					setVisibility(true);
					productList = productUnavailableResponse.getData().getProducts();
					productAdapter = new ProductUnAvailableItemAdapter(ProductUnavailableItemActivity.this, productList, position->productPosition = position);
					binding.recyclerProducts.setAdapter(productAdapter);
				}
			}
		});
	}

	private void loadProductList() {
		productPosition = -1;
		pagedAdapter.deselectProduct();
		binding.recyclerProducts.setAdapter(pagedAdapter);
		viewModel.getProductsList(this, i->setVisibility(i != 0), (i, value)->{
			if(i == -1) {
				setVisibility(false);
				Utils.showSnack(binding.getRoot(), value);
			} else {
				setVisibility(true);
			}
		});
		viewModel.pagedListLiveData.observe(this, products->{
			DialogUtils.dismissLoader();
			 productPosition = -1;
			pagedAdapter.submitList(products);
		});
	}

	private void setVisibility(boolean value) {
		if(value) {
			binding.recyclerProducts.setVisibility(View.VISIBLE);
			binding.requestUnavailableItem.setVisibility(View.VISIBLE);
			binding.noProduct.setVisibility(View.GONE);
		} else {
			binding.recyclerProducts.setVisibility(View.GONE);
			binding.requestUnavailableItem.setVisibility(View.GONE);
			binding.noProduct.setVisibility(View.VISIBLE);
		}
	}

	private void gotoDashboard() {
		Intent intent = new Intent(ProductUnavailableItemActivity.this, DashboardActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
	}

	private void gotoStore() {
		Intent intent = new Intent(ProductUnavailableItemActivity.this, ProductAddItemsToStoreActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
	}

	private void backPressed() {
		super.onBackPressed();
	}
}