package com.app.a3store.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3store.BaseActivity;
import com.app.a3store.R;
import com.app.a3store.constants.OrderConstants;
import com.app.a3store.constants.UrlConstants;
import com.app.a3store.databinding.ActivityDashboardBinding;
import com.app.a3store.dialogs.DialogUtils;
import com.app.a3store.model.dashboard.DashboardResponse;
import com.app.a3store.model.orders.OrderInfo;
import com.app.a3store.ui.adapter.HomeActionNeedAdapter;
import com.app.a3store.ui.adapter.HomeOrderListAdapter;
import com.app.a3store.utils.CustomBarChartRenderer;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;
import com.app.a3store.viewmodel.DashboardViewModel;
import com.app.a3store.viewmodel.OrdersViewModel;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

import static com.app.a3store.constants.AppConstants.kAction;
import static com.app.a3store.constants.AppConstants.kActive;
import static com.app.a3store.constants.AppConstants.kFalse;
import static com.app.a3store.constants.AppConstants.kInactive;
import static com.app.a3store.constants.IntentConstants.lowStock;
import static com.app.a3store.constants.IntentConstants.outOfStock;

public class DashboardActivity extends BaseActivity {

    HomeActionNeedAdapter homeActionNeedAdapter;
    HomeOrderListAdapter homeOrderListAdapter;
    SharedHelper sharedHelper;
    DashboardViewModel viewModel;
    OrdersViewModel ordersViewModel;
    ActivityDashboardBinding binding;
    OrderConstants.StatusType statusType;
    OrderConstants.StoreStatus storeStatus = OrderConstants.StoreStatus.ON;
    private boolean isResume = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title.
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard);
        initViews();
        initChart();
        setListeners();
        updateFirebaseToken();
        showAllowSoundAlert();
    }

    private void showAllowSoundAlert() {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            if (!sharedHelper.isShowSoundDialog())
                new AlertDialog.Builder(this)
                        .setTitle(R.string.enable_sound)
                        .setMessage(R.string.enable_sound_content)
                        .setPositiveButton(R.string.allow_sound, (dialogInterface, i) -> {

                            Intent settingsIntent = null;
                            settingsIntent = new Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                    .putExtra(Settings.EXTRA_APP_PACKAGE, getPackageName())
                                    .putExtra(Settings.EXTRA_CHANNEL_ID, "Notification_Sound");

                            startActivity(settingsIntent);

                            sharedHelper.setShowSoundDialog(true);
                        })
                        .setNegativeButton(R.string.skip, (dialogInterface, i) -> {
                            sharedHelper.setShowSoundDialog(true);
                            dialogInterface.dismiss();
                        }).show();
        }
    }

    public void initViews() {
        sharedHelper = new SharedHelper(this);
        viewModel = new ViewModelProvider(this).get(DashboardViewModel.class);
        ordersViewModel = new ViewModelProvider(this).get(OrdersViewModel.class);
        homeOrderListAdapter = new HomeOrderListAdapter(this, viewModel, position -> {
            DashboardResponse.Data.Order orderInfo = viewModel.ordersList.get(position);
            if (orderInfo != null) {
                DialogUtils.showLoader(DashboardActivity.this);
                updateOrderStatus(orderInfo, UrlConstants.statusAccepted);
            }
        }, position -> {
            DashboardResponse.Data.Order orderInfo = viewModel.ordersList.get(position);
            if (orderInfo != null) {
                DialogUtils.showLoader(DashboardActivity.this);
                updateOrderStatus(orderInfo, UrlConstants.statusRejected);
            }
        }, position -> {
            DashboardResponse.Data.Order orderInfo = viewModel.ordersList.get(position);
            if (orderInfo != null) {
                gotoTemplateOne(orderInfo.getOrderId());
            }
        }, position -> {
            //Ready For Pickup Status Update
            DashboardResponse.Data.Order orderInfo = viewModel.ordersList.get(position);
            if (orderInfo != null) {
                DialogUtils.showLoader(DashboardActivity.this);
                updateOrderStatus(orderInfo, UrlConstants.statusPickup);
            }
        }, position -> {
            //View Ready For Pickup detail page
            DashboardResponse.Data.Order orderInfo = viewModel.ordersList.get(position);
            if (orderInfo != null) {
                gotoTemplateTwo(orderInfo.getOrderId());
            }
        });
        binding.recyclerOrderList.setAdapter(homeOrderListAdapter);

        binding.name.setText(String.format("%s %s %s", getString(R.string.hi), sharedHelper.getFirstName(), sharedHelper.getLastName()));
        getDashboardInfo();
    }

    private void setListeners() {
        binding.name.setOnClickListener(view -> {
            Intent intent = new Intent(DashboardActivity.this, ProfileActivity.class);
            intent.putExtra("status", statusType);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, 100);
//			startActivity(intent);
        });

        binding.chartView.setOnClickListener(view -> {
//            Intent intent = new Intent(DashboardActivity.this, EarningsActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            startActivityForResult(intent, 100);
//			startActivity(intent);
        });
        binding.imgNotification.setOnClickListener(view -> {
            Intent intent = new Intent(this, NotificationActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, 100);
//			startActivity(intent);
        });

        binding.onGoingOrderSeeAll.setOnClickListener(view -> {
            Intent intent = new Intent(DashboardActivity.this, OrderListingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, 100);
//			startActivity(intent);
        });

        binding.bottomAssignmnet.setOnClickListener(view -> {
            Intent intent = new Intent(DashboardActivity.this, ProductAddItemsToStoreActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, 100);
//			startActivity(intent);
        });

        binding.orders.setOnClickListener(view -> {
            Intent intent = new Intent(DashboardActivity.this, OrderListingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, 100);
//			startActivity(intent);
        });
        binding.radioOn.setOnClickListener(view -> {

            updateStoreStatus(kActive);
//            if (checkedId == R.id.radioOn) {
//
//            } else if (checkedId == R.id.radioOff) {
//
//            }

        });

        binding.radioOff.setOnClickListener(view -> {
            updateStoreStatus(kInactive);
        });
    }

    private void updateStoreStatus(String status) {
        DialogUtils.showLoader(this);
        viewModel.updateStoreStatus(this, status).observe(this, commonResponse -> {
            DialogUtils.dismissLoader();
            if (commonResponse.getError().equalsIgnoreCase(kFalse)) {
                Utils.showSnack(binding.getRoot(), getString(R.string.store_status_success));

                if (storeStatus == OrderConstants.StoreStatus.ON) {
                    storeStatus = OrderConstants.StoreStatus.OFF;
                } else {
                    storeStatus = OrderConstants.StoreStatus.ON;
                }
            } else {
                if (storeStatus == OrderConstants.StoreStatus.ON) binding.radioOn.setChecked(true);
                else binding.radioOff.setChecked(true);
                Utils.showSnack(binding.getRoot(), commonResponse.getMessage());
            }
        });
    }

    private void initChart() {

        //Customizing Bar Graph
        CustomBarChartRenderer chartRenderer = new CustomBarChartRenderer(binding.barChart, binding.barChart.getAnimator(), binding.barChart.getViewPortHandler());
        chartRenderer.mRadius = 15;
        binding.barChart.setRenderer(chartRenderer);
        binding.barChart.setPinchZoom(false);

        ArrayList<BarEntry> barOrderList = new ArrayList<>();

        for (int i = 1; i <= 5; i++) {
            BarEntry barEntry = new BarEntry((float) i, (float) i);
            barOrderList.add(barEntry);
        }

        Legend l = binding.barChart.getLegend();
        l.setEnabled(false);

        YAxis leftAxis = binding.barChart.getAxisLeft();
        leftAxis.setDrawGridLines(false);
        leftAxis.setTextColor(getResources().getColor(R.color.nobel));
        leftAxis.setDrawZeroLine(true);

        binding.barChart.getAxisRight().setDrawZeroLine(true);
        binding.barChart.getXAxis().setDrawGridLines(false);
        binding.barChart.getAxisRight().setDrawLabels(false);
        binding.barChart.getAxisLeft().setDrawGridLines(false);

        binding.barChart.setDrawGridBackground(false);
        binding.barChart.setDrawValueAboveBar(false);
        binding.barChart.setPinchZoom(false);
        binding.barChart.setDrawBorders(false);
        binding.barChart.animateY(2000);

        BarDataSet set1;

        //		float groupSpace = 0.54f; float barSpace = 0.03f; // 2 DataSet

        float barWidth = 0.2f;   //((0.2)*2)+(0.54 + (2 * 0.03) = 1

        if (binding.barChart.getData() != null && binding.barChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) binding.barChart.getData().getDataSetByIndex(0);

            set1.setValues(barOrderList);
            binding.barChart.getData().notifyDataChanged();
        } else {
            // create 2 DataSets
            set1 = new BarDataSet(barOrderList, "");
            set1.setColor(getResources().getColor(R.color.colorPrimary));
            set1.setDrawValues(false);

            BarData data = new BarData(set1);
            binding.barChart.setData(data);
        }

        // specify the width each bar should have
        binding.barChart.getBarData().setBarWidth(barWidth);

        binding.barChart.setDrawValueAboveBar(false);
        binding.barChart.getDescription().setEnabled(false);

        binding.barChart.getXAxis().setDrawGridLines(false);
        binding.barChart.getXAxis().setDrawAxisLine(false);

        binding.barChart.getAxisLeft().setDrawAxisLine(false);
        binding.barChart.getAxisLeft().setDrawGridLines(false);

        binding.barChart.getAxisRight().setDrawAxisLine(false);
        binding.barChart.getAxisRight().setDrawGridLines(false);

        binding.barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        binding.barChart.getXAxis().setTextColor(getResources().getColor(R.color.nobel));

        binding.barChart.getXAxis().setCenterAxisLabels(false);

        binding.barChart.getXAxis().setAvoidFirstLastClipping(false);

        binding.barChart.setDoubleTapToZoomEnabled(false);

        binding.barChart.setTouchEnabled(false);
        binding.barChart.setHighlightFullBarEnabled(false);

        binding.barChart.getDescription().setEnabled(false);
        binding.barChart.setPadding(2, 2, 0, 0);
        binding.barChart.setScaleEnabled(false);
        binding.barChart.getAxisRight().setDrawZeroLine(true);
        binding.barChart.getXAxis().setDrawLimitLinesBehindData(true);

        binding.barChart.getXAxis().setDrawGridLines(false);
        LimitLine limitLine = new LimitLine(0f);
        limitLine.setLineColor(getResources().getColor(R.color.nobel));

        binding.barChart.getXAxis().addLimitLine(limitLine);
        //        barChart.getXAxis().setLabelCount(6, true);
        binding.barChart.getXAxis().setGranularity(1f);
        binding.barChart.getXAxis().setAxisMinimum(0f);
        binding.barChart.getXAxis().setAxisMaximum(6f);

        binding.barChart.getAxisRight().setLabelCount(6, true);
        binding.barChart.getAxisLeft().setLabelCount(6, true);

        binding.barChart.getAxisLeft().setGranularity(1f);
        binding.barChart.getAxisLeft().setAxisMinimum(0f);
        binding.barChart.getAxisRight().setGranularity(1f);
        binding.barChart.getAxisRight().setAxisMinimum(0f);

        binding.barChart.setDrawBorders(false);
        binding.barChart.setBorderColor(getResources().getColor(R.color.nobel));
        binding.barChart.setFitBars(true);
        binding.barChart.invalidate();
    }

    private void getDashboardInfo() {
//		DialogUtils.showLoader(this);
        viewModel.getDashboardInfo(this).observe(this, response -> {
            DialogUtils.dismissLoader();
            if (!response.getError().equalsIgnoreCase(kFalse)) {
                Utils.showSnack(binding.getRoot(), response.getMessage());
            } else {

                if (response.getData().getNotificationCount() == 0) {
                    binding.notificationCount.setVisibility(View.GONE);
                    binding.imgNotificationDot.setVisibility(View.GONE);
                    sharedHelper.setNcount(String.valueOf(0));
                } else {
                    binding.notificationCount.setVisibility(View.VISIBLE);
                    binding.imgNotificationDot.setVisibility(View.VISIBLE);
                    sharedHelper.setNcount(String.valueOf(response.getData().getNotificationCount()));
                    binding.notificationCount.setText(response.getData().getNotificationCount() + "");
                }
                sharedHelper.setProfileStatus(response.getData().getProfile().getStatus().equalsIgnoreCase(kActive));
                if (response.getData().getStockProduct().getOutOfStock() != null && response.getData().getStockProduct().getOutOfStock() <= 0) {
                    binding.actionsNeededTitle.setVisibility(View.GONE);
                    binding.recyclerActionsNeeded.setVisibility(View.GONE);
                } else {
                    homeActionNeedAdapter = new HomeActionNeedAdapter(this, 0, response.getData().getStockProduct().getOutOfStock(), position -> {
//						if(position == 0) {
//							openMyStore(true);
//						} else if(position == 1) {
//							openMyStore(false);
//						}
                        openMyStore(false);
                    });
                    binding.recyclerActionsNeeded.setAdapter(homeActionNeedAdapter);
                    binding.actionsNeededTitle.setVisibility(View.VISIBLE);
                    binding.recyclerActionsNeeded.setVisibility(View.VISIBLE);
                }
                if (response.getData().getStore().getStatus().equalsIgnoreCase(kActive)) {
                    binding.radioOn.setChecked(true);
                    storeStatus = OrderConstants.StoreStatus.ON;
                    statusType = OrderConstants.StatusType.active;
                } else {
                    binding.radioOff.setChecked(true);
                    storeStatus = OrderConstants.StoreStatus.OFF;
                    binding.groupOnGoingOrders.setVisibility(View.GONE);
                    statusType = OrderConstants.StatusType.inactive;
                }

                if (response.getData().getOrders().size() > 0) {
                    viewModel.ordersList = response.getData().getOrders();
                    homeOrderListAdapter.notifyDataSetChanged();
                    binding.groupOnGoingOrders.setVisibility(View.VISIBLE);
                } else {
                    binding.groupOnGoingOrders.setVisibility(View.GONE);
                }
            }
        });
//		ordersViewModel.getOngoingOrders(this).observe(this, response->{
//			DialogUtils.dismissLoader();
//			if(response.getError().equalsIgnoreCase("true")) {
//				Utils.showSnack(binding.getRoot(), response.getMessage());
//			} else {
//				if(response.getData().getOrderList().size() > 0) {
//					viewModel.ordersList = response.getData().getOrderList();
//					homeOrderListAdapter.notifyDataSetChanged();
//					binding.groupOnGoingOrders.setVisibility(View.VISIBLE);
//				} else {
//					binding.groupOnGoingOrders.setVisibility(View.GONE);
//				}
//			}
//		});
    }

    private void openMyStore(boolean isLowStock) {
        Intent intent = new Intent(this, ActionStoreActivity.class);
        if (isLowStock) intent.putExtra(kAction, lowStock);
        else intent.putExtra(kAction, outOfStock);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 100);
    }

    private void updateFirebaseToken() {
        Log.d("dkhvdsb","jhd"+sharedHelper.getFirebaseToken());
        if (sharedHelper.getFirebaseToken() != null && !sharedHelper.getFirebaseToken().isEmpty()) {
            viewModel.updateDeviceToken(this, UrlConstants.getOS, sharedHelper.getFirebaseToken(), sharedHelper.getUserId()).observe(this, updateDeviceTokenResponse -> {
                DialogUtils.dismissLoader();
                if (updateDeviceTokenResponse.getError().equalsIgnoreCase("true")) {
                    Utils.showSnack(binding.getRoot(), updateDeviceTokenResponse.getMessage());
                }
            });
        }
    }

    private void updateOrderStatus(DashboardResponse.Data.Order orderInfo, String status) {
        if (orderInfo != null) {
            ordersViewModel.updateOrderStatus(DashboardActivity.this, orderInfo.getOrderId(), status).observe(this, response -> {
                DialogUtils.dismissLoader();
                if (response.getError().equalsIgnoreCase("true")) {
                    Utils.showSnack(binding.getRoot(), response.getMessage());
                } else {
                    getDashboardInfo();
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//		DialogUtils.showLoader(this);
//		onGoingOrders();
        getDashboardInfo();
        if (!sharedHelper.getFirstName().isEmpty() || !sharedHelper.getLastName().isEmpty()) {
            binding.name.setText(String.format("%s %s %s", getString(R.string.hi), sharedHelper.getFirstName(), sharedHelper.getLastName()));
           // binding.name.setText(String.format("Hi, %s %s", sharedHelper.getFirstName(), sharedHelper.getLastName()));
        }
    }

    private void gotoTemplateOne(int id) {
        Intent intent = new Intent(DashboardActivity.this, OrderDetailOngoingOrder.class);
        intent.putExtra("id", id);
        intent.putExtra("orderType", OrderConstants.OrderType.onGoing);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 100);
    }

    private void gotoTemplateTwo(int id) {
        Intent intent = new Intent(this, OrderDetailReadyForPickup.class);
        intent.putExtra("id", id);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 100);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            getDashboardInfo();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getDashboardInfo();
    }
}