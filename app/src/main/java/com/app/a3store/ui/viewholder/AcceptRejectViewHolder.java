package com.app.a3store.ui.viewholder;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.model.dashboard.DashboardResponse;
import com.app.a3store.model.orders.OrderInfo;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.utils.TimeCalculator;

public class AcceptRejectViewHolder extends RecyclerView.ViewHolder {

	public TextView orderId, orderedOn, deliveredOn, accept, reject;
	private ItemClickListener acceptClickListener;
	private ItemClickListener rejectClickListener;
	private ItemClickListener viewClickListener;

	public AcceptRejectViewHolder(View itemView) {
		super(itemView);

		this.orderId = itemView.findViewById(R.id.orderIdVal);
		this.orderedOn = itemView.findViewById(R.id.orderOnVal);
		this.deliveredOn = itemView.findViewById(R.id.deliverByVal);
		this.accept = itemView.findViewById(R.id.accept);
		this.reject = itemView.findViewById(R.id.reject);
	}

	public void bindData(Context context, DashboardResponse.Data.Order data, int position, ItemClickListener mAcceptClickListener, ItemClickListener mRejectClickListener, ItemClickListener mViewClickListener) {
		this.acceptClickListener = mAcceptClickListener;
		this.rejectClickListener = mRejectClickListener;
		this.viewClickListener = mViewClickListener;
		this.orderId.setText(data.getOrderIDs());
		if(data.getOrderOn() != null) {
			String orderDate = TimeCalculator.getTime(data.getOrderOn()) + " | " + TimeCalculator.getDate(data.getOrderOn());
			this.orderedOn.setText(orderDate);
		}
		if(data.getDeliveryDate() != null) {
			String orderDate = TimeCalculator.getTimeSlot(data.getFromTime()) +"-"+TimeCalculator.getTimeSlot(data.getToTime()) + " | " + TimeCalculator.getDate(data.getDeliveryDate());
			this.deliveredOn.setText(orderDate);
		}

		this.accept.setText(context.getString(R.string.accept_order));
		this.reject.setText(context.getString(R.string.reject_order));

		this.accept.setOnClickListener(view->acceptClickListener.onItemClick(position));
		this.reject.setOnClickListener(view->rejectClickListener.onItemClick(position));

		itemView.setOnClickListener(view->viewClickListener.onItemClick(position));
	}
}
