package com.app.a3store.ui.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3store.BaseActivity;
import com.app.a3store.R;
import com.app.a3store.databinding.ActivityCurrentStockBinding;
import com.app.a3store.databinding.DialogAddNewVendorBinding;
import com.app.a3store.dialogs.DialogUtils;
import com.app.a3store.model.stock.Vendor;
import com.app.a3store.ui.adapter.StockHistoryAdapter;
import com.app.a3store.ui.adapter.VendorListAdapter;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;
import com.app.a3store.viewmodel.StockViewModel;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.app.a3store.constants.AppConstants.kFalse;

public class CurrentStockActivity extends BaseActivity {

	ActivityCurrentStockBinding currentStockBinding;
	StockHistoryAdapter stockHistoryAdapter;
	ArrayAdapter<String> operationAdapter;
	VendorListAdapter vendorAdapter;
	StockViewModel stockViewModel;
	int year, month, date;
	private Dialog dialog;
	DialogAddNewVendorBinding vendorBinding;
	int productId, vendorId;
	long storeStock;
	List<Vendor> vendorList;
	SharedHelper sharedHelper;
	String operationValue;
	String[] operationTypes;
	String[] operationTypes1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		currentStockBinding = DataBindingUtil.setContentView(this, R.layout.activity_current_stock);
		stockViewModel = new ViewModelProvider(this).get(StockViewModel.class);
		currentStockBinding.setStockViewModel(stockViewModel);
		productId = getIntent().getIntExtra("productId", 0);
		storeStock = getIntent().getLongExtra("storeStock", 0);
		sharedHelper = new SharedHelper(this);
		initViews();
		loadVendors();
		setAdapters();
		setListeners();
	}

	@Override
	public void onBackPressed() {
		closeActivity();
	}

	public void closeActivity(){
		Intent intent = new Intent();
		intent.putExtra("storeStock", storeStock);
		setResult(2, intent);
		finish();
	}
	private void initViews() {
		currentStockBinding.name.setText(String.format("%s %s %s", this.getString(R.string.hi), sharedHelper.getFirstName(), sharedHelper.getLastName()));
		currentStockBinding.currentStockValue.setText(String.format(Locale.ENGLISH,"%s %s", storeStock, this.getString(R.string.units)));
		if(sharedHelper.getNcount().equalsIgnoreCase("0")){
			currentStockBinding.notificationCount.setVisibility(View.GONE);
			currentStockBinding.imgNotificationDot.setVisibility(View.GONE);
		}
		else {
			currentStockBinding.notificationCount.setText(sharedHelper.getNcount());
		}
	}

	private void loadVendors() {
		stockViewModel.getVendorList(this).observe(this, storeVendorResponse->{
			Vendor chooseVendor = new Vendor(getString(R.string.choose_vendor), 0);
			Vendor other = new Vendor(getString(R.string.other), 0);
			storeVendorResponse.getData().getVendor().add(0, chooseVendor);
			storeVendorResponse.getData().getVendor().add(storeVendorResponse.getData().getVendor().size(), other);
			vendorList = storeVendorResponse.getData().getVendor();
			if(!storeVendorResponse.getError().equalsIgnoreCase(kFalse)) {
				Utils.showSnack(currentStockBinding.getRoot(), storeVendorResponse.getMessage());
			}
			setVendorAdapter();
		});
	}

	private void setVendorAdapter() {
		vendorAdapter = new VendorListAdapter(this, vendorList);
		vendorAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
		currentStockBinding.chooseVendor.setAdapter(vendorAdapter);
		currentStockBinding.chooseVendor.setOnItemSelectedListener(listener);
		currentStockBinding.vendorArrow.setOnClickListener(v->currentStockBinding.chooseVendor.performClick());
	}

	private void setAdapters() {
		stockViewModel.getStockHistory(this, productId).observe(this, stockHistoryResponse->{
			if(stockHistoryResponse.getError().equalsIgnoreCase(kFalse)) {
				if(stockHistoryResponse.getData().getProduct().isEmpty()) {
					currentStockBinding.stockHistoryGroup.setVisibility(View.GONE);
				} else {
					currentStockBinding.stockHistoryGroup.setVisibility(View.VISIBLE);
					stockHistoryAdapter = new StockHistoryAdapter(this, stockHistoryResponse.getData().getProduct());
					currentStockBinding.recyclerStockHistory.setAdapter(stockHistoryAdapter);
				}
			} else {
				Utils.showSnack(currentStockBinding.getRoot(), stockHistoryResponse.getMessage());
			}
		});
	}

	private void setListeners() {
		setOperationAdapter();
		currentStockBinding.bottomDashboard.setOnClickListener(view->gotoDashboard());
		currentStockBinding.bottomAssignmnet.setOnClickListener(view->gotoStore());

		currentStockBinding.imgNotification.setOnClickListener(view->{
			Intent intent = new Intent(this, NotificationActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(intent);
		});

		currentStockBinding.name.setOnClickListener(view->{
			Intent intent = new Intent(this, ProfileActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(intent);
		});

		currentStockBinding.orders.setOnClickListener(view->{
			Intent intent = new Intent(CurrentStockActivity.this, OrderListingActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(intent);
		});
		currentStockBinding.expiryDate.setOnClickListener(v->openDatePicker());

		currentStockBinding.makeEntry.setOnClickListener(v->{
			if(currentStockBinding.operation.getSelectedItemPosition() == 0) {
				Utils.showSnack(currentStockBinding.getRoot(), this.getString(R.string.operation_error));
			} else if(stockViewModel.units == null || stockViewModel.units.isEmpty()) {
				Utils.showSnack(currentStockBinding.getRoot(), this.getString(R.string.unit_error));
			} else if(currentStockBinding.operation.getSelectedItemPosition() == 2 && Integer.parseInt(stockViewModel.units) > storeStock) {
				Utils.showSnack(currentStockBinding.getRoot(), this.getString(R.string.unit_exceed_error));
			} else if(stockViewModel.expiryDate.trim().isEmpty()) {
				Utils.showSnack(currentStockBinding.getRoot(), this.getString(R.string.expiry_error));
			} else if(currentStockBinding.chooseVendor.getSelectedItemPosition() == 0) {
				Utils.showSnack(currentStockBinding.getRoot(), this.getString(R.string.vendor_error));
			} else {
				DialogUtils.showLoader(this);
				stockViewModel.updateStoreStock(this, vendorList.get(currentStockBinding.chooseVendor.getSelectedItemPosition()).getId(), productId, operationValue, stockViewModel.expiryDate.trim(), storeStock).observe(this, commonResponse->{
					DialogUtils.dismissLoader();
					if(commonResponse.getError().equalsIgnoreCase(kFalse)) {
						Utils.showSnack(currentStockBinding.getRoot(), this.getString(R.string.update_stock_message));
						setAdapters();
						resetValues();
						closeActivity();
					} else {
						Utils.showSnack(currentStockBinding.getRoot(), commonResponse.getMessage());
					}
				});
			}
		});
	}

	private void setOperationAdapter() {
		if(storeStock > 0) {
			operationTypes = new String[] {getString(R.string.operation), getString(R.string.add), getString(R.string.subtract_or_discount)};
			operationTypes1 = new String[] {"Operation", "ADD", "Subtract or discount"};

		} else {
			operationTypes = new String[] {getString(R.string.operation), getString(R.string.add)};
			operationTypes1 = new String[] {"Operation", "ADD"};
		}
		operationAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, operationTypes);
		operationAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
		currentStockBinding.operation.setAdapter(operationAdapter);
		currentStockBinding.operation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				operationValue = operationTypes1[position];
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
		currentStockBinding.operationArrow.setOnClickListener(v->currentStockBinding.operation.performClick());
	}

	private void resetValues() {
		if(currentStockBinding.operation.getSelectedItemPosition() == 1)
			storeStock += Integer.parseInt(stockViewModel.units);
		else if(currentStockBinding.operation.getSelectedItemPosition() == 2)
			storeStock -= Integer.parseInt(stockViewModel.units);
		setOperationAdapter();
		stockViewModel.units = "";
		currentStockBinding.units.clearFocus();
		currentStockBinding.expiryDate.setText(this.getString(R.string.enter_expiry_date));
		currentStockBinding.chooseVendor.setSelection(0);
		currentStockBinding.currentStockValue.setText(String.format(Locale.ENGLISH,"%s %s", storeStock, this.getString(R.string.units)));
	}

	private void openDatePicker() {
		//Locale.setDefault(Locale.ENGLISH);

		Calendar cal = Calendar.getInstance(Locale.ENGLISH);
		if(stockViewModel.expiryDate.trim().isEmpty()) {
			year = cal.get(Calendar.YEAR);
			month = cal.get(Calendar.MONTH);
			date = cal.get(Calendar.DAY_OF_MONTH);
		} else {
			year = stockViewModel.year;
			month = stockViewModel.month;
			date = stockViewModel.date;
		}

		DatePickerDialog dialog = new DatePickerDialog(this, R.style.DialogTheme, (datePicker, year, month, day)->{
			stockViewModel.date = day;
			stockViewModel.month = month;
			stockViewModel.year = year;
			stockViewModel.expiryDate = stockViewModel.year + "-" + (stockViewModel.month + 1) + "-" + stockViewModel.date;
			currentStockBinding.expiryDate.setText(stockViewModel.expiryDate);
		}, year, month, date);
		dialog.show();
		dialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
		dialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.white));
		dialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.dove_gray));
	}

	private final AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			if(position == vendorList.size() - 1) addVendor(CurrentStockActivity.this);
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
		}
	};

	public void addVendor(Context context) {
		dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		vendorBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_add_new_vendor, null, false);
		dialog.setContentView(vendorBinding.getRoot());
		if(dialog.getWindow() != null) {
			dialog.getWindow().setGravity(Gravity.CENTER);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
			ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
			InsetDrawable inset = new InsetDrawable(back, 20);
			dialog.getWindow().setBackgroundDrawable(inset);
		}
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
		vendorBinding.setStockViewModel(stockViewModel);
		vendorBinding.closeIcon.setOnClickListener(v->dialogClose());
		vendorBinding.addVendor.setOnClickListener(v->{
			if(stockViewModel.vendorName.trim().isEmpty()) {
				Utils.showSnack(vendorBinding.getRoot(), context.getString(R.string.enter_vendor_name));
			} else {
				DialogUtils.showLoader(context);
				stockViewModel.addStoreVendor(context).observe(this, addVendorResponse->{
					DialogUtils.dismissLoader();
					if(addVendorResponse.getError().equalsIgnoreCase(kFalse)) {
						Vendor newVendor = new Vendor(stockViewModel.vendorName.trim(), addVendorResponse.getData().getId());
						vendorList.add(1, newVendor);
						setVendorAdapter();
						currentStockBinding.chooseVendor.setSelection(1);
						vendorId = addVendorResponse.getData().getId();
						dialogClose();
					} else {
						Utils.showSnack(vendorBinding.getRoot(), addVendorResponse.getMessage());
					}
				});
			}
		});
		dialog.setOnDismissListener(dialog1->{
			if(currentStockBinding.chooseVendor.getSelectedItemPosition() == vendorList.size() - 1)
				currentStockBinding.chooseVendor.setSelection(0);
		});
	}

	private void dialogClose() {
		if(dialog != null) {
			dialog.cancel();
			dialog.hide();
		}
	}

	private void gotoDashboard() {
		Intent intent = new Intent(CurrentStockActivity.this, DashboardActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}

	private void gotoStore() {
		Intent intent = new Intent(CurrentStockActivity.this, ProductAddItemsToStoreActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}
}