package com.app.a3store.ui.paging.notificationList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.databinding.RowNotficationItemBinding;
import com.app.a3store.model.NotificationResponse;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.utils.TimeCalculator;
import com.app.a3store.utils.Utils;

import java.util.Objects;

public class NotificationPagedAdapter extends PagedListAdapter<NotificationResponse.Data.Notification, NotificationPagedAdapter.ViewHolder> {

	private final Context mContext;
	private final ItemClickListener clickListener;

	public NotificationPagedAdapter(Context context, ItemClickListener clickListener) {
		super(DIFF_CALLBACK);
		this.mContext = context;
		this.clickListener = clickListener;
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {

		RowNotficationItemBinding binding;

		public ViewHolder(View itemView) {
			super(itemView);
			binding = RowNotficationItemBinding.bind(itemView);
		}
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
		View listItem = layoutInflater.inflate(R.layout.row_notfication_item, parent, false);
		return new ViewHolder(listItem);
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
		NotificationResponse.Data.Notification notification = getItem(holder.getAdapterPosition());
		holder.binding.notificationText.setText(notification.getNotifyMessage());
		holder.binding.date.setText(TimeCalculator.getNotificationDate(notification.getCreatedAt()));
	}

	private static final DiffUtil.ItemCallback<NotificationResponse.Data.Notification> DIFF_CALLBACK = new DiffUtil.ItemCallback<NotificationResponse.Data.Notification>() {
		@Override
		public boolean areItemsTheSame(NotificationResponse.Data.Notification oldItem, NotificationResponse.Data.Notification newItem) {
			return oldItem.getId().equals(newItem.getId());
		}

		@Override
		public boolean areContentsTheSame(@NonNull NotificationResponse.Data.Notification oldItem, @NonNull NotificationResponse.Data.Notification newItem) {
			return Objects.equals(oldItem, newItem);
		}
	};
}
