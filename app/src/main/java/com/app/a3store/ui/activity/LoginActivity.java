package com.app.a3store.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3store.BaseActivity;
import com.app.a3store.R;
import com.app.a3store.databinding.ActivityLoginBinding;
import com.app.a3store.dialogs.DialogUtils;
import com.app.a3store.model.login.LoginData;
import com.app.a3store.ui.fragment.OTPVerificationFragment;
import com.app.a3store.utils.SessionManager;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;
import com.app.a3store.viewmodel.LoginViewModel;

public class LoginActivity extends BaseActivity {

	LoginViewModel viewModel;
	ActivityLoginBinding activityLoginBinding;
	SharedHelper sharedHelper;
	Handler handler = new Handler(Looper.getMainLooper());
	Runnable runnable;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
		viewModel = new ViewModelProvider(this).get(LoginViewModel.class);
		sharedHelper = new SharedHelper(this);
		/*if(sharedHelper.getSelectedLanguage().equalsIgnoreCase(kEnglish)) {
			activityLoginBinding.imageView3.setScaleX(1);
		} else {
			activityLoginBinding.imageView3.setScaleX(-1);
		}*/
		initListener();
	}

	private void initListener() {

		activityLoginBinding.next.setOnClickListener((view)->{

			if(activityLoginBinding.mobileNumber.getText().toString().length() >= 6) {

				DialogUtils.showLoader(this);
				viewModel.login(this, activityLoginBinding.countryCodePicker.getSelectedCountryCode(), activityLoginBinding.mobileNumber.getText().toString()).observe(this, loginResponse->{

					if(loginResponse.getError().equalsIgnoreCase("true")) {
						DialogUtils.dismissLoader();
						Utils.showSnack(findViewById(android.R.id.content), loginResponse.getMessage());
					} else {
						if(loginResponse.getData() != null && loginResponse.getData().isStoreExists().equalsIgnoreCase("true")) {
//							requestFirebaseVerificationCode(loginResponse.getData());
							navigateToVerification(loginResponse.getData());

						} else {
							navigateToVerification(loginResponse.getData());
//							requestFirebaseVerificationCode(loginResponse.getData());
							DialogUtils.dismissLoader();
							Utils.showSnack(findViewById(android.R.id.content), loginResponse.getMessage());
						}
					}
				});
			} else {
				Utils.showSnack(activityLoginBinding.parentLayout, getString(R.string.enter_mobile_number));
			}
		});

		activityLoginBinding.changeLanguage.setOnClickListener(view->{

			if(sharedHelper.getSelectedLanguage().equals("en")) {
				sharedHelper.setSelectedLanguage("ar");
			} else {
				sharedHelper.setSelectedLanguage("en");
			}
			Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(intent);
		});
	}

	private void requestFirebaseVerificationCode(LoginData data) {
		viewModel.requestVerificationCode(this, "+" + activityLoginBinding.countryCodePicker.getSelectedCountryCode() + activityLoginBinding.mobileNumber.getText().toString()).observe(this, fireBaseVerificationResponse->{
			if(fireBaseVerificationResponse.getError().equalsIgnoreCase("true")) {
				DialogUtils.dismissLoader();
				Utils.showSnack(activityLoginBinding.parentLayout, fireBaseVerificationResponse.getMessage());
			} else {
				SessionManager.getInstance().setCountryCode(activityLoginBinding.countryCodePicker.getSelectedCountryCode());
				SessionManager.getInstance().setMobileNumber(activityLoginBinding.mobileNumber.getText().toString());
				SessionManager.getInstance().setLoginData(data);
//				navigateToVerification(fireBaseVerificationResponse.getVerificationCode());
			}
		});
	}

	private void navigateToVerification(LoginData data) {
		SessionManager.getInstance().setCountryCode(activityLoginBinding.countryCodePicker.getSelectedCountryCode());
		SessionManager.getInstance().setMobileNumber(activityLoginBinding.mobileNumber.getText().toString());
		SessionManager.getInstance().setLoginData(data);

		runnable = ()->{
			DialogUtils.dismissLoader();
			SessionManager.getInstance().setVerificationCode(data.getOtp());

			Utils.navigateToFragment(getSupportFragmentManager(), R.id.container, new OTPVerificationFragment());
		};

		handler.postDelayed(runnable, 3000);
	}
	private void userExists(LoginData data) {

		SharedHelper sharedHelper = new SharedHelper(this);
		sharedHelper.setUserId("" + data.getId());
		sharedHelper.setFirstName(data.getFirstName());
		sharedHelper.setLastName(data.getLastName());
		sharedHelper.setEmail(data.getEmail());
		sharedHelper.setLatitude(data.getLatitude());
		sharedHelper.setLongitude(data.getLongitude());
		sharedHelper.setCountryCode(data.getCountryCode());
		sharedHelper.setMobileNumber(data.getMobileNum());
		sharedHelper.setProfilePic(data.getProfilePic());
		sharedHelper.setAuthToken(data.getToken());
		sharedHelper.setLoggedIn(true);

		if(SessionManager.getInstance().isRegisterLaterFlow()) {
			SessionManager.getInstance().setRegisterLaterFlow(false);
			finish();
		} else {
			Intent intent = new Intent(this, DashboardActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(intent);
		}
	}
}
