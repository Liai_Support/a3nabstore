package com.app.a3store.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.constants.AppConstants;
import com.app.a3store.databinding.RowCategoryNameInfoBinding;
import com.app.a3store.model.ProductSubCategoryResponse;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.utils.helper.SharedHelper;

import java.util.List;

public class ProductCategoriesNameAdapter extends RecyclerView.Adapter<ProductCategoriesNameAdapter.ViewHolder> {

	private final Context mContext;
	private final List<ProductSubCategoryResponse.Data.ProductCategory> productCategoryList;
	ItemClickListener itemClickListener;
	int previousSubCategoryPosition = 0, selectedSubCategoryPosition = 0;

	public ProductCategoriesNameAdapter(Context context, List<ProductSubCategoryResponse.Data.ProductCategory> productCategoryList, ItemClickListener itemClickListener) {
		this.mContext = context;
		this.productCategoryList = productCategoryList;
		this.itemClickListener = itemClickListener;
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {

		RowCategoryNameInfoBinding binding;

		public ViewHolder(View itemView) {
			super(itemView);
			binding = RowCategoryNameInfoBinding.bind(itemView);
		}
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
		View listItem = layoutInflater.inflate(R.layout.row_category_name_info, parent, false);
		return new ViewHolder(listItem);
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
		if(position == previousSubCategoryPosition) {
			itemClickListener.onItemClick(position);
			holder.binding.subCategoryName.setBackground(ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.sub_category_selected, null));
			holder.binding.subCategoryName.setTextColor(mContext.getResources().getColor(R.color.white));
		} else {
			holder.binding.subCategoryName.setBackground(ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.sub_category_unselected, null));
			holder.binding.subCategoryName.setTextColor(mContext.getResources().getColor(R.color.dorado));
		}

		if(new SharedHelper(mContext).getSelectedLanguage().equalsIgnoreCase(AppConstants.kEnglish)) {
			holder.binding.subCategoryName.setText(productCategoryList.get(position).getProductSubCategoryName());
		}
		else {
			holder.binding.subCategoryName.setText(productCategoryList.get(position).getArabicName());
		}
		holder.binding.root.setOnClickListener(v->{
			selectedSubCategoryPosition = position;
			notifyItemChanged(selectedSubCategoryPosition);
			notifyItemChanged(previousSubCategoryPosition);
			previousSubCategoryPosition = selectedSubCategoryPosition;
		});
	}

	@Override
	public int getItemCount() {
		return productCategoryList.size();
	}
}
