package com.app.a3store.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.databinding.RowAddProductToStoreBinding;
import com.app.a3store.model.product.Product;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;

import java.util.List;
import java.util.Locale;

import static com.app.a3store.constants.AppConstants.kActive;

public class ProductAddItemsToStoreAdapter extends RecyclerView.Adapter<ProductAddItemsToStoreAdapter.ViewHolder> {

	private final Context mContext;
	private final ItemClickListener availableClickListener, unavailableClickListener, editClickListener, updateClickListener;
	private final List<Product> productList;

	public ProductAddItemsToStoreAdapter(Context context, List<Product> productList, ItemClickListener availableClickListener, ItemClickListener unavailableClickListener, ItemClickListener editClickListener, ItemClickListener updateClickListener) {
		this.mContext = context;
		this.productList = productList;
		this.availableClickListener = availableClickListener;
		this.unavailableClickListener = unavailableClickListener;
		this.editClickListener = editClickListener;
		this.updateClickListener = updateClickListener;
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {

		RowAddProductToStoreBinding binding;

		public ViewHolder(View itemView) {
			super(itemView);
			binding = RowAddProductToStoreBinding.bind(itemView);
		}
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
		View listItem = layoutInflater.inflate(R.layout.row_add_product_to_store, parent, false);
		return new ViewHolder(listItem);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {

		Product product = productList.get(position);

		Utils.loadImage(holder.binding.productImage, product.getProductImages().get(0).getProductImage());
		if(product.getStoreStock() < 1) {
			holder.binding.stockCount.setText(mContext.getString(R.string.out_of_stock));
			holder.binding.stockCount.setTextColor(mContext.getResources().getColor(R.color.flush_orange));
			holder.binding.viewStatus.setBackgroundColor(mContext.getResources().getColor(R.color.flush_orange));
		} else {
			if(product.getStoreProductStatus() != null && product.getStoreProductStatus().equalsIgnoreCase(kActive)) {
				holder.binding.viewStatus.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
			} else {
				holder.binding.viewStatus.setBackgroundColor(mContext.getResources().getColor(R.color.flush_orange));
			}
			holder.binding.stockCount.setText(String.format(Locale.ENGLISH, "%s %d %s", mContext.getString(R.string.stock), product.getStoreStock(), mContext.getString(R.string.units_small)));
			holder.binding.stockCount.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
		}
		if(new SharedHelper(mContext).getSelectedLanguage().equalsIgnoreCase("ar")){
			holder.binding.productName.setText(product.getArabicName());
		}else {
			holder.binding.productName.setText(product.getProductName());
		}
		if(!product.getProductWeight().isEmpty() && product.getProductWeight() != null){
			holder.binding.productDetails.setText(""+product.getProductWeight());
		}
		holder.binding.productPrice.setText(String.format(Locale.ENGLISH, "%s", Utils.round(product.getProductPrice())));

		holder.binding.available.setOnClickListener(view->availableClickListener.onItemClick(position));
		holder.binding.unAvailable.setOnClickListener(view->unavailableClickListener.onItemClick(position));
		holder.binding.edit.setOnClickListener(view->editClickListener.onItemClick(position));
		holder.binding.update.setOnClickListener(view->updateClickListener.onItemClick(position));
	}

	@Override
	public int getItemCount() {
		return productList.size();
	}
}
