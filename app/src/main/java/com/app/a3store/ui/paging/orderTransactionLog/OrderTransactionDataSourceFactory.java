package com.app.a3store.ui.paging.orderTransactionLog;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.app.a3store.model.OrderTransactionResponse;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.ui.interfaces.ItemClickListenerWithTwoIds;

public class OrderTransactionDataSourceFactory extends DataSource.Factory<Integer, OrderTransactionResponse.Data.Order> {

	private final MutableLiveData<PageKeyedDataSource<Integer, OrderTransactionResponse.Data.Order>> itemLiveDataSource = new MutableLiveData<>();
	Context mContext;
	ItemClickListener mItemClickListener;
	ItemClickListenerWithTwoIds mItemClickListenerWithTwoIds;
	boolean isFilter;
	String fromDate, toDate;

	public OrderTransactionDataSourceFactory(Context context, boolean isFilter, String fromDate, String toDate, ItemClickListener itemClickListener, ItemClickListenerWithTwoIds mItemClickListenerWithTwoIds) {
		this.mContext = context;
		this.mItemClickListener = itemClickListener;
		this.isFilter = isFilter;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.mItemClickListenerWithTwoIds = mItemClickListenerWithTwoIds;
	}

	@Override
	public DataSource<Integer, OrderTransactionResponse.Data.Order> create() {
		OrderTransactionDataSource itemDataSource = new OrderTransactionDataSource(mContext,isFilter,fromDate,toDate, mItemClickListener, mItemClickListenerWithTwoIds);
		itemLiveDataSource.postValue(itemDataSource);
		return itemDataSource;
	}

	public MutableLiveData<PageKeyedDataSource<Integer, OrderTransactionResponse.Data.Order>> getItemLiveDataSource() {
		return itemLiveDataSource;
	}
}
