package com.app.a3store.ui.paging.pickuporders;

import android.content.Context;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.model.orders.OrderInfo;
import com.app.a3store.ui.fragment.ReadyToPickupFragment;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.utils.TimeCalculator;

import java.util.Objects;

import static com.app.a3store.constants.AppConstants.productCompletedStatusString;
import static com.app.a3store.constants.AppConstants.productOngoingStatusString;

public class OrdersPickupPagedAdapter extends PagedListAdapter<OrderInfo, OrdersPickupPagedAdapter.ItemViewHolder> {

	private final Context context;
	ItemClickListener itemClickListener;
	ItemClickListener pickUpClickListener;
	ItemClickListener callDriver;
	ReadyToPickupFragment readyToPickupFragment;
	long lastClickTime = 0;

	public OrdersPickupPagedAdapter(ReadyToPickupFragment readyToPickupFragment,Context mContext, ItemClickListener pickUpClickListener, ItemClickListener itemClickListener, ItemClickListener callDriver) {
		super(DIFF_CALLBACK);
		this.readyToPickupFragment = readyToPickupFragment;
		this.context = mContext;
		this.pickUpClickListener = pickUpClickListener;
		this.itemClickListener = itemClickListener;
		this.callDriver = callDriver;
	}

	@NonNull
	@Override
	public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(context).inflate(R.layout.row_orderslist_ready_to_pickup, parent, false);
		return new ItemViewHolder(view);
	}

	@Override
	public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
		OrderInfo itemInfo = getItem(holder.getAdapterPosition());

		if(itemInfo != null) {
			holder.bindData(itemInfo);
			holder.viewReadyToPickup.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// preventing double, using threshold of 1000 ms
					if (SystemClock.elapsedRealtime() - lastClickTime < 1000){
						holder.viewReadyToPickup.setEnabled(false);
						readyToPickupFragment.readytopick = holder.viewReadyToPickup;
						pickUpClickListener.onItemClick(holder.getAdapterPosition());
						return;
					}
					lastClickTime = SystemClock.elapsedRealtime();
				}
			});
			//holder.viewReadyToPickup.setOnClickListener(view-> pickUpClickListener.onItemClick(holder.getAdapterPosition()));
//			holder.viewCallDriver.setOnClickListener(view->callDriver.onItemClick(holder.getAdapterPosition()));
			holder.itemView.setOnClickListener(view->itemClickListener.onItemClick(holder.getAdapterPosition()));
		}
	}

	private static final DiffUtil.ItemCallback<OrderInfo> DIFF_CALLBACK = new DiffUtil.ItemCallback<OrderInfo>() {
		@Override
		public boolean areItemsTheSame(OrderInfo oldItem, OrderInfo newItem) {
			return oldItem.getId() == newItem.getId();
		}

		@Override
		public boolean areContentsTheSame(@NonNull OrderInfo oldItem, @NonNull OrderInfo newItem) {
			return Objects.equals(oldItem, newItem);
		}
	};

	class ItemViewHolder extends RecyclerView.ViewHolder {

		public TextView orderId, orderedOn, deliveredOn, viewReadyToPickup;
		public View viewCallDriver;

		public ItemViewHolder(View itemView) {
			super(itemView);
			this.orderId = itemView.findViewById(R.id.orderIdVal);
			this.orderedOn = itemView.findViewById(R.id.orderOnVal);
			this.deliveredOn = itemView.findViewById(R.id.deliverByVal);
			this.viewReadyToPickup = itemView.findViewById(R.id.viewReadyToPickup);
			this.viewCallDriver = itemView.findViewById(R.id.viewCallDriver);
		}

		public void bindData(OrderInfo data) {
			this.orderId.setText(data.getOrderIDs());
			if(data.getOrderOn() != null) {
				String orderDate = TimeCalculator.getTime(data.getOrderOn()) + " | " + TimeCalculator.getDate(data.getOrderOn());
				this.orderedOn.setText(orderDate);
			}
			if(data.getDeliveryDate() != null) {
				String orderDate = TimeCalculator.getTimeSlot(data.getFromTime()) +"-"+TimeCalculator.getTimeSlot(data.getToTime()) + " | " + TimeCalculator.getDate(data.getDeliveryDate());
				this.deliveredOn.setText(orderDate);
			}
			if(data.getStoreStatus().equalsIgnoreCase(productOngoingStatusString)  || data.getStoreStatus().equalsIgnoreCase(productCompletedStatusString)) {
				this.viewReadyToPickup.setText(R.string.ready_for_delivery);
				this.viewReadyToPickup.setEnabled(false);
				this.viewReadyToPickup.setBackgroundTintList(AppCompatResources.getColorStateList(context, R.color.nobel_with_opacity));
			} else {
				this.viewReadyToPickup.setText(R.string.ready_to_pickup_small);

				if(data.getProductcount() == data.getPackedcount()){
					this.viewReadyToPickup.setEnabled(true);
					this.viewReadyToPickup.setBackgroundTintList(AppCompatResources.getColorStateList(context, R.color.nobel));

				}else {

					this.viewReadyToPickup.setEnabled(false);
					this.viewReadyToPickup.setBackgroundTintList(AppCompatResources.getColorStateList(context, R.color.nobel_with_opacity));

				}


			}


		}
	}
}
