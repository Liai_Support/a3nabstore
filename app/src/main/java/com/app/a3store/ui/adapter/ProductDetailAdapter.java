package com.app.a3store.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.viewpager.widget.PagerAdapter;

import com.app.a3store.R;
import com.app.a3store.model.product.Product;
import com.app.a3store.utils.Utils;

import java.util.List;

public class ProductDetailAdapter extends PagerAdapter {

	private final Context mContext;
	List<Product.ProductImage> productItems;

	public ProductDetailAdapter(Context mContext, List<Product.ProductImage> items) {
		this.mContext = mContext;
		this.productItems = items;
	}

	@Override
	public int getCount() {
		return productItems.size();
	}

	@Override
	public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
		return view == object;
	}

	@NonNull
	@Override
	public Object instantiateItem(@NonNull ViewGroup container, int position) {
		View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_product_details_layout, container, false);

		AppCompatImageView imageView = itemView.findViewById(R.id.sliderImage);
		Utils.loadImage(imageView, productItems.get(position).getProductImage());
		container.addView(itemView);

		return itemView;
	}

	@Override
	public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
	}
}
