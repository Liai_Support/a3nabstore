package com.app.a3store.ui.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.model.dashboard.DashboardResponse;
import com.app.a3store.model.orders.OrderInfo;
import com.app.a3store.utils.TimeCalculator;

public class OrderCompleteViewHolder extends RecyclerView.ViewHolder {

	public TextView orderId, orderedOn, deliveredOn;

	public OrderCompleteViewHolder(View itemView) {
		super(itemView);
		this.orderId = itemView.findViewById(R.id.orderIdVal);
		this.orderedOn = itemView.findViewById(R.id.orderOnVal);
		this.deliveredOn = itemView.findViewById(R.id.deliverByVal);
	}

	public void bindData( DashboardResponse.Data.Order  data) {
		this.orderId.setText(data.getOrderIDs());
		this.orderedOn.setText(data.getOrderOn());
//		this.deliveredOn.setText(data.getDeliveryOn());

		if(data.getDeliveryDate() != null) {
			String orderDate = TimeCalculator.getTimeSlot(data.getFromTime()) +"-"+TimeCalculator.getTimeSlot(data.getToTime()) + " | " + TimeCalculator.getDate(data.getDeliveryDate());
			this.deliveredOn.setText(orderDate);
		}

	}
}
