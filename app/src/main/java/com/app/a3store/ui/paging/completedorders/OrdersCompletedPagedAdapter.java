package com.app.a3store.ui.paging.completedorders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.constants.AppConstants;
import com.app.a3store.model.orders.OrderInfo;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.utils.TimeCalculator;

import java.util.Objects;

public class OrdersCompletedPagedAdapter extends PagedListAdapter<OrderInfo, OrdersCompletedPagedAdapter.ItemViewHolder> {

	private final Context context;
	ItemClickListener itemClickListener;

	public OrdersCompletedPagedAdapter(Context mContext, ItemClickListener itemClickListener) {
		super(DIFF_CALLBACK);
		this.context = mContext;
		this.itemClickListener = itemClickListener;
	}

	@NonNull
	@Override
	public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(context).inflate(R.layout.row_orderslist_complete, parent, false);
		return new ItemViewHolder(view);
	}

	@Override
	public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
		OrderInfo itemInfo = getItem(holder.getAdapterPosition());

		if(itemInfo != null) {
			holder.bindData(context,itemInfo);
			holder.itemView.setOnClickListener(view->itemClickListener.onItemClick(holder.getAdapterPosition()));
		}
	}

	private static final DiffUtil.ItemCallback<OrderInfo> DIFF_CALLBACK = new DiffUtil.ItemCallback<OrderInfo>() {
		@Override
		public boolean areItemsTheSame(OrderInfo oldItem, OrderInfo newItem) {
			return oldItem.getId() == newItem.getId();
		}

		@Override
		public boolean areContentsTheSame(@NonNull OrderInfo oldItem, @NonNull OrderInfo newItem) {
			return Objects.equals(oldItem, newItem);
		}
	};

	static class ItemViewHolder extends RecyclerView.ViewHolder {

		public TextView orderId, orderedOn, deliveredOn,txtOrderStatus;

		public ItemViewHolder(View itemView) {
			super(itemView);
			this.orderId = itemView.findViewById(R.id.orderIdVal);
			this.orderedOn = itemView.findViewById(R.id.orderOnVal);
			this.deliveredOn = itemView.findViewById(R.id.deliverByVal);
			this.txtOrderStatus = itemView.findViewById(R.id.txtOrderStatus);
		}

		public void bindData(Context context, OrderInfo data) {
			this.orderId.setText(data.getOrderIDs());
			if(data.getOrderOn() != null) {
				String orderDate = TimeCalculator.getTime(data.getOrderOn()) + " | " + TimeCalculator.getDate(data.getOrderOn());
				this.orderedOn.setText(orderDate);
			}
			if(data.getDeliveryDate() != null) {
				String orderDate = TimeCalculator.getTimeSlot(data.getFromTime()) +"-"+TimeCalculator.getTimeSlot(data.getToTime()) + " | " + TimeCalculator.getDate(data.getDeliveryDate());
				this.deliveredOn.setText(orderDate);
			}


			if(data.getOrderStatus().equalsIgnoreCase(AppConstants.kRejected))
			{
				this.txtOrderStatus.setText(context.getString(R.string.order_rejected));
				this.txtOrderStatus.setBackground(context.getDrawable(R.drawable.rounded_circle_orange_filled));
			}else{
				this.txtOrderStatus.setText(context.getString(R.string.order_complete));
				this.txtOrderStatus.setBackground(context.getDrawable(R.drawable.rounded_circle_green_filled));
			}
		}
	}
}
