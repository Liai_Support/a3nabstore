package com.app.a3store.ui.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.databinding.RowProductInfoBinding;
import com.app.a3store.model.ProductUnavailableResponse;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;

import java.util.List;
import java.util.Locale;

import static com.app.a3store.constants.AppConstants.kActive;
import static com.app.a3store.constants.AppConstants.kPercentage;

public class ProductUnAvailableItemAdapter extends RecyclerView.Adapter<ProductUnAvailableItemAdapter.ViewHolder> {

    private final Context mContext;
    List<ProductUnavailableResponse.Data.Product> productList;
    ItemClickListener itemClickListener;
    int previousPosition = -1, currentPosition = -1;

    public ProductUnAvailableItemAdapter(Context context, List<ProductUnavailableResponse.Data.Product> productList, ItemClickListener itemClickListener) {
        this.mContext = context;
        this.productList = productList;
        this.itemClickListener = itemClickListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        RowProductInfoBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = RowProductInfoBinding.bind(itemView);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_product_info, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        ProductUnavailableResponse.Data.Product product = productList.get(position);

        Utils.loadImage(holder.binding.productImage, product.getProductImages().get(0).getProductImage());
        if(new SharedHelper(mContext).getSelectedLanguage().equalsIgnoreCase("ar")){
            holder.binding.productName.setText(product.getArabicName());
        }else {
            holder.binding.productName.setText(product.getProductName());
        }
        holder.binding.productWeight.setText(String.format(Locale.ENGLISH, "%s", product.getProductWeight()));

        if (product.getProductDiscountStatus().equalsIgnoreCase(kPercentage)) {
            if (product.getProductDiscount() > 0) {
                holder.binding.discount.setVisibility(View.VISIBLE);
                holder.binding.viewDiscount.setVisibility(View.VISIBLE);
                holder.binding.oldPrice.setVisibility(View.VISIBLE);
                holder.binding.discount.setText(String.format(Locale.ENGLISH, "%% %d", product.getProductDiscount()));
                holder.binding.oldPrice.setText(String.format(Locale.ENGLISH, "%s %s", Utils.round(product.getProductPrice()), mContext.getString(R.string.sar)));
                holder.binding.oldPrice.setPaintFlags(holder.binding.oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                holder.binding.discount.setVisibility(View.GONE);
                holder.binding.viewDiscount.setVisibility(View.GONE);
                holder.binding.oldPrice.setVisibility(View.GONE);
            }
            double price = product.getProductPrice() - ((product.getProductPrice() * product.getProductDiscount()) / 100);
            holder.binding.productPrice.setText(String.format(Locale.ENGLISH, "%s %s", Utils.round(price), mContext.getString(R.string.sar)));
        }
        if (product.getProductDiscountStatus().equalsIgnoreCase(kPercentage)) {
            holder.binding.discount.setText(String.format(Locale.ENGLISH, "%% %d", product.getProductDiscount()));
        }
        if (product.getProductStatus().equalsIgnoreCase(kActive)) {
            holder.binding.productStatus.setImageResource(R.drawable.green_enable_long);
        } else {
            holder.binding.productStatus.setImageResource(R.drawable.orange_disable_long);
        }
        if (currentPosition == position) {
            holder.binding.viewDisable.setVisibility(View.VISIBLE);
            holder.binding.check.setVisibility(View.VISIBLE);
        } else {
            holder.binding.viewDisable.setVisibility(View.GONE);
            holder.binding.check.setVisibility(View.INVISIBLE);
        }

        holder.itemView.setOnClickListener(v -> {
            if (currentPosition == holder.getAdapterPosition()) {
                itemClickListener.onItemClick(-1);
                currentPosition = -1;
                previousPosition = -1;
                notifyItemChanged(holder.getAdapterPosition());
            } else {
                itemClickListener.onItemClick(holder.getAdapterPosition());
                previousPosition = currentPosition;
                currentPosition = holder.getAdapterPosition();
                if (previousPosition != -1) notifyItemChanged(previousPosition);
                notifyItemChanged(currentPosition);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }
}
