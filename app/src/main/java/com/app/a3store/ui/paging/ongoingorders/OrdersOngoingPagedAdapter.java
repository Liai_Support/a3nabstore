package com.app.a3store.ui.paging.ongoingorders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.model.orders.OrderInfo;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.utils.TimeCalculator;

import java.util.Objects;

public class OrdersOngoingPagedAdapter extends PagedListAdapter<OrderInfo, OrdersOngoingPagedAdapter.ItemViewHolder> {

	private final Context context;
	private final ItemClickListener acceptClickListener;
	private final ItemClickListener rejectClickListener;
	ItemClickListener itemClickListener;

	public OrdersOngoingPagedAdapter(Context mContext, ItemClickListener mAcceptClickListener, ItemClickListener mRejectClickListener, ItemClickListener mItemClickListener) {
		super(DIFF_CALLBACK);
		this.context = mContext;
		this.acceptClickListener = mAcceptClickListener;
		this.rejectClickListener = mRejectClickListener;
		this.itemClickListener = mItemClickListener;
	}

	@NonNull
	@Override
	public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(context).inflate(R.layout.row_orders_accept_reject, parent, false);
		return new ItemViewHolder(view);
	}

	@Override
	public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
		OrderInfo itemInfo = getItem(holder.getAdapterPosition());

		if(itemInfo != null) {
			holder.bindData(context, itemInfo);
			holder.accept.setOnClickListener(view->acceptClickListener.onItemClick(holder.getAdapterPosition()));
			holder.reject.setOnClickListener(view->rejectClickListener.onItemClick(holder.getAdapterPosition()));
			holder.itemView.setOnClickListener(view->itemClickListener.onItemClick(holder.getAdapterPosition()));
		}
	}

	private static final DiffUtil.ItemCallback<OrderInfo> DIFF_CALLBACK = new DiffUtil.ItemCallback<OrderInfo>() {
		@Override
		public boolean areItemsTheSame(OrderInfo oldItem, OrderInfo newItem) {
			return oldItem.getId() == newItem.getId();
		}

		@Override
		public boolean areContentsTheSame(@NonNull OrderInfo oldItem, @NonNull OrderInfo newItem) {
			return Objects.equals(oldItem, newItem);
		}
	};

	static class ItemViewHolder extends RecyclerView.ViewHolder {

		public TextView orderId, orderedOn, deliveredOn, accept, reject;

		public ItemViewHolder(View itemView) {
			super(itemView);
			this.orderId = itemView.findViewById(R.id.orderIdVal);
			this.orderedOn = itemView.findViewById(R.id.orderOnVal);
			this.deliveredOn = itemView.findViewById(R.id.deliverByVal);
			this.accept = itemView.findViewById(R.id.accept);
			this.reject = itemView.findViewById(R.id.reject);
		}

		public void bindData(Context context, OrderInfo data) {
			this.orderId.setText(data.getOrderIDs());
			if(data.getOrderOn() != null) {
				String orderDate = TimeCalculator.getTime(data.getOrderOn()) + " | " + TimeCalculator.getDate(data.getOrderOn());
				this.orderedOn.setText(orderDate);
			}
			if(data.getDeliveryDate() != null) {
				String orderDate = TimeCalculator.getTimeSlot(data.getFromTime()) +"-"+TimeCalculator.getTimeSlot(data.getToTime()) + " | " + TimeCalculator.getDate(data.getDeliveryDate());
				this.deliveredOn.setText(orderDate);
			}
			this.accept.setText(context.getString(R.string.accept_order));
			this.reject.setText(context.getString(R.string.reject_order));
		}
	}
}
