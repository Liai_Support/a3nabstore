package com.app.a3store.ui.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RadioGroup;

import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3store.BaseActivity;
import com.app.a3store.R;
import com.app.a3store.constants.IntentConstants;
import com.app.a3store.constants.UrlConstants;
import com.app.a3store.databinding.ActivityProfileRegistrationBinding;
import com.app.a3store.dialogs.DialogUtils;
import com.app.a3store.model.profile.ProfileDetail;
import com.app.a3store.utils.TimeCalculator;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;
import com.app.a3store.viewmodel.ProfileViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.app.a3store.constants.AppConstants.kEnglish;

public class ProfileRegistrationActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {

    AppCompatRadioButton radioMale, radioFemale;
    RadioGroup rgStatus;
    ColorStateList colorStateSelectedList, colorStateUnSelectedList;
    ProfileViewModel profileViewModel;
    SharedHelper sharedHelper;
    String gender = "";
    ActivityProfileRegistrationBinding profileRegistrationBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        profileRegistrationBinding = DataBindingUtil.setContentView(this, R.layout.activity_profile_registration);
        profileViewModel = new ViewModelProvider(this).get(ProfileViewModel.class);
        profileRegistrationBinding.setViewmodel(profileViewModel);
        profileViewModel.setContext(this);
        profileRegistrationBinding.setLifecycleOwner(this);
        assignViewIDs();
        initViews();
        setListeners();
        setObservers();
    }

    private void setObservers() {
        profileViewModel.errorMessage.observe(this, message -> Utils.showSnack(profileRegistrationBinding.getRoot(), message));
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void assignViewIDs() {
        radioMale = findViewById(R.id.radioMale);
        radioFemale = findViewById(R.id.radioFemale);
        rgStatus = findViewById(R.id.rgStatus);
    }

    private void initViews() {
        colorStateSelectedList = new ColorStateList(new int[][]{new int[]{android.R.attr.state_enabled}}, new int[]{ContextCompat.getColor(this, R.color.colorPrimary)});//enabled
        colorStateUnSelectedList = new ColorStateList(new int[][]{new int[]{android.R.attr.state_enabled} //enabled
        }, new int[]{ContextCompat.getColor(this, R.color.dorado)}); //disabled

        sharedHelper = new SharedHelper(this);
        if (sharedHelper.getSelectedLanguage().equalsIgnoreCase(kEnglish)) {
            profileRegistrationBinding.backArrow.setScaleX(1);
        } else {
            profileRegistrationBinding.backArrow.setScaleX(-1);
        }
        rgStatus.setOnCheckedChangeListener((radioGroup, i) -> {
            if (radioMale.isChecked()) {
                gender = "male";
                radioMale.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
                radioFemale.setTextColor(ContextCompat.getColor(this, R.color.dorado));
                radioMale.setButtonTintList(colorStateSelectedList);
                radioFemale.setButtonTintList(colorStateUnSelectedList);
            } else {
                gender = "female";
                radioMale.setTextColor(ContextCompat.getColor(this, R.color.dorado));
                radioFemale.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
                radioMale.setButtonTintList(colorStateUnSelectedList);
                radioFemale.setButtonTintList(colorStateSelectedList);
            }
        });
        loadProfileData();
    }

    private void setListeners() {
        profileRegistrationBinding.bottomDashboard.setOnClickListener(view -> gotoDashboard());
        profileRegistrationBinding.bottomAssignmnet.setOnClickListener(view -> gotoStore());

        profileRegistrationBinding.day.setOnClickListener(view -> openDatePicker());
        profileRegistrationBinding.month.setOnClickListener(view -> openDatePicker());
        profileRegistrationBinding.year.setOnClickListener(view -> openDatePicker());

        profileRegistrationBinding.orders.setOnClickListener(view -> {
            Intent intent = new Intent(ProfileRegistrationActivity.this, OrderListingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        });

        profileRegistrationBinding.save.setOnClickListener(view -> {
            if (profileViewModel.validateProfileDetails()) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(UrlConstants.kID, sharedHelper.getUserId());
                    jsonObject.put("firstName", profileViewModel.firstName);
                    jsonObject.put("lastName", profileViewModel.lastName);
                    jsonObject.put("email", profileViewModel.emailId);
                    jsonObject.put("countryCode", profileRegistrationBinding.countryCodePicker.getSelectedCountryCode());
                    jsonObject.put("mobileNumber", profileViewModel.phoneNumber);
                    jsonObject.put("dob", profileViewModel.dob);
                    jsonObject.put("gender", gender);
                    if (sharedHelper.getProfileStatus()) {
                        jsonObject.put("status", "active");
                    } else {
                        jsonObject.put("status", "inactive");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                DialogUtils.showLoader(this);
                profileViewModel.updateProfile(this, jsonObject).observe(this, updateProfileResponse -> {
                    DialogUtils.dismissLoader();
                    if (updateProfileResponse.getError().equalsIgnoreCase("true")) {
                        Utils.showSnack(profileRegistrationBinding.getRoot(), updateProfileResponse.getMessage());
                    } else {

                        SharedHelper sharedHelper = new SharedHelper(this);
                        sharedHelper.setFirstName(profileViewModel.firstName);
                        sharedHelper.setLastName(profileViewModel.lastName);
                        sharedHelper.setEmail(profileViewModel.emailId);
                        sharedHelper.setCountryCode(profileRegistrationBinding.countryCodePicker.getSelectedCountryCode());
                        sharedHelper.setMobileNumber(profileViewModel.phoneNumber);
                        sharedHelper.setLoggedIn(true);
                        setResult(RESULT_OK);
                        finish();
                    }
                });
            }
        });

        profileRegistrationBinding.backArrow.setOnClickListener(view -> backPressed());
    }

    private void loadProfileData() {
        if (profileViewModel != null) {
            if (getIntent() != null && getIntent().hasExtra(IntentConstants.intentProfileEdit)) {
                profileViewModel.profileDetail = (ProfileDetail) getIntent().getSerializableExtra(IntentConstants.intentProfileEdit);
                if (profileViewModel.profileDetail != null) {

                    if (profileViewModel.profileDetail.getStatus() != null) {
                        profileViewModel.status = profileViewModel.profileDetail.getStatus();
                    } else {
                        profileViewModel.status = "";
                    }

                    if (profileViewModel.profileDetail.getFirstName() != null) {
                        profileViewModel.firstName = profileViewModel.profileDetail.getFirstName();
                    } else {
                        profileViewModel.firstName = "";
                    }

                    if (profileViewModel.profileDetail.getLastName() != null) {
                        profileViewModel.lastName = profileViewModel.profileDetail.getLastName();
                    } else {
                        profileViewModel.lastName = "";
                    }

                    if (profileViewModel.profileDetail.getEmail() != null) {
                        profileViewModel.emailId = profileViewModel.profileDetail.getEmail();
                    } else {
                        profileViewModel.emailId = "";
                    }

                    if (profileViewModel.profileDetail.getMobileNumber() != null) {
                        profileViewModel.phoneNumber = profileViewModel.profileDetail.getMobileNumber();
//						profileRegistrationBinding.countryCodePicker.setCountryForPhoneCode(profileViewModel.profileDetail.getCountryCode());
                    } else {
                        profileViewModel.phoneNumber = "";
                    }


                    if (profileViewModel.profileDetail.getGender().equalsIgnoreCase(getString(R.string.male)) || profileViewModel.profileDetail.getGender().equalsIgnoreCase("male")) {
                        profileRegistrationBinding.radioMale.setChecked(true);
                        gender = "male";
                    } else {
                        profileRegistrationBinding.radioFemale.setChecked(true);
                        gender = "female";
                    }

                    String date = TimeCalculator.convertDateFormat(profileViewModel.profileDetail.getDob());
                    profileViewModel.dob = date;
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                    try {
                        Date parse = sdf.parse(date);
                        Calendar c = Calendar.getInstance();
                        c.setTime(parse);
                        profileViewModel.date = c.get(Calendar.DATE);
                        profileViewModel.month = c.get(Calendar.MONTH);
                        profileViewModel.year = c.get(Calendar.YEAR);

                        profileRegistrationBinding.day.setText(String.format(Locale.getDefault(), "%d", profileViewModel.date));
                        profileRegistrationBinding.month.setText(String.format(Locale.getDefault(), "%d", profileViewModel.month + 1));
                        profileRegistrationBinding.year.setText(String.format(Locale.getDefault(), "%d", profileViewModel.year));

                        profileViewModel.dob = profileViewModel.year + "-" + (profileViewModel.month + 1) + "-" + profileViewModel.date;
                        // Utils.showSnack(profileRegistrationBinding.getRoot(), ""+(c.get(Calendar.MONTH)+1) + c.get(Calendar.DATE) + c.get(Calendar.YEAR));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void openDatePicker() {
        int selectedYear = profileViewModel.year;
        int selectedMonth = profileViewModel.month;
        int selectedDate = profileViewModel.date;

        DatePickerDialog dialog = new DatePickerDialog(this, R.style.DialogTheme, (datePicker, year, month, day) -> {
            profileViewModel.date = day;
            profileViewModel.month = month;
            profileViewModel.year = year;

            profileRegistrationBinding.day.setText(String.format(Locale.getDefault(), "%d", profileViewModel.date));
            profileRegistrationBinding.month.setText(String.format(Locale.getDefault(), "%d", profileViewModel.month + 1));
            profileRegistrationBinding.year.setText(String.format(Locale.getDefault(), "%d", profileViewModel.year));

            profileViewModel.dob = profileViewModel.year + "-" + (profileViewModel.month + 1) + "-" + profileViewModel.date;
        }, selectedYear, selectedMonth, selectedDate);
        dialog.show();
        dialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        dialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.white));
        dialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.dove_gray));

        dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
    }

    private void gotoDashboard() {
        Intent intent = new Intent(ProfileRegistrationActivity.this, DashboardActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private void gotoStore() {
        Intent intent = new Intent(ProfileRegistrationActivity.this, ProductAddItemsToStoreActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private void backPressed() {
        super.onBackPressed();
    }
}