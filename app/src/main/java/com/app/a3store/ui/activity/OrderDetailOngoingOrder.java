package com.app.a3store.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3store.BaseActivity;
import com.app.a3store.R;
import com.app.a3store.constants.OrderConstants;
import com.app.a3store.constants.UrlConstants;
import com.app.a3store.databinding.ActivityOrderDetailTemplateOneBinding;
import com.app.a3store.dialogs.DialogUtils;
import com.app.a3store.model.orders.OrderDetailsResponse;
import com.app.a3store.ui.adapter.OrderItemsAdapter;
import com.app.a3store.ui.adapter.OrderPaymentSummaryAdapter;
import com.app.a3store.ui.interfaces.ItemClickListenerWithTwoIds;
import com.app.a3store.utils.TimeCalculator;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;
import com.app.a3store.viewmodel.OderDetailsViewModel;

import java.util.List;

import static com.app.a3store.constants.AppConstants.kEnglish;

public class OrderDetailOngoingOrder extends BaseActivity {

	ActivityOrderDetailTemplateOneBinding orderDetailTemplateOneBinding;
	OrderItemsAdapter orderItemsAdapter;
	OrderPaymentSummaryAdapter orderPaymentSummaryAdapter;
	OderDetailsViewModel viewModel;
	int orderID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		orderDetailTemplateOneBinding = DataBindingUtil.setContentView(this, R.layout.activity_order_detail_template_one);
		viewModel = new ViewModelProvider(this).get(OderDetailsViewModel.class);
		Intent intent = getIntent();
		OrderConstants.OrderType orderType = (OrderConstants.OrderType) intent.getSerializableExtra("orderType");
		if(orderType == OrderConstants.OrderType.completed) {
			orderDetailTemplateOneBinding.accept.setVisibility(View.GONE);
			orderDetailTemplateOneBinding.reject.setVisibility(View.GONE);
		}
		orderID = intent.getIntExtra("id", 0);
		if(new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase(kEnglish)) {
			orderDetailTemplateOneBinding.backArrow.setScaleX(1);
		} else {
			orderDetailTemplateOneBinding.backArrow.setScaleX(-1);
		}
		getOrderDetails();
		setListeners();
	}

	private void getOrderDetails() {
		DialogUtils.showLoader(this);
		viewModel.orderDetails(this, orderID).observe(this, orderDetailsResponse->{
			DialogUtils.dismissLoader();
			if(orderDetailsResponse.getError().equalsIgnoreCase("true")) {
				Utils.showSnack(findViewById(android.R.id.content), orderDetailsResponse.getMessage());
			} else {
				if(orderDetailsResponse.getData() != null) {
					orderDetailTemplateOneBinding.invoiceValue.setText(String.format("SAR %s", Utils.round(orderDetailsResponse.getData().getTotalAmount())));
					orderDetailTemplateOneBinding.taxValue.setText(String.format("SAR %s", Utils.round(orderDetailsResponse.getData().getTax())));
					orderDetailTemplateOneBinding.txtFastDeliveryValue.setText(String.format("SAR %s", Utils.round(orderDetailsResponse.getData().getFasttax())));
					orderDetailTemplateOneBinding.totalValue.setText(String.format("SAR %s", Utils.round(orderDetailsResponse.getData().getGrandTotal())));
					if(orderDetailsResponse.getData().getOrdersInfo() != null) {

						if(orderDetailsResponse.getData().getOrdersInfo().getType() != null){
							orderDetailTemplateOneBinding.paymentStatus.setText(orderDetailsResponse.getData().getOrdersInfo().getType());
						}else{
							orderDetailTemplateOneBinding.paymentStatus.setText("");
						}
						if(orderDetailsResponse.getData().getOrdersInfo().getOrderOn() != null) {
							String orderDate = TimeCalculator.getTime(orderDetailsResponse.getData().getOrdersInfo().getOrderOn()) + " | " + TimeCalculator.getDate(orderDetailsResponse.getData().getOrdersInfo().getOrderOn());
							orderDetailTemplateOneBinding.orderOnVal.setText(orderDate);
						}
						if(orderDetailsResponse.getData().getOrdersInfo().getDeliveryDate() != null) {
							String orderDate = TimeCalculator.getTimeSlot(orderDetailsResponse.getData().getOrdersInfo().getFromTime()) +"-"+TimeCalculator.getTimeSlot(orderDetailsResponse.getData().getOrdersInfo().getToTime())+ " | " + TimeCalculator.getDate(orderDetailsResponse.getData().getOrdersInfo().getDeliveryDate());
							orderDetailTemplateOneBinding.deliverByVal.setText(orderDate);
						}
						orderDetailTemplateOneBinding.orderIdVal.setText(orderDetailsResponse.getData().getOrdersInfo().getOrderIDs());
						orderDetailTemplateOneBinding.deliveryPersonName.setText(orderDetailsResponse.getData().getOrdersInfo().getDriverName());

						if(orderDetailsResponse.getData().getOrdersInfo().getDriverNumber() != null && !orderDetailsResponse.getData().getOrdersInfo().getDriverNumber().trim().isEmpty()) {
							orderDetailTemplateOneBinding.groupCallDriver.setVisibility(View.VISIBLE);
						} else {
							orderDetailTemplateOneBinding.groupCallDriver.setVisibility(View.GONE);
						}
						orderDetailTemplateOneBinding.callDriver.setOnClickListener(v->{
							if(orderDetailsResponse.getData().getOrdersInfo().getDriverNumber() != null && !orderDetailsResponse.getData().getOrdersInfo().getDriverNumber().isEmpty()) {
								Intent intent = new Intent(Intent.ACTION_DIAL);
								intent.setData(Uri.parse("tel:" +orderDetailsResponse.getData().getOrdersInfo().getCountryCode() + orderDetailsResponse.getData().getOrdersInfo().getDriverNumber()));
								startActivity(intent);
							} else {
								Utils.showSnack(orderDetailTemplateOneBinding.getRoot(), getString(R.string.driver_call_error));
							}
						});
					}
					if(orderDetailsResponse.getData().getOrderItems() != null && orderDetailsResponse.getData().getOrderItems().size() > 0) {
						setAdapters(orderDetailsResponse.getData().getOrderItems());
					}
				}
			}
		});
	}

	private void setAdapters(List<OrderDetailsResponse.Data.OrderItem> orderItems) {

		orderItemsAdapter = new OrderItemsAdapter(this, orderItems,false, new ItemClickListenerWithTwoIds(){

			@Override
			public void onItemClick(int position, String value) {
				//Orders Status Change Function
			}
		} );
		orderDetailTemplateOneBinding.recyclerOrderItemsList.setAdapter(orderItemsAdapter);

		orderPaymentSummaryAdapter = new OrderPaymentSummaryAdapter(this);
		//		orderDetailTemplateOneBinding.recyclerPaymentInfo.setAdapter(orderPaymentSummaryAdapter);
	}

	private void setListeners() {
		orderDetailTemplateOneBinding.bottomDashboard.setOnClickListener(view->gotoDashboard());

		orderDetailTemplateOneBinding.bottomAssignmnet.setOnClickListener(view->{
			Intent intent = new Intent(OrderDetailOngoingOrder.this, ProductAddItemsToStoreActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(intent);
			finish();
		});

		orderDetailTemplateOneBinding.imgNotification.setOnClickListener(view->{
			Intent intent = new Intent(this, NotificationActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(intent);
		});

		orderDetailTemplateOneBinding.orders.setOnClickListener(view->{
			Intent intent = new Intent(OrderDetailOngoingOrder.this, OrderListingActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(intent);
			finish();
		});

		orderDetailTemplateOneBinding.imgNotification.setOnClickListener(view->gotoNotifications());

		orderDetailTemplateOneBinding.accept.setOnClickListener(view->{
			DialogUtils.showLoader(this);
			updateOrderStatus(orderID, UrlConstants.statusAccepted);
		});

		orderDetailTemplateOneBinding.reject.setOnClickListener(view->{
			DialogUtils.showLoader(this);
			updateOrderStatus(orderID, UrlConstants.statusRejected);
		});

		orderDetailTemplateOneBinding.backArrow.setOnClickListener(view->closeActivity());
	}

	private void updateOrderStatus(int orderID, String status) {
		viewModel.updateOrderStatus(OrderDetailOngoingOrder.this, orderID, status).observe(this, response->{
			DialogUtils.dismissLoader();
			if(response.getError().equalsIgnoreCase("true")) {
				Utils.showSnack(orderDetailTemplateOneBinding.getRoot(), response.getMessage());
			} else {
				closeActivity();
			}
		});
	}

	private void gotoDashboard() {
		Intent intent = new Intent(OrderDetailOngoingOrder.this, DashboardActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}

	private void gotoNotifications() {
		Intent intent = new Intent(OrderDetailOngoingOrder.this, NotificationActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
	}

	private void closeActivity() {
		setResult(RESULT_OK);
		finish();
	}

	@Override
	public void onBackPressed() {
		closeActivity();
	}
}