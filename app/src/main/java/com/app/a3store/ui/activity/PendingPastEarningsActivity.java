package com.app.a3store.ui.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3store.BaseActivity;
import com.app.a3store.R;
import com.app.a3store.constants.IntentConstants;
import com.app.a3store.databinding.ActivityPendingPastEarningListBinding;
import com.app.a3store.databinding.DialogImagePreviewBinding;
import com.app.a3store.model.EarningResponse;
import com.app.a3store.ui.adapter.EarningPastAdapter;
import com.app.a3store.ui.adapter.EarningPendingAdapter;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;
import com.app.a3store.viewmodel.EarningViewModel;

import java.util.List;

import static com.app.a3store.constants.AppConstants.kTransactionList;

public class PendingPastEarningsActivity extends BaseActivity {

	ActivityPendingPastEarningListBinding binding;
	EarningPendingAdapter pendingEarningsAdapter;
	EarningPastAdapter pastEarningAdapter;
	EarningViewModel viewModel;
	SharedHelper sharedHelper;
	private Dialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = DataBindingUtil.setContentView(this, R.layout.activity_pending_past_earning_list);
		viewModel = new ViewModelProvider(this).get(EarningViewModel.class);
		binding.setLifecycleOwner(this);
		sharedHelper = new SharedHelper(this);
		parseIntent();
		setAdapters();
		setListeners();
	}

	private void parseIntent() {
		if(getIntent() != null && getIntent().hasExtra(IntentConstants.pastEarning)) {
			binding.title.setText(getString(R.string.past_earnings));
			List<EarningResponse.Data.Past> pastList = (List<EarningResponse.Data.Past>) getIntent().getSerializableExtra(kTransactionList);
			pastEarningAdapter = new EarningPastAdapter(this, pastList, pastList.size(), viewReceiptPosition->{
				if(pastList.get(viewReceiptPosition).getImageUrl().isEmpty()) {
					Utils.showSnack(binding.getRoot(), getString(R.string.view_receipt_empty));
				} else {
					showDialog(this, pastList.get(viewReceiptPosition).getImageUrl());
				}
			});
			binding.recyclerEarnings.setAdapter(pastEarningAdapter);
		} else {
			binding.title.setText(getString(R.string.pending_earnings));
			List<EarningResponse.Data.Pending> pendingList = (List<EarningResponse.Data.Pending>) getIntent().getSerializableExtra(kTransactionList);
			pendingEarningsAdapter = new EarningPendingAdapter(this, pendingList, pendingList.size());
			binding.recyclerEarnings.setAdapter(pendingEarningsAdapter);
		}
	}

	private void showDialog(Context context, String imageUrl) {
		dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		DialogImagePreviewBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_image_preview, null, false);
		dialog.setContentView(binding.getRoot());
		if(dialog.getWindow() != null) {
			dialog.getWindow().setGravity(Gravity.CENTER);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
			ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
			InsetDrawable inset = new InsetDrawable(back, 20);
			dialog.getWindow().setBackgroundDrawable(inset);
		}
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
		binding.closeIcon.setOnClickListener(v->dialogClose());
		Utils.loadImage(binding.previewImage, imageUrl);
	}

	private void dialogClose() {
		if(dialog != null) {
			dialog.cancel();
			dialog.hide();
		}
	}

	private void setListeners() {
		binding.bottomDashboard.setOnClickListener(view->gotoDashboard());
		binding.bottomAssignmnet.setOnClickListener(view->gotoStore());
		binding.imgNotification.setOnClickListener(view->{
			Intent intent = new Intent(this, NotificationActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(intent);
		});
		binding.orders.setOnClickListener(view->{
			Intent intent = new Intent(PendingPastEarningsActivity.this, OrderListingActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(intent);
		});
	}

	private void setAdapters() {

	}

	/*private void getEarnings() {
		DialogUtils.showLoader(this);
		viewModel.getEarningList(this, "past").observe(this, earningResponse->{

			if(earningResponse.getError().equalsIgnoreCase(kFalse)) {

				//                if (earningResponse.getData().getPending().isEmpty()) {
				//                    binding.recyclerEarnings.setVisibility(View.GONE);
				//                } else {
				//                    pendingEarningsAdapter = new EarningPendingAdapter(this, earningResponse.getData().getPending());
				//                    binding.recyclerEarnings.setAdapter(pendingEarningsAdapter);
				//                }
			} else {
				Utils.showSnack(binding.getRoot(), earningResponse.getMessage());
			}
		});
	}*/

	private void gotoDashboard() {
		Intent intent = new Intent(PendingPastEarningsActivity.this, DashboardActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}

	private void gotoStore() {
		Intent intent = new Intent(PendingPastEarningsActivity.this, ProductAddItemsToStoreActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(!sharedHelper.getFirstName().isEmpty() || !sharedHelper.getLastName().isEmpty()) {
			binding.name.setText(String.format("Hi, %s %s", sharedHelper.getFirstName(), sharedHelper.getLastName()));
		}
	}
}