package com.app.a3store.ui.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3store.BaseActivity;
import com.app.a3store.R;
import com.app.a3store.constants.OrderConstants;
import com.app.a3store.databinding.ActivityOrderTransactionLogBinding;
import com.app.a3store.databinding.DialogDateFilterBinding;
import com.app.a3store.dialogs.DialogUtils;
import com.app.a3store.ui.paging.orderTransactionLog.OrderTransactionLogPagedAdapter;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;
import com.app.a3store.viewmodel.OrderTransactionViewModel;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static com.app.a3store.constants.AppConstants.kEnglish;
import static com.app.a3store.constants.AppConstants.kTotalAmount;
import static com.app.a3store.constants.AppConstants.kTotalOrders;

public class OrderTransactionLogActivity extends BaseActivity {

	ActivityOrderTransactionLogBinding binding;
	OrderTransactionLogPagedAdapter pagedAdapter;
	OrderTransactionViewModel viewModel;
	boolean isDateFilter = false;
	private Dialog dialog;
	DialogDateFilterBinding filterBinding;
	public int fromDay, fromMonth, fromYear, toDay, toMonth, toYear;
	public String fromDate = "", toDate = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = DataBindingUtil.setContentView(this, R.layout.activity_order_transaction_log);
		viewModel = new ViewModelProvider(this).get(OrderTransactionViewModel.class);
		binding.setLifecycleOwner(this);

		if(new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase(kEnglish)) {
			binding.backArrow.setScaleX(1);
		} else {
			binding.backArrow.setScaleX(-1);
		}
		binding.earnedValue.setText(getIntent().getStringExtra(kTotalAmount));
		binding.orderValue.setText(getIntent().getStringExtra(kTotalOrders));
		getOrderTransactionLog(false, "", "");
		setListeners();
	}

	private void setListeners() {
		binding.backArrow.setOnClickListener(view->backPressed());
		binding.bottomDashboard.setOnClickListener(view->gotoDashboard());
		binding.bottomAssignmnet.setOnClickListener(view->gotoStore());

		binding.orders.setOnClickListener(view->{
			Intent intent = new Intent(OrderTransactionLogActivity.this, OrderListingActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(intent);
		});
		binding.dateFilter.setOnClickListener(v->{
			if(isDateFilter) {
				binding.dateIcon.setImageResource(R.drawable.clock);
				binding.dateFilter.setText(R.string.date_filter);
				isDateFilter = false;
				getOrderTransactionLog(false, "", "");
			} else {
				showDialog();
			}
		});
		binding.dateIcon.setOnClickListener(v->{
			if(isDateFilter) {
				binding.dateIcon.setImageResource(R.drawable.clock);
				binding.dateFilter.setText(R.string.date_filter);
				isDateFilter = false;
				getOrderTransactionLog(false, "", "");
			}
		});
	}

	private void showDialog() {
		dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		filterBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_date_filter, null, false);
		filterBinding.setOrderTransaction(viewModel);
		dialog.setContentView(filterBinding.getRoot());
		if(dialog.getWindow() != null) {
			dialog.getWindow().setGravity(Gravity.CENTER);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
			ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
			InsetDrawable inset = new InsetDrawable(back, 20);
			dialog.getWindow().setBackgroundDrawable(inset);
		}
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
		dialog.setOnDismissListener(dialog1->{
			fromDate = "";
			toDate = "";
		});
		filterBinding.closeIcon.setOnClickListener(v->dialogClose());
		filterBinding.formDateValue.setOnClickListener(v->openDatePicker(true));
		filterBinding.toDateValue.setOnClickListener(v->openDatePicker(false));
		filterBinding.applyFilter.setOnClickListener(v->{
			if(fromDate.isEmpty() || toDate.isEmpty()) {
				Utils.showSnack(filterBinding.getRoot(), getString(R.string.date_error));
			} else {
				viewModel.fromDay = fromDay;
				viewModel.fromMonth = fromMonth + 1;
				viewModel.fromYear = fromYear;
				viewModel.toDay = toDay;
				viewModel.toMonth = toMonth + 1;
				viewModel.toYear = toYear;
				viewModel.fromDateFilter = viewModel.fromYear + "-" + (viewModel.fromMonth) + "-" + viewModel.fromDay;
				viewModel.toDateFilter = viewModel.toYear + "-" + (viewModel.toMonth) + "-" + viewModel.toDay;
				binding.dateIcon.setImageResource(R.drawable.close_red);
				binding.dateFilter.setText(R.string.clear);
				isDateFilter = true;
				fromDate = "";
				toDate = "";
				getOrderTransactionLog(true, viewModel.fromDateFilter, viewModel.toDateFilter);
				dialogClose();
			}
		});
	}

	private void dialogClose() {
		if(dialog != null) {
			dialog.cancel();
			dialog.hide();
		}
	}

	private void openDatePicker(boolean isFromDate) {
		int selectedYear;
		int selectedMonth;
		int selectedDate;
		if(isFromDate) {
			if(fromDate.isEmpty()) {
				Calendar c = Calendar.getInstance();
				selectedYear = c.get(Calendar.YEAR);
				selectedMonth = c.get(Calendar.MONTH);
				selectedDate = c.get(Calendar.DAY_OF_MONTH);
			} else {
				selectedYear = fromYear;
				selectedMonth = fromMonth;
				selectedDate = fromDay;
			}
		} else {
			if(toDate.isEmpty()) {
				Calendar c = Calendar.getInstance();
				selectedYear = c.get(Calendar.YEAR);
				selectedMonth = c.get(Calendar.MONTH);
				selectedDate = c.get(Calendar.DAY_OF_MONTH);
			} else {
				selectedYear = toYear;
				selectedMonth = toMonth;
				selectedDate = toDay;
			}
		}

		DatePickerDialog dialog = new DatePickerDialog(this, R.style.DialogTheme, (datePicker, year, month, day)->{
			if(isFromDate) {
				fromDay = day;
				fromMonth = month;
				fromYear = year;
				fromDate = fromDay + "/" + (fromMonth + 1) + "/" + fromYear;
				filterBinding.formDateValue.setText(fromDate);
			} else {
				toDay = day;
				toMonth = month;
				toYear = year;
				toDate = toDay + "/" + (toMonth + 1) + "/" + toYear;
				filterBinding.toDateValue.setText(toDate);
			}
		}, selectedYear, selectedMonth, selectedDate);
		dialog.show();
		dialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
		dialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.white));
		dialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.dove_gray));
		if(isFromDate) {
			if(toDate.isEmpty()) {
				dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
			} else {
				dialog.getDatePicker().setMaxDate(new GregorianCalendar(toYear, toMonth, toDay).getTimeInMillis());
			}
		} else {
			dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
			if(!fromDate.isEmpty()) {
				dialog.getDatePicker().setMinDate(new GregorianCalendar(fromYear, fromMonth, fromDay).getTimeInMillis());
			}
		}
	}

	private void getOrderTransactionLog(boolean isFilter, String fromDate, String toDate) {
		pagedAdapter = new OrderTransactionLogPagedAdapter(this, imageViewPosition->gotoOrderDetails(pagedAdapter.getCurrentList().get(imageViewPosition).getOrderId()));
		binding.recyclerOrderList.setAdapter(pagedAdapter);
		DialogUtils.showLoader(this);
		viewModel.orderTransactionLog(this, isFilter, fromDate, toDate, position->{
			if(position == 0) {
				binding.noTransactionLog.setVisibility(View.VISIBLE);
				binding.recyclerOrderList.setVisibility(View.GONE);
			} else {
				binding.noTransactionLog.setVisibility(View.GONE);
				binding.recyclerOrderList.setVisibility(View.VISIBLE);
			}
		}, (position, value)->{ });
		viewModel.orderTransactionPagedList.observe(this, products->{
			DialogUtils.dismissLoader();
			pagedAdapter.submitList(products);
		});
	}

	private void gotoOrderDetails(int id) {
		Intent intent = new Intent(this, OrderDetailOngoingOrder.class);
		intent.putExtra("id", id);
		intent.putExtra("orderType", OrderConstants.OrderType.completed);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
	}

	private void gotoDashboard() {
		Intent intent = new Intent(OrderTransactionLogActivity.this, DashboardActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
	}

	private void gotoStore() {
		Intent intent = new Intent(OrderTransactionLogActivity.this, ProductAddItemsToStoreActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
	}

	private void backPressed() {
		super.onBackPressed();
	}
}