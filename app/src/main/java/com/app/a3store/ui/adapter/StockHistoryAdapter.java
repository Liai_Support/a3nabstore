package com.app.a3store.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.databinding.RowStockHistoryBinding;
import com.app.a3store.model.stock.StockHistoryResponse;
import com.app.a3store.utils.TimeCalculator;

import java.util.List;
import java.util.Locale;

public class StockHistoryAdapter extends RecyclerView.Adapter<StockHistoryAdapter.ViewHolder> {

	private final Context mContext;
	List<StockHistoryResponse.Data.Product> stockHistoryList;

	public StockHistoryAdapter(Context context, List<StockHistoryResponse.Data.Product> stockHistoryList) {
		this.mContext = context;
		this.stockHistoryList = stockHistoryList;
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {

		RowStockHistoryBinding binding;

		public ViewHolder(View itemView) {
			super(itemView);
			binding = RowStockHistoryBinding.bind(itemView);
		}
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
		View listItem = layoutInflater.inflate(R.layout.row_stock_history, parent, false);
		return new ViewHolder(listItem);
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
		StockHistoryResponse.Data.Product stockHistory = stockHistoryList.get(position);
		holder.binding.units.setText(String.format(Locale.ENGLISH, "%s %s", stockHistory.getUnits(), mContext.getString(R.string.units)));
		holder.binding.action.setText(stockHistory.getStockType());
		holder.binding.vendorName.setText(stockHistory.getVendorName());
		holder.binding.date.setText(TimeCalculator.getDateValue(stockHistory.getDate()));
		holder.binding.addedByValue.setText(stockHistory.getManagerName());
	}

	@Override
	public int getItemCount() {
		return Math.min(5, stockHistoryList.size());
	}
}
