package com.app.a3store.ui.adapter;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.databinding.RowEarningPastInfoBinding;
import com.app.a3store.model.EarningResponse;
import com.app.a3store.ui.interfaces.ItemClickListener;

import java.text.DateFormatSymbols;
import java.util.List;

public class EarningPastAdapter extends RecyclerView.Adapter<EarningPastAdapter.ViewHolder> {

	private final Context mContext;
	private final List<EarningResponse.Data.Past> pastList;
	private final ItemClickListener clickListener;
	private final int size;

	public EarningPastAdapter(Context context, List<EarningResponse.Data.Past> pastList, int size, ItemClickListener clickListener) {
		this.mContext = context;
		this.pastList = pastList;
		this.size = size;
		this.clickListener = clickListener;
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {

		RowEarningPastInfoBinding binding;

		public ViewHolder(View itemView) {
			super(itemView);
			binding = RowEarningPastInfoBinding.bind(itemView);
		}
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
		View listItem = layoutInflater.inflate(R.layout.row_earning_past_info, parent, false);
		return new ViewHolder(listItem);
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
		EarningResponse.Data.Past past = pastList.get(position);
		String Date = new DateFormatSymbols().getShortMonths()[past.getMonth() - 1].toUpperCase();
		holder.binding.date.setText(new SpannableStringBuilder(Date + " : " + past.getFrom() + " - " + past.getTo()));
		holder.binding.orders.setText(new SpannableStringBuilder(past.getOrders() + " " + mContext.getString(R.string.orders)));
		holder.binding.amount.setText(new SpannableStringBuilder(mContext.getString(R.string.sar) + " " + past.getAmount()));
		holder.binding.viewReceipt.setOnClickListener(v->clickListener.onItemClick(holder.getAdapterPosition()));
	}

	@Override
	public int getItemCount() {
		return size;
	}
}
