package com.app.a3store.ui.paging.orderTransactionLog;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.app.a3store.constants.AppConstants;
import com.app.a3store.constants.URLHelper;
import com.app.a3store.constants.UrlConstants;
import com.app.a3store.model.OrderTransactionResponse;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.ui.interfaces.ItemClickListenerWithTwoIds;
import com.app.a3store.utils.UrlUtils;
import com.app.a3store.volley.ApiCall;
import com.app.a3store.volley.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class OrderTransactionDataSource extends PageKeyedDataSource<Integer, OrderTransactionResponse.Data.Order> {

	public static int TOTAL_PAGE_NUM = 1;
	public static int PAGE_NUM = 1;
	public static int FIRST_PAGE = 1;
	private final Context context;
	private final ItemClickListener itemClickListener;
	boolean isFilter;
	String fromDate, toDate;
	private final ItemClickListenerWithTwoIds itemClickListenerWithTwoIds;

	OrderTransactionDataSource(Context mContext, boolean isFilter, String fromDate, String toDate, ItemClickListener mItemClickListener, ItemClickListenerWithTwoIds itemClickListenerWithTwoIds) {
		this.context = mContext;
		this.isFilter = isFilter;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.itemClickListener = mItemClickListener;
		this.itemClickListenerWithTwoIds = itemClickListenerWithTwoIds;
	}

	@Override
	public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, OrderTransactionResponse.Data.Order> callback) {
		ApiCall.PostMethod(getInputs(PAGE_NUM), new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				OrderTransactionResponse result = gson.fromJson(response.toString(), OrderTransactionResponse.class);

				if(result.getError().equals(AppConstants.kFalse) && result.getData() != null) {
					TOTAL_PAGE_NUM = result.getData().getPages();
					if(result.getData().getOrders() != null && result.getData().getOrders().size() > 0) {
						callback.onResult(result.getData().getOrders(), null, FIRST_PAGE + 1);
						itemClickListener.onItemClick(1);
					} else {
						itemClickListener.onItemClick(0);
						TOTAL_PAGE_NUM = 1;
					}
				} else {
					if(!result.getMessage().trim().isEmpty()) {
						itemClickListenerWithTwoIds.onItemClick(-1, result.getMessage());
					}
				}
			}

			@Override
			public void setResponseError(String error) {
				OrderTransactionResponse result = new OrderTransactionResponse();
				result.setError("true");
				result.setMessage(error);
			}
		});
	}

	@Override
	public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, OrderTransactionResponse.Data.Order> callback) {
		if(params.key > TOTAL_PAGE_NUM) return;
		ApiCall.PostMethod(getInputs(params.key), new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				OrderTransactionResponse result = gson.fromJson(response.toString(), OrderTransactionResponse.class);

				if(result.getError().equals(AppConstants.kFalse) && result.getData() != null) {
					TOTAL_PAGE_NUM = result.getData().getPages();
					if(result.getData().getOrders() != null && result.getData().getOrders().size() > 0) {
						int loadAfter = params.key;
						if(loadAfter > 1) {
							loadAfter = loadAfter + 1;
							callback.onResult(result.getData().getOrders(), loadAfter);
						} else {
							callback.onResult(result.getData().getOrders(), null);
						}

						int prevNo = params.key;
						if(params.key > 1) {
							prevNo = prevNo - 1;
							callback.onResult(result.getData().getOrders(), prevNo);
						} else {
							callback.onResult(result.getData().getOrders(), null);
						}
						itemClickListener.onItemClick(1);
					} else {
						itemClickListener.onItemClick(0);
					}
				} else {
					if(!result.getMessage().trim().isEmpty()) {
						itemClickListenerWithTwoIds.onItemClick(-1, result.getMessage());
					}
				}
			}

			@Override
			public void setResponseError(String error) {
				OrderTransactionResponse result = new OrderTransactionResponse();
				result.setError("true");
				result.setMessage(error);
			}
		});
	}

	@Override
	public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, OrderTransactionResponse.Data.Order> callback) {
		if(params.key > TOTAL_PAGE_NUM) return;
		ApiCall.PostMethod(getInputs(params.key), new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				OrderTransactionResponse result = gson.fromJson(response.toString(), OrderTransactionResponse.class);

				if(result.getError().equals(AppConstants.kFalse) && result.getData() != null) {
					TOTAL_PAGE_NUM = result.getData().getPages();
					if(result.getData().getOrders() != null && result.getData().getOrders().size() > 0) {
						int loadAfter = params.key;
						if(loadAfter > 1) {
							loadAfter = loadAfter + 1;
							callback.onResult(result.getData().getOrders(), loadAfter);
						} else {
							callback.onResult(result.getData().getOrders(), null);
						}
						itemClickListener.onItemClick(1);
					} else {
						itemClickListener.onItemClick(0);
					}
				} else {
					if(!result.getMessage().trim().isEmpty()) {
						itemClickListenerWithTwoIds.onItemClick(-1, result.getMessage());
					}
				}
			}

			@Override
			public void setResponseError(String error) {

				OrderTransactionResponse result = new OrderTransactionResponse();
				result.setError("true");
				result.setMessage(error);
			}
		});
	}

	private InputForAPI getInputs(int pageNum) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put(UrlConstants.kPageNumber, pageNum);
			if(isFilter) {
				jsonObject.put(UrlConstants.kFromDate, fromDate);
				jsonObject.put(UrlConstants.kToDate, toDate);
			}
		} catch(JSONException e) {
			e.printStackTrace();
		}
		return UrlUtils.postInputs(context, jsonObject, URLHelper.OrderTransactionLog);
	}
}
