package com.app.a3store.ui.paging.completedorders;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.app.a3store.model.orders.OrderInfo;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.ui.interfaces.ItemClickListenerWithTwoIds;

public class OrdersCompletedSourceFactory extends DataSource.Factory<Integer, OrderInfo> {

	//creating the mutable live data
	private final MutableLiveData<PageKeyedDataSource<Integer, OrderInfo>> itemLiveDataSource = new MutableLiveData<>();
	Context mContext;
	ItemClickListener mItemClickListener;
	ItemClickListenerWithTwoIds mItemClickListenerWithTwoIds;
	String status;

	public OrdersCompletedSourceFactory(Context context, String mStatus, ItemClickListener itemClickListener, ItemClickListenerWithTwoIds itemClickListenerWithTwoIds) {
		this.mContext = context;
		this.mItemClickListener = itemClickListener;
		this.mItemClickListenerWithTwoIds = itemClickListenerWithTwoIds;
		this.status = mStatus;
	}

	@Override
	public DataSource<Integer, OrderInfo> create() {
		String TAG = "OrderPickupFactory";
		Log.d(TAG, "pagingOngoingOrders:");
		//getting our data source object
		OrdersCompletedDataSource itemDataSource = new OrdersCompletedDataSource(mContext, status, mItemClickListener, mItemClickListenerWithTwoIds);

		//posting the datasource to get the values
		itemLiveDataSource.postValue(itemDataSource);

		//returning the datasource
		return itemDataSource;
	}

	public MutableLiveData<PageKeyedDataSource<Integer, OrderInfo>> getItemLiveDataSource() {
		return itemLiveDataSource;
	}
}
