package com.app.a3store.ui.paging.notificationList;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.app.a3store.model.NotificationResponse;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.ui.interfaces.ItemClickListenerWithTwoIds;

public class NotificationDataSourceFactory extends DataSource.Factory<Integer, NotificationResponse.Data.Notification> {

	private final MutableLiveData<PageKeyedDataSource<Integer, NotificationResponse.Data.Notification>> itemLiveDataSource = new MutableLiveData<>();
	Context mContext;
	ItemClickListener mItemClickListener;
	ItemClickListenerWithTwoIds mItemClickListenerWithTwoIds;

	public NotificationDataSourceFactory(Context context, ItemClickListener itemClickListener, ItemClickListenerWithTwoIds mItemClickListenerWithTwoIds) {
		this.mContext = context;
		this.mItemClickListener = itemClickListener;
		this.mItemClickListenerWithTwoIds = mItemClickListenerWithTwoIds;
	}

	@Override
	public DataSource<Integer, NotificationResponse.Data.Notification> create() {
		NotificationDataSource itemDataSource = new NotificationDataSource(mContext, mItemClickListener, mItemClickListenerWithTwoIds);
		itemLiveDataSource.postValue(itemDataSource);
		return itemDataSource;
	}

	public MutableLiveData<PageKeyedDataSource<Integer, NotificationResponse.Data.Notification>> getItemLiveDataSource() {
		return itemLiveDataSource;
	}
}
