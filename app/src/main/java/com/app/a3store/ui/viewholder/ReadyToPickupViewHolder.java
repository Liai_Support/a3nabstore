package com.app.a3store.ui.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.model.dashboard.DashboardResponse;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.utils.TimeCalculator;

public class ReadyToPickupViewHolder extends RecyclerView.ViewHolder {
	private ItemClickListener readyForPickupListener;
	private ItemClickListener viewClickListener;
	public TextView orderId, orderedOn, deliveredOn,readyToPickup;

	public ReadyToPickupViewHolder(View itemView) {
		super(itemView);
		this.orderId = itemView.findViewById(R.id.orderIdVal);
		this.orderedOn = itemView.findViewById(R.id.orderOnVal);
		this.deliveredOn = itemView.findViewById(R.id.deliverByVal);
		this.readyToPickup = itemView.findViewById(R.id.readyToPickup);
	}

	public void bindData( int position, DashboardResponse.Data.Order  data,ItemClickListener mReadyForPickupListener, ItemClickListener mViewClickListener) {
		this.orderId.setText(data.getOrderIDs());
		this.orderedOn.setText(data.getOrderOn());
	//	this.deliveredOn.setText(data.getDeliveryOn());
		if(data.getDeliveryDate() != null) {
			String orderDate = TimeCalculator.getTimeSlot(data.getFromTime()) +"-"+TimeCalculator.getTimeSlot(data.getToTime()) + " | " + TimeCalculator.getDate(data.getDeliveryDate());
			this.deliveredOn.setText(orderDate);
		}

		this.viewClickListener = mViewClickListener;
		this.readyForPickupListener = mReadyForPickupListener;
		this.readyToPickup.setOnClickListener(view->readyForPickupListener.onItemClick(position));
		itemView.setOnClickListener(view->viewClickListener.onItemClick(position));

	}
}
