package com.app.a3store.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.model.orders.OrderDetailsResponse;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.ui.interfaces.ItemClickListenerWithTwoIds;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;
import java.util.Locale;
import java.util.logging.Handler;

public class OrderItemsAdapter extends RecyclerView.Adapter<OrderItemsAdapter.ViewHolder> {

    Context context;
    List<OrderDetailsResponse.Data.OrderItem> orderItems;
    boolean isEnable = false;
    ItemClickListenerWithTwoIds mItemClickListener;

    public OrderItemsAdapter(Context context, List<OrderDetailsResponse.Data.OrderItem> orderItems, Boolean isEnable, ItemClickListenerWithTwoIds itemClickListener) {
        this.context = context;
        this.orderItems = orderItems;
        this.isEnable = isEnable;
        this.mItemClickListener = itemClickListener;
    }

    public OrderDetailsResponse.Data.OrderItem getItem(int position) {
        if (position >= 0 && position < this.orderItems.size()) {
            return this.orderItems.get(position);
        }
        return null;
    }

    public void updateItemInfo(int position, int value) {
        if (orderItems != null && position < orderItems.size()) {
            OrderDetailsResponse.Data.OrderItem item = orderItems.get(position);
            item.setProductStatusPacked(value);
            orderItems.remove(position);
            orderItems.add(position, item);
            notifyItemChanged(position);
        }
    }

    public void setList(List<OrderDetailsResponse.Data.OrderItem> orderItems) {
        this.orderItems = orderItems;
        notifyDataSetChanged();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView itemName, itemPrice, priceQty, splInstruction, totalPrice;
        public AppCompatImageView orderImage, orderStatusImage;

        public AppCompatTextView appCompatTextView, text2;
        public AppCompatTextView cuttingStyle, boxingStyle;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemName = itemView.findViewById(R.id.itemName);
            this.itemPrice = itemView.findViewById(R.id.itemPrice);
            this.priceQty = itemView.findViewById(R.id.priceQty);
            this.orderImage = itemView.findViewById(R.id.orderImage);
            this.splInstruction = itemView.findViewById(R.id.splInstruction);
            this.totalPrice = itemView.findViewById(R.id.totalPrice);
            this.orderStatusImage = itemView.findViewById(R.id.orderStatusImage);

            this.appCompatTextView = itemView.findViewById(R.id.appCompatTextView);
            this.text2 = itemView.findViewById(R.id.text2);
            this.cuttingStyle = itemView.findViewById(R.id.cuttingStyle);
            this.boxingStyle = itemView.findViewById(R.id.boxingStyle);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_order_items_basic, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OrderDetailsResponse.Data.OrderItem orderItem = orderItems.get(position);

        if(new SharedHelper(context).getSelectedLanguage().equalsIgnoreCase("ar")){
            holder.itemName.setText(orderItem.getArabicName());
        }
        else {
            holder.itemName.setText(orderItem.getProductName());
        }

        double discountPrice = orderItem.getPrice();
        if (orderItem.getDiscount() != 0) {
            discountPrice = orderItem.getPrice() * (orderItem.getDiscount() / 100f);
            discountPrice = orderItem.getPrice() - discountPrice;
        }

        if(orderItem.getBoxPrice() != null){
            discountPrice += Double.parseDouble(orderItem.getBoxPrice());
        }

        if(orderItem.getCuttingPrice() != null){
            discountPrice += Double.parseDouble(orderItem.getCuttingPrice());
        }


        holder.itemPrice.setText(Utils.round(discountPrice));


        if(new SharedHelper(context).getSelectedLanguage().toLowerCase().equalsIgnoreCase("ar")){
            holder.priceQty.setText(String.format(Locale.ENGLISH, "%dX", orderItem.getQuantity()));
        }else {
            holder.priceQty.setText(String.format(Locale.ENGLISH, "X%d", orderItem.getQuantity()));
        }


        holder.totalPrice.setText(Utils.round(orderItem.getQuantity() * discountPrice));


        if (!orderItem.getOrderInstructions().isEmpty())
            holder.splInstruction.setText(orderItem.getOrderInstructions());
        RequestOptions options = new RequestOptions().centerCrop().placeholder(R.drawable.applogo).error(R.drawable.applogo);
        Glide.with(context).load(orderItem.getProductImage()).apply(options).into(holder.orderImage);

        if (isEnable) {
            holder.orderStatusImage.setVisibility(View.VISIBLE);
        } else {
            holder.orderStatusImage.setVisibility(View.INVISIBLE);
        }

        if (orderItem.getProductStatusPacked() == 1) {
            holder.orderStatusImage.setImageResource(R.drawable.green_enable);
        } else {
            holder.orderStatusImage.setImageResource(R.drawable.orange_disable);
        }

        holder.orderStatusImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (orderItem.getProductStatusPacked() == 1) {
                    mItemClickListener.onItemClick(holder.getAdapterPosition(), "0");
                } else {
                    mItemClickListener.onItemClick(holder.getAdapterPosition(), "1");
                }
            }
        });

        if (orderItem.getCuttingName() == null || orderItem.getCuttingName().toString().trim().isEmpty()) {
            holder.cuttingStyle.setVisibility(View.GONE);
            holder.appCompatTextView.setVisibility(View.GONE);
        } else {
            holder.cuttingStyle.setVisibility(View.VISIBLE);
            holder.appCompatTextView.setVisibility(View.VISIBLE);
            holder.cuttingStyle.setText(orderItem.getCuttingName().toString());
        }


        if (orderItem.getBoxName() == null || orderItem.getBoxName().toString().trim().isEmpty()) {
            holder.boxingStyle.setVisibility(View.GONE);
            holder.text2.setVisibility(View.GONE);
        } else {
            holder.boxingStyle.setVisibility(View.VISIBLE);
            holder.text2.setVisibility(View.VISIBLE);
            holder.boxingStyle.setText(orderItem.getBoxName().toString());
        }

    }

    @Override
    public int getItemCount() {
        return orderItems.size();
    }
}
