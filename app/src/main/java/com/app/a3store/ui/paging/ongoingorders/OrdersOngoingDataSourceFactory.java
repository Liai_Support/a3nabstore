package com.app.a3store.ui.paging.ongoingorders;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.app.a3store.model.orders.OrderInfo;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.ui.interfaces.ItemClickListenerWithTwoIds;

public class OrdersOngoingDataSourceFactory extends DataSource.Factory<Integer, OrderInfo> {

	//creating the mutable live data
	private final MutableLiveData<PageKeyedDataSource<Integer, OrderInfo>> itemLiveDataSource = new MutableLiveData<>();
	Context mContext;
	ItemClickListener mItemClickListener;
	ItemClickListenerWithTwoIds mItemClickListenerWithTwoIds;

	public OrdersOngoingDataSourceFactory(Context context, ItemClickListener itemClickListener, ItemClickListenerWithTwoIds itemClickListenerWithTwoIds) {
		this.mContext = context;
		this.mItemClickListener = itemClickListener;
		this.mItemClickListenerWithTwoIds = itemClickListenerWithTwoIds;
	}

	@Override
	public DataSource<Integer, OrderInfo> create() {
		String TAG = "OrderPickupFactory";
		Log.d(TAG, "pagingOngoingOrders:");
		//getting our data source object
		OrdersOngoingDataSource itemDataSource = new OrdersOngoingDataSource(mContext, mItemClickListener, mItemClickListenerWithTwoIds);

		//posting the dataSource to get the values
		itemLiveDataSource.postValue(itemDataSource);

		//returning the dataSource
		return itemDataSource;
	}

	public MutableLiveData<PageKeyedDataSource<Integer, OrderInfo>> getItemLiveDataSource() {
		return itemLiveDataSource;
	}
}
