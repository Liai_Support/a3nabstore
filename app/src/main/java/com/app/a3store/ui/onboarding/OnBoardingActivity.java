package com.app.a3store.ui.onboarding;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.viewpager.widget.ViewPager;

import com.app.a3store.R;
import com.app.a3store.model.onboard.OnBoardItem;
import com.app.a3store.ui.activity.LoginActivity;
import com.app.a3store.ui.adapter.OnBoardAdapter;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import java.util.ArrayList;

public class OnBoardingActivity extends AppCompatActivity {

	//Variables
	ViewPager viewPager;
	AppCompatButton next;
	AppCompatTextView skip;
	OnBoardAdapter adapter;
	int currentPos;
	ArrayList<OnBoardItem> onBoardItems = new ArrayList<>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_on_boarding);
		AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

		loadData();
		viewPager = findViewById(R.id.slider);
		DotsIndicator dotsIndicator = findViewById(R.id.dots_indicator);
		adapter = new OnBoardAdapter(this, onBoardItems);
		viewPager.setAdapter(adapter);
		dotsIndicator.setViewPager(viewPager);
		next = findViewById(R.id.next);
		skip = findViewById(R.id.skip);
		viewPager.addOnPageChangeListener(changeListener);

		next.setOnClickListener(view->{
			if(viewPager.getCurrentItem() != adapter.getCount() - 1) {
				viewPager.setCurrentItem(currentPos + 1);
			} else {
				openLoginScreen();
			}
		});

		skip.setOnClickListener(view->openLoginScreen());
	}

	public void loadData() {

		int[] header = {R.string.ob_title1, R.string.ob_title2, R.string.ob_title3, R.string.ob_title4,};
		int[] desc = {R.string.ob_desc1, R.string.ob_desc2, R.string.ob_desc3, R.string.ob_desc4,};
		int[] imageId = {R.drawable.slider_image_1, R.drawable.slider_image_2, R.drawable.slider_image_3, R.drawable.slider_image_4};

		for(int i = 0; i < imageId.length; i++) {
			OnBoardItem item = new OnBoardItem();
			item.setImageID(imageId[i]);
			item.setTitle(getResources().getString(header[i]));
			item.setDescription(getResources().getString(desc[i]));

			onBoardItems.add(item);
		}
	}

	ViewPager.OnPageChangeListener changeListener = new ViewPager.OnPageChangeListener() {
		@Override
		public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

		}

		@Override
		public void onPageSelected(int position) {
			currentPos = position;
		}

		@Override
		public void onPageScrollStateChanged(int state) {

		}
	};

	private void openLoginScreen() {
		Intent intent = new Intent(this, LoginActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		finishAffinity();
	}

	private void closeActivity() {
		finishAffinity();
	}

	@Override
	public void onBackPressed() {
		closeActivity();
	}
}