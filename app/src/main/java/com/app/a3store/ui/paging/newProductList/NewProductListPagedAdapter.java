package com.app.a3store.ui.paging.newProductList;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.constants.AppConstants;
import com.app.a3store.databinding.RowProductInfoBinding;
import com.app.a3store.model.ProductUnavailableResponse;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;

import java.util.Locale;
import java.util.Objects;

import static com.app.a3store.constants.AppConstants.kActive;
import static com.app.a3store.constants.AppConstants.kPercentage;

public class NewProductListPagedAdapter extends PagedListAdapter<ProductUnavailableResponse.Data.Product, NewProductListPagedAdapter.ItemViewHolder> {

    private final Context mContext;
    ItemClickListener itemClickListener;
    int previousPosition = -1, currentPosition = -1;

    public NewProductListPagedAdapter(Context context, ItemClickListener itemClickListener) {
        super(DIFF_CALLBACK);
        this.mContext = context;
        this.itemClickListener = itemClickListener;
    }

    public void deselectProduct() {
        itemClickListener.onItemClick(-1);
        currentPosition = -1;
        previousPosition = -1;
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        RowProductInfoBinding binding;

        public ItemViewHolder(View itemView) {
            super(itemView);
            binding = RowProductInfoBinding.bind(itemView);
        }
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_product_info, parent, false);
        return new ItemViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        ProductUnavailableResponse.Data.Product product = getItem(holder.getAdapterPosition());

        if (product.getProductImages() != null && product.getProductImages().size() != 0)
            Utils.loadImage(holder.binding.productImage, product.getProductImages().get(0).getProductImage());


        if(new SharedHelper(mContext).getSelectedLanguage().equalsIgnoreCase("ar")){
            holder.binding.productName.setText(product.getArabicName());
            holder.binding.productWeight.setText(String.format(Locale.getDefault(), "/ %s", product.getProductWeight()));
        }else {
            holder.binding.productName.setText(product.getProductName());
            holder.binding.productWeight.setText(String.format(Locale.getDefault(), "/ %s", product.getProductWeight()));
        }

        if (product.getProductDiscountStatus().equalsIgnoreCase(kPercentage)) {
            if (product.getProductDiscount() > 0) {
                holder.binding.discount.setVisibility(View.VISIBLE);
                holder.binding.viewDiscount.setVisibility(View.VISIBLE);
                holder.binding.oldPrice.setVisibility(View.VISIBLE);
                holder.binding.discount.setText(String.format(Locale.getDefault(), "%% %d", product.getProductDiscount()));
                holder.binding.oldPrice.setText(String.format(Locale.getDefault(), "%s %s", Utils.round(product.getProductPrice()), mContext.getString(R.string.sar)));
                holder.binding.oldPrice.setPaintFlags(holder.binding.oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                if(new SharedHelper(mContext).getSelectedLanguage().equals("en")) {
                    holder.binding.viewDiscount.setBackgroundResource(R.drawable.product_discount_bg);
                } else {
                    holder.binding.viewDiscount.setBackgroundResource(R.drawable.product_discount_bg_arabic);
                }

            } else {
                holder.binding.discount.setVisibility(View.GONE);
                holder.binding.viewDiscount.setVisibility(View.GONE);
                holder.binding.oldPrice.setVisibility(View.GONE);
            }
            double price = product.getProductPrice() - ((product.getProductPrice() * product.getProductDiscount()) / 100);
            holder.binding.productPrice.setText(String.format(Locale.getDefault(), "%s %s", Utils.round(price), mContext.getString(R.string.sar)));
        }
        if (product.getProductStatus().equalsIgnoreCase(kActive)) {
            holder.binding.productStatus.setImageResource(R.drawable.green_enable_long);
        } else {
            holder.binding.productStatus.setImageResource(R.drawable.orange_disable_long);
        }
        if (currentPosition == position) {
            holder.binding.viewDisable.setVisibility(View.VISIBLE);
            holder.binding.check.setVisibility(View.VISIBLE);
        } else {
            holder.binding.viewDisable.setVisibility(View.GONE);
            holder.binding.check.setVisibility(View.INVISIBLE);
        }

        holder.binding.viewUnit.setVisibility(View.GONE);

        holder.itemView.setOnClickListener(v -> {
            if (currentPosition == holder.getAdapterPosition()) {
                itemClickListener.onItemClick(-1);
                currentPosition = -1;
                previousPosition = -1;
                notifyItemChanged(holder.getAdapterPosition());
            } else {
                itemClickListener.onItemClick(holder.getAdapterPosition());
                previousPosition = currentPosition;
                currentPosition = holder.getAdapterPosition();
                if (previousPosition != -1) notifyItemChanged(previousPosition);
                notifyItemChanged(currentPosition);
            }
        });
    }

    private static final DiffUtil.ItemCallback<ProductUnavailableResponse.Data.Product> DIFF_CALLBACK = new DiffUtil.ItemCallback<ProductUnavailableResponse.Data.Product>() {
        @Override
        public boolean areItemsTheSame(ProductUnavailableResponse.Data.Product oldItem, ProductUnavailableResponse.Data.Product newItem) {
            return oldItem.getId().equals(newItem.getId());
        }

        @Override
        public boolean areContentsTheSame(@NonNull ProductUnavailableResponse.Data.Product oldItem, @NonNull ProductUnavailableResponse.Data.Product newItem) {
            return Objects.equals(oldItem, newItem);
        }
    };
}
