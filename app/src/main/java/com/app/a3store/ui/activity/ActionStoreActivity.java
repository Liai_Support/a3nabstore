package com.app.a3store.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3store.BaseActivity;
import com.app.a3store.R;
import com.app.a3store.databinding.ActivityActionStoreBinding;
import com.app.a3store.dialogs.DialogUtils;
import com.app.a3store.model.product.Product;
import com.app.a3store.ui.paging.storeProduct.StoreProductPagedAdapter;
import com.app.a3store.utils.Utils;
import com.app.a3store.viewmodel.ActionStoreViewModel;

import java.util.List;

import static com.app.a3store.constants.AppConstants.kAction;
import static com.app.a3store.constants.AppConstants.kActive;
import static com.app.a3store.constants.AppConstants.kFalse;
import static com.app.a3store.constants.AppConstants.kInactive;

public class ActionStoreActivity extends BaseActivity {

	ActivityActionStoreBinding binding;
	ActionStoreViewModel viewModel;
	List<Product> productList;
	StoreProductPagedAdapter pagedAdapter;
	String type;
	int productPosition;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = DataBindingUtil.setContentView(this, R.layout.activity_action_store);
		viewModel = new ViewModelProvider(this).get(ActionStoreViewModel.class);
		type = getIntent().getStringExtra(kAction);
		setAdapters(type);
		setListeners();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == 2 && data != null && data.getExtras() != null) {
			productList.get(productPosition).setStoreStock(data.getLongExtra("storeStock", 0));
			pagedAdapter.notifyItemChanged(productPosition);
		} else if(requestCode == 3 && data != null && data.getExtras() != null) {
			productList.get(productPosition).setStoreStock(data.getLongExtra("storeStock", 0));
			productList.get(productPosition).setProductPrice(data.getDoubleExtra("productPrice", 0));
			pagedAdapter.notifyItemChanged(productPosition);
		} else if(requestCode == 17 && resultCode == RESULT_OK) {
			DialogUtils.showLoader(this);
			setAdapters(type);
		}
	}

	private void setAdapters(String type) {
		pagedAdapter = new StoreProductPagedAdapter(this, availablePosition->{
			productPosition = availablePosition;
			if(productList.get(productPosition).getStoreProductStatus().equalsIgnoreCase(kActive)) {
				Utils.showSnack(binding.getRoot(), getString(R.string.available_error));
			} else {
				DialogUtils.showLoader(this);
				viewModel.changeStoreProduct(this, productList.get(productPosition).getId(), kActive).observe(this, commonResponse->{
					DialogUtils.dismissLoader();
					if(commonResponse.getError().equalsIgnoreCase(kFalse)) {
						productList.get(productPosition).setStoreProductStatus(kActive);
						pagedAdapter.notifyItemChanged(productPosition);
						Utils.showSnack(binding.getRoot(), getString(R.string.product_status_message));
					} else {
						Utils.showSnack(binding.getRoot(), commonResponse.getMessage());
					}
				});
			}
		}, unavailablePosition->{
			productPosition = unavailablePosition;
			if(productList.get(productPosition).getStoreProductStatus().equalsIgnoreCase(kInactive)) {
				Utils.showSnack(binding.getRoot(), getString(R.string.unavailable_error));
			} else {
				DialogUtils.showLoader(this);
				viewModel.changeStoreProduct(this, productList.get(productPosition).getId(), kInactive).observe(this, commonResponse->{
					DialogUtils.dismissLoader();
					if(commonResponse.getError().equalsIgnoreCase(kFalse)) {
						productList.get(productPosition).setStoreProductStatus(kInactive);
						pagedAdapter.notifyItemChanged(productPosition);
						Utils.showSnack(binding.getRoot(), getString(R.string.product_status_message));
					} else {
						Utils.showSnack(binding.getRoot(), commonResponse.getMessage());
					}
				});
			}
		}, editPosition->{
			productPosition = editPosition;
			Intent intent;
			if(productList.get(productPosition).getManagerPrice().equalsIgnoreCase(kFalse)) {
				intent = new Intent(this, ProductDetailWithOutPriceChange.class);
			} else {
				intent = new Intent(this, ProductDetailPage.class);
			}
			intent.putExtra("productDetails", productList.get(productPosition));
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivityForResult(intent, 3);
		}, updatePosition->{
			productPosition = updatePosition;
			Intent intent = new Intent(this, CurrentStockActivity.class);
			intent.putExtra("productId", productList.get(productPosition).getId());
			intent.putExtra("storeStock", productList.get(productPosition).getStoreStock());
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivityForResult(intent, 2);
		});
		binding.recyclerProducts.setAdapter(pagedAdapter);
		DialogUtils.showLoader(this);
		viewModel.getProductOutOfStock(this, type, position->{
			if(position == 0) {
				binding.noProduct.setVisibility(View.VISIBLE);
				binding.recyclerProducts.setVisibility(View.GONE);
			} else {
				binding.noProduct.setVisibility(View.GONE);
				binding.recyclerProducts.setVisibility(View.VISIBLE);
			}
		}, (i, value)->{

		});
		viewModel.storeProductPagedList.observe(this, products->{
			DialogUtils.dismissLoader();
			productList = products;
			pagedAdapter.submitList(products);
		});
	}

	private void setListeners() {

		binding.backArrow.setOnClickListener(view->backPressed());
		binding.bottomDashboard.setOnClickListener(view->gotoDashboard());
		binding.bottomAssignmnet.setOnClickListener(view->gotoMyStore());
		binding.orders.setOnClickListener(view->{
			Intent intent = new Intent(ActionStoreActivity.this, OrderListingActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(intent);
		});
	}

	private void gotoDashboard() {
		Intent intent = new Intent(ActionStoreActivity.this, DashboardActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}

	private void gotoMyStore() {
		Intent intent = new Intent(ActionStoreActivity.this, ProductAddItemsToStoreActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
		finish();
	}

	private void backPressed() {
		super.onBackPressed();
	}
}