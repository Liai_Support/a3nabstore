package com.app.a3store.ui.paging.storeProduct;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.constants.AppConstants;
import com.app.a3store.databinding.RowAddProductToStoreBinding;
import com.app.a3store.model.product.Product;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;

import java.util.Locale;
import java.util.Objects;

import static com.app.a3store.constants.AppConstants.kActive;

public class StoreProductPagedAdapter extends PagedListAdapter<Product, StoreProductPagedAdapter.ItemViewHolder> {

	private final Context context;
	private final ItemClickListener availableClickListener, unavailableClickListener, editClickListener, updateClickListener;

	public StoreProductPagedAdapter(Context mContext, ItemClickListener availableClickListener, ItemClickListener unavailableClickListener, ItemClickListener editClickListener, ItemClickListener updateClickListener) {
		super(DIFF_CALLBACK);
		this.context = mContext;
		this.availableClickListener = availableClickListener;
		this.unavailableClickListener = unavailableClickListener;
		this.editClickListener = editClickListener;
		this.updateClickListener = updateClickListener;
	}

	public static class ItemViewHolder extends RecyclerView.ViewHolder {

		RowAddProductToStoreBinding binding;

		public ItemViewHolder(View itemView) {
			super(itemView);
			binding = RowAddProductToStoreBinding.bind(itemView);
		}
	}

	@NonNull
	@Override
	public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(context).inflate(R.layout.row_add_product_to_store, parent, false);
		return new ItemViewHolder(view);
	}

	@Override
	public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
		Product product = getItem(holder.getAdapterPosition());
		if(!product.getProductImages().isEmpty())
			Utils.loadImage(holder.binding.productImage, product.getProductImages().get(0).getProductImage());
		if(product.getStoreStock() < 1) {
			holder.binding.stockCount.setText(context.getString(R.string.out_of_stock));
			holder.binding.stockCount.setTextColor(context.getResources().getColor(R.color.flush_orange));
			holder.binding.viewStatus.setBackgroundColor(context.getResources().getColor(R.color.flush_orange));
		} else {
			if(product.getStoreProductStatus() != null && product.getStoreProductStatus().equalsIgnoreCase(kActive)) {
				holder.binding.viewStatus.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
			} else {
				holder.binding.viewStatus.setBackgroundColor(context.getResources().getColor(R.color.flush_orange));
			}
			holder.binding.stockCount.setText(String.format(Locale.ENGLISH, "%s %d %s", context.getString(R.string.stock), product.getStoreStock(), context.getString(R.string.units_small)));
			holder.binding.stockCount.setTextColor(context.getResources().getColor(R.color.colorPrimary));
		}

		if(new SharedHelper(context).getSelectedLanguage().equalsIgnoreCase(AppConstants.kEnglish)) {
			holder.binding.productName.setText(product.getProductName());
		}
		else {
			holder.binding.productName.setText(product.getArabicName());
		}

		if(!product.getProductWeight().isEmpty() && product.getProductWeight() != null){
			holder.binding.productDetails.setText(""+product.getProductWeight());
		}
		holder.binding.productPrice.setText(String.format(Locale.ENGLISH, "%s", Utils.round(product.getProductPrice())));

		holder.binding.available.setOnClickListener(view->availableClickListener.onItemClick(holder.getAdapterPosition()));
		holder.binding.unAvailable.setOnClickListener(view->unavailableClickListener.onItemClick(holder.getAdapterPosition()));
		holder.binding.edit.setOnClickListener(view->editClickListener.onItemClick(holder.getAdapterPosition()));
		holder.binding.update.setOnClickListener(view->updateClickListener.onItemClick(holder.getAdapterPosition()));
	}

	private static final DiffUtil.ItemCallback<Product> DIFF_CALLBACK = new DiffUtil.ItemCallback<Product>() {
		@Override
		public boolean areItemsTheSame(Product oldItem, Product newItem) {
			return oldItem.getId().equals(newItem.getId());
		}

		@Override
		public boolean areContentsTheSame(@NonNull Product oldItem, @NonNull Product newItem) {
			return Objects.equals(oldItem, newItem);
		}
	};
}
