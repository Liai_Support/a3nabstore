package com.app.a3store.ui.paging.storeProduct;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.app.a3store.constants.AppConstants;
import com.app.a3store.constants.URLHelper;
import com.app.a3store.constants.UrlConstants;
import com.app.a3store.model.ActionStoreResponse;
import com.app.a3store.model.product.Product;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.ui.interfaces.ItemClickListenerWithTwoIds;
import com.app.a3store.utils.UrlUtils;
import com.app.a3store.volley.ApiCall;
import com.app.a3store.volley.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class ProductListDataSource extends PageKeyedDataSource<Integer, Product> {

	public static int TOTAL_PAGE_NUM = 1;
	public static int PAGE_NUM = 1;
	public static int FIRST_PAGE = 1;
	private final Context context;
	private final String type;
	private final int id;
	private final ItemClickListener itemClickListener;
	private final ItemClickListenerWithTwoIds itemClickListenerWithTwoIds;

	ProductListDataSource(Context mContext, int id, String type, ItemClickListener mItemClickListener, ItemClickListenerWithTwoIds itemClickListenerWithTwoIds) {
		this.context = mContext;
		this.type = type;
		this.id = id;
		this.itemClickListener = mItemClickListener;
		this.itemClickListenerWithTwoIds = itemClickListenerWithTwoIds;
	}

	@Override
	public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, Product> callback) {
		String TAG = "PickupDataSource";
		Log.d(TAG, "loadInitial: ");
		ApiCall.PostMethod(getInputs(PAGE_NUM, type), new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				ActionStoreResponse result = gson.fromJson(response.toString(), ActionStoreResponse.class);

				if(result.getError().equals(AppConstants.kFalse) && result.getData() != null) {
					TOTAL_PAGE_NUM = result.getData().getPages();
					if(result.getData().getProducts() != null && result.getData().getProducts().size() > 0) {
						callback.onResult(result.getData().getProducts(), null, FIRST_PAGE + 1);
						itemClickListener.onItemClick(1);
					} else {
						itemClickListener.onItemClick(0);
						TOTAL_PAGE_NUM = 1;
					}
				} else {
					if(!result.getMessage().trim().isEmpty()) {
						itemClickListenerWithTwoIds.onItemClick(-1, result.getMessage());
					}
				}
			}

			@Override
			public void setResponseError(String error) {

				ActionStoreResponse result = new ActionStoreResponse();
				result.setError("true");
				result.setMessage(error);
			}
		});
	}

	@Override
	public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Product> callback) {
		if(params.key > TOTAL_PAGE_NUM) return;
		ApiCall.PostMethod(getInputs(params.key, type), new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				ActionStoreResponse result = gson.fromJson(response.toString(), ActionStoreResponse.class);

				if(result.getError().equals(AppConstants.kFalse) && result.getData() != null) {
					TOTAL_PAGE_NUM = result.getData().getPages();
					if(result.getData().getProducts() != null && result.getData().getProducts().size() > 0) {
						int loadAfter = params.key;
						if(loadAfter > 1) {
							loadAfter = loadAfter + 1;
							callback.onResult(result.getData().getProducts(), loadAfter);
						} else {
							callback.onResult(result.getData().getProducts(), null);
						}

						int prevNo = params.key;
						if(params.key > 1) {
							prevNo = prevNo - 1;
							callback.onResult(result.getData().getProducts(), prevNo);
						} else {
							callback.onResult(result.getData().getProducts(), null);
						}
						itemClickListener.onItemClick(1);
					} else {
						itemClickListener.onItemClick(0);
					}
				} else {
					if(!result.getMessage().trim().isEmpty()) {
						itemClickListenerWithTwoIds.onItemClick(-1, result.getMessage());
					}
				}
			}

			@Override
			public void setResponseError(String error) {

				ActionStoreResponse result = new ActionStoreResponse();
				result.setError("true");
				result.setMessage(error);
			}
		});
	}

	@Override
	public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Product> callback) {
		if(params.key > TOTAL_PAGE_NUM) return;
		ApiCall.PostMethod(getInputs(params.key, type), new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				ActionStoreResponse result = gson.fromJson(response.toString(), ActionStoreResponse.class);

				if(result.getError().equals(AppConstants.kFalse) && result.getData() != null) {
					TOTAL_PAGE_NUM = result.getData().getPages();
					if(result.getData().getProducts() != null && result.getData().getProducts().size() > 0) {
						int loadAfter = params.key;
						if(loadAfter > 1) {
							loadAfter = loadAfter + 1;
							callback.onResult(result.getData().getProducts(), loadAfter);
						} else {
							callback.onResult(result.getData().getProducts(), null);
						}
						itemClickListener.onItemClick(1);
					} else {
						itemClickListener.onItemClick(0);
					}
				} else {
					if(!result.getMessage().trim().isEmpty()) {
						itemClickListenerWithTwoIds.onItemClick(-1, result.getMessage());
					}
				}
			}

			@Override
			public void setResponseError(String error) {

				ActionStoreResponse result = new ActionStoreResponse();
				result.setError("true");
				result.setMessage(error);
			}
		});
	}

	private InputForAPI getInputs(int pageNum, String type) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put(UrlConstants.kID, id);
			jsonObject.put(UrlConstants.kType, type);
			jsonObject.put(UrlConstants.kPageNumber, pageNum);
		} catch(JSONException e) {
			e.printStackTrace();
		}
		return UrlUtils.postInputs(context, jsonObject, URLHelper.getProductList);
	}
}
