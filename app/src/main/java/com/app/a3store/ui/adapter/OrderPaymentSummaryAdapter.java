package com.app.a3store.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.databinding.RowOrderPaymentSummaryBinding;

public class OrderPaymentSummaryAdapter extends RecyclerView.Adapter<OrderPaymentSummaryAdapter.ViewHolder> {

	private final Context mContext;
	RowOrderPaymentSummaryBinding rowOrderPaymentSummaryBinding;

	public OrderPaymentSummaryAdapter(Context context) {
		this.mContext = context;
	}

	public class ViewHolder extends RecyclerView.ViewHolder {

		public ViewHolder(View itemView) {
			super(itemView);
			rowOrderPaymentSummaryBinding = RowOrderPaymentSummaryBinding.bind(itemView);
		}
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
		View listItem = layoutInflater.inflate(R.layout.row_order_payment_summary, parent, false);
		return new ViewHolder(listItem);
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

	}

	@Override
	public int getItemCount() {
		return 4;
	}
}
