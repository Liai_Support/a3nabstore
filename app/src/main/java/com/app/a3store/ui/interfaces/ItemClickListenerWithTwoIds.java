package com.app.a3store.ui.interfaces;

public interface ItemClickListenerWithTwoIds {

	void onItemClick(int position, String value);
}
