package com.app.a3store.ui.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.app.a3store.ui.fragment.CompletedFragment;
import com.app.a3store.ui.fragment.OngoingFragment;
import com.app.a3store.ui.fragment.ReadyToPickupFragment;

public class OrderStatusFragmentPagerAdapter extends FragmentStatePagerAdapter {

	public OrderStatusFragmentPagerAdapter(@NonNull FragmentManager fm, int behavior) {
		super(fm, behavior);
	}

	@Override
	public int getItemPosition(@NonNull Object object) {
		return POSITION_NONE;
	}

	@NonNull
	@Override
	public Fragment getItem(int position) {
		switch(position) {
			case 0:
				return new OngoingFragment();
			case 1:
				return new ReadyToPickupFragment();
			case 2:
				return new CompletedFragment();
			default:
				return null;
		}
	}

	@Override
	public int getCount() {
		return 3;
	}
}