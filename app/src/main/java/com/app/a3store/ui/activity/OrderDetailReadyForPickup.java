package com.app.a3store.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3store.BaseActivity;
import com.app.a3store.R;
import com.app.a3store.constants.UrlConstants;
import com.app.a3store.databinding.ActivityOrderDetailTemplateTwoBinding;
import com.app.a3store.dialogs.DialogUtils;
import com.app.a3store.model.orders.OrderDetailsResponse;
import com.app.a3store.ui.adapter.OrderItemsAdapter;
import com.app.a3store.ui.adapter.OrderItemsTemplateTwoAdapter;
import com.app.a3store.ui.adapter.OrderPaymentSummaryAdapter;
import com.app.a3store.ui.interfaces.ItemClickListenerWithTwoIds;
import com.app.a3store.utils.TimeCalculator;
import com.app.a3store.utils.Utils;
import com.app.a3store.utils.helper.SharedHelper;
import com.app.a3store.viewmodel.OderDetailsViewModel;

import java.util.List;

import static com.app.a3store.constants.AppConstants.kEnglish;
import static com.app.a3store.constants.AppConstants.kFalse;
import static com.app.a3store.constants.AppConstants.productCompletedStatusString;
import static com.app.a3store.constants.AppConstants.productOngoingStatusString;

public class OrderDetailReadyForPickup extends BaseActivity {

    ActivityOrderDetailTemplateTwoBinding orderDetailTemplateTwoBinding;
    OrderItemsTemplateTwoAdapter orderItemsAdapter;
    OrderItemsAdapter itemsAdapter;
    OrderPaymentSummaryAdapter orderPaymentSummaryAdapter;
    OderDetailsViewModel viewModel;
    public static final int REQUEST_CODE = 101;
    int orderID;
    boolean isOnGoing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        orderDetailTemplateTwoBinding = DataBindingUtil.setContentView(this, R.layout.activity_order_detail_template_two);
        viewModel = new ViewModelProvider(this).get(OderDetailsViewModel.class);
        Intent intent = getIntent();
        orderID = intent.getIntExtra("id", 0);
        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase(kEnglish)) {
            orderDetailTemplateTwoBinding.backArrow.setScaleX(1);
        } else {
            orderDetailTemplateTwoBinding.backArrow.setScaleX(-1);
        }
        setListeners();
        getOrderDetails();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {

                getOrderDetails();
            }
        } catch (Exception ex) {
            Utils.showSnack(orderDetailTemplateTwoBinding.getRoot(), ex.toString());
        }
    }

    private void setPickUpAdapters(List<OrderDetailsResponse.Data.OrderItem> orderItems, int isDeleteOrReplace) {
        orderItemsAdapter = new OrderItemsTemplateTwoAdapter(this, orderItems, isDeleteOrReplace,
                position -> {

            Intent intent = new Intent(OrderDetailReadyForPickup.this, SubCategoryActivity.class);
            intent.putExtra("categoryId", orderItems.get(position).getCategoryId());
            intent.putExtra("orderItemId", orderItems.get(position).getId());
            intent.putExtra("quantity", orderItems.get(position).getQuantity());
            startActivityForResult(intent, REQUEST_CODE);
        },
                position -> {
            DialogUtils.showLoader(this);
            viewModel.deleteOrderItem(this, orderItems.get(position).getId()).observe(this, orderDetailsResponse -> {
                DialogUtils.dismissLoader();
                if (!orderDetailsResponse.getError().equalsIgnoreCase(kFalse))
                    Utils.showSnack(orderDetailTemplateTwoBinding.getRoot(), orderDetailsResponse.getMessage());
                else {
                    getOrderDetails();
                }
            });
        },
                new ItemClickListenerWithTwoIds() {
            @Override
            public void onItemClick(int position, String value) {
                //Orders Status Change Function

                OrderDetailsResponse.Data.OrderItem itemInfo = orderItemsAdapter.getItem(position);
                if (itemInfo != null) {
                    viewModel.updateOrderPackedStatus(OrderDetailReadyForPickup.this, orderID, itemInfo.getProductId(), value).observe(OrderDetailReadyForPickup.this, response ->
                    {
                        DialogUtils.dismissLoader();
                        if (response.getError().equalsIgnoreCase("true")) {
                            Utils.showSnack(findViewById(android.R.id.content), response.getMessage());
                        } else {
                            orderItemsAdapter.updateItemInfo(position, Integer.parseInt(value));

//                            orderDetailTemplateTwoBinding.readyToPickup.postDelayed(() -> {
//                                if (isOnGoing) {
//                                    boolean isEnable = orderItemsAdapter.changeMakeReady();
//                                    if (isEnable) {
//                                        orderDetailTemplateTwoBinding.readyToPickup.setAlpha(1f);
//                                    } else {
//                                        orderDetailTemplateTwoBinding.readyToPickup.setAlpha(0.5f);
//                                    }
//
//                                    orderDetailTemplateTwoBinding.readyToPickup.setEnabled(isEnable);
//                                }
//                            }, 1000);

                        }
                    });
                }
            }
        }, (position, value) -> {

            orderDetailTemplateTwoBinding.readyToPickup.postDelayed(() -> {
                if (isOnGoing) {
                    boolean isEnable = orderItemsAdapter.changeMakeReady();
                    if (isEnable) {
                        orderDetailTemplateTwoBinding.readyToPickup.setAlpha(1f);
                    } else {
                        orderDetailTemplateTwoBinding.readyToPickup.setAlpha(0.5f);
                    }

                    orderDetailTemplateTwoBinding.readyToPickup.setEnabled(isEnable);
                }
            }, 1000);

        });

        orderDetailTemplateTwoBinding.recyclerOrderItemsList.setAdapter(orderItemsAdapter);
        orderPaymentSummaryAdapter = new OrderPaymentSummaryAdapter(this);

//        orderDetailTemplateTwoBinding.readyToPickup.postDelayed(() -> {
//            if (isOnGoing) {
//                boolean isEnable = orderItemsAdapter.changeMakeReady();
//                if (isEnable) {
//                    orderDetailTemplateTwoBinding.readyToPickup.setAlpha(1f);
//                } else {
//                    orderDetailTemplateTwoBinding.readyToPickup.setAlpha(0.5f);
//                }
//                orderDetailTemplateTwoBinding.readyToPickup.setEnabled(isEnable);
//            }
//        }, 1000);


    }

    private void setDeliveryAdapters(List<OrderDetailsResponse.Data.OrderItem> orderItems) {

        orderPaymentSummaryAdapter = new OrderPaymentSummaryAdapter(this);
        if (itemsAdapter != null) {
            itemsAdapter.setList(orderItems);

            return;
        }
        itemsAdapter = new OrderItemsAdapter(this, orderItems, false, new ItemClickListenerWithTwoIds() {
            @Override
            public void onItemClick(int position, String value) {
                //Orders Status Change Function
                OrderDetailsResponse.Data.OrderItem itemInfo = itemsAdapter.getItem(position);
                if (itemInfo != null) {
//                    viewModel.updateOrderPackedStatus(OrderDetailReadyForPickup.this, orderID, itemInfo.getProductId(), value).observe(OrderDetailReadyForPickup.this, response ->
//                    {
//                        DialogUtils.dismissLoader();
//                        if (response.getError().equalsIgnoreCase("true")) {
//                            Utils.showSnack(findViewById(android.R.id.content), response.getMessage());
//                        } else {
//                            itemsAdapter.updateItemInfo(position, Integer.parseInt(value));
//                        }
//                    });
                }
            }
        });
        orderDetailTemplateTwoBinding.recyclerOrderItemsList.setAdapter(itemsAdapter);

    }

    private void getOrderDetails() {
        DialogUtils.showLoader(this);
        viewModel.orderDetails(this, orderID).observe(this, orderDetailsResponse -> {
            DialogUtils.dismissLoader();
            if (orderDetailsResponse.getError().equalsIgnoreCase("true")) {
                Utils.showSnack(findViewById(android.R.id.content), orderDetailsResponse.getMessage());
            } else {
                if (orderDetailsResponse.getData() != null) {

                    orderDetailTemplateTwoBinding.invoiceValue.setText(String.format("SAR %s", Utils.round(orderDetailsResponse.getData().getTotalAmount())));
                    orderDetailTemplateTwoBinding.taxValue.setText(String.format("SAR %s", Utils.round(orderDetailsResponse.getData().getTax())));
                    orderDetailTemplateTwoBinding.txtFastDeliveryValue.setText(String.format("SAR %s", Utils.round(orderDetailsResponse.getData().getFasttax())));
                    orderDetailTemplateTwoBinding.totalValue.setText(String.format("SAR %s", Utils.round(orderDetailsResponse.getData().getGrandTotal())));
                    if (orderDetailsResponse.getData().getOrdersInfo() != null) {

                        if (orderDetailsResponse.getData().getOrdersInfo().getType() != null) {
                            orderDetailTemplateTwoBinding.paymentStatus.setText(orderDetailsResponse.getData().getOrdersInfo().getType());
                        } else {
                            orderDetailTemplateTwoBinding.paymentStatus.setText("");
                        }

                        if (orderDetailsResponse.getData().getOrderItems().size() != 0)
                            if (orderDetailsResponse.getData().getOrderItems().get(0).getStoreStatus().equalsIgnoreCase(productOngoingStatusString) || orderDetailsResponse.getData().getOrderItems().get(0).getStoreStatus().equalsIgnoreCase(productCompletedStatusString)) {
                                orderDetailTemplateTwoBinding.readyToPickup.setText(R.string.ready_for_delivery);
                                orderDetailTemplateTwoBinding.readyToPickup.setEnabled(false);
                                orderDetailTemplateTwoBinding.readyToPickup.setAlpha(0.5f);
                                orderDetailTemplateTwoBinding.readyToPickup.setBackgroundTintList(AppCompatResources.getColorStateList(this, R.color.nobel_with_opacity));
                                isOnGoing = false;
                            } else {
                                isOnGoing = true;
                                orderDetailTemplateTwoBinding.readyToPickup.setText(R.string.ready_to_pickup_small);
                                orderDetailTemplateTwoBinding.readyToPickup.setEnabled(false);
                                orderDetailTemplateTwoBinding.readyToPickup.setAlpha(0.5f);
                                orderDetailTemplateTwoBinding.readyToPickup.setBackgroundTintList(AppCompatResources.getColorStateList(this, R.color.nobel));
                            }
                        else
                            orderDetailTemplateTwoBinding.readyToPickup.setVisibility(View.GONE);

                        if (orderDetailsResponse.getData().getOrdersInfo().getOrderOn() != null) {
                            String orderDate = TimeCalculator.getTime(orderDetailsResponse.getData().getOrdersInfo().getOrderOn()) + " | " + TimeCalculator.getDate(orderDetailsResponse.getData().getOrdersInfo().getOrderOn());
                            orderDetailTemplateTwoBinding.orderOnVal.setText(orderDate);
                        }

//						if(orderDetailsResponse.getData().getOrdersInfo().getDeliveryOn() != null) {
//							String orderDate = TimeCalculator.getTime(orderDetailsResponse.getData().getOrdersInfo().getDeliveryOn()) + " | " + TimeCalculator.getDate(orderDetailsResponse.getData().getOrdersInfo().getDeliveryOn());
//							orderDetailTemplateTwoBinding.deliverByVal.setText(orderDate);
//						}
                        if (orderDetailsResponse.getData().getOrdersInfo().getDeliveryDate() != null) {
                            String orderDate = TimeCalculator.getTimeSlot(orderDetailsResponse.getData().getOrdersInfo().getFromTime())
                                    + "-" + TimeCalculator.getTimeSlot(orderDetailsResponse.getData().getOrdersInfo().getToTime())
                                    + " | " + TimeCalculator.getDate(orderDetailsResponse.getData().getOrdersInfo().getDeliveryDate());
                            orderDetailTemplateTwoBinding.deliverByVal.setText(orderDate);
                        }
                        orderDetailTemplateTwoBinding.orderIdVal.setText(orderDetailsResponse.getData().getOrdersInfo().getOrderIDs());
                        orderDetailTemplateTwoBinding.deliveryPersonName.setText(orderDetailsResponse.getData().getOrdersInfo().getDriverName());

                        if (orderDetailsResponse.getData().getOrdersInfo().getDriverNumber() != null && !orderDetailsResponse.getData().getOrdersInfo().getDriverNumber().trim().isEmpty()) {
                            orderDetailTemplateTwoBinding.groupCallDriver.setVisibility(View.VISIBLE);
                        } else {
                            orderDetailTemplateTwoBinding.groupCallDriver.setVisibility(View.GONE);
                        }
                        orderDetailTemplateTwoBinding.callDriver.setOnClickListener(v -> {
                            if (orderDetailsResponse.getData().getOrdersInfo().getDriverNumber() != null && !orderDetailsResponse.getData().getOrdersInfo().getDriverNumber().isEmpty()) {
                                Intent intent = new Intent(Intent.ACTION_DIAL);
                                intent.setData(Uri.parse("tel:" + orderDetailsResponse.getData().getOrdersInfo().getDriverNumber()));
                                startActivity(intent);
                            } else {
                                Utils.showSnack(orderDetailTemplateTwoBinding.getRoot(), getString(R.string.driver_call_error));
                            }
                        });
                        //						orderDetailTemplateTwoBinding.orderOnVal.setText(orderDetailsResponse.getData().getOrdersInfo().getOrderOn());
                        //						orderDetailTemplateTwoBinding.deliverByVal.setText(orderDetailsResponse.getData().getOrdersInfo().getDeliveryOn());
                    }
                    //					&& orderDetailsResponse.getData().getOrderItems().size() > 0
                    if (orderDetailsResponse.getData().getOrderItems() != null && orderDetailsResponse.getData().getOrderItems().size() != 0) {
                        if (orderDetailsResponse.getData().getOrderItems().get(0).getStoreStatus().equalsIgnoreCase(productOngoingStatusString) || orderDetailsResponse.getData().getOrderItems().get(0).getStoreStatus().equalsIgnoreCase(productCompletedStatusString)) {
                            setDeliveryAdapters(orderDetailsResponse.getData().getOrderItems());
                        } else {
                            setPickUpAdapters(orderDetailsResponse.getData().getOrderItems(), orderDetailsResponse.getData().getOrdersInfo().getDeleteItems());
                        }
                    }
                    if (orderDetailsResponse.getData().getOrderItems().isEmpty()) {
                        orderDetailTemplateTwoBinding.readyToPickup.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    private void setListeners() {

        orderDetailTemplateTwoBinding.backArrow.setOnClickListener(view -> backPressed());
        orderDetailTemplateTwoBinding.bottomDashboard.setOnClickListener(view -> gotoDashboard());
        orderDetailTemplateTwoBinding.imgNotification.setOnClickListener(view -> {
            Intent intent = new Intent(this, NotificationActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        });

        orderDetailTemplateTwoBinding.bottomAssignmnet.setOnClickListener(view -> {
            Intent intent = new Intent(OrderDetailReadyForPickup.this, ProductAddItemsToStoreActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        });

        orderDetailTemplateTwoBinding.orders.setOnClickListener(view -> {
            Intent intent = new Intent(OrderDetailReadyForPickup.this, OrderListingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        });

        orderDetailTemplateTwoBinding.imgNotification.setOnClickListener(view -> gotoNotifications());

        orderDetailTemplateTwoBinding.readyToPickup.setOnClickListener(view -> {
            orderDetailTemplateTwoBinding.readyToPickup.setEnabled(false);
            DialogUtils.showLoader(this);
            updateOrderStatus(orderID, UrlConstants.statusPickup);
        });
    }

    private void updateOrderStatus(int orderID, String status) {
        orderDetailTemplateTwoBinding.readyToPickup.setEnabled(true);
        viewModel.updateOrderStatus(OrderDetailReadyForPickup.this, orderID, status).observe(this, response -> {
            DialogUtils.dismissLoader();
            if (response.getError().equalsIgnoreCase(kFalse)) {
                orderDetailTemplateTwoBinding.readyToPickup.setText(R.string.ready_for_delivery);
                orderDetailTemplateTwoBinding.readyToPickup.setEnabled(false);
                finish();
            }
            Utils.showSnack(orderDetailTemplateTwoBinding.getRoot(), response.getMessage());
        });
    }

    private void gotoDashboard() {
        Intent intent = new Intent(OrderDetailReadyForPickup.this, DashboardActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private void gotoNotifications() {
        Intent intent = new Intent(OrderDetailReadyForPickup.this, NotificationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private void backPressed() {
        super.onBackPressed();
    }
}