package com.app.a3store.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3store.R;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.utils.helper.SharedHelper;

import static com.app.a3store.constants.AppConstants.kEnglish;

public class HomeActionNeedAdapter extends RecyclerView.Adapter<HomeActionNeedAdapter.ViewHolder> {

	private final Context mContext;
	private final int lowStock, outOfStock;
	private final ItemClickListener clickListener;

	public HomeActionNeedAdapter(Context context, int lowStock, int outOfStock, ItemClickListener clickListener) {
		this.mContext = context;
		this.lowStock = lowStock;
		this.outOfStock = outOfStock;
		this.clickListener = clickListener;
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {

		public TextView actionText;
		public AppCompatImageView arrow;

		public ViewHolder(View itemView) {
			super(itemView);
			this.actionText = itemView.findViewById(R.id.actionText);
			this.arrow = itemView.findViewById(R.id.arrow);
		}
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
		View listItem = layoutInflater.inflate(R.layout.row_actions_needed, parent, false);
		return new ViewHolder(listItem);
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
		if(new SharedHelper(mContext).getSelectedLanguage().equalsIgnoreCase(kEnglish)) {
			holder.arrow.setScaleX(1);
		} else {
			holder.arrow.setScaleX(-1);
		}
		if(holder.getAdapterPosition() == 0 && lowStock != 0) {
			if(lowStock == 0) {
				holder.itemView.setVisibility(View.GONE);
			} else {
				holder.itemView.setVisibility(View.GONE);
				holder.actionText.setText(new StringBuilder(lowStock + " " + mContext.getString(R.string.sample_data_1)));
			}
		} else {
			if(outOfStock == 0) {
				holder.itemView.setVisibility(View.GONE);
			} else {
				holder.itemView.setVisibility(View.VISIBLE);
				holder.actionText.setText(new StringBuilder(outOfStock + " " + mContext.getString(R.string.sample_data_2)));
			}
		}
		holder.itemView.setOnClickListener(view->clickListener.onItemClick(position));
	}

	@Override
	public int getItemCount() {
		return 1;
	}
}
