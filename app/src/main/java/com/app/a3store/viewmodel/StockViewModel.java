package com.app.a3store.viewmodel;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.app.a3store.constants.URLHelper;
import com.app.a3store.model.CommonResponse;
import com.app.a3store.model.stock.AddVendorResponse;
import com.app.a3store.model.stock.StockHistoryResponse;
import com.app.a3store.model.stock.StoreVendorResponse;
import com.app.a3store.repository.StockHistoryRepository;
import com.app.a3store.utils.UrlUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class StockViewModel extends ViewModel {

	public String expiryDate = "", vendorName = "";
	public String units = "";
	public int date, month, year;
	StockHistoryRepository stockHistoryRepository = new StockHistoryRepository();

	public LiveData<StockHistoryResponse> getStockHistory(Context context, int productId) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("productId", productId);
		} catch(JSONException e) {
			e.printStackTrace();
		}
		return stockHistoryRepository.getStockHistory(UrlUtils.postInputs(context, jsonObject, URLHelper.viewStockHistory));
	}

	public LiveData<StoreVendorResponse> getVendorList(Context context) {
		return stockHistoryRepository.getStoreVendorList(UrlUtils.getInputs(context, URLHelper.getStoreVendorList));
	}

	public LiveData<AddVendorResponse> addStoreVendor(Context context) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("vendorName", vendorName);
		} catch(JSONException e) {
			e.printStackTrace();
		}
		return stockHistoryRepository.addStoreVendor(UrlUtils.postInputs(context, jsonObject, URLHelper.addStoreVendor));
	}

	public LiveData<CommonResponse> updateStoreStock(Context context, int vendorId, int productId, String stockType, String expiryDate, long currentStock) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("vendorId", vendorId);
			jsonObject.put("productId", productId);
			jsonObject.put("stockType", stockType);
			jsonObject.put("units", units);
			jsonObject.put("expiryDate", expiryDate);
			jsonObject.put("currentStock", currentStock);
		} catch(JSONException e) {
			e.printStackTrace();
		}
		return stockHistoryRepository.updateStoreStock(UrlUtils.postInputs(context, jsonObject, URLHelper.updateStoreStock));
	}
}
