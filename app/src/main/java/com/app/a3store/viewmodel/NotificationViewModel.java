package com.app.a3store.viewmodel;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;

import com.app.a3store.constants.URLHelper;
import com.app.a3store.model.NotificationResponse;
import com.app.a3store.repository.CategoryRepository;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.ui.interfaces.ItemClickListenerWithTwoIds;
import com.app.a3store.ui.paging.notificationList.NotificationDataSource;
import com.app.a3store.ui.paging.notificationList.NotificationDataSourceFactory;
import com.app.a3store.utils.UrlUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class NotificationViewModel extends ViewModel {

	public LiveData<PageKeyedDataSource<Integer, NotificationResponse.Data.Notification>> pageKeyedDataSourceLiveData;
	public LiveData<PagedList<NotificationResponse.Data.Notification>> pagedListLiveData;

	CategoryRepository repository = new CategoryRepository();
	public void notificationList(Context context, ItemClickListener itemClickListener, ItemClickListenerWithTwoIds itemClickListenerWithTwoIds) {

		NotificationDataSourceFactory itemDataSourceFactory = new NotificationDataSourceFactory(context, itemClickListener, itemClickListenerWithTwoIds);
		pageKeyedDataSourceLiveData = itemDataSourceFactory.getItemLiveDataSource();
		PagedList.Config pagedListConfig = (new PagedList.Config.Builder()).setEnablePlaceholders(false).setPageSize(NotificationDataSource.TOTAL_PAGE_NUM).build();
		pagedListLiveData = (new LivePagedListBuilder<>(itemDataSourceFactory, pagedListConfig)).build();
	}

	public void updateNotificationView(Context context) {


		 repository.updateManagerCount(UrlUtils.getInputs(context, URLHelper.updateManagerCount));

	}
}
