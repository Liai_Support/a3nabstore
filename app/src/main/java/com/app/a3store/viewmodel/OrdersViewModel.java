package com.app.a3store.viewmodel;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;

import com.app.a3store.constants.URLHelper;
import com.app.a3store.constants.UrlConstants;
import com.app.a3store.model.CommonResponse;
import com.app.a3store.model.orders.OrderInfo;
import com.app.a3store.model.orders.OrderResponse;
import com.app.a3store.repository.OrderRepository;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.ui.interfaces.ItemClickListenerWithTwoIds;
import com.app.a3store.ui.paging.completedorders.OrdersCompletedDataSource;
import com.app.a3store.ui.paging.completedorders.OrdersCompletedPagedAdapter;
import com.app.a3store.ui.paging.completedorders.OrdersCompletedSourceFactory;
import com.app.a3store.ui.paging.ongoingorders.OrdersOngoingDataSource;
import com.app.a3store.ui.paging.ongoingorders.OrdersOngoingDataSourceFactory;
import com.app.a3store.ui.paging.ongoingorders.OrdersOngoingPagedAdapter;
import com.app.a3store.ui.paging.pickuporders.OrdersPickupDataSource;
import com.app.a3store.ui.paging.pickuporders.OrdersPickupDataSourceFactory;
import com.app.a3store.ui.paging.pickuporders.OrdersPickupPagedAdapter;
import com.app.a3store.utils.UrlUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class OrdersViewModel extends ViewModel {

	OrderRepository orderRepository = new OrderRepository();
	public OrdersPickupPagedAdapter readyToPickupAdapter;
	public OrdersOngoingPagedAdapter ongoingAdapter;
	public OrdersCompletedPagedAdapter completedPagedAdapter;

	public LiveData<CommonResponse> updateOrderStatus(Context context, int orderId, String status) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put(UrlConstants.kOrderId, orderId);
			jsonObject.put(UrlConstants.kStatus, status);
		} catch(JSONException e) {
			e.printStackTrace();
		}
		return orderRepository.updateOrderStatus(UrlUtils.postInputs(context, jsonObject, URLHelper.updateOrderStatus));
	}

	public LiveData<OrderResponse> getOngoingOrders(Context context) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put(UrlConstants.kPageNumber, 1);
		} catch(JSONException e) {
			e.printStackTrace();
		}
		return orderRepository.getOngoingOrders(UrlUtils.postInputs(context, jsonObject, URLHelper.ongoingOrderList));
	}

	/*****************************   PAGINATION **********************************/
	public LiveData<PagedList<OrderInfo>> orderPickupPagedList;
	public LiveData<PagedList<OrderInfo>> orderOngoingPagedList;
	public LiveData<PagedList<OrderInfo>> orderCompletedPagedList;
	public LiveData<PageKeyedDataSource<Integer, OrderInfo>> orderPickupDataSource;
	public LiveData<PageKeyedDataSource<Integer, OrderInfo>> orderCompletedDataSource;
	public LiveData<PageKeyedDataSource<Integer, OrderInfo>> orderOngoingDataSource;

	public void pagingPickupOrders(Context context, ItemClickListener itemClickListener, ItemClickListenerWithTwoIds itemClickListenerWithTwoIds) {
		OrdersPickupDataSourceFactory itemDataSourceFactory = new OrdersPickupDataSourceFactory(context, UrlConstants.Type_ReadyToPickup, itemClickListener, itemClickListenerWithTwoIds);

		//getting the live data source from data source factory
		orderPickupDataSource = itemDataSourceFactory.getItemLiveDataSource();

		//Getting PagedList config
		PagedList.Config pagedListConfig = (new PagedList.Config.Builder()).setEnablePlaceholders(false).setPageSize(OrdersPickupDataSource.TOTAL_PAGE_NUM).build();

		//Building the paged list
		orderPickupPagedList = (new LivePagedListBuilder<>(itemDataSourceFactory, pagedListConfig)).build();
	}

	public void pagingCompletedOrders(Context context, ItemClickListener itemClickListener, ItemClickListenerWithTwoIds itemClickListenerWithTwoIds) {
		OrdersCompletedSourceFactory itemDataSourceFactory = new OrdersCompletedSourceFactory(context, UrlConstants.Type_Completed, itemClickListener, itemClickListenerWithTwoIds);

		//getting the live data source from data source factory
		orderCompletedDataSource = itemDataSourceFactory.getItemLiveDataSource();

		//Getting PagedList config
		PagedList.Config pagedListConfig = (new PagedList.Config.Builder()).setEnablePlaceholders(false).setPageSize(OrdersCompletedDataSource.TOTAL_PAGE_NUM).build();

		//Building the paged list
		orderCompletedPagedList = (new LivePagedListBuilder<>(itemDataSourceFactory, pagedListConfig)).build();
	}

	public void pagingOngoingOrders(Context context, ItemClickListener itemClickListener, ItemClickListenerWithTwoIds itemClickListenerWithTwoIds) {
		String TAG = "OrdersVM";
		Log.d(TAG, "pagingOngoingOrders:");
		OrdersOngoingDataSourceFactory itemDataSourceFactory = new OrdersOngoingDataSourceFactory(context, itemClickListener, itemClickListenerWithTwoIds);

		//getting the live data source from data source factory
		orderOngoingDataSource = itemDataSourceFactory.getItemLiveDataSource();

		//Getting PagedList config
		PagedList.Config pagedListConfig = (new PagedList.Config.Builder()).setEnablePlaceholders(false).setPageSize(OrdersOngoingDataSource.TOTAL_PAGE_NUM).build();

		//Building the paged list
		orderOngoingPagedList = (new LivePagedListBuilder<>(itemDataSourceFactory, pagedListConfig)).build();
	}
}
