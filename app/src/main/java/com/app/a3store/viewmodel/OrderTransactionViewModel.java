package com.app.a3store.viewmodel;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;

import com.app.a3store.model.OrderTransactionResponse;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.ui.interfaces.ItemClickListenerWithTwoIds;
import com.app.a3store.ui.paging.ongoingorders.OrdersOngoingDataSource;
import com.app.a3store.ui.paging.orderTransactionLog.OrderTransactionDataSourceFactory;

import static com.app.a3store.constants.AppConstants.kDateLabel;

public class OrderTransactionViewModel extends ViewModel {

	public int fromDay, fromMonth, fromYear, toDay, toMonth, toYear;
	public String fromDate = kDateLabel, toDate = kDateLabel, fromDateFilter = "", toDateFilter = "";
	//	OrderTransactionRepository repository = new OrderTransactionRepository();
	public LiveData<PageKeyedDataSource<Integer, OrderTransactionResponse.Data.Order>> orderTransactionDataSource;
	public LiveData<PagedList<OrderTransactionResponse.Data.Order>> orderTransactionPagedList;

	/*public LiveData<OrderTransactionResponse> OrderTransactionLog(Context context, int orderId) {

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put(kOrderId, orderId);
		} catch(JSONException e) {
			e.printStackTrace();
		}

		return repository.OrderTransactionLog(UrlUtils.postLang(context, jsonObject, URLHelper.viewOrderDetails));
	}*/

	public void orderTransactionLog(Context context, boolean isFilter, String fromDate, String toDate, ItemClickListener itemClickListener, ItemClickListenerWithTwoIds itemClickListenerWithTwoIds) {

		OrderTransactionDataSourceFactory itemDataSourceFactory = new OrderTransactionDataSourceFactory(context, isFilter, fromDate, toDate, itemClickListener, itemClickListenerWithTwoIds);
		orderTransactionDataSource = itemDataSourceFactory.getItemLiveDataSource();
		PagedList.Config pagedListConfig = (new PagedList.Config.Builder()).setEnablePlaceholders(false).setPageSize(OrdersOngoingDataSource.TOTAL_PAGE_NUM).build();
		orderTransactionPagedList = (new LivePagedListBuilder<>(itemDataSourceFactory, pagedListConfig)).build();
	}
}
