package com.app.a3store.viewmodel;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.app.a3store.constants.URLHelper;
import com.app.a3store.model.CommonResponse;
import com.app.a3store.repository.ProductDetailRepository;
import com.app.a3store.utils.UrlUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class ProductDetailViewModel extends ViewModel {

	ProductDetailRepository productDetailRepository = new ProductDetailRepository();

	public LiveData<CommonResponse> updateStock(Context context, int productId, String productPrice, long storeStock, boolean inStock, boolean specialInstruction) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("productId", productId);
			jsonObject.put("productPrice", productPrice);
			jsonObject.put("storeStock", storeStock);
			jsonObject.put("inStock", inStock);
			jsonObject.put("specialInstruction", specialInstruction);
		} catch(JSONException e) {
			e.printStackTrace();
		}
		return productDetailRepository.updateStock(UrlUtils.postInputs(context, jsonObject, URLHelper.updateStock));
	}
}
