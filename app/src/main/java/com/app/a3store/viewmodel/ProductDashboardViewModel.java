package com.app.a3store.viewmodel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;

import com.app.a3store.constants.URLHelper;
import com.app.a3store.model.CommonResponse;
import com.app.a3store.model.ProductDashboardResponse;
import com.app.a3store.model.ProductSubCategoryResponse;
import com.app.a3store.model.product.Product;
import com.app.a3store.model.product.ProductListResponse;
import com.app.a3store.repository.ProductDashboardRepository;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.ui.interfaces.ItemClickListenerWithTwoIds;
import com.app.a3store.ui.paging.ongoingorders.OrdersOngoingDataSource;
import com.app.a3store.ui.paging.storeProduct.ProductListDataSourceFactory;
import com.app.a3store.utils.UrlUtils;
import com.app.a3store.utils.helper.SharedHelper;

import org.json.JSONException;
import org.json.JSONObject;

public class ProductDashboardViewModel extends AndroidViewModel {

	SharedHelper sharedHelper;
	ProductDashboardRepository repository = new ProductDashboardRepository();
	public int selectedCategoryId = 0, selectedSubCategoryId = 0;
	public LiveData<PageKeyedDataSource<Integer, Product>> storeProductDataSource;
	public LiveData<PagedList<Product>> storeProductPagedList;

	public ProductDashboardViewModel(@NonNull Application application) {
		super(application);
		sharedHelper = new SharedHelper(application.getBaseContext());
	}

	public MutableLiveData<ProductDashboardResponse> getCategoryDetails(Context context) {
		return repository.getProductDashboard(UrlUtils.getInputs(context, URLHelper.homeDashboard));
	}

	public MutableLiveData<ProductSubCategoryResponse> getSubCategoryDetails(Context context) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("id", selectedCategoryId);
		} catch(JSONException e) {
			e.printStackTrace();
		}
		return repository.getProductSubCategory(UrlUtils.postInputs(context, jsonObject, URLHelper.getProductSubCategory));
	}

	public MutableLiveData<ProductListResponse> getProductsList(Context context) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("id", selectedSubCategoryId > 0 ? selectedSubCategoryId : selectedCategoryId);
			jsonObject.put("type", selectedSubCategoryId > 0 ? "SUBCATEGORY" : "CATEGORY");
			jsonObject.put("pageNumber", 1);
		} catch(JSONException e) {
			e.printStackTrace();
		}
		return repository.getProductList(UrlUtils.postInputs(context, jsonObject, URLHelper.getProductList));
	}

	public MutableLiveData<ProductListResponse> getSearchProductList(Context context, String name) {

		JSONObject jsonObject = new JSONObject();
		try {

			jsonObject.put("id", selectedSubCategoryId > 0 ? selectedSubCategoryId : selectedCategoryId);
			jsonObject.put("type", selectedSubCategoryId > 0 ? "SUBCATEGORY" : "CATEGORY");
			jsonObject.put("name", name);
		} catch(JSONException e) {
			e.printStackTrace();
		}

		return repository.getProductList(UrlUtils.postInputs(context, jsonObject, URLHelper.productSearch));
	}

	public MutableLiveData<CommonResponse> changeStoreProduct(Context context, int productId, String storeProductStatus) {

		JSONObject jsonObject = new JSONObject();
		try {

			jsonObject.put("productId", productId);
			jsonObject.put("storeProductStatus", storeProductStatus);
		} catch(JSONException e) {
			e.printStackTrace();
		}

		return repository.changeStoreProduct(UrlUtils.postInputs(context, jsonObject, URLHelper.changeStoreProduct));
	}

	public MutableLiveData<CommonResponse> changeStoreCategoryProduct(Context context, int categoryId, String storeProductStatus) {

		JSONObject jsonObject = new JSONObject();
		try {

			jsonObject.put("categoryId", categoryId);
			jsonObject.put("storeProductStatus", storeProductStatus);
		} catch(JSONException e) {
			e.printStackTrace();
		}

		return repository.changeStoreProduct(UrlUtils.postInputs(context, jsonObject, URLHelper.changeStoreCategoryProduct));
	}

	public void getProductOutOfStock(Context context, ItemClickListener itemClickListener, ItemClickListenerWithTwoIds itemClickListenerWithTwoIds) {
		int id = selectedSubCategoryId > 0 ? selectedSubCategoryId : selectedCategoryId;
		String type = selectedSubCategoryId > 0 ? "SUBCATEGORY" : "CATEGORY";
		ProductListDataSourceFactory itemDataSourceFactory = new ProductListDataSourceFactory(context, id, type, itemClickListener, itemClickListenerWithTwoIds);
		storeProductDataSource = itemDataSourceFactory.getItemLiveDataSource();
		PagedList.Config pagedListConfig = (new PagedList.Config.Builder()).setEnablePlaceholders(false).setPageSize(OrdersOngoingDataSource.TOTAL_PAGE_NUM).build();
		storeProductPagedList = (new LivePagedListBuilder<>(itemDataSourceFactory, pagedListConfig)).build();
	}
}
