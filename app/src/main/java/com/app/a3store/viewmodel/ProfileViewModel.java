package com.app.a3store.viewmodel;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.a3store.R;
import com.app.a3store.constants.URLHelper;
import com.app.a3store.constants.UrlConstants;
import com.app.a3store.model.CommonResponse;
import com.app.a3store.model.profile.ProfileDashboardResponse;
import com.app.a3store.model.profile.ProfileDetail;
import com.app.a3store.model.profile.UpdateProfileResponse;
import com.app.a3store.repository.ProfileRepository;
import com.app.a3store.utils.UrlUtils;
import com.app.a3store.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class ProfileViewModel extends ViewModel {

	private WeakReference<Context> context;
	public String firstName = "";
	public String lastName = "";
	public String emailId = "";
	public String phoneNumber = "";
	public String dob = "";
	public int date;
	public int month;
	public int year;
	public String status = "";
	public ProfileDetail profileDetail = new ProfileDetail();
	public MutableLiveData<String> errorMessage = new MutableLiveData<>();
	ProfileRepository profileRepository = new ProfileRepository();

	public void setContext(Context context) {
		this.context = new WeakReference<>(context);
	}

	/*******************************  Validations & Business Logic *****************************/
	private boolean validateFirstName() {
		if(!firstName.trim().isEmpty()) {
//			if(Utils.validName(firstName)) {
				return true;
//			} else {
//				errorMessage.setValue(context.get().getString(R.string.error_first_name_invalid));
//			}
		} else {
			errorMessage.setValue(context.get().getString(R.string.error_first_name_empty));
		}
		return false;
	}

	private boolean validateLastName() {
		if(!lastName.trim().isEmpty()) {
//			if(Utils.validName(lastName)) {
				return true;
//			} else {
//				errorMessage.setValue(context.get().getString(R.string.error_last_name_invalid));
//			}
		} else {
			errorMessage.setValue(context.get().getString(R.string.error_last_name_empty));
		}
		return false;
	}

	private boolean validateEmail() {
		if(emailId != null && !emailId.trim().isEmpty()) {
			if(Utils.validateEmail(emailId)) {
				return true;
			} else {
				errorMessage.setValue(context.get().getString(R.string.error_emailid_invalid));
			}
		} else {
			errorMessage.setValue(context.get().getString(R.string.error_emailid_empty));
		}
		return false;
	}

	private boolean validateMobileNumber() {
		if(!phoneNumber.trim().isEmpty()) {
			return true;
		} else {
			errorMessage.setValue(context.get().getString(R.string.error_emailid_empty));
		}
		return false;
	}

	public boolean validateProfileDetails() {
		return validateFirstName() && validateLastName() && validateEmail() && validateMobileNumber();
	}

	/********************************  API CALL *************************************************/

	public LiveData<ProfileDashboardResponse> profileDashBoard(Context context) {
		return profileRepository.profileDashBoard(UrlUtils.getInputs(context, URLHelper.profileDashBoard));
	}

	public LiveData<UpdateProfileResponse> updateProfile(Context context, JSONObject jsonObject) {
		return profileRepository.updateProfile(UrlUtils.postInputs(context, jsonObject, URLHelper.updateProfile));
	}

	public LiveData<CommonResponse> updateOnlineStatus(Context context, String status) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put(UrlConstants.kStatus, status);
		} catch(JSONException e) {
			e.printStackTrace();
		}
		return profileRepository.updateOnlineStatus(UrlUtils.postInputs(context,jsonObject, URLHelper.updateOnlineStatus));
	}
}
