package com.app.a3store.viewmodel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;

import com.app.a3store.constants.URLHelper;
import com.app.a3store.model.CommonResponse;
import com.app.a3store.model.ProductDashboardResponse;
import com.app.a3store.model.ProductSubCategoryResponse;
import com.app.a3store.model.ProductUnavailableResponse;
import com.app.a3store.repository.ProductUnavailableRepository;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.ui.interfaces.ItemClickListenerWithTwoIds;
import com.app.a3store.ui.paging.newProductList.NewProductListDataSourceFactory;
import com.app.a3store.ui.paging.pickuporders.OrdersPickupDataSource;
import com.app.a3store.utils.UrlUtils;
import com.app.a3store.utils.helper.SharedHelper;

import org.json.JSONException;
import org.json.JSONObject;

public class ProductUnavailableViewModel extends AndroidViewModel {

	SharedHelper sharedHelper;
	ProductUnavailableRepository repository = new ProductUnavailableRepository();
	public int selectedCategoryId = 0, selectedSubCategoryId = 0;
	public LiveData<PageKeyedDataSource<Integer, ProductUnavailableResponse.Data.Product>> dataSourceLiveData;
	public LiveData<PagedList<ProductUnavailableResponse.Data.Product>> pagedListLiveData;

	public ProductUnavailableViewModel(@NonNull Application application) {
		super(application);
		sharedHelper = new SharedHelper(application.getBaseContext());
	}

	public MutableLiveData<ProductDashboardResponse> getCategoryDetails(Context context) {
		return repository.getProductDashboard(UrlUtils.getInputs(context, URLHelper.homeDashboard));
	}

	public MutableLiveData<ProductSubCategoryResponse> getSubCategoryDetails(Context context) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("id", selectedCategoryId);
		} catch(JSONException e) {
			e.printStackTrace();
		}
		return repository.getProductSubCategory(UrlUtils.postInputs(context, jsonObject, URLHelper.getProductSubCategory));
	}

	public MutableLiveData<ProductUnavailableResponse> getSearchProductList(Context context, String name) {

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("text", name);
			jsonObject.put("id", selectedSubCategoryId > 0 ? selectedSubCategoryId : selectedCategoryId);
			jsonObject.put("type", selectedSubCategoryId > 0 ? "SUBCATEGORY" : "CATEGORY");
		} catch(JSONException e) {
			e.printStackTrace();
		}

		return repository.getProductList(UrlUtils.postInputs(context, jsonObject, URLHelper.searchNewProduct));
	}

	public MutableLiveData<CommonResponse> addNewProduct(Context context, int productId, int categoryId) {

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("productId", productId);
			jsonObject.put("categoryId", categoryId);
		} catch(JSONException e) {
			e.printStackTrace();
		}

		return repository.addNewProduct(UrlUtils.postInputs(context, jsonObject, URLHelper.addNewProduct));
	}

	public void getProductsList(Context context, ItemClickListener itemClickListener, ItemClickListenerWithTwoIds itemClickListenerWithTwoIds) {

		int id = selectedSubCategoryId > 0 ? selectedSubCategoryId : selectedCategoryId;
		String type = selectedSubCategoryId > 0 ? "SUBCATEGORY" : "CATEGORY";

		NewProductListDataSourceFactory itemDataSourceFactory = new NewProductListDataSourceFactory(context, itemClickListener, itemClickListenerWithTwoIds, id, type);

		//getting the live data source from data source factory
		dataSourceLiveData = itemDataSourceFactory.getItemLiveDataSource();

		//Getting PagedList config
		PagedList.Config pagedListConfig = (new PagedList.Config.Builder()).setEnablePlaceholders(false).setPageSize(OrdersPickupDataSource.TOTAL_PAGE_NUM).build();

		//Building the paged list
		pagedListLiveData = (new LivePagedListBuilder<>(itemDataSourceFactory, pagedListConfig)).build();
	}
}
