package com.app.a3store.viewmodel;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.app.a3store.constants.URLHelper;
import com.app.a3store.model.EarningResponse;
import com.app.a3store.model.profile.ProfileResponse;
import com.app.a3store.repository.EarningRepository;
import com.app.a3store.utils.UrlUtils;

public class EarningViewModel extends ViewModel {

	EarningRepository repository = new EarningRepository();

	public LiveData<EarningResponse> getReport(Context context) {

		return repository.getReport(UrlUtils.getInputs(context, URLHelper.getReport));
	}

	public LiveData<ProfileResponse> getMyProfile(Context context) {
		return repository.getProfile(UrlUtils.getInputs(context, URLHelper.getMyProfile));
	}

	public LiveData<ProfileResponse> getEarningList(Context context, String type) {
		return repository.getEarningList(UrlUtils.getInputs(context, URLHelper.getMyProfile));
	}
}
