package com.app.a3store.viewmodel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.app.a3store.constants.URLHelper;
import com.app.a3store.model.ProductDashboardResponse;
import com.app.a3store.model.ProductListResponse;
import com.app.a3store.model.SubCategoryResponse;
import com.app.a3store.repository.CategoryRepository;
import com.app.a3store.utils.UrlUtils;
import com.app.a3store.utils.helper.SharedHelper;

import org.json.JSONException;
import org.json.JSONObject;

public class CategoryViewModel extends AndroidViewModel {

	CategoryRepository repository = new CategoryRepository();
	SharedHelper sharedHelper;
	public int selectedCategoryId = 0;
	public int selectedSubCategoryId = 0;
	public int havingSubCategory = 0;

	public CategoryViewModel(@NonNull Application application) {
		super(application);
		sharedHelper = new SharedHelper(application.getBaseContext());
	}

	public MutableLiveData<ProductDashboardResponse> getCategoryDetails(Context context, int categoryId) {

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("categoryId", categoryId);
			jsonObject.put("userId", sharedHelper.getUserId());
		} catch(JSONException e) {
			e.printStackTrace();
		}

		return repository.getCategoryDetails(UrlUtils.postInputs(context, jsonObject, URLHelper.getProductCategory));
	}

	public MutableLiveData<SubCategoryResponse> getSubCategory(Context context) {

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("id", selectedCategoryId);
			jsonObject.put("type", "SUBCATEGORY");
		} catch(JSONException e) {
			e.printStackTrace();
		}

		return repository.getSubCategory(UrlUtils.postInputs(context, jsonObject, URLHelper.getProductSubCategory));
	}

	public MutableLiveData<ProductListResponse> getProductList(Context context) {
		JSONObject jsonObject = new JSONObject();
		try {

			jsonObject.put("id", selectedSubCategoryId > 0 ? selectedSubCategoryId : selectedCategoryId);
			jsonObject.put("type", selectedSubCategoryId > 0 ? "SUBSUBCATEGORY" : "SUBCATEGORY");
			jsonObject.put("pageNumber", 1);
		} catch(JSONException e) {
			e.printStackTrace();
		}

		return repository.getProductListStatics(UrlUtils.postInputs(context, jsonObject, URLHelper.getProductList));
	}

	public MutableLiveData<ProductListResponse> getSearchProductList(Context context, String name) {

		JSONObject jsonObject = new JSONObject();
		try {

			if(havingSubCategory == 0) {
				jsonObject.put("id", selectedCategoryId);
				jsonObject.put("type", "CATEGORY");
			} else {
				jsonObject.put("id", selectedSubCategoryId);
				jsonObject.put("type", "SUBCATEGORY");
			}

			jsonObject.put("name", name);
		} catch(JSONException e) {
			e.printStackTrace();
		}

		return repository.getProductListStatics(UrlUtils.postInputs(context, jsonObject, URLHelper.productSearch));
	}
}
