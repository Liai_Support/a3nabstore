package com.app.a3store.viewmodel;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.app.a3store.constants.URLHelper;
import com.app.a3store.constants.UrlConstants;
import com.app.a3store.model.CommonResponse;
import com.app.a3store.model.orders.OrderDetailsResponse;
import com.app.a3store.repository.OrderDetailsRepository;
import com.app.a3store.utils.UrlUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class OderDetailsViewModel extends ViewModel {

	OrderDetailsRepository orderDetailsRepository = new OrderDetailsRepository();

	public LiveData<OrderDetailsResponse> orderDetails(Context context, int orderId) {

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("orderId", orderId);
		} catch(JSONException e) {
			e.printStackTrace();
		}

		return orderDetailsRepository.viewOrderItem(UrlUtils.postInputs(context, jsonObject, URLHelper.viewOrder));
	}

	public LiveData<CommonResponse> deleteOrderItem(Context context, int orderItemId) {

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("id", orderItemId);
		} catch(JSONException e) {
			e.printStackTrace();
		}

		return orderDetailsRepository.deleteOrderItem(UrlUtils.postInputs(context, jsonObject, URLHelper.deleteOrderItems));
	}

	public LiveData<CommonResponse> updateOrderStatus(Context context, int orderId, String status) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put(UrlConstants.kOrderId, orderId);
			jsonObject.put(UrlConstants.kStatus, status);
		} catch(JSONException e) {
			e.printStackTrace();
		}
		return orderDetailsRepository.updateOrderStatus(UrlUtils.postInputs(context, jsonObject, URLHelper.updateOrderStatus));
	}

	public LiveData<CommonResponse> updateOrderPackedStatus(Context context, int orderId, int productId, String productStatus) {

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("orderId", orderId);
			jsonObject.put("productId", productId);
			jsonObject.put("productStatusPacked", productStatus);
		} catch(JSONException e) {
			e.printStackTrace();
		}

		return orderDetailsRepository.updateOrderPackedStatus(UrlUtils.postInputs(context, jsonObject, URLHelper.updateProductPackedStatus));
	}
}
