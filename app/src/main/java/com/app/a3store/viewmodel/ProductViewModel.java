package com.app.a3store.viewmodel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.app.a3store.constants.URLHelper;
import com.app.a3store.model.CommonResponse;
import com.app.a3store.repository.ProductRepository;
import com.app.a3store.utils.UrlUtils;
import com.app.a3store.utils.helper.SharedHelper;

import org.json.JSONException;
import org.json.JSONObject;

public class ProductViewModel extends AndroidViewModel {

	ProductRepository repository = new ProductRepository();
	SharedHelper sharedHelper;
	public int selectedQuantity = 0;

	public ProductViewModel(@NonNull Application application) {
		super(application);
		sharedHelper = new SharedHelper(application.getBaseContext());
	}

	public LiveData<CommonResponse> replaceOrderItem(Context context, int orderId, int productId, int quantity, int storeId ) {

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("id", orderId);
			jsonObject.put("productId", productId);
			jsonObject.put("quantity", quantity);
			jsonObject.put("storeId", storeId);
		} catch(JSONException e) {
			e.printStackTrace();
		}

		return repository.replaceOrderItem(UrlUtils.postInputs(context, jsonObject, URLHelper.replaceOrderItems));
	}
}
