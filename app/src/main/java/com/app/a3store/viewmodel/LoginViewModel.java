package com.app.a3store.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.os.CountDownTimer;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.a3store.constants.URLHelper;
import com.app.a3store.model.firebase.FireBaseVerificationResponse;
import com.app.a3store.model.login.LoginResponse;
import com.app.a3store.model.login.VerifyOTPResponse;
import com.app.a3store.repository.LoginRepository;
import com.app.a3store.utils.UrlUtils;
import com.app.a3store.utils.Utils;
import com.google.firebase.auth.PhoneAuthCredential;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginViewModel extends ViewModel {

	LoginRepository loginRepository = new LoginRepository();
	public MutableLiveData<String> time = new MutableLiveData<>();

	public MutableLiveData<FireBaseVerificationResponse> requestVerificationCode(Activity context, String mobile) {
		return loginRepository.requestVerificationCode(context, mobile);
	}

	public MutableLiveData<FireBaseVerificationResponse> verifyOtp(PhoneAuthCredential credential) {
		return loginRepository.verifyOtp(credential);
	}

	public void resetTimer() {

		new CountDownTimer(1000 * 60 * 5, 1000) {
			@Override
			public void onTick(long l) {
				int minute = (int) ((l / 1000) / 60);
				int seconds = (int) ((l / 1000) % 60);
				time.setValue(Utils.formattedValues(minute) + ":" + Utils.formattedValues(seconds));
			}

			@Override
			public void onFinish() {
				time.setValue("");
			}
		}.start();
	}

	public LiveData<LoginResponse> login(Context context, String countryCode, String mobile) {

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("countryCode", countryCode);
			jsonObject.put("mobileNumber", mobile);
		} catch(JSONException e) {
			e.printStackTrace();
		}

		return loginRepository.login(UrlUtils.postLang(context, jsonObject, URLHelper.login));
	}

	public LiveData<VerifyOTPResponse> verifyOTP(Context context, String otp, String mobile) {

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("otp", otp);
			jsonObject.put("mobile", mobile);
		} catch(JSONException e) {
			e.printStackTrace();
		}

		return loginRepository.verifyOTP(UrlUtils.postLang(context, jsonObject, URLHelper.verifyOTP));
	}
}
