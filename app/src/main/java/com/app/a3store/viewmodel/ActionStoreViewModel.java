package com.app.a3store.viewmodel;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;

import com.app.a3store.constants.URLHelper;
import com.app.a3store.model.CommonResponse;
import com.app.a3store.model.product.Product;
import com.app.a3store.repository.ActionStoreRepository;
import com.app.a3store.ui.interfaces.ItemClickListener;
import com.app.a3store.ui.interfaces.ItemClickListenerWithTwoIds;
import com.app.a3store.ui.paging.ongoingorders.OrdersOngoingDataSource;
import com.app.a3store.ui.paging.storeProduct.StoreProductDataSource;
import com.app.a3store.ui.paging.storeProduct.StoreProductDataSourceFactory;
import com.app.a3store.utils.UrlUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class ActionStoreViewModel extends ViewModel {

	ActionStoreRepository repository = new ActionStoreRepository();
	public LiveData<PageKeyedDataSource<Integer, Product>> storeProductDataSource;
	public LiveData<PagedList<Product>> storeProductPagedList;

	public MutableLiveData<CommonResponse> changeStoreProduct(Context context, int productId, String storeProductStatus) {

		JSONObject jsonObject = new JSONObject();
		try {

			jsonObject.put("productId", productId);
			jsonObject.put("storeProductStatus", storeProductStatus);
		} catch(JSONException e) {
			e.printStackTrace();
		}

		return repository.changeStoreProduct(UrlUtils.postInputs(context, jsonObject, URLHelper.changeStoreProduct));
	}

	public void getProductOutOfStock(Context context, String type, ItemClickListener itemClickListener, ItemClickListenerWithTwoIds itemClickListenerWithTwoIds) {
		StoreProductDataSourceFactory itemDataSourceFactory = new StoreProductDataSourceFactory(context, type, itemClickListener, itemClickListenerWithTwoIds);
		storeProductDataSource = itemDataSourceFactory.getItemLiveDataSource();
		PagedList.Config pagedListConfig = (new PagedList.Config.Builder()).setEnablePlaceholders(false).setPageSize(StoreProductDataSource.TOTAL_PAGE_NUM).build();
		storeProductPagedList = (new LivePagedListBuilder<>(itemDataSourceFactory, pagedListConfig)).build();
	}
}
