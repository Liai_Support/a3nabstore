package com.app.a3store.viewmodel;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.app.a3store.constants.URLHelper;
import com.app.a3store.constants.UrlConstants;
import com.app.a3store.model.CommonResponse;
import com.app.a3store.model.dashboard.DashboardResponse;
import com.app.a3store.model.orders.OrderInfo;
import com.app.a3store.repository.DashboardRepository;
import com.app.a3store.utils.UrlUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DashboardViewModel extends ViewModel {

	DashboardRepository dashboardRepository = new DashboardRepository();
	public ArrayList<OrderInfo> ordersInfoList = new ArrayList<>();
	public ArrayList<DashboardResponse.Data.Order> ordersList = new ArrayList<>();

	public LiveData<CommonResponse> updateDeviceToken(Context context, String OS, String fcmToken, String id) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put(UrlConstants.kOS, OS);
			jsonObject.put(UrlConstants.kFCMToken, fcmToken);
			jsonObject.put(UrlConstants.kID, id);
		} catch(JSONException e) {
			e.printStackTrace();
		}
		return dashboardRepository.updateDeviceToken(UrlUtils.postInputs(context, jsonObject, URLHelper.updateDeviceToken));
	}

	public LiveData<DashboardResponse> getDashboardInfo(Context context) {
		return dashboardRepository.dashboardApi(UrlUtils.getInputs(context, URLHelper.dashboard));
	}

	public LiveData<CommonResponse> updateStoreStatus(Context context, String status) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put(UrlConstants.kStatus, status);
		} catch(JSONException e) {
			e.printStackTrace();
		}
		return dashboardRepository.updateOnlineStatus(UrlUtils.postInputs(context,jsonObject, URLHelper.updateStoreStatus));
	}
}
