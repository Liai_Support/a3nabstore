package com.app.a3store.firebase

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.Ringtone
import android.media.RingtoneManager
import android.net.Uri
import android.util.Log
import androidx.core.app.NotificationCompat
import com.app.a3store.R
import com.app.a3store.ui.onboarding.SplashScreen
import com.app.a3store.utils.Utils
import com.google.firebase.messaging.FirebaseMessagingService
import com.app.a3store.utils.helper.SharedHelper
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONObject

class MyFirebaseMessagingService : FirebaseMessagingService() {
    var sharedHelper: SharedHelper? = null
    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Utils.logD("MyFirebaseMessagingService", token)
        sharedHelper = SharedHelper(this)
        sharedHelper!!.firebaseToken = token

    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        val jsonObject = JSONObject(remoteMessage.data as Map<*, *>)
        Log.d("Firebasemessage ", remoteMessage.data.toString())


        setNotification(jsonObject.optString("title"), jsonObject.optString("body"))
    }


    private fun setNotification(s: String, s1: String) {


		val intent = Intent(this, SplashScreen::class.java)
		intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

//		val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationManager =
            this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


        val notificationSound = Uri.parse(
            "android.resource://"
                    + packageName + "/" + R.raw.notification
        );

        val notification: NotificationCompat.Builder = NotificationCompat.Builder(this, "Alert")
            .setContentText(s1)
            .setContentTitle(s)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setSound(notificationSound)
            .setDefaults(Notification.DEFAULT_ALL)
//            .setFullScreenIntent(pendingIntent, true)
//        val r = RingtoneManager.getRingtone(this, notificationSound)
//        if (!r.isPlaying) r.play()

        notificationManager.notify(System.currentTimeMillis().toInt(), notification.build())

    }
}