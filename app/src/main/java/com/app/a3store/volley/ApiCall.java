package com.app.a3store.volley;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.a3store.R;
import com.app.a3store.utils.Utils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ApiCall {

	private static final String TAG = ApiCall.class.getSimpleName();
	private static final int MY_SOCKET_TIMEOUT_MS = 60 * 1000;

	public static void PostMethod(InputForAPI input, final ResponseHandler volleyCallback) {
		final String url = input.getUrl();
		final Context context = input.getContext();
		JSONObject params = input.getJsonObject();
		final HashMap<String, String> headers = input.getHeaders();

		if(Utils.isNetworkConnected(context)) {

			Utils.logD(TAG, "url:" + url + "--input: " + params + "--headers: " + headers.toString());
			final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, response->{
				Utils.logD(TAG, "url:" + url + ",response: " + response);
				if(response.optBoolean("error")) {
					if(response.optString("message").equalsIgnoreCase("Unauthorized")) {
						/*Toast.makeText(input.getContext(), "Your session is expired please login again", Toast.LENGTH_SHORT).show();
						database.loginDao().deleteUser(new AppSettings(input.getContext()).getLoginId());
						new AppSettings(input.getContext()).setLoggedIn(false);
						new AppSettings(input.getContext()).setProfileImage("");
						Intent intent = new Intent(input.getContext(), SplashAndLoginActivity.class);
						intent.putExtra(ConstantKeys.name, "login");
						intent.putExtra(ConstantKeys.mobileNumber, new AppSettings(input.getContext()).getLoginId());
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						context.startActivity(intent);*/
					} else {
						volleyCallback.setDataResponse(response);
					}
				} else {

					volleyCallback.setDataResponse(response);
				}
			}, error->{
				Utils.logD(TAG, "url:" + url + ", onErrorResponse: " + error);
				if(error instanceof TimeoutError || error instanceof NoConnectionError) {

					volleyCallback.setResponseError(context.getResources().getString(R.string.error_no_internet_connection));
				} else if(error instanceof AuthFailureError) {

					volleyCallback.setResponseError(context.getResources().getString(R.string.error_authentication));
				} else if(error instanceof ServerError) {

					volleyCallback.setResponseError(context.getResources().getString(R.string.error_server));
				} else if(error instanceof NetworkError) {

					volleyCallback.setResponseError(context.getResources().getString(R.string.error_network));
				} else if(error instanceof ParseError) {

					volleyCallback.setResponseError(context.getResources().getString(R.string.error_parse));
				}
			}) {
				@Override
				public Map<String, String> getHeaders() {
					return headers;
				}
			};

			jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			AppController.getInstance().addToRequestQueue(jsonObjReq);
		} else {
			volleyCallback.setResponseError(context.getResources().getString(R.string.error_no_internet_connection));
		}
	}

	public static void GetMethod(InputForAPI input, final ResponseHandler volleyCallback) {

		final String url = input.getUrl();
		final Context context = input.getContext();
		final HashMap<String, String> headers = input.getHeaders();
		if(Utils.isNetworkConnected(context)) {
			Utils.logD(TAG, "url:" + url + "--headers: " + headers.toString());

			final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null, response->{
				Utils.logD(TAG, "GetMethod url:" + url + ",response: " + response);

				if(response.optBoolean("error")) {
					if(response.optString("message").equalsIgnoreCase("Unauthorized")) {
						/*Toast.makeText(input.getContext(), "Your session is expired please login again", Toast.LENGTH_SHORT).show();
						database.loginDao().deleteUser(new AppSettings(input.getContext()).getLoginId());
						new AppSettings(input.getContext()).setLoggedIn(false);
						new AppSettings(input.getContext()).setProfileImage("");
						Intent intent = new Intent(input.getContext(), SplashAndLoginActivity.class);
						intent.putExtra(ConstantKeys.name, "login");
						intent.putExtra(ConstantKeys.mobileNumber, new AppSettings(input.getContext()).getLoginId());
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						context.startActivity(intent);*/
					} else {
						volleyCallback.setDataResponse(response);
					}
				} else {

					volleyCallback.setDataResponse(response);
				}
			}, error->{
				Utils.logD(TAG, "url:" + url + ", onErrorResponse: " + error);
				if(error instanceof TimeoutError || error instanceof NoConnectionError) {

					volleyCallback.setResponseError(context.getResources().getString(R.string.error_no_internet_connection));
				} else if(error instanceof AuthFailureError) {

					volleyCallback.setResponseError(context.getResources().getString(R.string.error_authentication));
				} else if(error instanceof ServerError) {

					volleyCallback.setResponseError(context.getResources().getString(R.string.error_server));
				} else if(error instanceof NetworkError) {

					volleyCallback.setResponseError(context.getResources().getString(R.string.error_network));
				} else if(error instanceof ParseError) {

					volleyCallback.setResponseError(context.getResources().getString(R.string.error_parse));
				}
			}) {
				@Override
				public Map<String, String> getHeaders() {
					return headers;
				}
			};

			jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			AppController.getInstance().addToRequestQueue(jsonObjReq);
		} else {
			volleyCallback.setResponseError(context.getResources().getString(R.string.error_no_internet_connection));
		}
	}

	public interface ResponseHandler {

		void setDataResponse(JSONObject response);
		void setResponseError(String error);
	}
}