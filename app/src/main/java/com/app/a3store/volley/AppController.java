package com.app.a3store.volley;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.media.AudioAttributes;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.app.a3store.R;

public class AppController extends Application {

    public static final String TAG = AppController.class.getSimpleName();
    private static Context mContext;
    private static AppController mInstance;
    private RequestQueue mRequestQueue;

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public static Context getContext() {
        return mContext;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        mContext = this;
        mInstance = this;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel();
        }
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Application instance = this;
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private void createNotificationChannel() {

        Uri notificationSound = Uri.parse(
                "android.resource://"
                        + getPackageName() + "/" + R.raw.notification
        );

        AudioAttributes attributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();


        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel chatGroupHangUp = new NotificationChannel(
                "Notification",
                "Notification",
                NotificationManager.IMPORTANCE_HIGH
        );
        notificationManager.createNotificationChannel(chatGroupHangUp);
        NotificationChannel notifyChannel = new NotificationChannel("Notification_Sound", "Notification_Sound", NotificationManager.IMPORTANCE_HIGH);
        notifyChannel.setSound(notificationSound, attributes);
        notifyChannel.enableLights(true);
        notifyChannel.enableVibration(true);
        notificationManager.createNotificationChannel(notifyChannel);
    }


}