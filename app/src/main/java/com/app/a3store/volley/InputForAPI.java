package com.app.a3store.volley;

import android.content.Context;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

public class InputForAPI {

	JSONObject jsonObject = new JSONObject();
	String url = "";
	HashMap<String, String> headers = new HashMap<>();
	File file;
	Context context;

	public InputForAPI(Context mContext) {
		this.context = mContext;
	}

	public JSONObject getJsonObject() {
		return jsonObject;
	}

	public void setJsonObject(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public HashMap<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(HashMap<String, String> headers) {
		this.headers = headers;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}
}
