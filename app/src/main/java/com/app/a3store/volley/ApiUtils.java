package com.app.a3store.volley;

import android.content.Context;

import com.app.a3store.constants.AppConstants;
import com.app.a3store.utils.helper.SharedHelper;

import java.util.HashMap;

public class ApiUtils {
	public static HashMap<String, String> getLangHeader(Context context) {

		HashMap<String, String> headers = new HashMap<>();
		SharedHelper sharedHelper = new SharedHelper(context);
		if(sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")){
			headers.put(AppConstants.kLang, "en");
		}
		else {
			headers.put(AppConstants.kLang, "en");
		}
		headers.put(AppConstants.kContentType, AppConstants.kContentTypeJSON);
		return headers;
	}

	public static HashMap<String, String> getAuthorizationHeaderTwo(Context context) {
		SharedHelper sharedHelper = new SharedHelper(context);
		HashMap<String, String> headers = new HashMap<>();
		headers.put(AppConstants.kAuthorization, sharedHelper.getAuthToken());
		headers.put(AppConstants.kRole, AppConstants.kStoreManager);
		if(sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")){
			headers.put(AppConstants.kLang, "en");
		}
		else {
			headers.put(AppConstants.kLang, "en");
		}		headers.put(AppConstants.kContentType, AppConstants.kContentTypeJSON);

		return headers;
	}
}
