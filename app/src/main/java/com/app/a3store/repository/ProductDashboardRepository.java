package com.app.a3store.repository;

import androidx.lifecycle.MutableLiveData;

import com.app.a3store.model.CommonResponse;
import com.app.a3store.model.ProductDashboardResponse;
import com.app.a3store.model.ProductSubCategoryResponse;
import com.app.a3store.model.product.ProductListResponse;
import com.app.a3store.volley.ApiCall;
import com.app.a3store.volley.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class ProductDashboardRepository {

	public MutableLiveData<ProductDashboardResponse> getProductDashboard(InputForAPI inputs) {

		MutableLiveData<ProductDashboardResponse> responseValue = new MutableLiveData<>();

		ApiCall.GetMethod(inputs, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				ProductDashboardResponse result = gson.fromJson(response.toString(), ProductDashboardResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				ProductDashboardResponse result = new ProductDashboardResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});
		return responseValue;
	}

	public MutableLiveData<ProductSubCategoryResponse> getProductSubCategory(InputForAPI inputs) {

		MutableLiveData<ProductSubCategoryResponse> responseValue = new MutableLiveData<>();

		ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				ProductSubCategoryResponse result = gson.fromJson(response.toString(), ProductSubCategoryResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				ProductSubCategoryResponse result = new ProductSubCategoryResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});
		return responseValue;
	}

	public MutableLiveData<ProductListResponse> getProductList(InputForAPI inputs) {

		MutableLiveData<ProductListResponse> responseValue = new MutableLiveData<>();

		ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				ProductListResponse result = gson.fromJson(response.toString(), ProductListResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				ProductListResponse result = new ProductListResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});
		return responseValue;
	}

	public MutableLiveData<CommonResponse> changeStoreProduct(InputForAPI inputs) {

		MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

		ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				CommonResponse result = new CommonResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});
		return responseValue;
	}
}
