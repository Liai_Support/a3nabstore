package com.app.a3store.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.a3store.model.EarningResponse;
import com.app.a3store.model.profile.ProfileResponse;
import com.app.a3store.volley.ApiCall;
import com.app.a3store.volley.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class EarningRepository {

	public LiveData<EarningResponse> getReport(InputForAPI inputForAPI) {

		MutableLiveData<EarningResponse> responseValue = new MutableLiveData<>();

		ApiCall.GetMethod(inputForAPI, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {
				Gson gson = new Gson();
				EarningResponse result = gson.fromJson(response.toString(), EarningResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {
				EarningResponse result = new EarningResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});
		return responseValue;
	}

	public LiveData<ProfileResponse> getProfile(InputForAPI inputForAPI) {

		MutableLiveData<ProfileResponse> responseValue = new MutableLiveData<>();

		ApiCall.GetMethod(inputForAPI, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				ProfileResponse result = gson.fromJson(response.toString(), ProfileResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				ProfileResponse result = new ProfileResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});

		return responseValue;
	}
	public LiveData<ProfileResponse> getEarningList(InputForAPI inputForAPI) {

		MutableLiveData<ProfileResponse> responseValue = new MutableLiveData<>();

		ApiCall.GetMethod(inputForAPI, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				ProfileResponse result = gson.fromJson(response.toString(), ProfileResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				ProfileResponse result = new ProfileResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});

		return responseValue;
	}
}
