package com.app.a3store.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.a3store.model.CommonResponse;
import com.app.a3store.model.orders.OrderResponse;
import com.app.a3store.volley.ApiCall;
import com.app.a3store.volley.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class OrderRepository {

	public LiveData<OrderResponse> getOngoingOrders(InputForAPI inputForAPI) {

		MutableLiveData<OrderResponse> responseValue = new MutableLiveData<>();

		ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				OrderResponse result = gson.fromJson(response.toString(), OrderResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				OrderResponse result = new OrderResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});

		return responseValue;
	}

	public LiveData<CommonResponse> updateOrderStatus(InputForAPI inputForAPI) {

		MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

		ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				CommonResponse result = new CommonResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});

		return responseValue;
	}
}
