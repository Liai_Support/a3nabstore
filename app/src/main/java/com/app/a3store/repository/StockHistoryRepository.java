package com.app.a3store.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.a3store.model.CommonResponse;
import com.app.a3store.model.stock.AddVendorResponse;
import com.app.a3store.model.stock.StockHistoryResponse;
import com.app.a3store.model.stock.StoreVendorResponse;
import com.app.a3store.volley.ApiCall;
import com.app.a3store.volley.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class StockHistoryRepository {

	public LiveData<StockHistoryResponse> getStockHistory(InputForAPI inputForAPI) {

		MutableLiveData<StockHistoryResponse> responseValue = new MutableLiveData<>();

		ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				StockHistoryResponse result = gson.fromJson(response.toString(), StockHistoryResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				StockHistoryResponse result = new StockHistoryResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});

		return responseValue;
	}

	public LiveData<StoreVendorResponse> getStoreVendorList(InputForAPI inputForAPI) {

		MutableLiveData<StoreVendorResponse> responseValue = new MutableLiveData<>();

		ApiCall.GetMethod(inputForAPI, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				StoreVendorResponse result = gson.fromJson(response.toString(), StoreVendorResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				StoreVendorResponse result = new StoreVendorResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});

		return responseValue;
	}

	public LiveData<AddVendorResponse> addStoreVendor(InputForAPI inputForAPI) {

		MutableLiveData<AddVendorResponse> responseValue = new MutableLiveData<>();

		ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				AddVendorResponse result = gson.fromJson(response.toString(), AddVendorResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				AddVendorResponse result = new AddVendorResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});

		return responseValue;
	}

	public LiveData<CommonResponse> updateStoreStock(InputForAPI inputForAPI) {

		MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

		ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				CommonResponse result = new CommonResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});

		return responseValue;
	}
}
