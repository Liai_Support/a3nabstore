package com.app.a3store.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.a3store.model.CommonResponse;
import com.app.a3store.model.dashboard.DashboardResponse;
import com.app.a3store.volley.ApiCall;
import com.app.a3store.volley.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class DashboardRepository {

	public LiveData<CommonResponse> updateDeviceToken(InputForAPI inputForAPI) {

		MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

		ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				CommonResponse result = new CommonResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});

		return responseValue;
	}

	public LiveData<DashboardResponse> dashboardApi(InputForAPI inputForAPI) {

		MutableLiveData<DashboardResponse> responseValue = new MutableLiveData<>();

		ApiCall.GetMethod(inputForAPI, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				DashboardResponse result = gson.fromJson(response.toString(), DashboardResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				DashboardResponse result = new DashboardResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});

		return responseValue;
	}

	public LiveData<CommonResponse> updateOnlineStatus(InputForAPI inputForAPI) {

		MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

		ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				CommonResponse result = new CommonResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});

		return responseValue;
	}
}
