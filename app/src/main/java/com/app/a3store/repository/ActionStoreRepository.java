package com.app.a3store.repository;

import androidx.lifecycle.MutableLiveData;

import com.app.a3store.model.ActionStoreResponse;
import com.app.a3store.model.CommonResponse;
import com.app.a3store.model.ProductDashboardResponse;
import com.app.a3store.model.ProductSubCategoryResponse;
import com.app.a3store.model.product.ProductListResponse;
import com.app.a3store.volley.ApiCall;
import com.app.a3store.volley.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class ActionStoreRepository {

	public MutableLiveData<CommonResponse> changeStoreProduct(InputForAPI inputs) {

		MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

		ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				CommonResponse result = new CommonResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});
		return responseValue;
	}

}
