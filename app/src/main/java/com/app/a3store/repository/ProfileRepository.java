package com.app.a3store.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.a3store.model.CommonResponse;
import com.app.a3store.model.profile.ProfileDashboardResponse;
import com.app.a3store.model.profile.UpdateProfileResponse;
import com.app.a3store.volley.ApiCall;
import com.app.a3store.volley.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class ProfileRepository {

	public LiveData<ProfileDashboardResponse> profileDashBoard(InputForAPI inputForAPI) {

		MutableLiveData<ProfileDashboardResponse> responseValue = new MutableLiveData<>();

		ApiCall.GetMethod(inputForAPI, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				ProfileDashboardResponse result = gson.fromJson(response.toString(), ProfileDashboardResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				ProfileDashboardResponse result = new ProfileDashboardResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});

		return responseValue;
	}

	public LiveData<UpdateProfileResponse> updateProfile(InputForAPI inputForAPI) {

		MutableLiveData<UpdateProfileResponse> responseValue = new MutableLiveData<>();

		ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				UpdateProfileResponse result = gson.fromJson(response.toString(), UpdateProfileResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				UpdateProfileResponse result = new UpdateProfileResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});

		return responseValue;
	}

	public LiveData<CommonResponse> updateOnlineStatus(InputForAPI inputForAPI) {

		MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

		ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				CommonResponse result = new CommonResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});

		return responseValue;
	}
}
