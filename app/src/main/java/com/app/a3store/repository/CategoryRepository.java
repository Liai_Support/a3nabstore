package com.app.a3store.repository;

import androidx.lifecycle.MutableLiveData;

import com.app.a3store.model.ProductDashboardResponse;
import com.app.a3store.model.ProductListResponse;
import com.app.a3store.model.SubCategoryResponse;
import com.app.a3store.volley.ApiCall;
import com.app.a3store.volley.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class CategoryRepository {

	public MutableLiveData<ProductDashboardResponse> getCategoryDetails(InputForAPI inputs) {

		MutableLiveData<ProductDashboardResponse> responseValue = new MutableLiveData<>();

		ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				ProductDashboardResponse result = gson.fromJson(response.toString(), ProductDashboardResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				ProductDashboardResponse result = new ProductDashboardResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});
		return responseValue;
	}

	public MutableLiveData<ProductListResponse> getProductListStatics(InputForAPI inputs) {

		MutableLiveData<ProductListResponse> responseValue = new MutableLiveData<>();

		ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				ProductListResponse result = gson.fromJson(response.toString(), ProductListResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				ProductListResponse result = new ProductListResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});
		return responseValue;
	}

	public MutableLiveData<SubCategoryResponse> getSubCategory(InputForAPI inputs) {

		MutableLiveData<SubCategoryResponse> responseValue = new MutableLiveData<>();

		ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				SubCategoryResponse result = gson.fromJson(response.toString(), SubCategoryResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				SubCategoryResponse result = new SubCategoryResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});
		return responseValue;
	}

	public void updateManagerCount(InputForAPI inputs) {


		ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) { }
			@Override
			public void setResponseError(String error) { }
		});
	}
}
