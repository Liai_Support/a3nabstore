package com.app.a3store.repository;

import androidx.lifecycle.MutableLiveData;

import com.app.a3store.model.CommonResponse;
import com.app.a3store.model.ProductDashboardResponse;
import com.app.a3store.model.ProductSubCategoryResponse;
import com.app.a3store.model.ProductUnavailableResponse;
import com.app.a3store.volley.ApiCall;
import com.app.a3store.volley.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class ProductUnavailableRepository {

	public MutableLiveData<ProductDashboardResponse> getProductDashboard(InputForAPI inputs) {

		MutableLiveData<ProductDashboardResponse> responseValue = new MutableLiveData<>();

		ApiCall.GetMethod(inputs, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				ProductDashboardResponse result = gson.fromJson(response.toString(), ProductDashboardResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				ProductDashboardResponse result = new ProductDashboardResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});
		return responseValue;
	}

	public MutableLiveData<ProductSubCategoryResponse> getProductSubCategory(InputForAPI inputs) {

		MutableLiveData<ProductSubCategoryResponse> responseValue = new MutableLiveData<>();

		ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				ProductSubCategoryResponse result = gson.fromJson(response.toString(), ProductSubCategoryResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				ProductSubCategoryResponse result = new ProductSubCategoryResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});
		return responseValue;
	}

	public MutableLiveData<ProductUnavailableResponse> getProductList(InputForAPI inputs) {

		MutableLiveData<ProductUnavailableResponse> responseValue = new MutableLiveData<>();

		ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				ProductUnavailableResponse result = gson.fromJson(response.toString(), ProductUnavailableResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				ProductUnavailableResponse result = new ProductUnavailableResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});
		return responseValue;
	}

	public MutableLiveData<CommonResponse> addNewProduct(InputForAPI inputs) {

		MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

		ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
			@Override
			public void setDataResponse(JSONObject response) {

				Gson gson = new Gson();
				CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
				responseValue.setValue(result);
			}

			@Override
			public void setResponseError(String error) {

				CommonResponse result = new CommonResponse();
				result.setError("true");
				result.setMessage(error);
				responseValue.setValue(result);
			}
		});
		return responseValue;
	}
}
